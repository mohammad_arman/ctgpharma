<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['branch_name','contact_number','address'];
    
//    public function supplier()
//    {
//        return $this->belongsToMany(Supplier::class);
//    }
}
