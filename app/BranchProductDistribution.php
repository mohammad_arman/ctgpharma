<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BranchProductDistribution extends Model
{
    public function branch() {
	return $this->belongsTo(Branch::class);
    }
    public function branchProduct() {
        return $this->belongsTo('App\BranchProduct','product_id');
    }
}
