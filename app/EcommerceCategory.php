<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcommerceCategory extends Model
{
    protected $fillable = ['category_name','description','publication_status','author_name'];
}
