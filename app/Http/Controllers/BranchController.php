<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use Illuminate\Support\Facades\Auth;
use DB;

class BranchController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }
    
    public function index()
    {
        // echo '<pre>';print_r($this->user_role->role_id);exit;   
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            return view('admin.branch.branch-add');           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
    }
    
    public function storeBranch(Request $request) {
        $store_branch_data = new Branch();
        $this->validate($request, [
            'branch_name'    => 'required|unique:branches',
            'contact_number' => 'required',
            'address'        => 'required',
        ]);
        $store_branch_data->branch_name    = $request->branch_name;
        $store_branch_data->contact_number = $request->contact_number;
        $store_branch_data->address        = $request->address;
        $store_branch_data->save();
        session()->flash('message','Branch Data Inserted Successfully');
        return redirect('/branch/add');
    }
    
    public function manageBranch() {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $branch_info = Branch::orderBy('id','desc')->get();
            // echo '<pre>';print_r($branch_info->toArray());exit();
             return view('admin.branch.branch-manage', compact('branch_info'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
    
    public function editBranch($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $branch_info_by_id = Branch::where('id',$id)->first();
            return view('admin.branch.branch-edit', compact('branch_info_by_id'));       
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function updateBranch(Request $request) {
        $update_branch_info = Branch::find($request->id);
        $this->validate($request, [
            'branch_name'    => 'required',
            'contact_number' => 'required',
            'address'        => 'required',
        ]);
        $update_branch_info->branch_name    = $request->branch_name;
        $update_branch_info->contact_number = $request->contact_number;
        $update_branch_info->address        = $request->address;
        $update_branch_info->save();
        return redirect('/branch/manage')->with('message','Branch Info Updated Successfully');
    }
    
    public function viewBranch($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $view_branch_info = Branch::where('id',$id)->first();
	        return view('admin.branch.branch-view')->with('view_branch_info', $view_branch_info);          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
	
	
    }
    
    public function deleteBranch($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_branch_info = Branch::find($id);
            $delete_branch_info->delete();
            return redirect('/branch/manage')->with('message','Branch Info Deleted Successfully');       
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

}
