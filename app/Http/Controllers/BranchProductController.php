<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BranchProduct;
use App\BranchProductDistribution;
use DB;
use Session;

class BranchProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');       
    }
    
    public function index($id)
    {
	$branch_product_info = DB::table('branch_products')  
                                    ->where('id',$id)
                                    ->first();
	
	$branch_list = DB::table('branches')->where('id','!=',1)->get();
	
        return view('admin.branch-product.branch-product-add')
		->with('branch_product_info', $branch_product_info)
		->with('branch_list', $branch_list);
    }

    private function dateFormat($date){
	
        return date('Y-m-d',strtotime($date));
    }
    
    public function storeProduct(Request $request) {
	
	$branchProduct = new BranchProductDistribution();    


	$this->validate($request, [
	    'product_name'                => 'required',
	    'branch_id'                   => 'required',
	    'piece_qty'                   => 'required',
	    'created_by'                  => 'required',
	]);

        $product_id                  = $request->product_id; 
        $branch_id                  = $request->branch_id;  
        $product_data = BranchProduct::where('id',$product_id)->first();
        $branch_product_distribution_data = BranchProductDistribution::where([
								       ['branch_id',$branch_id],
								       ['product_id',$product_id],
								   ])->first();
    //    echo '<pre>';print_r($branch_product_distribution_data);exit();
       $branchProductUpdate = BranchProduct::find($product_id);
       if(count($branch_product_distribution_data) > 0){
	    $branch_product_distribution_data->branch_id                   = $branch_id; 
	    $branch_product_distribution_data->product_id                  = $product_id; 
	    $branch_product_distribution_data->product_name                = $request->product_name;

	    if(empty($request->carton_qty)){
		$ctn_qty = 0;
	    }else{
		$ctn_qty = $request->carton_qty;
	    }
	    if(empty($request->box_qty)){
		$bx_qty = 0;
	    }else{
		$bx_qty = $request->box_qty;
	    }
	    if(empty($request->strip_qty)){
		$stp_qty = 0;
	    }else{
		$stp_qty = $request->strip_qty;
	    }


	    $branch_product_distribution_data->carton_qty                  = $ctn_qty + $branch_product_distribution_data->carton_qty;
	    $branch_product_distribution_data->box_qty                     = $bx_qty + $branch_product_distribution_data->box_qty;
	    $branch_product_distribution_data->strip_qty                   = $stp_qty + $branch_product_distribution_data->strip_qty;
	    $branch_product_distribution_data->piece_qty                   = $request->piece_qty + $branch_product_distribution_data->piece_qty;

	    $branch_product_distribution_data->purchase_rate               = $product_data->purchase_rate;
	    $branch_product_distribution_data->sale_rate                   = $product_data->sale_rate;

	    $branch_product_distribution_data->carton_sales_rate           = $product_data->carton_sales_rate;
	    $branch_product_distribution_data->box_sales_rate              = $product_data->box_sales_rate;
	    $branch_product_distribution_data->strip_sales_rate            = $product_data->strip_sales_rate;
	    $branch_product_distribution_data->piece_sales_rate            = $product_data->piece_sales_rate;

	    $branch_product_distribution_data->weight                      = $product_data->weight;
	    $branch_product_distribution_data->manufacturing_date          = $product_data->manufacturing_date;
	    $branch_product_distribution_data->expiry_date                 = $product_data->expiry_date;
	    $branch_product_distribution_data->reorder_level               = $product_data->reorder_level;
	    $branch_product_distribution_data->created_by                  = $request->created_by;

	    if($branch_product_distribution_data->save()){
		$carton_qtu = $product_data->carton_qty - $ctn_qty;
		$box_qty    = $product_data->box_qty - $bx_qty;
		$strip_qty  = $product_data->strip_qty - $stp_qty;
		$piece_qty  = $product_data->piece_qty - $request->piece_qty;

		$branchProductUpdate->carton_qty                  = $carton_qtu;
		$branchProductUpdate->box_qty                     = $box_qty;
		$branchProductUpdate->strip_qty                   = $strip_qty;
		$branchProductUpdate->piece_qty                   = $piece_qty;
		if($branchProductUpdate->save()){
		    session()->flash('success','Product Has Been Distributed.');
		    return redirect('/branch-product/manage');
		}


	    }

       }else{
	    $branchProduct->branch_id                   = $branch_id; 
	    $branchProduct->product_id                  = $product_id; 
	    $branchProduct->product_name                = $request->product_name;

	    if(empty($request->carton_qty)){
		$ctn_qty = 0;
	    }else{
		$ctn_qty = $request->carton_qty;
	    }
	    if(empty($request->box_qty)){
		$bx_qty = 0;
	    }else{
		$bx_qty = $request->box_qty;
	    }
	    if(empty($request->strip_qty)){
		$stp_qty = 0;
	    }else{
		$stp_qty = $request->strip_qty;
	    }


	    $branchProduct->carton_qty                  = $ctn_qty;
	    $branchProduct->box_qty                     = $bx_qty;
	    $branchProduct->strip_qty                   = $stp_qty;
	    $branchProduct->piece_qty                   = $request->piece_qty;

	    $branchProduct->purchase_rate               = $product_data->purchase_rate;
	    $branchProduct->sale_rate                   = $product_data->sale_rate;

	    $branchProduct->carton_sales_rate           = $product_data->carton_sales_rate;
	    $branchProduct->box_sales_rate              = $product_data->box_sales_rate;
	    $branchProduct->strip_sales_rate            = $product_data->strip_sales_rate;
	    $branchProduct->piece_sales_rate            = $product_data->piece_sales_rate;

	    $branchProduct->weight                      = $product_data->weight;
	    $branchProduct->manufacturing_date          = $product_data->manufacturing_date;
	    $branchProduct->expiry_date                 = $product_data->expiry_date;
	    $branchProduct->reorder_level               = $product_data->reorder_level;
	    $branchProduct->created_by                  = $request->created_by;

	    if($branchProduct->save()){
		$carton_qtu = $product_data->carton_qty - $ctn_qty;
		$box_qty    = $product_data->box_qty - $bx_qty;
		$strip_qty  = $product_data->strip_qty - $stp_qty;
		$piece_qty  = $product_data->piece_qty - $request->piece_qty;

		$branchProductUpdate->carton_qty                  = $carton_qtu;
		$branchProductUpdate->box_qty                     = $box_qty;
		$branchProductUpdate->strip_qty                   = $strip_qty;
		$branchProductUpdate->piece_qty                   = $piece_qty;
		if($branchProductUpdate->save()){
		    session()->flash('success','Product Has Been Distributed.');
		    return redirect('/branch-product/manage');
		}


	    }
       }
    


    } 
    
    public function manageBranchDistributedProduct() {
		$branch_id = Session::get('branch_id');
		if($branch_id == 1){
			$products = BranchProductDistribution::orderBy('id', 'desc')->get();
		}else{
			$products = BranchProductDistribution::where('branch_id',$branch_id)->orderBy('id', 'desc')->get();
		}
		
		return view('admin.distributed-product.product-manage', compact('products'));
    }

    
    public function viewProduct($id) {
		$branch_id = Session::get('branch_id');
		if($branch_id == 1){
            $product_info = BranchProductDistribution::find($id);
	        return view('admin.distributed-product.product-view', compact('product_info'));
		}else{
			$product_info = BranchProductDistribution::where('id',$id)->where('branch_id',$branch_id)->first();
			// echo '<pre>';print_r($product_info);exit;
			if(empty($product_info)){
				return redirect('/dashboard');
				
			}else{
				return view('admin.distributed-product.product-view', compact('product_info'));
			}
	       
		}
	
    }
    
	public function editProduct($id) {
		$branch_id = Session::get('branch_id');
		if($branch_id == 1){
            $product_info = BranchProductDistribution::find($id);
            $branch_product_info = DB::table('branch_products')->where('id',$product_info->product_id)->first();
	        return view('admin.distributed-product.product-edit')
			    ->with('branch_product_info', $branch_product_info)
				->with('product_info', $product_info);
		}else{
			$product_info = BranchProductDistribution::where('id',$id)->where('branch_id',$branch_id)->first();
			// echo '<pre>';print_r($product_info);exit;
            $branch_product_info = DB::table('branch_products')->where('id',$product_info->product_id)->first();
			if(empty($product_info)){
				return redirect('/dashboard');
				
			}else{
				return view('admin.distributed-product.product-edit')
			    ->with('branch_product_info', $branch_product_info)
				->with('product_info', $product_info);
			}
	       
		}
	
    }
    
   public function updateProduct(Request $request) {
        
        $update_branch_info = BranchProduct::find($request->product_id);
        $update_branch_info->carton_qty    = $update_branch_info->carton_qty - $request->change_carton_qty;
        $update_branch_info->box_qty       = $update_branch_info->box_qty - $request->change_box_qty; 
        $update_branch_info->strip_qty     = $update_branch_info->strip_qty - $request->change_strip_qty;
        $update_branch_info->piece_qty     = $update_branch_info->piece_qty - $request->change_piece_qty;
        $update_branch_info->save();
       
        $update_distributed_branch_info = BranchProductDistribution::find($request->id);
        if(!empty($request->piece_qty))
        {
            $update_distributed_branch_info->carton_qty    = $request->carton_qty;
            $update_distributed_branch_info->box_qty       = $request->box_qty; 
            $update_distributed_branch_info->strip_qty     = $request->strip_qty;
            $update_distributed_branch_info->piece_qty     = $request->piece_qty;
        }
        $update_distributed_branch_info->rack_number   = $request->rack_number;
        $update_distributed_branch_info->reorder_level = $request->reorder_level;
        $update_distributed_branch_info->save();
        return redirect('/product/manage')->with('success','Distributed Product Info Updated Successfully');
    }

    public function deleteProduct($id) {
		$branch_id = Session::get('branch_id');
		if($branch_id == 1){
        $delete_product = BranchProductDistribution::find($id);
        $delete_product->delete();
		return redirect('/product/manage')->with('success','Product Deleted Successfully');
		}else{
			return redirect('/dashboard');
		}
    }



}
