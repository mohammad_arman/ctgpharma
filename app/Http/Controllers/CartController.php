<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EcommerceProduct;
use Cart;
use DB;
class CartController extends Controller
{

    public function addCart(Request $request) {
	$qty = $request->qty;
	$product_id = $request->id;
	$product = EcommerceProduct::where('id', $product_id)->first();
	$data['id'] = $product->id;
	$data['name'] = $product->product_name;
	if($product->discount > 0){
		$discount = 
		$price =( ($product->piece_sales_rate) - ($product->piece_sales_rate * ($product->discount/100)));
	}else{
		$price =$product->piece_sales_rate;
	}
	$data['price'] = $price;
	$data['qty'] = $qty;
	$data['options']['image'] = $product->product_image;
	$data['options']['discount'] = $product->discount;
	$data['options']['old_qty'] = $product->piece_qty;
	$data['options']['category'] = $product->category_id;
	Cart::add($data);
	return redirect()->back();
	
    }
    public function cart() {
	return view('ecommerce-site.checkout.cart');
    }
    
    public function updateCart(Request $request) {
	$rowId = $request->rowId;
	$qty   = $request->qty;
	Cart::update($rowId,$qty);
	return redirect('/cart');
    }
    
    public function deleteCart(Request $request) {
	$rowId = $request->rowId;
	Cart::remove($rowId);
	return redirect()->back();
    }
}
