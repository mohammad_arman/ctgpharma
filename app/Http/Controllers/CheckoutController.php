<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Validator;
use URL;
use Redirect;
use Session;
use Input;
use App\Customer;
use App\Shipping;
use Cart;
use App\Payment;
use App\Order;
use App\OrderDetail;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

class CheckoutController extends Controller
{
	public $check_category = 1;
	public function checkout()
	{
		if ($this->auth_check() == true) {
			
			return view('ecommerce-site.login-registration.login-registration');
		} else {
			
			return redirect('/billing');
		}
	}

	public function customerRegistration(Request $request)
	{

		
		$customer = new Customer();
		$this->validate($request, [
			'username' => 'required|string|max:100|unique:customers',
			'email' => 'required|string|email|max:255|unique:customers',
			'phone_num_one' => 'required|numeric',
			'password' => 'required|string|min:6',
		]);
		$customer->username = $request->username;
		$customer->email = $request->email;
		$customer->phone_num_one = $request->phone_num_one;
		$customer->password = md5($request->password);
		$customer->created_on = date('Y-m-d H:i:s');
		$customer->status = 1;
		if ($customer->save()) {
			$customer_id = $customer->id;
			Session::put('customer_id', $customer_id);
			Session::put('customer_name', $request->username);
			if (count(Cart::content()) > 0) {
				return redirect('/billing');
			} else {
				return redirect('/');
			}

		}
	}

	public function customerLogin(Request $request)
	{
		
		$this->validate($request, [
			'login_email' => 'required|email',
			'login_password' => 'required',
		]);

		$customer_email = $request->login_email;
		$customer_password = $request->login_password;

		$result = DB::table('customers')->select('*')
			->where('email', $customer_email)
			->where('password', md5($customer_password))
			->first();

		if ($result) {
			if ($result->status != 1) {
				session()->flash('message', 'Your account is blocked! Please contact with admin through contact page.');
				return redirect::to('/authentication');
			} else {
				Session::put('customer_id', $result->id);
				Session::put('customer_name', $result->username);
				Session::put('customer_email', $result->email);
				Session::put('customer_phone_number', $result->phone_num_one);
				if (count(Cart::content()) > 0) {
					
					return redirect::to('/cart');
				}else{
					
					return redirect::to('/');
				}
				// echo count(Cart::content());
				
			}

		} else {
			session()->flash('message', 'Your email or password is wrong!');
			return redirect::to('/authentication');
		}

	}

	public function billing()
	{
		//  echo Session::get('shipping_id');exit;
		if ($this->auth_check() == true) {
			return view('ecommerce-site.login-registration.login-registration');
		} else {
			$customer_id = Session::get('customer_id');
			$customer_info = DB::table('customers')
				->where('id', $customer_id)
				->first();
			$billing_address = $customer_info->address;
				// echo '<pre>';print_r($billing_address);exit;
			if (empty($billing_address) && count(Cart::content()) > 0) {
				return view('ecommerce-site.checkout.billing-address')->with('customer_info', $customer_info);
			}else {
				return redirect()->back();
			}

		}
	}

	public function updateCustomer(Request $request)
	{
		$update_customer_info = Customer::find($request->id);
		$this->validate($request, [

			'firstname' => 'required',
			'lastname' => 'required',
			'email' => 'required|email',
			'phone_num_one' => 'required',
			'address' => 'required',
		]);
		$update_customer_info->firstname = $request->firstname;
		$update_customer_info->lastname = $request->lastname;
		$update_customer_info->email = $request->email;
		$update_customer_info->phone_num_one = $request->phone_num_one;
		$update_customer_info->phone_num_two = $request->phone_num_two;
		$update_customer_info->address = $request->address;
		$update_customer_info->address_two = $request->address_two;
		$update_customer_info->additional_information = $request->additional_information;
		$update_customer_info->modified_on = date('Y-m-d H:i:s');
		$update_customer_info->status = 1;

		if($update_customer_info->save()){
			return redirect::to('/shipping')->send();
		}else{
			return redirect()->back();
		}
		
	}

	public function shipping()
	{
		if ($this->auth_check() == true) {
			return view('ecommerce-site.login-registration.login-registration');
		} else {
			$customer_id = Session::get('customer_id');
			$customer_data = DB::table('customers')->where('id',$customer_id)->first();
			$billing_address = $customer_data->address;
			// echo $billing_address;exit;
			$shipping_id = Session::get('shipping_id');
			// echo $shipping_id;exit;
			if (count(Cart::content()) > 0) {

				if(!empty($billing_address)){
					if( $shipping_id == ''){
						return view('ecommerce-site.checkout.shipping-address');	
					}else{
						return redirect('/payment');	
					}
				}else{
					return redirect('/billing');	
				}
			} else {
				return redirect('/');
			}
		}
	}

	public function saveShipping(Request $request)
	{

		$this->validate($request, [

			'name' => 'required',
			'email' => 'required|email',
			'phone_num_one' => 'required',
			'address' => 'required',
		]);
		$data = array();
		$data['customer_id'] = $request->customer_id;
		$data['name'] = $request->name;
		$data['email'] = $request->email;
		$data['company_name'] = $request->company_name;
		$data['phone_num_one'] = $request->phone_num_one;
		$data['phone_num_two'] = $request->phone_num_two;
		$data['address'] = $request->address;
		$data['address_two'] = $request->address_two;
		$data['additional_information'] = $request->additional_information;
		$data['created_at'] = date('Y-m-d H:i:s');


		$shipping_id = DB::table('shippings')->insertGetId($data);
		Session::put('shipping_id', $shipping_id);
		return redirect::to('/payment')->send();

		
	}

	public function prescription(){
		$shipping_id = Session::get('shipping_id');
		if (count(Cart::content()) > 0 && !empty($shipping_id)) {
			return view('ecommerce-site.checkout.prescription-upload');
		} else {
			return redirect('/shipping');
		}
		
	}

	public function prescriptionSave(Request $request){
		$this->validate($request, [
            'prescription' => 'required|mimes:jpeg,png',
        ]);
		$image                   = $request->file('prescription');
		$image_name              = $image->getClientOriginalName();
		$div                     = explode('.', $image_name);
		$base_name               = substr($image_name, 0, strrpos($image_name, "."));
		$file_ext                = strtolower(end($div));
		$unique_name             = $base_name.substr(md5(time()), 0, 10);
		$image_full_name         = $unique_name.'.'.$file_ext;
		$upload_path             = 'public/admin-frontend-assets/prescription/';
		$image->move($upload_path,$image_full_name);
		Session::put('prescription_image',$image_full_name );
		return redirect('/payment');
	}

	public function payment()
	{
		if ($this->auth_check() == true) {
			return view('ecommerce-site.login-registration.login-registration');
		} else {

			$products = Cart::content();
			$cat_array = array();
			foreach ($products as $product) {
				array_push($cat_array,$product->options->category);
			}
			// echo '<pre>';print_r($cat_array);
			$prescription_image = Session::get('prescription_image');
			// echo $prescription_image;exit;
			if(in_array('1',$cat_array)){
				if(empty($prescription_image)){
					$this->check_category = 6;
				}
			}
			// echo $this->check_category;
			if($this->check_category == 1){
				$shipping_id = Session::get('shipping_id');
				if (count(Cart::content()) > 0 && !empty($shipping_id)) {
					return view('ecommerce-site.checkout.payment');
				} else {
					return redirect('/shipping');
				}
	
			} else{
				return redirect('/prescription');
			}

			
		}

	}

	public function placeOrder(Request $request)
	{
		if ($this->auth_check() == true) {
			return view('ecommerce-site.login-registration.login-registration');
		} else {

			if ($request->payment_type == 'Cash') {
				$payment_data = new Payment();
				$payment_data->payment_type = $request->payment_type;
				$payment_data->payment_status = 'pending';
				$payment_data->save();

				date_default_timezone_set('Asia/Dhaka');
				$date = date('Y-m-d H:i:s');
				$order_data = new Order();
				$order_data->customer_id = Session::get('customer_id');
				$order_data->shipping_id = Session::get('shipping_id');
				$order_data->payment_id = $payment_data->id;
				$order_data->order_total = Session::get('final_total');
				$order_data->order_status = 'pending';
				$order_data->prescription_image = Session::get('prescription_image');
				$order_data->created_at = $date;
				$order_data->save();
				$order_id = $order_data->id;
				Session::put('order_id', $order_id);
					// $order_details_data = new OrderDetail();
				$order_details_data = array();
				$update_old_data = array();
				$products = Cart::content();
				foreach ($products as $product) {
					$update_data = DB::table('ecommerce_products')->where('id', $product->id)->first();
					$order_details_data[] = [
						'order_id' => $order_data->id,
						'product_id' => $product->id,
						'product_name' => $product->name,
						'product_price' => $product->price,
						'product_sales_qty' => $product->qty,
					];

					$p_qty = $update_data->piece_qty - $product->qty;
					if ($update_data->strip_qty <= 0) {
						$s_qty = 0;
					} else {
						$per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;
						$s_qty = $p_qty / $per_strip_piece;
					}
					if ($update_data->box_qty <= 0) {
						$b_qty = 0;
					} else {
						$per_box_strip = $update_data->strip_qty / $update_data->box_qty;
						($s_qty <= 0) ? $b_qty = 0 : $b_qty = $s_qty / $per_box_strip;
					}

					if ($update_data->carton_qty <= 0) {
						$c_qty = 0;
					} else {
						$per_ctn_box = $update_data->box_qty / $update_data->carton_qty;//3
						($b_qty <= 0) ? $c_qty = 0 : $c_qty = $b_qty / $per_ctn_box;
					}


					$update_old_data[] = [
						'id' => $update_data->id,
						'carton_qty' => $c_qty,
						'box_qty' => $b_qty,
						'strip_qty' => $s_qty,
						'piece_qty' => $p_qty,
					];
				}
				if (OrderDetail::insert($order_details_data)) {
					foreach ($update_old_data as $value) {
						DB::update("update ecommerce_products SET carton_qty=?,box_qty=?,strip_qty=?,piece_qty=? WHERE id=?", [$value['carton_qty'], $value['box_qty'], $value['strip_qty'], $value['piece_qty'], $value['id']]);
					}

				}
				$products = DB::table('order_details')
					->join('ecommerce_products', 'order_details.product_id', '=', 'ecommerce_products.id')
					->select('order_details.*', 'ecommerce_products.product_image')
					->where('order_details.order_id', $order_id)
					->get();
				Cart::destroy();
				Session::forget('shipping_id');
				Session::forget('order_id');
				Session::forget('final_total');
				Session::forget('prescription_image');
				return view('ecommerce-site.checkout.order-complete', compact('order_id'), compact('products'));

			} elseif ($request->payment_type == 'Stripe') {
				$payment_data = new Payment();
				$payment_data->payment_type = $request->payment_type;
				$payment_data->payment_status = 'pending';
				$payment_data->save();

				date_default_timezone_set('Asia/Dhaka');
				$date = date('Y-m-d H:i:s');
				$order_data = new Order();
				$order_data->customer_id = Session::get('customer_id');
				$order_data->shipping_id = Session::get('shipping_id');
				$order_data->payment_id = $payment_data->id;
				$order_data->order_total = Session::get('final_total');
				$order_data->order_status = 'pending';
				$order_data->prescription_image = Session::get('prescription_image');
				$order_data->created_at = $date;
				$order_data->save();
				$order_id = $order_data->id;
				Session::put('order_id', $order_id);
					// $order_details_data = new OrderDetail();
				$order_details_data = array();
				$update_old_data = array();
				$products = Cart::content();
				foreach ($products as $product) {
					$update_data = DB::table('ecommerce_products')->where('id', $product->id)->first();
					$order_details_data[] = [
						'order_id' => $order_data->id,
						'product_id' => $product->id,
						'product_name' => $product->name,
						'product_price' => $product->price,
						'product_sales_qty' => $product->qty,
					];

					$p_qty = $update_data->piece_qty - $product->qty;
					if ($update_data->strip_qty <= 0) {
						$s_qty = 0;
					} else {
						$per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;
						$s_qty = $p_qty / $per_strip_piece;
					}
					if ($update_data->box_qty <= 0) {
						$b_qty = 0;
					} else {
						$per_box_strip = $update_data->strip_qty / $update_data->box_qty;
						($s_qty <= 0) ? $b_qty = 0 : $b_qty = $s_qty / $per_box_strip;
					}

					if ($update_data->carton_qty <= 0) {
						$c_qty = 0;
					} else {
						$per_ctn_box = $update_data->box_qty / $update_data->carton_qty;//3
						($b_qty <= 0) ? $c_qty = 0 : $c_qty = $b_qty / $per_ctn_box;
					}


					$update_old_data[] = [
						'id' => $update_data->id,
						'carton_qty' => $c_qty,
						'box_qty' => $b_qty,
						'strip_qty' => $s_qty,
						'piece_qty' => $p_qty,
					];
				}
				if (OrderDetail::insert($order_details_data)) {
					foreach ($update_old_data as $value) {
						DB::update("update ecommerce_products SET carton_qty=?,box_qty=?,strip_qty=?,piece_qty=? WHERE id=?", [$value['carton_qty'], $value['box_qty'], $value['strip_qty'], $value['piece_qty'], $value['id']]);
					}

				}
				$total = Session::get('final_total');
				return view('ecommerce-site.checkout.paywithstripe', compact('total'));

			} else{
				return redirect::to('/payment')->with('error','Please select at least one payment method!');
			}
			// elseif($request->payment_type == 'SSLCommerz') {
			// 	$payment_data = new Payment();
			// 	$payment_data->payment_type = $request->payment_type;
			// 	$payment_data->payment_status = 'pending';
			// 	$payment_data->save();

			// 	$order_data = new Order();
			// 	$order_data->customer_id = Session::get('customer_id');
			// 	$order_data->shipping_id = Session::get('shipping_id');
			// 	$order_data->payment_id = $payment_data->id;
			// 	$order_data->order_total = Session::get('final_total');
			// 	$order_data->order_status = 'pending';
			// 	$order_data->save();
			// 	$order_id = $order_data->id;
			// 	Session::put('order_id', $order_id);
			// 		// $order_details_data = new OrderDetail();
			// 	$order_details_data = array();
			// 	$update_old_data = array();
			// 	$products = Cart::content();
			// 	foreach ($products as $product) {
			// 		$update_data = DB::table('ecommerce_products')->where('id', $product->id)->first();
			// 		$order_details_data[] = [
			// 			'order_id' => $order_data->id,
			// 			'product_id' => $product->id,
			// 			'product_name' => $product->name,
			// 			'product_price' => $product->price,
			// 			'product_sales_qty' => $product->qty,
			// 		];

			// 		$p_qty = $update_data->piece_qty - $product->qty;
			// 		if ($update_data->strip_qty <= 0) {
			// 			$s_qty = 0;
			// 		} else {
			// 			$per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;
			// 			$s_qty = $p_qty / $per_strip_piece;
			// 		}
			// 		if ($update_data->box_qty <= 0) {
			// 			$b_qty = 0;
			// 		} else {
			// 			$per_box_strip = $update_data->strip_qty / $update_data->box_qty;
			// 			($s_qty <= 0) ? $b_qty = 0 : $b_qty = $s_qty / $per_box_strip;
			// 		}

			// 		if ($update_data->carton_qty <= 0) {
			// 			$c_qty = 0;
			// 		} else {
			// 			$per_ctn_box = $update_data->box_qty / $update_data->carton_qty;//3
			// 			($b_qty <= 0) ? $c_qty = 0 : $c_qty = $b_qty / $per_ctn_box;
			// 		}


			// 		$update_old_data[] = [
			// 			'id' => $update_data->id,
			// 			'carton_qty' => $c_qty,
			// 			'box_qty' => $b_qty,
			// 			'strip_qty' => $s_qty,
			// 			'piece_qty' => $p_qty,
			// 		];
			// 	}
			// 	if (OrderDetail::insert($order_details_data)) {
			// 		foreach ($update_old_data as $value) {
			// 			DB::update("update ecommerce_products SET carton_qty=?,box_qty=?,strip_qty=?,piece_qty=? WHERE id=?", [$value['carton_qty'], $value['box_qty'], $value['strip_qty'], $value['piece_qty'], $value['id']]);
			// 		}

			// 	}
			// }
			
			
		}

	}

	public function paymentWithStripe(Request $request)
	{

			$validator = Validator::make($request->all(), [
				'card_no' => 'required|',
				'ccExpiryMonth' => 'required',
				'ccExpiryYear' => 'required',
				'cvvNumber' => 'required',
				//'amount' => 'required',
			]);
			$input = $request->all();
			if ($validator->passes()) {
				$input = array_except($input, array('_token'));
				$stripe = Stripe::make('sk_test_mVfXndabecE2rokeoo1caoFP');
				try {
					$token = $stripe->tokens()->create([
						'card' => [
							'number' => $request->get('card_no'),
							'exp_month' => $request->get('ccExpiryMonth'),
							'exp_year' => $request->get('ccExpiryYear'),
							'cvc' => $request->get('cvvNumber'),
						],
					]);
	 // $token = $stripe->tokens()->create([
	 // 'card' => [
	 // 'number' => '4242424242424242',
	 // 'exp_month' => 10,
	 // 'cvc' => 314,
	 // 'exp_year' => 2020,
	 // ],
	 // ]);
					if (!isset($token['id'])) {
						return redirect::to('/addmoney/stripe');
					}
					// $total = Session::get('final_total');
					$charge = $stripe->charges()->create([
						'card' => $token['id'],
						'currency' => 'USD',
						'amount' => $request->get('amount'),
						'description' => 'Add in wallet',
					]);
	
					if ($charge['status'] == 'succeeded') {
						$order_id = Session::get('order_id');
						$order_data = Order::find($order_id);
						$payment_id = $order_data->payment_id;
						$payment_data = Payment::find($payment_id);
						$payment_data->payment_status = 'done';
						$payment_data->payment_date = date('Y-m-d');
						if($payment_data->save()){
							$products = DB::table('order_details')
										->join('ecommerce_products', 'order_details.product_id', '=', 'ecommerce_products.id')
										->select('order_details.*', 'ecommerce_products.product_image')
										->where('order_details.order_id', $order_id)
										->get();
							Cart::destroy();
							Session::forget('shipping_id');
							Session::forget('order_id');
							Session::forget('final_total');
							Session::forget('prescription_image');
							return view('ecommerce-site.checkout.order-complete', compact('order_id'), compact('products'));
	
						}
						// echo "< pre > ";
						// print_r($charge);
						// exit();
						// return redirect()->route('addmoney . paywithstripe');
					} else {
						// Session::put('error', 'Money not add in wallet !!');
						return redirect('/payment')->with('error', 'Money not add in wallet !!');
					}
				} catch (Exception $e) {
					// \Session::put('error', $e->getMessage());
					return redirect('/payment')->with('error', $e->getMessage());
				} catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
					return redirect('/payment')->with('error', $e->getMessage());
				} catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
					return redirect('/payment')->with('error', $e->getMessage());
				}
			}else{
				return redirect()->back();
			}

		
	}

	public function auth_check()
	{
		
		session_start();
		$customer_id = Session::get('customer_id');
		if (empty($customer_id)) {
			return redirect::to('/authentication')->send();//specific kore push korar jonno send() name er function dite hoy
		}
	}
	public function logout()
	{
		Session::flush();
		return redirect('/');
	}

}
