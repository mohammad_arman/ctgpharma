<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Illuminate\Support\Facades\Auth;
use DB;

class CompanyController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth'); 
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        });       
    }
    
    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            return view('admin.company.company-add');         
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function storeCompany(Request $request) {
        $company = new Company();
        $this->validate($request, [
            'company_name'       => 'required|unique:companies',
            'contact_number'     => 'nullable',
            'address'            => 'nullable',
            'created_by'         => 'required',
        ]);
        $company->company_name   = $request->company_name;
        $company->contact_number = $request->contact_number;
        $company->address        = $request->address;
        $company->created_by     = $request->created_by;
        $company->save();
        session()->flash('message','Company Data Inserted Successfully');
        return redirect('/company/add');
    }
    
    public function manageCompany() {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $company_info = Company::orderBy('id','desc')->get();
            return view('admin.company.company-manage', compact('company_info'));         
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function editCompany($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $edit_company_info = Company::where('id',$id)->first();
             return view('admin.company.company-edit', compact('edit_company_info'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
    
    public function updateCompany(Request $request) {
        $update_company_info = Company::find($request->id);
        $this->validate($request, [
            'company_name'   => 'required',
            'contact_number' => 'nullable',
            'address'        => 'nullable',
        ]);
        $update_company_info->company_name   = $request->company_name;
        $update_company_info->contact_number = $request->contact_number;
        $update_company_info->address        = $request->address;
        $update_company_info->save();
        return redirect('/company/manage')->with('message','Company Info Updated Successfully');
    }
    
    public function viewCompany($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $view_company_info = Company::where('id',$id)->first();
             return view('admin.company.company-view', compact('view_company_info'));               
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
    
    public function deleteCompany($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_company = Company::find($id);
            $delete_company->delete();
            return redirect('/company/manage')->with('message','Company Deleted Successfully');         
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    
}
