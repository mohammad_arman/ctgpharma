<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
//    public function branchId() {
//	$user_id = auth()->user()->id;
//	$branch_id = DB::table('role_user')->select('branch_id')->where('user_id',$user_id)->first();
//	Session::put('branch_id',$branch_id);
//    }
}
