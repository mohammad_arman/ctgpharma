<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use DB;

class CustomerController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }


    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $customers = Customer::all();
            return view('admin.customer.customer-manage',compact('customers'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function view($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $customer = Customer::find($id);
            return view('admin.customer.customer-details',compact('customer'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function active($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $customer = Customer::find($id);
            $customer->status = 1;
            if($customer->save()){
                return redirect()->back()->with('success','Customer account activated Successfully');
            }else{
                return redirect()->back()->with('error','Customer account Can\'t activate ');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
    public function inactive($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $customer = Customer::find($id);
            $customer->status = 0;
            if($customer->save()){
                return redirect()->back()->with('success','Customer account inactivated Successfully');
            }else{
                return redirect()->back()->with('error','Customer account Can\'t inactivate ');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function delete($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $customer = Customer::find($id);
            if($customer->delete()){
                return redirect()->back()->with('success','Customer account deleted Successfully');
            }else{
                return redirect()->back()->with('error','Customer account Can\'t delete ');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
}
