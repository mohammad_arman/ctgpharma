<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EcommerceCategory;
use Illuminate\Support\Facades\Auth;
use DB;

class EcommerceCategoryController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }
    
    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            return view('admin.category.ecommerce-product-category-add');         
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function storeCategory(Request $request) {

        $category = new EcommerceCategory();
        $this->validate($request, [
            'category_name' => 'required|unique:ecommerce_categories',
            'description' => 'nullable',
            'publication_status' => 'required',
            'created_by' => 'required',
        ]);
        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->publication_status = $request->publication_status;
        $category->created_by = $request->created_by;
        $category->save();
        session()->flash('message','Category Inserted successfully');
        return redirect('/category/add');
    }
    
    public function manageCategory() {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $categories_info = EcommerceCategory::orderBy('id','desc')->get();
            return view('admin.category.ecommerce-product-category-manage',compact('categories_info'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function unpublishedCategory($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $unpublished_category = EcommerceCategory::find($id);       
            $unpublished_category->publication_status = 0;
            $unpublished_category->save();
            return redirect('/category/manage');           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function publishedCategory($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $published_category = EcommerceCategory::find($id);       
            $published_category->publication_status = 1;
            $published_category->save();
            return redirect('/category/manage');           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function editCategory($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $edit_category_info = EcommerceCategory::where('id',$id)->first();
            return view('admin.category.ecommerce-product-category-edit',compact('edit_category_info'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function updateCategory(Request $request) { 
        $update_category_info = EcommerceCategory::find($request->id);
        $this->validate($request, [
            'category_name' => 'required',
            'description' => 'nullable',
        ]);
        $update_category_info->category_name = $request->category_name;
        $update_category_info->description   = $request->description;
        $update_category_info->save();
        return redirect('/category/manage')->with('message','Category Info Updated Successfully');
    }
    
    public function viewCategory($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $view_category_info = EcommerceCategory::where('id',$id)->first();
            return view('admin.category.ecommerce-product-category-view',compact('view_category_info'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
    
    public function deleteCategory($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_category = EcommerceCategory::find($id);
            $delete_category->delete();
            return redirect('/category/manage')->with('message','Category Deleted Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }


    
}
