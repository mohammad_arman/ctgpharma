<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EcommerceProduct;
use App\EcommerceCategory;
use App\Customer;
use DB;
use Session;
use Redirect;
use App\Order;
use App\OrderDetail;
use App\Payment;
use App\Shipping;

class EcommerceSiteController extends Controller
{
    public function auth_check() {
        session_start();
        $customer_id = Session::get('customer_id');
        if(empty($customer_id)){
            return redirect::to('/authentication')->send();//specific kore push korar jonno send() name er function dite hoy
        }
    }
    public function index()
    {
	
	$featured_product = EcommerceProduct::where('publication_status', 1)
                ->where('feature_product', 1)
                ->where('discount','=', 0)
                ->where('piece_qty','>', 0)
               ->orderBy('id', 'desc')
               ->take(50)
               ->get()
               ->random(8);
	$discount_product = EcommerceProduct::where('publication_status', 1)
               ->where('discount','>', 0)
               ->where('piece_qty','>', 0)
               ->take(100)
               ->get()
               ->random(8);
    $regular_product = EcommerceProduct::where('publication_status', 1)
                ->where('feature_product', 0)
                ->where('discount','=', 0)
               ->where('piece_qty','>', 0)
               ->take(100)
               ->get()
               ->random(8);
            //    echo '<pre>';print_r($regular_product->toArray());exit;
	$category = EcommerceCategory::where('publication_status', 1)->take(8)->get();
        return view('ecommerce-site.home.home-content')
		->with('featured_product', $featured_product)
		->with('discount_product', $discount_product)
		->with('regular_product', $regular_product)
		->with('category', $category);
    }
    
    
    
    public function category($id) {
	$category_name = EcommerceCategory::where('id',$id)->first();
	$cat_list = DB::table('ecommerce_products')  
                    ->where('category_id',$id)
				    ->paginate(8);
        return view('ecommerce-site.category.category-content')->with('category_name', $category_name)
							       ->with('cat_list', $cat_list);
							
    }
    
    public function productDetails($id) {
	
	$product_by_id = DB::table('ecommerce_products')
                                    ->join('ecommerce_categories','ecommerce_products.category_id','=','ecommerce_categories.id')
                                    ->select('ecommerce_products.*','ecommerce_categories.category_name')
                                    ->where('ecommerce_products.id',$id)
                                    ->first();
        return view('ecommerce-site.product-details.product-details')->with('product', $product_by_id);
							       
							
    }
    
    public function search() {
        $searching_keyword = \Request::get('search');
       
    // $searching_keyword = $request->get('search');
    // echo '<pre>';print_r($searching_keyword);exit;
	$result  = DB::table('ecommerce_products')
                    ->join('ecommerce_categories','ecommerce_products.category_id','=','ecommerce_categories.id')
                    ->select('ecommerce_products.*','ecommerce_categories.category_name')
                    ->where('product_name','like','%'.$searching_keyword.'%')
                    ->get();
                    // echo '<pre>';print_r($result->toArray());exit;
	return view('ecommerce-site.search-products.search-products',compact('result'));	
    }
    
    public function authentication() {
        // echo $_SERVER['HTTP_REFERER'];exit();
	    session_start();
        $customer_id = Session::get('customer_id');
        if(empty($customer_id)){
            return view('ecommerce-site.login-registration.login-registration');
        }else{
            return redirect('/');
        }
    }
    
    public function myAccount() {
        if($this->auth_check() == FALSE){
            return view('ecommerce-site.user-account.my-account');
        }
    }
    
    public function myProfile() {
        if($this->auth_check() == FALSE){
            $customer_id = Session::get('customer_id');
            $customer_info = Customer::find($customer_id);
            return view('ecommerce-site.user-account.my-profile',compact('customer_info'));
        }
    }
    
    public function myAddress() {
        if($this->auth_check() == FALSE){
            return view('ecommerce-site.user-account.my-address');
        }
    }
    
    // public function addAddress() {
	// return view('ecommerce-site.user-account.add-address');
    // }
    
    public function personalInformation() {
        if($this->auth_check() == FALSE){
            $customer_id = Session::get('customer_id');
            $customer_info = DB::table('customers')->select('*')
				  ->where('id',$customer_id)
				  ->first(); 
            return view('ecommerce-site.user-account.add-personal-information')->with('customer_info', $customer_info);
        }
    }

    public function updateCPersonalInfo(Request $request)
    {
        $update_customer_info = Customer::find($request->id);

        
        $this->validate($request, [
        
            'firstname'		=> 'required',
            'lastname'		=> 'required',
            'email'			=> 'required|email',
            'phone_num_one'	=> 'required',		
            'password'      => 'required|string|min:6|confirmed',
            'address'		=> 'required',
            ]);
        
        $old_password =  $update_customer_info->password;
        if($old_password == $request->password){
            $password = $request->password;
        }else{
            $password = md5($request->password);
        }

        $update_customer_info->firstname              = $request->firstname;
        $update_customer_info->lastname               = $request->lastname;
        $update_customer_info->email                  = $request->email;
        $update_customer_info->phone_num_one          = $request->phone_num_one;
        $update_customer_info->phone_num_two          = $request->phone_num_two;
        $update_customer_info->password               = $password;
        $update_customer_info->address                = $request->address;
        $update_customer_info->address_two            = $request->address_two;
        $update_customer_info->additional_information = $request->additional_information;
        $update_customer_info->modified_on = date('Y-m-d H:i:s');
        $update_customer_info->status = 1;
        
        if($update_customer_info->save()){
            return redirect()->back()->with('success','Information updated successfully');
        }else{
            return redirect()->back()->with('error','Information can\'t update!');
        }
        
    }
    
    public function wishlist() {
	return view('ecommerce-site.user-account.wishlist');
    }
    
    public function orderList() {
        if($this->auth_check() == FALSE){
            $customer_id = Session::get('customer_id');
            $order_data = DB::table('orders')->join('payments','orders.payment_id','=','payments.id')
                                            ->select('orders.*','payments.payment_type')
                                            ->where('orders.customer_id',$customer_id)
                                            ->orderBy('orders.id','desc')
                                            ->get();

            $order_list = array();
            foreach ($order_data as $value) {               
                $products_data = OrderDetail::where('order_id',$value->id)->count();
                $count_product = count($products_data);
                
                $order_list[] = [
                    'id' => $value->id,
                    'payment_id' => $value->payment_id,
                    'order_total' => $value->order_total,
                    'order_status' => $value->order_status,
                    'created_at' => $value->created_at,
                    'payment_type' => $value->payment_type,
                    'total_product' => $products_data
                ];
                
            }
            // echo '<pre>';print_r($payment_info);exit;
            return view('ecommerce-site.user-account.orderlist',compact('order_list'));
        }
    }
    
    public function orderStatus($id) {
        if($this->auth_check() == FALSE){
            $order_data = DB::table('orders')->join('customers','orders.customer_id','=','customers.id')
                                        ->join('payments','orders.payment_id','=','payments.id')
                                        ->select('orders.*','customers.firstname','customers.lastname',
                                        'customers.email','customers.phone_num_one','customers.phone_num_two',
                                        'customers.address','customers.address_two','customers.additional_information',
                                        'payments.payment_type','payments.payment_status','payments.payment_date')
                                        ->where('orders.id',$id)
                                        ->first();
            $shipping_info    = DB::table('shippings')->where('id',$order_data->shipping_id)->first();  
            $ordered_products = DB::table('order_details')
                                        ->join('ecommerce_products','order_details.product_id','=','ecommerce_products.id')
                                        ->select('order_details.*','ecommerce_products.product_image','ecommerce_products.weight')
                                        ->where('order_details.order_id',$id)
                                        ->get();                            
            return view('ecommerce-site.user-account.order-status')
                        ->with('order_data',$order_data)
                        ->with('shipping_info',$shipping_info)
                        ->with('ordered_products',$ordered_products);
        }
    }
}
