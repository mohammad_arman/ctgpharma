<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\Expense;
use App\Invoice;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;

class ExpenseController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }

    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $branch_list = Branch::all();
            return view('admin.expense.expense-add',compact('branch_list'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    
    public function saveExpense(Request $request)
    {
        date_default_timezone_set('Asia/Dhaka');
        $this->validate($request, [
            'expense_name' => 'required',
            'branch_id' => 'required',
            'amount' => 'required|numeric|min:1',
     
        ]);
        if($request->amount <= 0){
            return redirect()->back()->with('error',"Amount must be greater than zero ");
        }else{
            $expense = new Expense();
            $expense->expense_name = $request->expense_name;
            $expense->branch_id = $request->branch_id;
            $expense->amount = $request->amount;
            $expense->date = date('Y-m-d');
            $expense->created_at = date('Y-m-d H:i:s');
            if($expense->save()){
                return redirect()->back()->with('success','Expense data added Successfully');
            }else{
                return redirect()->back()->with('error','Expense data can\'t add ');
            }
    
        }
        
    }


    public function manageExpense()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $expense_data = Expense::orderBy('id','desc')->get();
            return view('admin.expense.expense-manage',compact('expense_data'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }


    public function viewExpense($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $expense = Expense::find($id);
            return view('admin.expense.expense-view',compact('expense'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }


    public function editExpense($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $expense = Expense::find($id);
            $branch_list = Branch::all();
            return view('admin.expense.expense-edit',compact('expense'),compact('branch_list'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

   
    public function updateExpense(Request $request)
    {
        date_default_timezone_set('Asia/Dhaka');
        $this->validate($request, [
            'expense_name' => 'required',
            'branch_id' => 'required',
            'amount' => 'required|numeric|min:1',
     
        ]);
        if($request->amount <= 0){
            return redirect()->back()->with('error',"Amount must be greater than zero ");
        }else{
            $expense = Expense::find($request->id);
            $expense->expense_name = $request->expense_name;
            $expense->branch_id = $request->branch_id;
            $expense->amount = $request->amount;
            $expense->updated_at = date('Y-m-d H:i:s');
            if($expense->save()){
                return redirect()->back()->with('success','Expense data updated Successfully');
            }else{
                return redirect()->back()->with('error','Expense data can\'t update ');
            }
    
        }
    }


    public function deleteExpense($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_expense = Expense::find($id);
            if($delete_expense->delete()){
                return redirect()->back()->with('success','Expense data deleted Successfully');
            }else{
                return redirect()->back()->with('error','Expense data can\'t delete ');
            }          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }


    public function expenseReport()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $expense_data = Expense::orderBy('id','desc')->get();
            return view('admin.expense.expense-report')->with('expense_data',$expense_data);          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        

    }

    public function expenseData(Request $request)
    {
        if($request->ajax()){
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');
            $table_data = '';
            if($start_date != '' && $end_date != ''){
                $expense_data = DB::select("select expenses.*,branches.branch_name from expenses INNER JOIN branches ON expenses.branch_id = branches.id
                 where expenses.date between ? ANd ? ORDER BY expenses.id DESC",[$start_date,$end_date]);
 
            }else{
                $expense_data = DB::select("select expenses.*,branches.branch_name from expenses INNER JOIN branches ON expenses.branch_id = branches.id ORDER BY expenses.id DESC");
            }
            $total_row =count( $expense_data);
            if($total_row > 0){
                $i=1;
                foreach ($expense_data as $row) {
                    $table_data .= '
                    <tr>
                    <td>'.$i++.'</td>
                    <td>'.$row->expense_name.'</td>
                    <td>'.$row->branch_name.'</td>
                    <td>'.number_format($row->amount,2).'</td>
                    <td>'.date('F j, Y', strtotime($row->date)).'</td>
                   </tr>
                   ';
                }
                
            }else{
                $table_data = '
                    <tr>
                        <td align="center" colspan="5">No Data Found</td>
                    </tr>
                    ';
            }
        }
        echo json_encode($table_data);
    }

    public function incomeReport()
    {

        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $income_data = Invoice::where('invoice_type',1)->orderBy('id','desc')->get();
            return view('admin.expense.income-report')->with('income_data',$income_data);          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
        
    }
    // public function incomeData(Request $request)
    // {
    //     if($request->ajax()){
    //         $start_date = $request->get('start_date');
    //         $end_date = $request->get('end_date');
    //         $output = '';
    //         if($start_date != '' && $end_date != ''){
    //             $income_data = DB::select("select inv.invoice_number as income,inv.paid_amount as amount,inv.paid_date as date from invoices inv WHERE inv.invoice_type=1 AND inv.paid_date 
    //     BETWEEN ? AND ? UNION ALl select o.id as income,o.order_total as amount,o.created_at as date from orders o
    //     WHERE o.order_status='done' AND  DATE(o.created_at) BETWEEN ? AND ?",[$start_date,$end_date,$start_date,$end_date]);
 
    //         }else{
    //             $income_data = DB::select("select inv.invoice_number as income,inv.paid_amount as amount,inv.paid_date as date from invoices inv WHERE inv.invoice_type=1
	// 			 UNION ALl select o.id as income,o.order_total as amount,o.created_at as date from orders o
	// 			WHERE o.order_status='done'");
    //         }
    //         $total_row =count($income_data);
    //         if($total_row > 0){
    //             $i=1;
    //             foreach ($income_data as $row) {
    //                 $output .= '<tr>
    //                 <td>'.$i++.'</td>
    //                 <td>'.$row->income.'</td>
    //                 <td>'.$row->amount.'</td>
    //                 <td>'.$row->date.'</td>
    //                 </tr>';
    //             }
                
    //         }else{
    //             $output = '
    //                 <tr>
    //                     <td align="center" colspan="5">No Data Found</td>
    //                 </tr>
    //                 ';
    //         }
    //     }
    //     echo json_encode($output);
        
    // }
}
