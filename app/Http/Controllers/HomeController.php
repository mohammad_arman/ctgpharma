<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Customer;
use App\Order;
use App\Invoice;
use App\Branch;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }

   
    public function index(Request $request)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $request->user()->authorizeRoles(['Admin', 'Sub-Admin','Manager','Employee']);
            $current_date = date('Y-m-d');
            $year = date('Y');
            $month = date('m');
            $month_begining_date = "'".$year.'-'.$month."-01'";
            $month_ending_date = "'".$year.'-'.$month."-31'";

            $new_customers = DB::select("select * from customers where DATE(created_at) =?",[$current_date]);
            $total_customers = count($new_customers);

            $new_orders = DB::select("select * from orders where DATE(created_at) =?",[$current_date]);
            $total_orders = count($new_orders);

            $new_invoices = DB::select("select * from invoices where DATE(created_at) =?",[$current_date]);
            $total_invoices = count($new_invoices);
            $status = 'done';
            $current_month_ecommerce_income = DB::select("select sum(order_total) as income from orders where order_status='done' AND DATE(created_at) between ".$month_begining_date." AND ".$month_ending_date);
            if(!empty($current_month_ecommerce_income)){
                $total_current_month_ecommerce_income = $current_month_ecommerce_income[0]->income;
            }else{
                $total_current_month_ecommerce_income = 0;
            }
            
            $current_month_ecommerce_expense = DB::select("select sum(amount) as expense from expenses where branch_id=1 AND date between ".$month_begining_date." AND ".$month_ending_date);
            if(!empty($current_month_ecommerce_expense)){
                $total_current_month_ecommerce_expense = $current_month_ecommerce_expense[0]->expense;
            }else{
                $total_current_month_ecommerce_expense = 0;
            }

            $total_current_month_ecommerce_profit = $total_current_month_ecommerce_income - $total_current_month_ecommerce_expense;

            $current_month_branch_income = DB::select("select sum(paid_amount) as income from invoices where invoice_type=1 and  DATE(created_at) between ".$month_begining_date." AND ".$month_ending_date);
            if(!empty($current_month_branch_income)){
                $total_current_month_branch_income = $current_month_branch_income[0]->income;
            }else{
                $total_current_month_branch_income = 0;
            }
            $current_month_branch_expense = DB::select("select sum(amount) as expense from expenses where branch_id != 1 AND date between ".$month_begining_date." AND ".$month_ending_date);
            if(!empty($current_month_branch_expense)){
                $total_current_month_branch_expense = $current_month_branch_expense[0]->expense;
            }else{
                $total_current_month_branch_expense = 0;
            }
            $total_current_month_branch_profit = $total_current_month_branch_income - $total_current_month_branch_expense;
            
            // $year = '2018';
            // $requestedYearBegining = $year.'-01-01';
            // $requestedYearEnding   = $year.'-12-31';
            // $current_month_ecommerce_income = DB::select("select sum(order_total) as income from orders where order_status='done' AND DATE(created_at) between ? ANd ?",[$requestedYearBegining,$requestedYearEnding]);
            // $total_ecommerce_income = $current_month_ecommerce_income[0]->income;
            // print_r($total_current_month_ecommerce_expense);exit;


            
                        // echo 'pre>';print_r($requestArray);exit;

        $branch_list = Branch::all();
        return view('admin.home.home')
            ->with('total_customers',$total_customers)
            ->with('total_orders',$total_orders)
            ->with('total_invoices',$total_invoices)
            ->with('total_current_month_ecommerce_income',$total_current_month_ecommerce_income)
            ->with('total_current_month_ecommerce_expense',$total_current_month_ecommerce_expense)
            ->with('total_current_month_ecommerce_profit',$total_current_month_ecommerce_profit)
            ->with('total_current_month_branch_income',$total_current_month_branch_income)     
            ->with('total_current_month_branch_expense',$total_current_month_branch_expense)     
            ->with('total_current_month_branch_profit',$total_current_month_branch_profit)     
            ->with('branch_list',$branch_list) ;                
        }else{
            return redirect('/product/manage');
        }
        
    }

    public function branchStatistics(Request $request)
    {
        if($request->ajax())
        {
            $branch_id = $request->get('branch');
            $year = $request->get('year');
            $requestedYearBegining = $year.'-01-01';
            $requestedYearEnding   = $year.'-12-31';
            if($branch_id != '' && $year != ''){
                if($branch_id == 0){
                    $all_branch_income = DB::select("select sum(paid_amount) as branch_income from invoices where invoice_type=1 and  paid_date between ? AND ?",[$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($all_branch_income)){
                        $total_branch_yearly_income = $all_branch_income[0]->branch_income;
                    }else{
                        $total_branch_yearly_income = 0;
                    }
                    
                    $ecommerce_income = DB::select("select sum(order_total) as ecommerce_income from orders where order_status='done' AND DATE(created_at) between ? ANd ?",[$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($ecommerce_income)){
                        $total_ecommerce_income = $ecommerce_income[0]->ecommerce_income;
                    }else{
                        $total_ecommerce_income = 0;
                    }
                    
                    $total_yearly_income = $total_branch_yearly_income +  $total_ecommerce_income;

                    $all_expense = DB::select("select sum(amount) as expense from expenses where date between ? AND ?",[$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($all_expense)){
                        $total_yearly_expense = $all_expense[0]->expense;
                    }else{
                        $total_yearly_expense = 0;
                    }
                    
                    $total_yearly_profit = $total_yearly_income - $total_yearly_expense;

                    $requestArray = [
                        'total_yearly_income'  => number_format((float)$total_yearly_income,2,'.',''),
                        'total_yearly_expense' => number_format((float)$total_yearly_expense,2,'.',''),
                        'year'                 => $year,
                        'income'               => number_format((float)$total_yearly_income,2,'.',''),
                        'expense'              => number_format((float)$total_yearly_expense,2,'.',''),
                        'totalProfit'          => number_format((float)$total_yearly_profit,2,'.','')
                    ];
                }elseif($branch_id == 1){
                    
                    $ecommerce_income = DB::select("select sum(order_total) as ecommerce_income from orders where order_status='done' AND DATE(created_at) between ? ANd ?",[$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($ecommerce_income)){
                        $total_ecommerce_income = $ecommerce_income[0]->ecommerce_income;
                    }else{
                        $total_ecommerce_income = 0;
                    }
                    
                    $total_yearly_income = $total_ecommerce_income;

                    $ecommerce_expense = DB::select("select sum(amount) as expense from expenses where branch_id=1 AND date between ? AND ?",[$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($ecommerce_expense)){
                        $total_yearly_expense = $ecommerce_expense[0]->expense;
                    }else{
                        $total_yearly_expense = 0;
                    }
                    
                    $total_yearly_profit = $total_yearly_income - $total_yearly_expense;

                    $requestArray = [
                        'total_yearly_income'  => number_format((float)$total_yearly_income,2,'.',''),
                        'total_yearly_expense' => number_format((float)$total_yearly_expense,2,'.',''),
                        'year'                 => $year,
                        'income'               => number_format((float)$total_yearly_income,2,'.',''),
                        'expense'              => number_format((float)$total_yearly_expense,2,'.',''),
                        'totalProfit'          => number_format((float)$total_yearly_profit,2,'.','')
                    ];
                }elseif($branch_id != 0 && $branch_id != 1){
                    $each_branch_income = DB::select("select sum(paid_amount) as branch_income from invoices where invoice_type=1 AND branch_id=? AND paid_date between ? AND ?",[$branch_id,$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($each_branch_income)){
                        $total_branch_yearly_income = $each_branch_income[0]->branch_income;
                    }else{
                        $total_branch_yearly_income = 0;
                    }
                    
                    $total_yearly_income = $total_branch_yearly_income;

                    $each_branch_expense = DB::select("select sum(amount) as expense from expenses where branch_id=? AND date between ? AND ?",[$branch_id,$requestedYearBegining,$requestedYearEnding]);
                    if(!empty($each_branch_expense)){
                        $total_yearly_expense = $each_branch_expense[0]->expense;
                    }else{
                        $total_yearly_expense = 0;
                    }
                    
                    $total_yearly_profit = $total_yearly_income - $total_yearly_expense;

                    $requestArray = [
                        'total_yearly_income'  => number_format((float)$total_yearly_income,2,'.',''),
                        'total_yearly_expense' => number_format((float)$total_yearly_expense,2,'.',''),
                        'year'                 => $year,
                        'income'               => number_format((float)$total_yearly_income,2,'.',''),
                        'expense'              => number_format((float)$total_yearly_expense,2,'.',''),
                        'totalProfit'          => number_format((float)$total_yearly_profit,2,'.','')
                    ];
                }else{
                    $requestArray = '';
                }

            }else{
                $requestArray = '';
            }
            echo json_encode($requestArray);
        }
    }
}
