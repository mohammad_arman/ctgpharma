<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BranchProductDistribution;
use DB;
use App\Invoice;
use App\InvoiceItem;
use App\InvoicePayment;
use App\Supplier;
use PDF;
use Session;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller {

    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }
    
    public function index() {
        $branch_id = Session::get('branch_id');
		if($branch_id == 1){
            $products = BranchProductDistribution::where('piece_qty','>',0)->get();
        }else{
            $products = BranchProductDistribution::where('piece_qty','>',0)->where('branch_id',$branch_id)->get();
        }
        $last_id = DB::select('SELECT id FROM invoices ORDER BY id DESC LIMIT 1');
        (count($last_id) > 0)  ? $num = $last_id[0]->id : $num = 0;
        $invoice_number = 100 + $num;
        date_default_timezone_set('Asia/Dhaka');
        $issue_date = date('Y-m-d h:i:s A');

        $output = '';
        if(count($products) > 0){
            foreach ($products as $row) {
                    $output .= '<option value="'.$row->product_name.'">'.$row->product_name.'</option>';
            }
        }
        
        return view('admin.invoice.invoice-add', compact('output','issue_date','invoice_number'));
    }
    
    private function dateFormat($date){
        return date('Y-m-d H:i:s',strtotime($date));
    }

    public function productType(Request $request) {
        
        if($request->ajax())
        {
            $output ='';
            $query = $request->get('query');
            if($query != ''){
                $branch_id = Session::get('branch_id');
                $data = BranchProductDistribution::where('product_name',$query)->where('branch_id',$branch_id)->first();
                $carton = $data->carton_qty;
                $box = $data->box_qty;
                $strip = $data->strip_qty;
                $piece = $data->piece_qty;
                if($carton > 0 && $box > 0 && $strip > 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Carton">Carton</option>
                    <option value="Box">Box</option>
                    <option value="Strip">Strip</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton == 0 && $box > 0 && $strip > 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Box">Box</option>
                    <option value="Strip">Strip</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton == 0 && $box == 0 && $strip > 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Strip">Strip</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton == 0 && $box == 0 && $strip == 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton > 0 && $box > 0 && $strip == 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Carton">Carton</option>
                    <option value="Box">Box</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton == 0 && $box > 0 && $strip == 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Box">Box</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton > 0 && $box == 0 && $strip == 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Carton">Carton</option>
                    <option value="Piece">Piece</option>';
                }elseif($carton > 0 && $box == 0 && $strip > 0 && $piece > 0){
                    $output .= 
                    '<option value="">Please Select</option>
                    <option value="Carton">Carton</option>
                    <option value="Strip">Strip</option>
                    <option value="Piece">Piece</option>';
                }else{
                    $output .= '';
                }

                $data = [
                    'output'=>$output,
                    'id'=>$data->id
                ];
                echo json_encode($data);
            }
        }
        
    }

    public function productPrice(Request $request)
    {
        if($request->ajax()){
            $price = '';
            $type = $request->get('type');
            $id = $request->get('id');
            if($type !== '' && $id !== ''){
                $branch_id = Session::get('branch_id');
                $data = BranchProductDistribution::where('id',$id)->where('branch_id',$branch_id)->first();
                if($type == 'Carton'){
                    $price = $data->carton_sales_rate;
                    $qty   = $data->carton_qty;
                }elseif($type == 'Box'){
                    $price = $data->box_sales_rate;
                    $qty   = $data->box_qty;
                }elseif ($type == 'Strip'){
                    $price = $data->strip_sales_rate;
                    $qty   = $data->strip_qty;
                }elseif ($type == 'Piece'){
                    $price = $data->piece_sales_rate;
                    $qty   = $data->piece_qty;
                }else{
                    $price = '';
                    $qty   = '';
                }
            }
            $data = [
                'price' => $price,
                'qty' => $qty,
            ];
            echo json_encode($data);
        }
    }
    
    public function storeInvoice(Request $request) {
        $this->validate($request, [
            'receiver_name'    => 'required',
            'mobile_number' => 'required',
        ]);
        // $invoice_item = new InvoiceItem();
        $created_by      = $request->created_by;
        $item_name       = $request->item_name;
        $item_type       = $request->item_type;
        $item_qty        = $request->item_qty;
        $item_price      = $request->item_price;
        $data            = array();
        $update_old_data = array();
        $id = array();
        
        $selling_invoice = SELLING_INVOICE;
        date_default_timezone_set('Asia/Dhaka');
        if(($request->total > $request->paid) && ($request->paid > 0)){
            $date = date('Y-m-d H:i:s');
            $invoice_status = DUE;
        }elseif ($request->total == $request->paid) {
            $date = $this->dateFormat($request->issue_date);
            $invoice_status = PAID;
        }else{
            $date = NULL;
            $invoice_status = DUE;
        }
        $branch_id = Session::get('branch_id');
        $invoice = new Invoice;
        $invoice->invoice_number = $request->invoice_number;
        $invoice->branch_id      = $branch_id;
        $invoice->receiver_name  = $request->receiver_name;
        $invoice->mobile_number  = $request->mobile_number;
        $invoice->issue_date     = $this->dateFormat($request->issue_date);
        $invoice->paid_date      = $date;
        $invoice->total_amount   = $request->total;
        $invoice->paid_amount    = $request->paid;
        $invoice->due_amount     = $request->outstanding;
        $invoice->invoice_type   = $selling_invoice;
        $invoice->invoice_status = $invoice_status;
        $invoice->created_by     = $created_by;
        // echo '<pre>';
        // print_r($invoice->toArray());
        // exit();
        if($invoice->save()){
            $invoice_id = $invoice->id;
            for($count = 0; $count<count($item_name); $count++){
            
                $product_name  = $item_name[$count];
                $product_type  = $item_type[$count];
                $quantity      = $item_qty[$count];
                $price         = $item_price[$count];

                $update_data = DB::table('branch_product_distributions')->where('product_name',$product_name)->where('branch_id',$branch_id)->first();
                
                if($product_type == 'Piece'){

                    $p_qty =  $update_data->piece_qty - $quantity ;
                    if(!empty($update_data->strip_qty)){
                        $per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;
                        $s_qty = $p_qty / $per_strip_piece;
                    }else{
                        $s_qty = 0;
                    }
                    if(!empty($update_data->box_qty)){
                        $per_box_strip = $update_data->strip_qty / $update_data->box_qty ;
                        ($s_qty <= 0 ) ? $b_qty = 0 :  $b_qty =  $s_qty / $per_box_strip;
                    }else{
                        $b_qty = 0;
                    }

                    if(!empty($update_data->carton_qty)){
                        $per_ctn_box = $update_data->box_qty /  $update_data->carton_qty;//3
                        ($b_qty <= 0 ) ? $c_qty = 0 : $c_qty = $b_qty / $per_ctn_box;
                    }else{
                        $c_qty = 0;
                    }
                    

                     
                
                }elseif ($product_type == 'Strip') {

                   
                    if(!empty($update_data->strip_qty)){
                        $s_qty =  $update_data->strip_qty - $quantity ;//40
                        $per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;//10
                        $p_qty = $per_strip_piece * $s_qty;
                    }else{
                        $s_qty = 0;
                        $p_qty = 0;
                    }
                    if(!empty($update_data->box_qty)){
                        $per_box_strip = $update_data->strip_qty / $update_data->box_qty ;//15
                        ($s_qty <= 0 ) ? $b_qty = 0 :  $b_qty =  $s_qty / $per_box_strip;
                    }else{
                        $b_qty = 0 ;
                    }
                    if(!empty($update_data->carton_qty)){
                        $per_ctn_box = $update_data->box_qty /  $update_data->carton_qty;//3
                        ($b_qty <= 0 ) ? $c_qty = 0 : $c_qty = $b_qty / $per_ctn_box;
                    }else{
                        $c_qty = 0 ;
                    }
                    
                

                }elseif ($product_type == 'Box') {
                    if(!empty($update_data->box_qty)){
                        $b_qty =  $update_data->box_qty - $quantity ;
                        $per_box_strip = $update_data->strip_qty / $update_data->box_qty ;
                        ($b_qty <= 0 ) ? $s_qty = 0 : $s_qty =  $per_box_strip * $b_qty;
                    }else{
                        $s_qty = 0;
                        $b_qty = 0 ;
                    }
                    if(!empty($update_data->strip_qty)){
                        $per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;
                        ($s_qty <= 0 ) ? $p_qty = 0 :  $p_qty = $per_strip_piece * $s_qty;
                    }else{
                        $p_qty = 0;
                    }
                    if(!empty($update_data->carton_qty)){
                        $per_ctn_box = $update_data->box_qty /  $update_data->carton_qty;
                        ($b_qty <= 0 ) ? $c_qty = 0 : $c_qty = $b_qty / $per_ctn_box;
                    }else{
                        $c_qty = 0;                             
                    }
                    
                    
                }else{
                    if(!empty($update_data->carton_qty)){
                        $c_qty =  $update_data->carton_qty - $quantity ;
                        $per_ctn_box = $update_data->box_qty /  $update_data->carton_qty;
                        ($c_qty <= 0 ) ? $b_qty = 0 : $b_qty = $per_ctn_box * $c_qty;
                    }else{
                        $b_qty = 0 ;
                        $c_qty = 0 ;
                    }
                    if(!empty($update_data->box_qty)){
                        $per_box_strip = $update_data->strip_qty / $update_data->box_qty ;
                        ($b_qty <= 0 ) ? $s_qty = 0 : $s_qty =  $per_box_strip * $b_qty;
                    }else{
                        $s_qty = 0;
                    }  
                    if(!empty($update_data->strip_qty)){ 
                        $per_strip_piece = $update_data->piece_qty / $update_data->strip_qty;
                        ($s_qty <= 0 ) ? $p_qty = 0 :  $p_qty = $per_strip_piece * $s_qty;
                    }else{
                        $p_qty = 0;
                    }
                   
                }


                $data[]        = [
                    'invoice_id'  => $invoice_id,
                    'product_name'=>$product_name,
                    'product_type'=>$product_type,
                    'quantity'    =>$quantity,
                    'price'       =>$price,
                    'created_by'  =>$created_by,
                ];

                array_push($id,$update_data->id);
                $update_old_data[] = [        
                    'id' =>$update_data->id,        
                    'carton_qty' => $c_qty,
                    'box_qty' => $b_qty,
                    'strip_qty' => $s_qty,
                    'piece_qty' => $p_qty,
                ];
                
            }
            // echo '<pre>';print_r($id);echo '<br/>';
            // echo '<pre>';print_r($update_old_data);echo '<br/>';
            // exit;


            if(InvoiceItem::insert($data)){
                    foreach($update_old_data as $value){
                        DB::update("update branch_product_distributions SET carton_qty=?,box_qty=?,strip_qty=?,piece_qty=? WHERE id=? AND branch_id=?",[$value['carton_qty'],$value['box_qty'],$value['strip_qty'],$value['piece_qty'],$value['id'],$branch_id]);
                        // $update_product = DB::table('branch_product_distributions')->whereIn('id',[2,3])->update($update_old_data);
                    }
                    return redirect()->back()->with('success','Invoice has been saved successfully');
                    // $invoice_data = DB::table('invoices')->where('id',$invoice_id)->first();
                    // $item_data    = DB::table('invoice_items')->where('invoice_id',$invoice_id)->get();
                    // $pdf = PDF::loadView('admin.invoice.invoice-print',compact('invoice_data'), compact('item_data'));
                    // $file_name = "selling_invoice".$invoice_id.".pdf";
                    // //$pdf->save(public_path().'/'.$file_name); //save file in public folder
                    // return $pdf->setPaper('a4')->stream($file_name, array( 'Attachment'=>1 ));

                    
            }else{
                return redirect()->back()->with('error','Invoice Item can\'t save');
            }
        }else{
            return redirect()->back()->with('error','Invoice can\'t save');
        }

    }

    public function manageInvoice()
    {
        $branch_id = Session::get('branch_id');
		if($branch_id == 1){
             $invoice_data = Invoice::where('invoice_type',1)->orderBy('id','desc')->get();
        }else{
            $invoice_data = Invoice::where('invoice_type',1)
                                    ->where('branch_id',$branch_id)
                                    ->orderBy('id','desc')
                                    ->get();
        }
        return view('admin.invoice.invoice-manage',compact('invoice_data'));
    }

    public function viewInvoice($id) {
        $branch_id = Session::get('branch_id');
		if($branch_id == 1){
            $invoice_data         = Invoice::where('id',$id)->first();
            $item_data            = DB::table('invoice_items')->where('invoice_id',$id)->get();
            return view('admin.invoice.invoice-view')
                    ->with('invoice_data',$invoice_data)
                    ->with('item_data',$item_data);
        }else{
            $invoice_data         = Invoice::where('id',$id)->where('branch_id',$branch_id)->first();
            if(empty($invoice_data )){
                return redirect('/dashboard');
            }else{
                $item_data            = DB::table('invoice_items')->where('invoice_id',$id)->get();
                return view('admin.invoice.invoice-view')
                        ->with('invoice_data',$invoice_data)
                        ->with('item_data',$item_data);
            }
            
        }
    }
    public function printInvoice($id) {
        $invoice_data         = Invoice::where('id',$id)->first();
        $item_data            = DB::table('invoice_items')->where('invoice_id',$id)->get();
        $pdf = PDF::loadView('admin.invoice.invoice-print',compact('invoice_data'), compact('item_data'));
        $file_name = "buying_invoice_".$id.".pdf";
        //$pdf->save(public_path().'/'.$file_name); //save file in public folder
        return $pdf->setPaper('a4')->stream($file_name, array( 'Attachment'=>1 ));
    }

    public function deleteInvoice($id) {
        if($this->user_role->role_id == 1 ){
            $delete_invoice_item = InvoiceItem::where('invoice_id',$id)->delete();
            if($delete_invoice_item){
                $delete_invoice = Invoice::find($id);
                $delete_invoice->delete();
                return redirect()->back()->with('success','Invoice Deleted Successfully');
            }else{
                return redirect()->back()->with('error','Invoice Can\'t Deleted ');
            }          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function createInvoice()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $last_id = DB::select('SELECT id FROM invoices ORDER BY id DESC LIMIT 1');
            (count($last_id) > 0)  ? $num = $last_id[0]->id : $num = 0;
            $invoice_number = 100 + $num;
            date_default_timezone_set('Asia/Dhaka');
            $issue_date = date('Y-m-d h:i:s A');
    
            $suppliers = DB::table('suppliers')                                   
                        ->join('companies','suppliers.company_id','=','companies.id')                  
                        ->select('suppliers.*','companies.company_name')
                        ->get();
            $output = '';
            if(count($suppliers) > 0){
                foreach($suppliers as $supplier_value){
                    $output .= '<option value="'.$supplier_value->id.'">'.$supplier_value->supplier_name. '<span>  ('.$supplier_value->company_name.')</span></option>';
                }
            }
            
            return view('admin.invoice.buying-invoice-add', compact('output','issue_date','invoice_number'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function storeBuyingInvoice(Request $request)
    {
        $this->validate($request, [
            'supplier_id'    => 'required',
        ]);
        $created_by = $request->created_by;
        $item_name  = $request->item_name;
        $item_type  = $request->item_type;
        $item_qty   = $request->item_qty;
        $item_price = $request->item_price;
        $data = array();
        $buying_invoice = BUYING_INVOICE;
        date_default_timezone_set('Asia/Dhaka');
        if(($request->total > $request->paid) && ($request->paid > 0)){
            $date = date('Y-m-d H:i:s');
            $invoice_status = DUE;
        }elseif ($request->total == $request->paid) {
            $date = $this->dateFormat($request->issue_date);
            $invoice_status = PAID;
        }else{
            $date = NULL;
            $invoice_status = DUE;
        }
        if(empty($request->paid)){
            $paid = 0;
        }else{
            $paid = $request->paid;
        }
        $invoice = new Invoice;
        $invoice->invoice_number = $request->invoice_number;
        $invoice->supplier_id    = $request->supplier_id;
        $invoice->issue_date     = $this->dateFormat($request->issue_date);
        $invoice->paid_date      = $date;
        $invoice->total_amount   = $request->total;
        $invoice->paid_amount    = $paid;
        $invoice->due_amount     = $request->outstanding;
        $invoice->invoice_type   = $buying_invoice;
        $invoice->invoice_status = $invoice_status;
        $invoice->created_by     = $created_by;
        // echo '<pre>';
        // print_r($invoice->toArray());
        // exit();
        if($invoice->save()){
            $invoice_id = $invoice->id;
            for($count = 0; $count<count($item_name); $count++){
            
                $product_name  = $item_name[$count];
                $product_type  = $item_type[$count];
                $quantity   = $item_qty[$count];
                $price = $item_price[$count];
                $data[] = [
                    'invoice_id'=> $invoice_id,
                    'product_name'=>$product_name,
                    'product_type'=>$product_type,
                    'quantity'=>$quantity,
                    'price'=>$price,
                    'created_by'=>$created_by,
                ];
                
            }
            $payment_data = [
                'invoice_id'=> $invoice_id,
                'amount'=> $paid,
                'payment_date'=> $date,
                'created_by'=> $created_by,
            ];
            if(InvoiceItem::insert($data)){
                if($paid != 0){
                    InvoicePayment::insert($payment_data);
                }
                session()->flash('message','Invoice Saved Successfully');
                return redirect('/buying-invoice/add/');
            }
           
        }
    }

    public function manageBuyingInvoice()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $invoice_data = Invoice::where('invoice_type',2)->orderBy('id','desc')->get();
            return view('admin.invoice.buying-invoice-manage',compact('invoice_data'));
            // echo '<pre>';
            // print_r(json_encode($invocie_data));
            // exit;          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function viewBuyingInvoice($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $invoice_data         = Invoice::where('id',$id)->first();
            $company_id           = $invoice_data->supplier->company_id;
            $company_name         = DB::table('companies')->where('id',$company_id)->first();
            // echo $company_name->company_name ;exit;
            $item_data            = DB::table('invoice_items')->where('invoice_id',$id)->get();
            // echo '<pre>';print_r($item_data->toArray());exit;
            $invoice_payment_data = DB::table('invoice_payments')->where('invoice_id',$id)->get();
            return view('admin.invoice.buying-invoice-view')
                        ->with('invoice_data',$invoice_data)
                        ->with('company_name',$company_name)
                        ->with('item_data',$item_data)
                        ->with('invoice_payment_data',$invoice_payment_data);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
 
    }
    public function printBuyingInvoice($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $invoice_data         = Invoice::where('id',$id)->first();
            $company_id           = $invoice_data->supplier->company_id;
            $item_data            = DB::table('invoice_items')->where('invoice_id',$id)->get();
            $pdf = PDF::loadView('admin.invoice.buying-invoice-print',compact('invoice_data'), compact('item_data'));
            $file_name = "buying_invoice_".$id.".pdf";
            //$pdf->save(public_path().'/'.$file_name); //save file in public folder
            return $pdf->setPaper('a4')->stream($file_name, array( 'Attachment'=>1 ));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function updatePayment(Request $request)
    {
        // echo '<pre>';print_r($request->all());exit;
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $id           = $request->id;
            $total_amount = $request->total_amount;
            $paid_amount  = $request->paid_amount;
            $due_amount   = $request->due_amount;
            if($request->amount != ''){
                $amount   = $request->amount;
            }else{
                $amount   = 0;
            }
            $created_by   = $request->created_by;
            $total_amount_paid = $paid_amount + $amount;
            $outstandng = $total_amount - $total_amount_paid;
            if($outstandng > 0){
                $invoice_status = 2;
            }else{
                $invoice_status = 1;
            }

            $update_invoice = Invoice::find($id);
            // echo '<pre>';print_r($update_invoice);exit;
            $update_invoice->paid_amount    = $total_amount_paid;
            $update_invoice->due_amount     = $outstandng;
            $update_invoice->invoice_status = $invoice_status;
            $update_invoice->paid_date      = $request->date;
            $update_invoice->updated_at      = date('Y-m-d H:i:s');
            if($update_invoice->save()){
                $payment_data = new InvoicePayment;
                $payment_data->invoice_id   = $id;
                $payment_data->amount       = $amount;
                $payment_data->payment_date = $request->date;
                $payment_data->created_by   = $created_by;
                
                if($payment_data->save()){
                    return redirect()->back()->with('success','Payment added successfully');
                }else{
                    return redirect()->back()->with('error','Payment can\'t add');
                }
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }

    }
    public function deleteBuyingInvoice($id)
    {   
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            // $delete_invoice_item = DB::delete('delete from invoice_items where invoice_id=?',[$id]);
            $delete_invoice_item = InvoiceItem::where('invoice_id',$id)->delete();
            $delete_invoice_payment = InvoicePayment::where('invoice_id',$id)->delete();
            if($delete_invoice_item && $delete_invoice_payment){
                $delete_invoice = Invoice::find($id);
                $delete_invoice->delete();
                return redirect('/buying-invoice/manage')->with('success','Invoice Deleted Successfully');
            }else{
                return redirect('/buying-invoice/manage')->with('error','Invoice Can\'t Deleted ');
            }          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        

    }

}
