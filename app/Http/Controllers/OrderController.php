<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use DB;
use App\Payment;
use App\orderDetail;
use App\Shipping;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }

    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $order_data = DB::table('orders')->join('customers','orders.customer_id','=','customers.id')
                                        ->join('payments','orders.payment_id','=','payments.id')
                                        ->select('orders.*','customers.firstname','customers.lastname',
                                        'customers.email','customers.phone_num_one','payments.payment_type','payments.payment_status')
                                        ->orderBy('orders.id','desc')
                                        ->get();
            return view('admin.orders.order-manage',compact('order_data'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function orderDetails($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $shipping_id =  DB::table('orders')->where('id',$id)->first();
            $shipping_info = DB::table('shippings')->where('id',$shipping_id->shipping_id)->first();

            $order_details_data = DB::table('orders')->join('customers','orders.customer_id','=','customers.id')
                                ->select('orders.*','customers.firstname','customers.lastname','customers.email',
                                'customers.phone_num_one','customers.phone_num_two','customers.address','customers.address_two','customers.additional_information')
                                ->where('orders.id',$id)
                                ->first();
            $ordered_products = DB::table('order_details')
                                ->join('ecommerce_products','order_details.product_id','=','ecommerce_products.id')
                                ->select('order_details.*','ecommerce_products.product_image','ecommerce_products.weight')
                                ->where('order_details.order_id',$id)
                                ->get();
            return view('admin.orders.order-details')
                            ->with('shipping_info',$shipping_info)
                            ->with('order_details_data',$order_details_data)
                            ->with('ordered_products',$ordered_products);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function orderStatusDone($id){
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $order_status = Order::find($id);
            $order_status->order_status = "done";
            if($order_status->save()){
                return redirect()->back()->with('success','Order status updated successfully');
            }else{
                return redirect()->back()->with('error','Order status can\'t update');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    public function orderPaymentDone($id){
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $order_data = Order::find($id);
            $payment_id = $order_data->payment_id;
            $payment_status = Payment::find($payment_id);        
            $payment_status->payment_status = "paid";
            $payment_status->payment_date   = date('Y-m-d');
            if($payment_status->save()){
                return redirect()->back()->with('success','Order Payment status updated successfully');
            }else{
                return redirect()->back()->with('error','Order Payment status can\'t update');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function orderDelete($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_order_details_data = DB::table('order_details')->where('order_id',$id)->delete();
            if($delete_order_details_data){
                $delete_order_data = Order::find($id);
                $payment_id = $delete_order_data->payment_id;
                $shipping_id = $delete_order_data->shipping_id;
                if($delete_order_data->delete()){
                    $delete_payment_data = Payment::find($payment_id);
                    $delete_payment_data->delete();
                    $delete_shipping_data = Shipping::find($shipping_id);                
                    $delete_shipping_data->delete();
                    return redirect()->back()->with('success','Order data deleted successfully');
                }else{
                    return redirect()->back()->with('error','Order data can\'t delete');
                }
            }else{
                return redirect()->back()->with('error','Order data can\'t delete');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
        
    }

}
