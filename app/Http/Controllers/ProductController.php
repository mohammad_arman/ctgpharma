<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EcommerceProduct;
use App\BranchProduct;
use App\EcommerceCategory;
use DB;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }
    
    public function index()
    {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            //	    $branch_id = session()->get('branch_id');
			//	    echo $this->branch_id;exit;
			$category = EcommerceCategory::where('publication_status',1)->get();
			$supplier = DB::table('suppliers')                                   
									->join('companies','suppliers.company_id','=','companies.id')                                
									->select('suppliers.*','companies.company_name')
									->get();
			return view('admin.ecommerce-product.ecommerce-product-add')
					->with('category', $category)
					->with('supplier', $supplier);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }


    }
    public function dateFormat($date){
        return date('Y-m-d',strtotime($date));
    }
    
    public function storeProduct(Request $request) {
     
	$product = new BranchProduct();
	$ecommerceProduct = new EcommerceProduct();
	if($request->stock == 'Branch'){
		$product_name = $request->product_name;
		$product_exist = BranchProduct::where('product_name',$product_name)->first();
		// echo '<pre>';print_r($product_exist->toArray());exit;
		if(count($product_exist) > 0){
			if(!empty($request->carton_qty)){
				$product_exist->carton_qty                  = $request->carton_qty + $product_exist->carton_qty;
			}
			if(!empty($request->box_qty)){
				$product_exist->box_qty                     = $request->box_qty + $product_exist->box_qty;
			}
			if(!empty($request->strip_qty)){
				$product_exist->strip_qty                   = $request->strip_qty + $product_exist->strip_qty ;
			}

			$product_exist->piece_qty                   = $request->piece_qty + $product_exist->piece_qty ;

			$product_exist->purchase_rate               = $request->purchase_rate;
			$product_exist->sale_rate                   = $request->sale_rate;

			$product_exist->carton_sales_rate           = $request->carton_sales_rate;
			$product_exist->box_sales_rate              = $request->box_sales_rate;
			$product_exist->strip_sales_rate            = $request->strip_sales_rate;
			$product_exist->piece_sales_rate            = $request->piece_sales_rate;
			
			$product_exist->manufacturing_date          = $this->dateFormat($request->manufacturing_date);
			$product_exist->expiry_date                 = $this->dateFormat($request->expiry_date);
			$product_exist->rack_number                 = $request->rack_number;
			$product_exist->reorder_level               = $request->reorder_level;
			$product_exist->created_by                  = $request->created_by;
			$product_exist->save();
			return redirect()->back()->with('message','Product Has Been Saved.');
		}else{
			$product->product_name                = $request->product_name;
			$product->supplier_id                 = $request->supplier_id; 

			$product->carton_qty                  = $request->carton_qty;
			$product->box_qty                     = $request->box_qty;
			$product->strip_qty                   = $request->strip_qty;
			$product->piece_qty                   = $request->piece_qty;

			$product->purchase_rate               = $request->purchase_rate;
			$product->sale_rate                   = $request->sale_rate;

			$product->carton_sales_rate           = $request->carton_sales_rate;
			$product->box_sales_rate              = $request->box_sales_rate;
			$product->strip_sales_rate            = $request->strip_sales_rate;
			$product->piece_sales_rate            = $request->piece_sales_rate;

			$product->weight                      = $request->weight;
			$product->manufacturing_date          = $this->dateFormat($request->manufacturing_date);
			$product->expiry_date                 = $this->dateFormat($request->expiry_date);
			$product->rack_number                 = $request->rack_number;
			$product->reorder_level               = $request->reorder_level;
			$product->created_by                  = $request->created_by;
			$product->save();
			session()->flash('message','Product Has Been Saved.');
			return redirect('/product/add');
		}
	    
	
	    
	} elseif ($request->stock == 'Ecommerce') {
	    $product_name = $request->product_name;
		$product_exist = EcommerceProduct::where('product_name',$product_name)->first();
		// echo '<pre>';print_r($product_exist->toArray());exit;
		if(count($product_exist) > 0){
			if(!empty($request->carton_qty)){
				$product_exist->carton_qty                  = $request->carton_qty + $product_exist->carton_qty;
			}
			if(!empty($request->box_qty)){
				$product_exist->box_qty                     = $request->box_qty + $product_exist->box_qty;
			}
			if(!empty($request->strip_qty)){
				$product_exist->strip_qty                   = $request->strip_qty + $product_exist->strip_qty ;
			}

			$product_exist->piece_qty                   = $request->piece_qty + $product_exist->piece_qty ;
				
				$product_exist->purchase_rate               = $request->purchase_rate;
				$product_exist->sale_rate                   = $request->sale_rate;
				
				$product_exist->carton_sales_rate           = $request->carton_sales_rate;
				$product_exist->box_sales_rate              = $request->box_sales_rate;
				$product_exist->strip_sales_rate            = $request->strip_sales_rate;
				$product_exist->piece_sales_rate            = $request->piece_sales_rate;
				
				$product_exist->discount                    = $request->discount;
				$product_exist->manufacturing_date          = $this->dateFormat($request->manufacturing_date);
				$product_exist->expiry_date                 = $this->dateFormat($request->expiry_date);
				$product_exist->publication_status          = $request->publication_status;
				$product_exist->feature_product             = $request->feature_product;
				$product_exist->created_by                  = $request->created_by;
	
				$product_exist->save();
				return redirect()->back()->with('message','Product Has Been Saved.');
		}else{
			$product_image = $request->file('product_image');
	    
			if($product_image){
	
			$image_name              = $product_image->getClientOriginalName();
			$div                     = explode('.', $image_name);
			$base_name               = substr($image_name, 0, strrpos($image_name, "."));
			$file_ext                = strtolower(end($div));
			//$image_name              = $product_image->getClientOriginalName().substr(md5(time()), 0, 10);
			//$ext                     = strtolower($product_image->getClientOriginalExtension());
			$unique_name             = $base_name.substr(md5(time()), 0, 10);
			$product_image_full_name = $unique_name.'.'.$file_ext;
			$upload_path             = 'public/admin-frontend-assets/product-image/';
			//$image_url               = $upload_path.$product_image_full_name;
			$success                 = $product_image->move($upload_path,$product_image_full_name);
			if($success){
				
				$ecommerceProduct->product_name                = $request->product_name;
				$ecommerceProduct->product_image               = $product_image_full_name;
				$ecommerceProduct->category_id                 = $request->category_id;
				$ecommerceProduct->supplier_id                 = $request->supplier_id;  
				
				$ecommerceProduct->carton_qty                  = $request->carton_qty;
				$ecommerceProduct->box_qty                     = $request->box_qty;
				$ecommerceProduct->strip_qty                   = $request->strip_qty;
				$ecommerceProduct->piece_qty                   = $request->piece_qty;
				
				$ecommerceProduct->purchase_rate               = $request->purchase_rate;
				$ecommerceProduct->sale_rate                   = $request->sale_rate;
				
				$ecommerceProduct->carton_sales_rate           = $request->carton_sales_rate;
				$ecommerceProduct->box_sales_rate              = $request->box_sales_rate;
				$ecommerceProduct->strip_sales_rate            = $request->strip_sales_rate;
				$ecommerceProduct->piece_sales_rate            = $request->piece_sales_rate;
				
				$ecommerceProduct->discount                    = $request->discount;
				$ecommerceProduct->weight                      = $request->weight;
				$ecommerceProduct->manufacturing_date          = $this->dateFormat($request->manufacturing_date);
				$ecommerceProduct->expiry_date                 = $this->dateFormat($request->expiry_date);
				$ecommerceProduct->description                 = $request->description;
				$ecommerceProduct->rack_number                 = $request->rack_number;
				$ecommerceProduct->reorder_level               = $request->reorder_level;
				$ecommerceProduct->publication_status          = $request->publication_status;
				$ecommerceProduct->feature_product             = $request->feature_product;
				$ecommerceProduct->created_by                  = $request->created_by;
	
				$ecommerceProduct->save();
				session()->flash('message','Product Has Been Saved.');
				return redirect('/product/add');
	
			}
			}
		}
	    
	}

   
    } 
   
    public function manageBranchProduct() {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $product_info = BranchProduct::orderBy('id', 'desc')->get();	
    		return view('admin.branch-product.branch-product-manage', compact('product_info'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function manageEcommerceProduct() {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
			$product_info = DB::table('ecommerce_products')
						->join('ecommerce_categories','ecommerce_products.category_id','=','ecommerce_categories.id')
						->join('suppliers','ecommerce_products.supplier_id','=','suppliers.id')
						->select('ecommerce_products.*','ecommerce_categories.category_name','suppliers.company_id')
						->orderBy('ecommerce_products.id','desc')
						->get();
			return view('admin.ecommerce-product.ecommerce-product-manage', compact('product_info'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    
    public function editBranchProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
			$category = EcommerceCategory::where('publication_status',1)->get();
			$supplier = DB::table('suppliers')                                   
									->join('companies','suppliers.company_id','=','companies.id')                                
									->select('suppliers.*','companies.company_name')
									->get();
									
			$product_info = DB::table('branch_products')
							->join('suppliers','branch_products.supplier_id','=','suppliers.id')
							->select('branch_products.*','suppliers.supplier_name','suppliers.company_id')
			   ->where('branch_products.id',$id)
							->first();
							
			return view('admin.branch-product.branch-product-edit')
					->with('category', $category)
					->with('supplier', $supplier)
					->with('product_info', $product_info);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
		
    }
    
    public function updateBranchProduct(Request $request) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            return view('admin.branch.branch-add');           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
		date_default_timezone_set('Asia/Dhaka');
		// echo $request->purchase_rate;exit();
		// echo '<pre>';print_r($request->all());exit();
        $this->validate($request, [
            'product_name' => 'required',
            'supplier_id' => 'required',
            'piece_qty' => 'required|numeric|min:1',
            'purchase_rate' => 'required|numeric|min:1',
            'sale_rate' => 'required|numeric|min:1',
            'piece_sales_rate' => 'required|numeric|min:1',
            'weight' => 'required',
            'manufacturing_date' => 'required',
            'expiry_date' => 'required',
            'rack_number' => 'required',
            'reorder_level' => 'required',
		]);
		$id = $request->id;
		// echo $id;
		$product = BranchProduct::find($id);
		$product->product_name                = $request->product_name;
		$product->supplier_id                 = $request->supplier_id; 

		$product->carton_qty                  = $request->carton_qty;
		$product->box_qty                     = $request->box_qty;
		$product->strip_qty                   = $request->strip_qty;
		$product->piece_qty                   = $request->piece_qty;

		$product->purchase_rate               = $request->purchase_rate;
		$product->sale_rate                   = $request->sale_rate;

		$product->carton_sales_rate           = $request->carton_sales_rate;
		$product->box_sales_rate              = $request->box_sales_rate;
		$product->strip_sales_rate            = $request->strip_sales_rate;
		$product->piece_sales_rate            = $request->piece_sales_rate;

		$product->weight                      = $request->weight;
		$product->manufacturing_date          = $this->dateFormat($request->manufacturing_date);
		$product->expiry_date                 = $this->dateFormat($request->expiry_date);
		$product->rack_number                 = $request->rack_number;
		$product->reorder_level               = $request->reorder_level;
		$product->updated_at                  = date('Y-m-d H:i:s');
		$product->save();
		session()->flash('message','Product Has Been Saved.');
		return redirect()->back();
    }
    
    public function viewBranchProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
           		// $product_info = BranchProduct::where('id',$id)->first();
	
				$product_info = DB::table('branch_products')
				->join('suppliers','branch_products.supplier_id','=','suppliers.id')
				->select('branch_products.*','suppliers.supplier_name','suppliers.company_id')
				->where('branch_products.id',$id)
				->first();
			return view('admin.branch-product.branch-product-view', compact('product_info'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
	

    }
    
    public function deleteBranchProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_product = BranchProduct::find($id);
			$delete_product->delete();
			return redirect('/branch-product/manage')->with('message','Product Deleted Successfully');        
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    public function editEcommerceProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
			$product_data = EcommerceProduct::where('id',$id)->first();
			$category = EcommerceCategory::where('publication_status',1)->get();
            $supplier = DB::table('suppliers')                                   
                                    ->join('companies','suppliers.company_id','=','companies.id')                                
                                    ->select('suppliers.*','companies.company_name')
                                    ->get();
            return view('admin.ecommerce-product.ecommerce-product-edit')
                    ->with('product_data', $product_data)
                    ->with('category', $category)
                    ->with('supplier', $supplier);
			// echo '<pre>';print_r($product_data);exit;			           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
																				
    }
    
    public function updateEcommerceProduct(Request $request) {
		date_default_timezone_set('Asia/Dhaka');
		// echo $request->purchase_rate;exit();
		// echo '<pre>';print_r($request->all());exit();
        $this->validate($request, [
            'product_name' => 'required',
            'category_id' => 'required',
            'supplier_id' => 'required',
            'piece_qty' => 'required',
            'purchase_rate' => 'required',
            'sale_rate' => 'required',
            'piece_sales_rate' => 'required',
            'weight' => 'required',
            'manufacturing_date' => 'required',
            'expiry_date' => 'required',
            'rack_number' => 'required',
            'reorder_level' => 'required',
            'description' => 'required',
            'product_image' => 'mimes:jpeg,bmp,png',
		]);
		$id = $request->id;
		// echo $id;
		$product_data = EcommerceProduct::find($id);
		// echo '<pre>';print_r($update_product_data);exit();
		$old_image = $product_data->product_image;

		
		$product_data->product_name                = $request->product_name;
		if(empty($request->file('product_image'))){
            $product_data->product_image = $old_image;
        }else{
            $image                   = $request->file('product_image');
            $image_name              = $image->getClientOriginalName();
            $div                     = explode('.', $image_name);
            $base_name               = substr($image_name, 0, strrpos($image_name, "."));
            $file_ext                = strtolower(end($div));
            $unique_name             = $base_name.substr(time(), 0, 10);
            $image_full_name         = $unique_name.'.'.$file_ext;
            $upload_path             = 'public/admin-frontend-assets/product-image/';
            $product_data->product_image    = $image_full_name;
            
		}
		$product_data->category_id                 = $request->category_id;
		$product_data->supplier_id                 = $request->supplier_id;  
		
		$product_data->carton_qty                  = $request->carton_qty;
		$product_data->box_qty                     = $request->box_qty;
		$product_data->strip_qty                   = $request->strip_qty;
		$product_data->piece_qty                   = $request->piece_qty;
		
		$product_data->purchase_rate               = $request->purchase_rate;
		$product_data->sale_rate                   = $request->sale_rate;
		
		$product_data->carton_sales_rate           = $request->carton_sales_rate;
		$product_data->box_sales_rate              = $request->box_sales_rate;
		$product_data->strip_sales_rate            = $request->strip_sales_rate;
		$product_data->piece_sales_rate            = $request->piece_sales_rate;
		
		$product_data->discount                    = $request->discount;
		$product_data->weight                      = $request->weight;
		$product_data->manufacturing_date          = $this->dateFormat($request->manufacturing_date);
		$product_data->expiry_date                 = $this->dateFormat($request->expiry_date);
		$product_data->description                 = $request->description;
		$product_data->rack_number                 = $request->rack_number;
		$product_data->reorder_level               = $request->reorder_level;
		$product_data->updated_at                  = date('Y-m-d H:i:s');
		// echo '<pre>';print_r($product_data);exit();
		if($product_data->save()){
            
            if(!empty($request->file('product_image'))){
                $image->move($upload_path,$image_full_name);
                if($old_image != ''){
                    $remove_image = 'public/admin-frontend-assets/product-image/'.$old_image;
                    @unlink($remove_image);
                }
			}
			return redirect()->back()->with('success','Data Updated Successfully');
		}else{
            return redirect()->back()->with('error','Data Can\'t Update ');
        }
    }
    
    public function viewEcommerceProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $product_info = DB::table('ecommerce_products')
                            ->join('ecommerce_categories','ecommerce_products.category_id','=','ecommerce_categories.id')
                            ->join('suppliers','ecommerce_products.supplier_id','=','suppliers.id')
                            ->select('ecommerce_products.*','ecommerce_categories.category_name','suppliers.supplier_name','suppliers.company_id')
	           ->where('ecommerce_products.id',$id)
                            ->first();
			return view('admin.ecommerce-product.ecommerce-product-view', compact('product_info'));        
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
	
    }
    
    public function deleteEcommerceProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_product = EcommerceProduct::find($id);
			$delete_product->delete();
			return redirect('/ecommerce-product/manage')->with('message','Product Deleted Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function unpublishedProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
			$unpublish_product = EcommerceProduct::find($id);
			$unpublish_product->publication_status = 0;
			$unpublish_product->save();
			return redirect('/ecommerce-product/manage')->with('message','Product Unpublished Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
		
    }
    
    public function publishedProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
			$publish_product = EcommerceProduct::find($id);
			$publish_product->publication_status = 1;
			$publish_product->save();
			return redirect('/ecommerce-product/manage')->with('message','Product Published Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
		
    }
    
    public function featuredProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $feature_product = EcommerceProduct::find($id);
			$feature_product->feature_product = 1;
			$feature_product->save();
			return redirect('/ecommerce-product/manage')->with('message','Product Made Featured Product Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
		
    }
    
    public function unfeaturedProduct($id) {
		if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
			$unfeature_product = EcommerceProduct::find($id);
			$unfeature_product->feature_product = 0;
			$unfeature_product->save();
			return redirect('/ecommerce-product/manage')->with('message','Product Made Unfeatured Product Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
		
    }
    
}
