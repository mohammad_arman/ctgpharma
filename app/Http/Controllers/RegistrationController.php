<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Branch;
use App\Role;
use DB;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }
    
    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $branch = Branch::all();
            $role = Role::all();
            return view('admin/registration/registration')->with('branch', $branch)->with('role', $role);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }


    public function saveEmployee(Request $request)
    {
	
        $user_data = new User();
	    $data = array();
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'branch_id' => 'required',
            'role_id' => 'required',
        ]);
//	print_r($request->_token);exit;
        $user_data->name     = $request->name;
        $user_data->email    = $request->email;
        $user_data->password = bcrypt($request->password);
        $user_data->remember_token = $request->_token;
        $data['branch_id'] = $request->branch_id;
        $data['role_id'] = $request->role_id;
        if($user_data->save()){
            $data['user_id'] = $user_data->id;
            
            DB::table('role_user')->insert($data);
            session()->flash('message','Employee Data Inserted Successfully');
            return redirect('/add-employee');
        }
        
    }

    public function manageEmployee()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $employees_data = DB::table('role_user')
                    ->join('users','role_user.user_id','=','users.id')
                    ->join('roles','role_user.role_id','=','roles.id')
                    ->join('branches','role_user.branch_id','=','branches.id')
                    ->select('role_user.*','users.name','users.email','users.image','roles.role_name','branches.branch_name')
                    // ->where('role_user.role_id','!=',1)
                    ->orderBy('role_user.role_id','asc')
                    ->get();
        // echo '<pre>';print_r($employees_data);exit;
        return view('admin.employee.employee-manage',compact('employees_data'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
    public function viewEmployee($id)
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $employees_data = DB::table('role_user')
                ->join('users','role_user.user_id','=','users.id')
                ->join('roles','role_user.role_id','=','roles.id')
                ->join('branches','role_user.branch_id','=','branches.id')
                ->select('role_user.*','users.name','users.email','users.image','roles.role_name','branches.branch_name')
                // ->where('role_user.role_id','!=',1)
                ->where('role_user.user_id',$id)
                ->first();
            // echo '<pre>';print_r($employees_data);exit;
            return view('admin.employee.employee-details',compact('employees_data'));          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function editEmployee($id)
    {
        if($this->user_role->role_id == 1){
            $branch = Branch::all();
            $role = Role::all();
            $employee_data = DB::table('role_user')
                                    ->join('users','role_user.user_id','=','users.id')
                                    ->join('roles','role_user.role_id','=','roles.id')
                                    ->join('branches','role_user.branch_id','=','branches.id')
                                    ->select('role_user.*','users.name','users.email','users.password','users.image','roles.role_name','branches.branch_name')
                                    ->where('role_user.user_id',$id)
                                    ->first();
            return view('admin.employee.employee-edit')
            ->with('branch', $branch)
            ->with('role', $role)
            ->with('employee_data', $employee_data);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

    public function updateEmployee(Request $request)
    {
        // echo '<pre>';print_r($request->all());exit;
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
            'branch_id' => 'required',
            'role_id' => 'required',
            'image' => 'mimes:jpeg,bmp,png',
        ]);
        $user_id = $request->user_id;
        $employee_data = User::where('id',$user_id)->first();
        $old_password = $employee_data->password;
        $old_image = $employee_data->image;

        $employee_data->name     = $request->name;
        $employee_data->email    = $request->email;
        if($old_password == $request->password){
            $employee_data->password = $request->password;
            
        }else{
            $employee_data->password = bcrypt($request->password);

        }
        
        if(empty($request->file('image'))){
            $employee_data->image = $old_image;
        }else{
            $image                   = $request->file('image');
            $image_name              = $image->getClientOriginalName();
            $div                     = explode('.', $image_name);
            $base_name               = substr($image_name, 0, strrpos($image_name, "."));
            $file_ext                = strtolower(end($div));
            $unique_name             = $base_name.substr(md5(time()), 0, 10);
            $image_full_name         = $unique_name.'.'.$file_ext;
            $upload_path             = 'public/admin-frontend-assets/employee_image/';
            $employee_data->image    = $image_full_name;
            
        }
        $employee_data->remember_token = $request->_token;

        $data = array();
        $data['role_id']   = $request->role_id;
        $data['branch_id'] = $request->branch_id;
        $data['user_id']   = $user_id;
        // echo '<pre>';print_r($employee_data->toArray());exit;
        
        // echo '<pre>';print_r($data);exit;
        if($employee_data->save()){
            // echo '<pre>';print_r($employee_data);exit;
            if(!empty($request->file('image'))){
                $image->move($upload_path,$image_full_name);
                if($old_image != ''){
                    $remove_image = 'public/admin-frontend-assets/employee_image/'.$old_image;
                    @unlink($remove_image);
                }
            }
            
            $update = DB::table('role_user')->where('user_id',$user_id)->update($data);
            return redirect()->back()->with('success','Data Updated Successfully');
        }else{
            return redirect()->back()->with('error','Data Can\'t Update ');
        }
    }

    public function deleteEmployee($id)
    {
        if($this->user_role->role_id == 1){
            $delete_role_user = DB::table('role_user')->where('user_id',$id)->delete();
            if($delete_role_user){
                $delete_user = User::find($id);
                if($delete_user->delete()){
                    return redirect()->back()->with('success','Data Deleted Successfully');
                }else{
                    return redirect()->back()->with('error','Data Can\'t Delete ');
                }
            }else{
                return redirect()->back()->with('error','Data Can\'t Delete ');
            }           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }

    public function viewProfile($id)
    {
        // echo $this->user->id;exit;
        if($this->user->id == $id){
            $user_data = DB::table('role_user')
                        ->join('users','role_user.user_id','=','users.id')
                        ->join('roles','role_user.role_id','=','roles.id')
                        ->join('branches','role_user.branch_id','=','branches.id')
                        ->select('role_user.*','users.name','users.email','users.image','roles.role_name','branches.branch_name')
                        // ->where('role_user.role_id','!=',1)
                        ->where('role_user.user_id',$id)
                        ->first();
            // echo '<pre>';print_r($user_data);exit;
            return view('admin.employee.profile',compact('user_data'));
        }else{
            if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
                return redirect('/dashboard');
            }else{
                return redirect('/product/manage');
            }
        }
       
    }
}
