<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Branch;
use App\Supplier;
use Illuminate\Support\Facades\Auth;
use DB;

class SupplierController extends Controller
{
    protected $user;
    protected $user_role;
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(function ($request, $next)
        {
            $this->user = Auth::user();
            $this->user_role = DB::table('role_user')->select('role_id')->where('user_id', $this->user->id)->first();           
            return $next($request);
        }); 
    }
    
    public function index()
    {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $company = Company::all();
            $branch = Branch::all();
            return view('admin.supplier.supplier-add')->with('company', $company)
                                                    ->with('branch', $branch);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function storeSupplier(Request $request) {
        $store_supplier_data = new Supplier();
        $this->validate($request, [
            'supplier_name'  => 'required',
            'email'          => 'nullable',
            'contact_number' => 'required',
            'address'        => 'required',
            'company_id'     => 'required',
            'created_by'     => 'required',
        ]);
        $store_supplier_data->supplier_name  = $request->supplier_name;
        $store_supplier_data->email          = $request->email;
        $store_supplier_data->contact_number = $request->contact_number;
        $store_supplier_data->address        = $request->address;
        $store_supplier_data->company_id     = $request->company_id;
        $store_supplier_data->created_by     = $request->created_by;
        $store_supplier_data->save();
        session()->flash('message','Supplier Data Inserted Successfully');
        return redirect('/supplier/add');
    }

    public function manageSupplier() {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $supplier_info = DB::table('suppliers')
                                    ->join('companies','suppliers.company_id','=','companies.id')
                                    ->select('suppliers.*','companies.company_name')
                                    ->latest()
                                    ->get();
            return view('admin.supplier.supplier-manage', compact('supplier_info'));           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    public function editSupplier($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $company = Company::all();
            $supplier_info_by_id = Supplier::where('id',$id)->first();
            return view('admin.supplier.supplier-edit')
                        ->with('company',$company)
                        ->with('supplier_info_by_id', $supplier_info_by_id);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }
    
    
    public function updateSupplier(Request $request) {
        $update_supplier_info = Supplier::find($request->id);
        $this->validate($request, [
            'supplier_name'  => 'required',
            'email'          => 'nullable',
            'contact_number' => 'required',
            'address'        => 'required',
            'company_id'     => 'required',
        ]);
        $update_supplier_info->supplier_name  = $request->supplier_name;
        $update_supplier_info->email          = $request->email;
        $update_supplier_info->contact_number = $request->contact_number;
        $update_supplier_info->address        = $request->address;
        $update_supplier_info->company_id     = $request->company_id;
//        $update_supplier_info->branch_id      = $request->branch_id;
        $update_supplier_info->save();
        return redirect('/supplier/manage')->with('message', 'Supplier Data Updated Successfully');
    }
    
    public function viewSupplier($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $supplier_info_by_id = Supplier::where('id',$id)->first();
            return view('admin.supplier.supplier-view')->with('supplier_info_by_id', $supplier_info_by_id);           
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
       
    }
   
    public function deleteSupplier($id) {
        if($this->user_role->role_id == 1 || $this->user_role->role_id == 2){
            $delete_supplier_info = Supplier::find($id);
            $delete_supplier_info->delete();
            return redirect('/supplier/manage')->with('message', 'Supplier Data Deleted Successfully');          
        }else{
            return redirect('/product/manage')->with('error','You are not authorized to access this page!');
        }
        
    }

}
