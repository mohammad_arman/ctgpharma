<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'invoice_number', 'receiver_name', 'mobile_number','issue_date','total_amount','paid_amount','due_amount','created_by',
    ];

    public function supplier()
    {
       return $this->belongsTo('App\Supplier','supplier_id');
    }
    public function branch()
    {
       return $this->belongsTo('App\Branch','branch_id');
    }
}
