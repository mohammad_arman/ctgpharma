<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $fillable = [
        'invoice_id', 'product_id', 'type','quantity','amount','created_by',
    ];
    
    public function invoice() {
	return $this->belongsTo('App\Invoice', 'invoice_id');
    }
    
    public function branchproductdistribution() {
	return $this->belongsTo(BranchProductDistribution::class);
    }
    
    
}
