<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function customer() {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
    public function shipping() {
        return $this->belongsTo('App\Shipping', 'shipping_id');
    }
    public function payment() {
        return $this->belongsTo('App\Payment', 'payment_id');
    }
}
