<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Company;

class Supplier extends Model
{
    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
}
