<?php
define('PUBLICATION_STATUS', json_encode(['Unpublished','Published']));
define('INVOICE_TYPE', json_encode(['','Selling Invoice','Buying Invoice']));
define('INVOICE_STATUS', json_encode(['','Paid','Due']));
define('SELLING_INVOICE', 1);
define('BUYING_INVOICE', 2);
define('PAID', 1);
define('DUE', 2);

?>
