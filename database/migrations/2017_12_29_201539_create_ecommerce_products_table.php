<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_products', function (Blueprint $table) {
            $table->increments('id');
	    // $table->string('stock',10)->unique();
            $table->string('product_name',150)->unique();
            $table->string('product_image')->nullable();
            $table->integer('category_id')->unsigned();
            $table->integer('supplier_id')->unsigned();
//            $table->string('package_name');//box

            $table->integer('carton_qty')->nullable();//12
//            $table->string('items_package_name')->nullable();//12 strip in per box
            
            $table->integer('box_qty')->nullable();//per strip sub item 10
//            $table->string('sub_items_package_name')->nullable();//number
            
            $table->integer('strip_qty');//how much item in per strip
            $table->integer('piece_qty');//how much item in per strip
            
	    $table->float('purchase_rate',8,2);
            $table->float('sale_rate',8,2);//600
	    
            $table->float('carton_sales_rate',8,2)->nullable();//600
            $table->float('box_sales_rate',8,2)->nullable();//600
            $table->float('strip_sales_rate',8,2)->nullable();//600
            $table->float('piece_sales_rate',8,2);//600
	    
	    $table->float('discount',8,2);
            $table->string('weight')->nullable();
            $table->date('manufacturing_date');
            $table->date('expiry_date');
            $table->string('rack_number');
            $table->string('reorder_level');
            $table->tinyInteger('publication_status')->nullable();
            $table->tinyInteger('feature_product')->nullable();
	    
            $table->text('description')->nullable();
            $table->string('created_by');
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('ecommerce_categories');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_products');
    }
}
