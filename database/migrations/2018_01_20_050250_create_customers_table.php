<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email',150)->unique();
	        $table->string('phone_num_one');
            $table->string('phone_num_two')->nullable();
            $table->string('password');
            $table->text('address')->nullable();
            $table->text('address_two')->nullable();
            $table->text('additional_information')->nullable();
            $table->dateTime('created_on')->nullable();
            $table->dateTime('modified_on')->nullable();
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
