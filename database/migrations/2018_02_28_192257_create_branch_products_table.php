<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name',150)->unique();
            $table->integer('supplier_id')->unsigned();

            $table->integer('carton_qty')->nullable();          
            $table->integer('box_qty')->nullable();          
            $table->integer('strip_qty');
            $table->integer('piece_qty');
            
	        $table->float('purchase_rate',8,2);
            $table->float('sale_rate',8,2);//600
	    
            $table->float('carton_sales_rate',8,2)->nullable();//600
            $table->float('box_sales_rate',8,2)->nullable();//600
            $table->float('strip_sales_rate',8,2)->nullable();//600
            $table->float('piece_sales_rate',8,2);//600
	    
            $table->string('weight')->nullable();
            $table->date('manufacturing_date');
            $table->date('expiry_date');
            $table->string('rack_number');
            $table->string('reorder_level');
            $table->string('created_by');
            $table->timestamps();
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_products');
    }
}
