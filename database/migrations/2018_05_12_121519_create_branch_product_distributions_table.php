<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchProductDistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_product_distributions', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('branch_id')->unsigned();
            $table->string('product_name',150);
            
            $table->integer('carton_qty')->nullable();          
            $table->integer('box_qty')->nullable();          
            $table->integer('strip_qty')->nullable();
            $table->integer('piece_qty');
            
	        $table->float('purchase_rate',8,2);
            $table->float('sale_rate',8,2);//600
	    
            $table->float('carton_sales_rate',8,2)->nullable();//600
            $table->float('box_sales_rate',8,2)->nullable();//600
            $table->float('strip_sales_rate',8,2)->nullable();//600
            $table->float('piece_sales_rate',8,2);//600
	    
            $table->string('weight')->nullable();
            $table->date('manufacturing_date');
            $table->date('expiry_date');
            $table->string('rack_number')->nullable();
            $table->string('reorder_level');
            $table->string('created_by');
            $table->timestamps();
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_product_distributions');
    }
}
