<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number',150)->unique();
            $table->integer('supplier_id')->nullable();
            $table->string('receiver_name',150)->nullable();
            $table->string('mobile_number',15)->nullable();
            $table->dateTime('issue_date');
            $table->date('paid_date')->nullable();
            $table->double('total_amount',8,2);
            $table->double('paid_amount',8,2);
            $table->double('due_amount',8,2);
            $table->integer('invoice_type');
            $table->integer('invoice_status');
            $table->string('created_by');
            $table->timestamps();
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
