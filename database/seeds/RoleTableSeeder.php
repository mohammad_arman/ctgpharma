<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'Admin';
        $role_employee->description = 'A Admin User';
        $role_employee->save();
        
        $role_manager = new Role();
        $role_manager->name = 'Sub-Admin';
        $role_manager->description = 'A Sub Admin User';
        $role_manager->save();
        
        $role_employee = new Role();
        $role_employee->name = 'Manager';
        $role_employee->description = 'A Manager User';
        $role_employee->save();
        
        $role_employee = new Role();
        $role_employee->name = 'Employee';
        $role_employee->description = 'A Employee User';
        $role_employee->save();
        
       
    }
}
