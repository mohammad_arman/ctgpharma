<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
     public function run()
    {
        $role_admin = Role::where('name', 'Admin')->first();
        $role_sub_admin  = Role::where('name', 'Sub-Admin')->first();
        $role_manager  = Role::where('name', 'Manager')->first();
        $role_employee  = Role::where('name', 'Employee')->first();
        
        $admin = new User();
        $admin->name = 'Mohammad Arman';
        $admin->email = 'mohammadarman.iiuc.cse@gmail.com';
        $admin->password = bcrypt('123456');
        $admin->save();
        $admin->roles()->attach($role_admin);
        
        $sub_admin = new User();
        $sub_admin->name = 'Md. Rifat Hasan';
        $sub_admin->email = 'rifathasan@gmail.com';
        $sub_admin->password = bcrypt('123456');
        $sub_admin->save();
        $sub_admin->roles()->attach($role_sub_admin);
        
        $manager = new User();
        $manager->name = 'Md. Arman';
        $manager->email = 'mohammadarman123456@gmail.com';
        $manager->password = bcrypt('123456');
        $manager->save();
        $manager->roles()->attach($role_manager);
        
        $employee = new User();
        $employee->name = 'Arman';
        $employee->email = 'mohammadarman456789@gmail.com';
        $employee->password = bcrypt('123456');
        $employee->save();
        $employee->roles()->attach($role_employee);
    }
}
