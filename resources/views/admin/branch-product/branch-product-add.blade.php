@extends('admin.master') 
@section('page_title') Distribute Branch Product
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">shopping_basket</i> Distribute Branch Product</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        DISTRIBUTE BRANCH PRODUCT
                    </h2>
                    <a href="{{url('/branch-product/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>

                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{session()->get('message')}}
                    </div>
                    @endif



                    <form method="POST" id="distribute_form" action="{{ url('/branch-product/save') }}">
                        {{ csrf_field() }}
                        <input type="hidden" id="product_id" name="product_id" value="{{$branch_product_info->id}}" class="form-control">

                        <div class="col-md-4">
                            <label for="branch_id">Branch Name <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('branch_id') ? ' has-error' : '' }}" style="z-index:8;">
                                    <select class="form-control show-tick" name="branch_id" id="branch_id" required>
                                        <option value="">Please Select</option>
                                       @foreach($branch_list as $branch)
                                        <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('branch_id'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('branch_id') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_branch_id"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="product_name">Product Name <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                    <input type="text" id="product_name" name="product_name" value="{{$branch_product_info->product_name}}" class="form-control"
                                        placeholder="Enter product name">
                                </div>
                                @if ($errors->has('product_name'))
                                <span class="help-block">
                                    <strong style="color: red;">{{ $errors->first('product_name') }}</strong>
                                    </span> @endif

                                <span style="color: red;" id="error_product_name"></span>
                            </div>
                        </div>

                        @if($branch_product_info->carton_qty != 0)
                        <div class="col-md-4" id="carton_quantity_section">
                            <label for="items_in_pack">No of Carton <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('carton_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="carton_qty" name="carton_qty" value="{{$branch_product_info->carton_qty}}" class="form-control"
                                        autocomplete="off">
                                </div>
                                @if ($errors->has('carton_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('carton_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_ctn_qty"></span>
                            </div>
                        </div>
                        @endif


                        <div style="clear: both;"></div>
                        @if($branch_product_info->box_qty != 0)
                        <div class="col-md-4" id="box_quantity_section">
                            <label for="box_qty">No of Box <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('box_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="box_qty" name="box_qty" value="{{$branch_product_info->box_qty}}" class="form-control" autocomplete="off">
                                </div>
                                @if ($errors->has('box_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('box_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_box_qty"></span>
                            </div>
                        </div>
                        @endif 
                        
                        @if($branch_product_info->strip_qty != 0)
                        <div class="col-md-4" id="strip_quantity_section">
                            <label for="strip_qty">No of Strip <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('strip_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="strip_qty" name="strip_qty" value="{{$branch_product_info->strip_qty}}" class="form-control" autocomplete="off">
                                </div>
                                @if ($errors->has('strip_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('strip_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_strip_qty"></span>

                            </div>
                        </div>
                        @endif
                        <div style="clear: both;"></div>
                        <div class="col-md-4" id="piece_quantity_section">
                            <label for="piece_qty">No of Piece <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('piece_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="piece_qty" name="piece_qty" value="{{$branch_product_info->piece_qty}}" class="form-control" autocomplete="off">
                                </div>

                                @if ($errors->has('piece_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('piece_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_piece_qty"></span>

                            </div>
                        </div>


                        <input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">
                        <br>
                        <div class="col-md-12">
                            <div class="input-group">
                                    <button type="reset" class="btn bg-blue-grey waves-effect">
                                            <i class="material-icons">cached</i>
                                            <span>RESET</span>
                                        </button>
                                <button type="submit" class="btn bg-light-blue waves-effect save-btn" style="margin-left:5px;">
                                    <i class="material-icons">save</i>
                                    <span>SAVE</span>
                                </button>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->
</div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script>
    $(document).ready(function(){
    var old_piece_qty = "{{$branch_product_info->piece_qty}}";
    var old_strip_qty = "{{$branch_product_info->strip_qty}}";
    var old_box_qty   = "{{$branch_product_info->box_qty}}";
    var old_ctn_qty   = "{{$branch_product_info->carton_qty}}";
    
    $("#branch_id").change(function(){
        if($.trim($('#branch_id').val()).length == 0){
            $("#error_branch_id").text("Branch name field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_branch_id").text("");
            $(".save-btn").prop("disabled",false);
        }
    });
    $("#product_name").change(function(){
        if($.trim($('#product_name').val()).length == 0){
            $("#error_product_name").text("Product name field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_product_name").text("");
            $(".save-btn").prop("disabled",false);
        }
    });
    $('#piece_qty').change(function(){
        var new_piece_qty = $('#piece_qty').val();
        var check_qty = old_piece_qty - new_piece_qty;
        if($.trim($('#piece_qty').val()).length == 0){
            $("#error_piece_qty").text("Piece quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else if(new_piece_qty == 0){
            $("#error_piece_qty").text("Piece quantity field can't be zero!");
            $(".save-btn").prop("disabled",true);
        }else if(new_piece_qty < 0){
            $("#error_piece_qty").text("Piece quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            // console.log(new_piece_qty+" "+old_piece_qty+" "+check_qty);
            $("#error_piece_qty").text("Piece quantity can't be greater than stock piece quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_piece_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
    });
    $('#strip_qty').change(function(){
        var new_strip_qty = $('#strip_qty').val();
        var check_qty  = old_strip_qty - new_strip_qty;
        if($.trim($('#strip_qty').val()).length == 0){
            $("#error_strip_qty").text("Strip quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else if(new_strip_qty == 0){
            $("#error_strip_qty").text("Strip quantity field can't be zero!");
            $(".save-btn").prop("disabled",true);
        }else if(new_strip_qty < 0){
            $("#error_strip_qty").text("Strip quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            $("#error_strip_qty").text("Strip quantity field can't be greater than stock strip quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_strip_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
        
    });
    $('#box_qty').change(function(){
        var new_box_qty = $('#box_qty').val();
        var check_qty  = old_box_qty - new_box_qty;
        if($.trim($('#box_qty').val()).length == 0){
            $("#error_box_qty").text("Box quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else if(new_box_qty == 0){
            $("#error_box_qty").text("Box quantity field can't be zero!");
            $(".save-btn").prop("disabled",true);
        }else if(new_box_qty < 0){
            $("#error_box_qty").text("Box quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            $("#error_box_qty").text("Box quantity can't be greater than stock bpx quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_box_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
        
    });
    $('#carton_qty').change(function(){
        var new_ctn_qty = $('#carton_qty').val();
        var check_qty = old_ctn_qty - new_ctn_qty;
        if($.trim($('#carton_qty').val()).length == 0){
            $("#error_ctn_qty").text("Carton quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else if(new_ctn_qty == 0){
            $("#error_ctn_qty").text("Carton quantity field can't be zero!");
            $(".save-btn").prop("disabled",true);
        }else if(new_ctn_qty < 0){
            $("#error_ctn_qty").text("Carton quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            $("#error_ctn_qty").text("Carton quantity can't be greater than stock carton quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_ctn_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
        
    });

    //======form submit validation code start=========
    $('.save-btn').click(function(){
        // alert('ok');
        if($.trim($('#branch_id').val()).length == 0){
            $("#error_branch_id").text("Branch name field can't left empty!");
            $(".save-btn").prop("disabled",true);
            return false;
        }

        if($.trim($('#product_name').val()).length == 0){
            $("#error_product_name").text("Product name field can't left empty!");
            $(".save-btn").prop("disabled",true);
            return false;
        }
        //piece qty check code start
        var new_piece_qty = $('#piece_qty').val();
        var check_piece_qty = old_piece_qty - new_piece_qty;
        console.log(check_piece_qty);
        if($.trim($('#piece_qty').val()).length == 0){
            // alert('piece');
            $("#error_piece_qty").text("Piece quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
            return false;
        }
        if(new_piece_qty == 0){
            $("#error_piece_qty").text("Piece quantity field can't be zero!");
            $(".save-btn").prop("disabled",true);
            return false;
        }
        if(new_piece_qty < 0){
            $("#error_piece_qty").text("Piece quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
            return false;
        }
        if(check_piece_qty < 0){
            // console.log(new_piece_qty+" "+old_piece_qty+" "+check_qty);
            $("#error_piece_qty").text("Piece quantity can't be greater than stock piece quantity!");
            $(".save-btn").prop("disabled",true);
            return false;
        }
        //piece qty check code end

        //strip qty check code start
        if(old_strip_qty != 0){
            // alert('strip');
            var new_strip_qty = $('#strip_qty').val();
            var check_strip_qty  = old_strip_qty - new_strip_qty;
            if($.trim($('#strip_qty').val()).length == 0){
                $("#error_strip_qty").text("Strip quantity field can't left empty!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(new_strip_qty == 0){
                $("#error_strip_qty").text("Strip quantity field can't be zero!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(new_strip_qty < 0){
                $("#error_strip_qty").text("Strip quantity field can't be less than zero!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(check_strip_qty < 0){
                $("#error_strip_qty").text("Strip quantity field can't be greater than stock strip quantity!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
        }
        //strip qty check code end
        
        //box qty check code start
        if(old_box_qty != 0){
            // alert('box');
            var new_box_qty = $('#box_qty').val();
            var check_box_qty  = old_box_qty - new_box_qty;
            if($.trim($('#box_qty').val()).length == 0){
                $("#error_box_qty").text("Box quantity field can't left empty!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(new_box_qty == 0){
                $("#error_box_qty").text("Box quantity field can't be zero!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(new_box_qty < 0){
                $("#error_box_qty").text("Box quantity field can't be less than zero!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(check_box_qty < 0){
                $("#error_box_qty").text("Box quantity can't be greater than stock bpx quantity!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
        }
        //box qty check code end

        //carton qty check code start
        if(old_ctn_qty != 0){
            // alert('ctn');
            var new_ctn_qty = $('#carton_qty').val();
            var check_ctn_qty = old_ctn_qty - new_ctn_qty;
            if($.trim($('#carton_qty').val()).length == 0){
                $("#error_ctn_qty").text("Carton quantity field can't left empty!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(new_ctn_qty == 0){
                $("#error_ctn_qty").text("Carton quantity field can't be zero!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(new_ctn_qty < 0){
                $("#error_ctn_qty").text("Carton quantity field can't be less than zero!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
            if(check_ctn_qty < 0){
                $("#error_ctn_qty").text("Carton quantity can't be greater than stock carton quantity!");
                $(".save-btn").prop("disabled",true);
                return false;
            }
        }   
        //carton qty check code end
        $('#distribute_form').submit();
    });
});

</script>
@endsection