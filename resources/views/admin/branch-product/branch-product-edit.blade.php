@extends('admin.master')

@section('page_title')
Edit Product
@endsection

@section('admin_main_content')
<link href="{{asset('public/admin-frontend-assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">shopping_basket</i> Edit Product</li>
    </ol>
</div>  
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EDIT PRODUCT
                    </h2>    
		    <div class="btn-group pull-right header-button">
			<button type="button" class="btn bg-brown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			     <i class="material-icons">view_list</i> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu action-menu">
			    <li><a href="{{url('/branch-product/manage/')}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Branch Product List"> Branch Product List</a></li>

			    <li><a href="{{url('/ecommerce-product/manage/')}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ecommerce Product List"> Ecommerce Product List</a></li>
			</ul>
		    </div>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
		    
		    
                    
                    <form method="POST" action="{{ url('/branch-product/update') }}" id="edit_product"  enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Basic Section</legend>
                            <div class="col-md-12">
                            <input type="hidden" name="id" value="{{$product_info->id}}" />
                                <div class="col-md-4">
                                    <label for="product_name">Product Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                    <div class="form-line{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                        <input type="text" id="product_name" name="product_name" class="form-control" value="{{$product_info->product_name}}">
                                    </div>  
                                    @if ($errors->has('product_name'))
                                        <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('product_name') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="supplier_id">Supplier Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('supplier_id') ? ' has-error' : '' }}" style="z-index:9;">                                
                                            <select class="form-control show-tick" name="supplier_id" id="supplier_id" data-live-search="true" >
                                                <option value="">Please Select</option>
                                                @foreach($supplier as $supplier_value)
                                                <option <?php if($product_info->supplier_id == $supplier_value->id){ echo 'selected'; }?> value="{{$supplier_value->id}}">{{$supplier_value->supplier_name}} <span>({{$supplier_value->company_name}})</span></option>
                                                @endforeach
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('supplier_id'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('supplier_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Package Section</legend>
                            <div class="col-md-12">
                                
                                <div class="col-md-4" id="carton_quantity_section">
                                    <label for="items_in_pack">No of Carton <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('carton_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="carton_qty" name="carton_qty" class="form-control" value="{{$product_info->carton_qty}}">
                                        </div> 
                                        @if ($errors->has('carton_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('carton_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-4" id="box_quantity_section">
                                    <label for="box_qty">No of Box <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('box_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="box_qty" name="box_qty" class="form-control" value="{{$product_info->box_qty}}">
                                        </div> 
                                        @if ($errors->has('box_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('box_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                
                    
                     <div class="col-md-4" id="strip_quantity_section">
                                    <label for="strip_qty">No of Strip <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('strip_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="strip_qty" name="strip_qty" class="form-control" value="{{$product_info->strip_qty}}">
                                        </div> 
                                        @if ($errors->has('strip_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('strip_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4" id="piece_quantity_section">
                                    <label for="piece_qty">No of Piece <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('piece_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="piece_qty" name="piece_qty"  value="{{$product_info->piece_qty}}" class="form-control">
                                        </div> 
                                        <span class="input-group-addon">
                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                        </span>
                                        @if ($errors->has('piece_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('piece_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                         </fieldset>   
			
                                         
			
			
                       
			{{-- <div style="clear: both;"></div>
                        
                        
			
			
			
			 <div style="clear: both;"></div>
			
			
			 
			  <div style="clear: both;"></div> --}}
			  
              <fieldset class="the-fieldset" id="diffuser_fieldset">
                <legend class="the-legend">Price Section</legend>
                <div class="col-md-12">    
                        <div class="col-md-4">
                            <label for="purchase_rate"> Purchase Rate <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('purchase_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="purchase_rate" value="{{$product_info->purchase_rate}}"  name="purchase_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('purchase_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('purchase_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="sale_rate">Sales Rate <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('sale_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="sale_rate" name="sale_rate" value="{{$product_info->sale_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('sale_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('sale_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
			  <div class="col-md-4" id="carton_sales_rate_section">
                            <label for="carton_sales_rate">Carton Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('carton_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="carton_sales_rate" name="carton_sales_rate" value="{{$product_info->carton_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('carton_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('carton_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="box_sales_rate_section">
                            <label for="box_sales_rate">Box Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('box_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="box_sales_rate" name="box_sales_rate" value="{{$product_info->box_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('box_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('box_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="strip_sales_rate_section">
                            <label for="strip_sales_rate">Strip Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('strip_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="strip_sales_rate" name="strip_sales_rate" value="{{$product_info->strip_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('strip_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('strip_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="piece_sales_rate_section">
                            <label for="piece_sales_rate">Piece Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('piece_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="piece_sales_rate" name="piece_sales_rate" value="{{$product_info->piece_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('piece_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('piece_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>   
                <fieldset class="the-fieldset" id="diffuser_fieldset">
                    <legend class="the-legend">Other Section</legend>
                    <div class="col-md-12">   
                    
			  
                        <div class="col-md-4">
                            <label for="weight">Weight <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('weight') ? ' has-error' : '' }}">
                                    <input type="text" id="weight" name="weight" class="form-control" value="{{$product_info->weight}}">
                                </div>  
                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="manufacturing_date">Manufacturing Date <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="manufacturing_date" id="manufacturing_date" value="{{$product_info->manufacturing_date}}">
                                </div> 
                                @if ($errors->has('manufacturing_date'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('manufacturing_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="expiry_date">Expiry Date <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="expiry_date" id="expiry_date" value="{{$product_info->expiry_date}}">
                                </div> 
                                @if ($errors->has('expiry_date'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('expiry_date') }}</strong>
                                    </span>
                                @endif
                                <span id="error_date" style="color:red;"></span>
                            </div>
                        </div>
                        
                        
                        <div class="col-md-4">
                            <label for="rack_number">Rack Number <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('rack_number') ? ' has-error' : '' }}">
                                    <input type="text" id="rack_number" name="rack_number" class="form-control" value="{{$product_info->rack_number}}">
                                </div>  
                                @if ($errors->has('rack_number'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('rack_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="reorder_level">Reorder Level <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('reorder_level') ? ' has-error' : '' }}">
                                    <input type="text" id="reorder_level" name="reorder_level" class="form-control" value="{{$product_info->reorder_level}}">
                                </div>  
                                @if ($errors->has('reorder_level'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('reorder_level') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                         
                    </div>
                </fieldset>
                        <br>
                        <div class="col-md-12">
                            <div class="input-group"> 
                                    <a href="{{url('/branch-product/manage')}}" type="submit" class="btn bg-blue-grey waves-effect">
                                        <i class="material-icons">clear</i>
                                        <span>CANCEL</span>
                                    </a>
                                <button type="submit" class="btn bg-light-blue waves-effect" id="submit-btn"  style="margin-left:5px;">
                                    <i class="material-icons">save</i>
                                    <span>UPDATE</span>
                                </button>
                            </div> 
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>                      
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->
    <script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/admin-frontend-assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#manufacturing_date').datetimepicker({
        //language:  'fr',
	 direction: "auto",
	format: "yyyy-mm-dd",
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 4
    });
    $('#expiry_date').datetimepicker({
        //language:  'fr',
	 direction: "auto",
	format: "yyyy-mm-dd",
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 4
    });
    $(document).on('click','#submit-btn',function(){
        var mfg_date = $('#manufacturing_date').val();
        var exp_date = $('#expiry_date').val();
        if(exp_date <= mfg_date){
            $('#error_date').text('Expiry date must be greater than manufacturing date!');
            return false;
        }else{
            $('#error_date').text('');
            return true;
        }
        $('#edit_product').submit();
    });
</script>
</div>

@endsection

    





