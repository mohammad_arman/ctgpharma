@extends('admin.master')

@section('page_title')
Branch Product  Manage
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Branch Product Manage</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                       BRANCH PRODUCT MANAGEMENT
                    </h2>
                    <a href="{{url('/product/add')}}">
                        <button type="button" class="btn bg-brown waves-effect pull-right header-button" >
                            <i class="material-icons">add_box</i> ADD PRODUCT
                        </button>
                        </a>
                </div>
                <div class="body" style="min-height: 400px;">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$success_message}}
                    </div>
                    @endif
                    @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$error_message}}
                    </div>
                    @endif
                    <div class="table-responsive" style="min-height: 500px;">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Product</th>
                                    <th>Carton</th>
                                    <th>Box</th>
                                    <th>Strip</th>
                                    <th>Piece</th>
                                    <th>Carton Sale Rate</th>
                                    <th>Box Sale Rate</th>
                                    <th>Strip Sale Rate</th>
                                    <th>Piece Sale Rate</th>
                                    <th>Weight</th>
                                    <th>Exp Date</th>
                                    <th>RackName</th>
                                    <th>Stock</th>
                                    <th>Reorder Level</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp
                                @foreach($product_info as $product)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>{{$product->product_name}}</td>

                                    <td>{{$product->carton_qty}}</td>
                                    <td>{{$product->box_qty}}</td>
                                    <td>{{$product->strip_qty}}</td>
                                    <td>{{$product->piece_qty}}</td>

                                    <td>{{number_format($product->carton_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->box_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->strip_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->piece_sales_rate,2)}}Tk</td>
                                    
                                    <td>{{$product->weight}}</td>
                                    <td>{{$product->expiry_date}}</td>
                                    <td>{{$product->rack_number}}</td>
                                    <td>
                                        @if($product->piece_qty > 0)
                                            <span class="label bg-teal">Stock In</span>
                                        @else
                                            <span class="label bg-red">Stock Out</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($product->reorder_level >= $product->piece_qty)
                                            <span class="label bg-red">Reorder Product</span>
                                        @else
                                            <span class="label bg-blue">Not Now</span>
                                        @endif
                                    </td>
                                    <td>
					<div class="btn-group">
					    <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
					    <ul class="dropdown-menu action-menu">
						<li><a href="{{url('/branch-product/edit/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$product->product_name}}"><i class="material-icons">mode_edit</i> Edit</a></li>
						
						<li><a href="{{url('/branch-product/view/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$product->product_name}} Details"><i class="material-icons">visibility</i> View</a></li>
						
						<li><a href="{{url('/branch-product/delete/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$product->product_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$product->product_name}}"><i class="material-icons">delete</i> Delete</a></li>		
						@if ($product->piece_qty > 0)
                            <li><a href="{{url('/branch-product/add/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$product->product_name}}"><i class="material-icons">library_add</i> Add to branch</a></li>
                        @endif
                        </ul>
					</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

