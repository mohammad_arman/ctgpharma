@extends('admin.master')

@section('page_title')
Add Branch
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">store</i> Add Branch</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD BRANCH
                    </h2> 
		    <a href="{{url('/branch/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <form id="save_company" method="POST" action="{{ url('branch/save') }}">
                        {{ csrf_field() }}

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="branch_name">Branch Name<b style="color: red;"> *</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('branch_name') ? ' has-error' : '' }}">
					<input type="text" id="branch_name" name="branch_name" class="form-control" placeholder="Enter branch name">
				    </div>  
				    @if ($errors->has('branch_name'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('branch_name') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

                        <div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="contact_number">Branch Contact Number<b style="color: red;"> *</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('contact_number') ? ' has-error' : '' }}">
					<input type="text" id="contact_number" name="contact_number" class="form-control" placeholder="Enter company contact number">
				    </div> 
				    @if ($errors->has('contact_number'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('contact_number') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="address">Branch Address<b style="color: red;"> *</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('address') ? ' has-error' : '' }}">
					<textarea name="address"  class="editable"></textarea>
				    </div>
				    @if ($errors->has('address'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('address') }}</strong>
				    </span>
				    @endif
				</div>                       
			    </div>                       
                        </div>                       

                        <br>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
						<button type="reset" class="btn bg-blue-grey waves-effect">
								<i class="material-icons">cached</i>
								<span>RESET</span>
							</button>
				<button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
				    <i class="material-icons">save</i>
				    <span>SAVE</span>
				</button>
			    </div>
			</div>
			<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection


