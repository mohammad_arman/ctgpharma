@extends('admin.master')

@section('page_title')
View Branch Details
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">remove_red_eye</i> View Branch Details</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW BRANCH DETAILS
                    </h2>   
		    <a href="{{url('/branch/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>

                <div class="body" style="min-height: 500px;">
	    <ul class="view-list col-md-12">
		<li>
		    <span>Branch Name:</span>
		    <span>{{$view_branch_info->branch_name}}</span>
		</li>
		<li>
		    <span>Contact Number:</span>
		    <span>{{$view_branch_info->contact_number}}</span>
		</li>
		<li>
		    <span>Branch Address:</span>
		    <span>@php echo $view_branch_info->address; @endphp</span>
		</li>
		
	    </ul>
	    <div class="col-md-12">
		<a href="{{url('/branch/manage')}}">
		    <button type="button" class="btn bg-light-blue waves-effect pull-left" >
			<i class="material-icons">arrow_back</i> BACK
		    </button>
		</a>
	    </div>
                </div>
	<div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection


