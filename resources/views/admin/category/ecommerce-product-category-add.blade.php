@extends('admin.master')

@section('page_title')
Add Category
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">playlist_add</i> Add Category</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD E-COMMERCE PRODUCT CATEGORY
                    </h2>   
		    <a href="{{url('/category/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <form id="save_category" method="POST" action="{{ url('category/save') }}">
                        {{ csrf_field() }}
			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="category_name">Category Name<b style="color: red;"> *</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('category_name') ? ' has-error' : '' }}">
					<input type="text" id="category_name" name="category_name" class="form-control" placeholder="Enter category name">
				    </div>  
				    @if ($errors->has('category_name'))
					<span class="help-block">
					    <strong style="color: red;">{{ $errors->first('category_name') }}</strong>
					</span>
				    @endif
				</div>
			    </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 0px;">
			     <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="description">Description</label>
				<div class="form-group">
				    <div class="form-line">
					<textarea name="description"  class="form-control editable"></textarea>
				    </div>
				</div>
			    </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="publication_status">Publication Status<b style="color: red;"> *</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('publication_status') ? ' has-error' : '' }}">                                
					<select class="form-control show-tick" name="publication_status" id="publication_status" required>
					    <option value=""> Please Select </option>
					    <option value="1">Published</option>
					    <option value="0">Unpublished</option>
					</select>                               
				    </div>  
				    @if ($errors->has('publication_status'))
					<span class="help-block">
					    <strong style="color: red;">{{ $errors->first('publication_status') }}</strong>
					</span>
				    @endif
				</div>
			    </div>
                        </div>
                        <input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">
                            
                        <br>
			<div class="col-md-12">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<button type="reset" class="btn bg-blue-grey waves-effect">
					<i class="material-icons">cached</i>
					<span>RESET</span>
				</button>
				<button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
				    <i class="material-icons">save</i>
				    <span>SAVE</span>
				</button>
			    </div>
			</div>
			<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection
