@extends('admin.master')

@section('page_title')
Manage Category
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Category</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                        CATEGORY MANAGEMENT
                    </h2>
		    <a href="{{url('/category/add')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">add_box</i> ADD CATEGORY
			</button>
		    </a>
                </div>
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Category Name</th>
                                    <th>Description</th>
                                    <th>Added By</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Category Name</th>
                                    <th>Description</th>
                                    <th>Added By</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach($categories_info as $cat_info)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>{{$cat_info->category_name}}</td>
                                    <td>{!! $cat_info->description !!}</td>
                                    <td>{{$cat_info->created_by }}</td>
                                    <td>
                                        @if($cat_info->publication_status == 1)
                                        <span class="label bg-teal">Published</span>
                                        @else
                                        <span class="label bg-brown">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
					<div class="btn-group">
					    <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
					    <ul class="dropdown-menu action-menu">
						@if($cat_info->publication_status == 1)
						<li><a href="{{url('/category/unpublished/'.$cat_info->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unpublished {{$cat_info->category_name}}"><i class="material-icons">thumb_down</i> Unpublished</a></li>
						@else
						<li><a href="{{url('/category/published/'.$cat_info->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Published {{$cat_info->category_name}}"><i class="material-icons">thumb_up</i> Published</a></li>
						@endif
						<li><a href="{{url('/category/edit/'.$cat_info->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$cat_info->category_name}}"><i class="material-icons">mode_edit</i> Edit</a></li>
						<li><a href="{{url('/category/view/'.$cat_info->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$cat_info->category_name}} Details"><i class="material-icons">visibility</i> View</a></li>
						<li><a href="{{url('/category/delete/'.$cat_info->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$cat_info->category_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$cat_info->category_name}}"><i class="material-icons">delete</i> Delete</a></li>
						
					    </ul>
					</div>
<!--                                        @if($cat_info->publication_status == 1)
                                        <a href="{{url('/category/unpublished/'.$cat_info->id)}}" style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unpublished Category">
                                            <button type="button" class="btn bg-brown waves-effect"  >
                                                <i class="material-icons">thumb_down</i>
                                            </button>
                                        </a>
                                        @else
                                        <a href="{{url('/category/published/'.$cat_info->id)}}" style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Published Category">
                                            <button type="button" class="btn bg-teal waves-effect"  >
                                                <i class="material-icons">thumb_up</i>
                                            </button>
                                        </a>
                                        @endif
                                        <a href="{{url('/category/edit/'.$cat_info->id)}}" style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Category">
                                            <button type="button" class="btn bg-light-blue waves-effect">
                                                <i class="material-icons">edit</i>
                                            </button>
                                        </a>
                                        <a href="{{url('/category/delete/'.$cat_info->id)}}" style="text-decoration:none;" onclick="return confirm('Are you sure to delete this category?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Category">
                                            <button type="button" class="btn bg-pink waves-effect">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </a>
					-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection