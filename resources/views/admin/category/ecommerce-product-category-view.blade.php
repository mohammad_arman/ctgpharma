@extends('admin.master')

@section('page_title')
View Category
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i> View Category</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW E-COMMERCE PRODUCT CATEGORY
                    </h2>   
		    <a href="{{url('/category/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                       <ul class="view-list col-md-12">
		<li>
		    <span>Category Name:</span>
		    <span>{{$view_category_info->category_name}}</span>
		</li>
		
		<li>
		    <span>Description:</span>
		    <span>@php echo $view_category_info->description; @endphp</span>
		</li>
		
		<li>
		    <span>Publication Status:</span>
		    <span><?php echo json_decode(PUBLICATION_STATUS)[$view_category_info->publication_status];?></span>
		</li>
		
		<li>
		    <span>Added By:</span>
		    <span>{{$view_category_info->created_by}}</span>
		</li>
		
	    </ul>
	    <div class="col-md-12">
		<a href="{{url('/category/manage')}}">
		    <button type="button" class="btn bg-light-blue waves-effect pull-left" >
			<i class="material-icons">arrow_back</i> BACK
		    </button>
		</a>
	    </div>
                </div>
                 <div style="clear: both;"></div>       
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection

