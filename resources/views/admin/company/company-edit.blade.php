@extends('admin.master')

@section('page_title')
Edit Company Details
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">border_color</i> Edit Company Details</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EDIT COMPANY DETAILS
                    </h2>  
		    <a href="{{url('/company/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <form id="save_company" method="POST" action="{{ url('company/update') }}">
                        {{ csrf_field() }}
                        <input type="hidden"  name="id" value="{{$edit_company_info->id}}" class="form-control">
			
			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="company_name">Company Name<b style="color: red;"> *</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('company_name') ? ' has-error' : '' }}">
					<input type="text" id="company_name" name="company_name" value="{{$edit_company_info->company_name}}" class="form-control" placeholder="Enter company name">
				    </div>  
				    @if ($errors->has('company_name'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('company_name') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="contact_number">Company Contact Number</label>
				<div class="form-group">
				    <div class="form-line">
					<input type="text" id="contact_number" name="contact_number" value="{{$edit_company_info->contact_number}}" class="form-control" placeholder="Enter company contact number">
				    </div>  
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="address">Company Address</label>
				<div class="form-group">
				    <div class="form-line">
					<textarea name="address" class="editable">{{$edit_company_info->address}}</textarea>
				    </div>
				</div>                                                
			    </div>                                                
                        </div>                                                
                        <br>
			
			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
						<a href="{{url('/company/manage')}}" type="submit" class="btn bg-blue-grey waves-effect">
							<i class="material-icons">clear</i>
							<span>CANCEL</span>
						</a>
				<button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
				    <i class="material-icons">publish</i>
				    <span>UPDATE</span>
				</button>
			    </div>
			</div>
			<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection
