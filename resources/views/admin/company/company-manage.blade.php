@extends('admin.master')

@section('page_title')
Manage Company
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Company</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                        COMPANY MANAGEMENT
                    </h2>
		    <a href="{{url('/company/add')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">add_box</i> ADD COMPANY
			</button>
		    </a>
                </div>
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Company Name</th>
                                    <th>Contact Number</th>
                                    <th>Address</th>
                                    <th>Added By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Company Name</th>
                                    <th>Contact Number</th>
                                    <th>Address</th>
                                    <th>Added By</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach($company_info as $company)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>{{$company->company_name}}</td>
                                    <td>{{$company->contact_number}}</td>
                                    <td>{!! $company->address !!}</td>
                                    <td>{{$company->created_by }}</td>
                                    <td>
					<div class="btn-group">
					    <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
					    <ul class="dropdown-menu action-menu">
						<li><a href="{{url('/company/edit/'.$company->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$company->company_name}}"><i class="material-icons">mode_edit</i> Edit</a></li>
						<li><a href="{{url('/company/view/'.$company->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$company->company_name}} Details"><i class="material-icons">visibility</i> View</a></li>
						<li><a href="{{url('/company/delete/'.$company->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$company->company_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$company->company_name}}"><i class="material-icons">delete</i> Delete</a></li>						
					    </ul>
					</div>
<!--                                        <a href="{{url('/company/edit/'.$company->id)}}" style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit This Company Details">
                                            <button type="button" class="btn bg-light-blue waves-effect">
                                                <i class="material-icons">edit</i>
                                            </button>
                                        </a>
                                        <a href="{{url('/company/delete/'.$company->id)}}" style="text-decoration:none;" onclick="return confirm('Are you sure to delete this company?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete This Company Details">
                                            <button type="button" class="btn bg-pink waves-effect">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection