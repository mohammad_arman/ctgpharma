@extends('admin.master') 
@section('page_title')
 View Customer Details
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i> View Customer Details</li>
    </ol>
</div>
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW CUSTOMER DETAILS
                    </h2>
                    <a href="{{url('/customer/manage')}}">
                    <button type="button" class="btn bg-brown waves-effect pull-right header-button" >
                        <i class="material-icons">view_list</i> LIST
                    </button>
                    </a>
                </div>

                <div class="body">
                    <ul class="view-list col-md-12">
                        <li>
                            <span>Username:</span>
                            <span>{{$customer->username}}</span>
                        </li>
                        <li>
                            <span>First Name:</span>
                            <span>{{$customer->firstname}}</span>
                        </li>
                        <li>
                            <span>Last Name:</span>
                            <span>{{$customer->lastname}}</span>
                        </li>
                        <li>
                            <span>Email:</span>
                            <span>{{$customer->email}}</span>
                        </li>
                        <li>
                            <span>Contact Number One:</span>
                            <span>{{$customer->phone_num_one}}</span>
                        </li>
                        @if (!empty($customer->phone_num_two))
                        <li>
                            <span>Additional Contact Number:</span>
                            <span>{{$customer->phone_num_two}}</span>
                        </li>    
                        @endif
                        
                        <li>
                            <span>Address:</span>
                            <span>{{$customer->address}}</span>
                        </li>
                        @if (!empty($customer->address_two))
                        <li>
                            <span>Additional Address:</span>
                            <span>{{$customer->address_two}}</span>
                        </li>
                        @endif
                        @if (!empty($customer->additional_information))
                        <li>
                            <span>Additional Infromation:</span>
                            <span>{{$customer->additional_information}}</span>
                        </li>
                        @endif
                    </ul>
                    <div class="col-md-12" style="padding-left:8px;">
                        <a href="{{url('/customer/manage')}}">
                            <button type="button" class="btn bg-light-blue waves-effect pull-left" >
                            <i class="material-icons">arrow_back</i> BACK
                            </button>
                        </a>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection