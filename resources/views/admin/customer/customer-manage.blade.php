@extends('admin.master')

@section('page_title')
Manage Customer
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Customer</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                        CUSTOMER MANAGEMENT
                    </h2>
                </div>
                <div class="body" style="min-height:500px;">
                        @if($success_message = Session::get('success'))
                        <div class="alert bg-teal alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{$success_message}}
                        </div>
                        @endif
                        @if($error_message = Session::get('error'))
                        <div class="alert bg-red alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{$error_message}}
                        </div>
                        @endif
                        <div class="table-responsive" style="min-height: 500px;">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Contact No.</th>
                                    <th>Joining Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach($customers as $customer)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>{{$customer->username}}</td>
                                    <td>{{$customer->email}}</td>
                                    <td>{{ $customer->phone_num_one }}</td>
                                    <td>{{ date('d-m-Y h:i:s A', strtotime($customer->created_on)) }}</td>
                                    <td>
                                        @if($customer->status == 0)
                                        <span class="label label-danger">Inactive</span>
                                        @else
                                        <span class="label label-success">Active</span>
                                        @endif
                                    </td>
                                    <td>
					<div class="btn-group">
					    <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
					    <ul class="dropdown-menu" style="margin-top: 0px !important;left:-100px;">
                        
                        @if ($customer->status == 0)
                        <li><a href="{{url('/customer/active/'.$customer->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to active this {{$customer->username}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Active {{$customer->username}}"><i class="material-icons">thumb_up</i> Active</a></li>
                        @else
                        <li><a href="{{url('/customer/inactive/'.$customer->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to inactive this {{$customer->username}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive {{$customer->username}}"><i class="material-icons">thumb_down</i> Inactive</a></li>
                        @endif
                        
                        
                        <li><a href="{{url('/customer/view/'.$customer->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$customer->username}} Details"><i class="material-icons">remove_red_eye</i> View</a></li>
                        
						<li><a href="{{url('/customer/delete/'.$customer->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$customer->username}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$customer->username}}"><i class="material-icons">delete</i> Delete</a></li>
						
					    </ul>
					</div>
					
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection