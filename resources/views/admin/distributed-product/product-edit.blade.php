@extends('admin.master')

@section('page_title')
Edit Product
@endsection

@section('admin_main_content')
<link href="{{asset('public/admin-frontend-assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">shopping_basket</i> Edit Product</li>
    </ol>
</div>  
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EDIT PRODUCT
                    </h2>    
		    <div class="btn-group pull-right header-button">
			<a href="{{url('/product/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
			
		    </div>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
		    
		    
                    
                    <form method="POST" action="{{ url('/product/update') }}" id="edit_product"  enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Basic Section</legend>
                            <div class="col-md-12">
                            <input type="hidden" name="id" value="{{$product_info->id}}" />
                            <input type="hidden" name="product_id" value="{{$product_info->product_id}}" />
                                <div class="col-md-4">
                                    <label for="product_name">Product Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                    <div class="form-line{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                        <input type="text" disabled id="product_name" name="product_name" class="form-control" value="{{$product_info->product_name}}">
                                    </div>  
                                    @if ($errors->has('product_name'))
                                        <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('product_name') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                   
                    @if(Session::get('branch_id') == 1)
                        @if($product_info->carton_qty != 0)
                        <div class="col-md-4" id="carton_quantity_section">
                            <label for="items_in_pack">No of Carton <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('carton_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="carton_qty" name="carton_qty" value="{{$product_info->carton_qty}}" class="form-control"
                                        autocomplete="off">
                                    <input type="hidden" name="change_carton_qty" id="change_carton_qty" value=0>
                                </div>
                                @if ($errors->has('carton_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('carton_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_ctn_qty"></span>
                            </div>
                        </div>
                        @endif


                        <div style="clear: both;"></div>
                        @if($product_info->box_qty != 0)
                        <div class="col-md-4" id="box_quantity_section">
                            <label for="box_qty">No of Box <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('box_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="box_qty" name="box_qty" value="{{$product_info->box_qty}}" class="form-control" autocomplete="off">
                                    <input type="hidden" name="change_box_qty" id="change_box_qty" value=0>
                                </div>
                                @if ($errors->has('box_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('box_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_box_qty"></span>
                            </div>
                        </div>
                        @endif 
                        
                        @if($product_info->strip_qty != 0)
                        <div class="col-md-4" id="strip_quantity_section">
                            <label for="strip_qty">No of Strip <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('strip_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="strip_qty" name="strip_qty" value="{{$product_info->strip_qty}}" class="form-control" autocomplete="off">
                                    <input type="hidden" name="change_strip_qty" id="change_strip_qty" value=0>
                                </div>
                                @if ($errors->has('strip_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('strip_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_strip_qty"></span>

                            </div>
                        </div>
                        @endif
                        <div style="clear: both;"></div>
                        <div class="col-md-4" id="piece_quantity_section">
                            <label for="piece_qty">No of Piece <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('piece_qty') ? ' has-error' : '' }}">
                                    <input type="number" id="piece_qty" name="piece_qty" value="{{$product_info->piece_qty}}" class="form-control" autocomplete="off">
                                    <input type="hidden" name="change_piece_qty" id="change_piece_qty" value=0>
                                </div>

                                @if ($errors->has('piece_qty'))
                                <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('piece_qty') }}</strong>
                                    </span> @endif
                                <span style="color: red;" id="error_piece_qty"></span>

                            </div>
                        </div>
                        @endif
                    </div>
                </fieldset>

			
			
			
			 <div style="clear: both;"></div>
			
			  
                <fieldset class="the-fieldset" id="diffuser_fieldset">
                    <legend class="the-legend">Other Section</legend>
                    <div class="col-md-12">   
                    @if(Session::get('branch_id') != 1)
                        <div class="col-md-4">
                            <label for="rack_number">Rack Number <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('rack_number') ? ' has-error' : '' }}">
                                    <input type="text" id="rack_number" name="rack_number" class="form-control" value="{{$product_info->rack_number}}">
                                </div>  
                                @if ($errors->has('rack_number'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('rack_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    @endif
                        <div class="col-md-4">
                            <label for="reorder_level">Reorder Level <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('reorder_level') ? ' has-error' : '' }}">
                                    <input type="text" id="reorder_level" name="reorder_level" class="form-control" value="{{$product_info->reorder_level}}">
                                </div>  
                                @if ($errors->has('reorder_level'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('reorder_level') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                         
                    </div>
                </fieldset>
                        <br>
                        <div class="col-md-12">
                            <div class="input-group"> 
                                    <a href="{{url('/product/manage')}}" type="submit" class="btn bg-blue-grey waves-effect">
                                        <i class="material-icons">clear</i>
                                        <span>CANCEL</span>
                                    </a>
                                <button type="submit" class="btn bg-light-blue waves-effect save-btn" style="margin-left:5px;" id="submit-btn">
                                    <i class="material-icons">publish</i>
                                    <span>UPDATE</span>
                                </button>
                            </div> 
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>                      
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->
    <script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/admin-frontend-assets/js/bootstrap-datetimepicker.min.js')}}"></script>

</div>
<script>
    $(document).ready(function(){
    var old_piece_qty = "{{$branch_product_info->piece_qty}}";
    var old_strip_qty = "{{$branch_product_info->strip_qty}}";
    var old_box_qty   = "{{$branch_product_info->box_qty}}";
    var old_ctn_qty   = "{{$branch_product_info->carton_qty}}";
    
    $("#branch_id").change(function(){
        if($.trim($('#branch_id').val()).length == 0){
            $("#error_branch_id").text("Branch name field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_branch_id").text("");
            $(".save-btn").prop("disabled",false);
        }
    });
    $("#product_name").change(function(){
        if($.trim($('#product_name').val()).length == 0){
            $("#error_product_name").text("Product name field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_product_name").text("");
            $(".save-btn").prop("disabled",false);
        }
    });
    $('#piece_qty').change(function(){
        var new_piece_qty = $('#piece_qty').val();
        var change_piece_qty = new_piece_qty - "{{$product_info->piece_qty}}";
        var check_qty = old_piece_qty - change_piece_qty;
        $("#change_piece_qty").val(change_piece_qty);
        if($.trim($('#piece_qty').val()).length == 0){
            $("#error_piece_qty").text("Piece quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }
        //else if(new_piece_qty == 0){
          //  $("#error_piece_qty").text("Piece quantity field can't be zero!");
        //    $(".save-btn").prop("disabled",true);
        //}
        else if(new_piece_qty < 0){
            $("#error_piece_qty").text("Piece quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            // console.log(new_piece_qty+" "+old_piece_qty+" "+check_qty);
            $("#error_piece_qty").text("Can't exceed stock piece quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_piece_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
    });
    $('#strip_qty').change(function(){
        var new_strip_qty = $('#strip_qty').val();
        var change_strip_qty = new_strip_qty - "{{$product_info->strip_qty}}";
        var check_qty  = old_strip_qty - change_strip_qty;
        $("#change_strip_qty").val(change_strip_qty);
        if($.trim($('#strip_qty').val()).length == 0){
            $("#error_strip_qty").text("Strip quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }//else if(new_strip_qty == 0){
           // $("#error_strip_qty").text("Strip quantity field can't be zero!");
            //$(".save-btn").prop("disabled",true);
        //}
        else if(new_strip_qty < 0){
            $("#error_strip_qty").text("Strip quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            $("#error_strip_qty").text("Can't exceed stock strip quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_strip_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
        
    });
    $('#box_qty').change(function(){
        var new_box_qty = $('#box_qty').val();
        var change_box_qty = new_box_qty - "{{$product_info->box_qty}}";
        var check_qty  = old_box_qty - change_box_qty;
        $("#change_box_qty").val(change_box_qty);
        if($.trim($('#box_qty').val()).length == 0){
            $("#error_box_qty").text("Box quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }//else if(new_box_qty == 0){
            //$("#error_box_qty").text("Box quantity field can't be zero!");
          //  $(".save-btn").prop("disabled",true);
        //}
        else if(new_box_qty < 0){
            $("#error_box_qty").text("Box quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            $("#error_box_qty").text("Can't exceed stock box quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_box_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
        
    });
    $('#carton_qty').change(function(){
        var new_ctn_qty = $('#carton_qty').val();
        var change_carton_qty = new_ctn_qty - "{{$product_info->carton_qty}}";
        var check_qty = old_ctn_qty - change_carton_qty;
        $("#change_carton_qty").val(change_carton_qty);
        if($.trim($('#carton_qty').val()).length == 0){
            $("#error_ctn_qty").text("Carton quantity field can't left empty!");
            $(".save-btn").prop("disabled",true);
        }//else if(new_ctn_qty == 0){
           // $("#error_ctn_qty").text("Carton quantity field can't be zero!");
            //$(".save-btn").prop("disabled",true);
        //}
        else if(new_ctn_qty < 0){
            $("#error_ctn_qty").text("Carton quantity field can't be less than zero!");
            $(".save-btn").prop("disabled",true);
        }else if(check_qty < 0){
            $("#error_ctn_qty").text("Can't exceed stock carton quantity!");
            $(".save-btn").prop("disabled",true);
        }else{
            $("#error_ctn_qty").text("");
            $(".save-btn").prop("disabled",false);
        }
        
    });

    //======form submit validation code start=========
 
});

</script>
@endsection

    





