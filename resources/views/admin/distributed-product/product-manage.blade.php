@extends('admin.master')

@section('page_title')
Product Manage
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Product Manage</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                     PRODUCT MANAGEMENT
                    </h2>
		    
                </div>
                <div class="body" style="min-height: 400px;">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$success_message}}
                    </div>
                    @endif
                    @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$error_message}}
                    </div>
                    @endif
                    <div class="table-responsive" style="min-height: 500px;">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    
                                    @if(Auth::user()->hasRole('Admin')) 
                                    <th>Branch</th>
                                    @endif
                                    <th>Product</th>
                                    <th>Company</th>
                                    <th>Carton</th>
                                    <th>Box</th>
                                    <th>Strip</th>
                                    <th>Piece</th>
                                    <th>Ctn Sale Rate</th>
                                    <th>Box Sale Rate</th>
                                    <th>Strip Sale Rate</th>
                                    <th>Piece Sale Rate</th>
                                    <th>Weight</th>
                                    <th>Exp Date</th>
                                    <th>Rack No</th>
                                    <th>Stock</th>
                                    <th>Reorder Level</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp
                                @foreach($products as $product)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    @if(Auth::user()->hasRole('Admin')) 
                                    <td>{{$product->branch->branch_name}}</td>
                                    @endif
                                    <td>{{$product->product_name}}</td>
                                    <td>
                                        <?php
                                            $supplier_id = $product->branchProduct->supplier_id;
        
                                            $supplier = DB::table('suppliers')->where('id',$supplier_id)->first();
                                            $company_id = $supplier->company_id;
                                            $company_name = DB::table('companies')->where('id',$company_id)->first();
                                            echo $company_name->company_name;
                                            ?>
                                    </td>
                                    <td>{{$product->carton_qty}}</td>
                                    <td>{{$product->box_qty}}</td>
                                    <td>{{$product->strip_qty}}</td>
                                    <td>{{$product->piece_qty}}</td>
				    
                                    <td>{{number_format($product->carton_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->box_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->strip_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->piece_sales_rate,2)}}Tk</td>
                                    
                                    <td>{{$product->weight}}</td>
                                    <td>{{$product->expiry_date}}</td>
                                    <td>{{$product->rack_number}}</td>
                                    <td>
                                        @if($product->piece_qty > 0)
                                            <span class="label bg-teal">Stock In</span>
                                        @else
                                            <span class="label bg-red">Stock Out</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($product->reorder_level >= $product->piece_qty)
                                            <span class="label bg-red">Reorder Product</span>
                                        @else
                                            <span class="label bg-blue">Not Now</span>
                                        @endif
                                    </td>
                                    <td>
					<div class="btn-group">
					    <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
					    <ul class="dropdown-menu action-menu">
                            @if((Auth::user()->hasRole('Admin') && empty($product->rack_number)) || Auth::user()->hasRole('Employee'))   
                            <li><a href="{{url('/product/edit/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$product->product_name}}"><i class="material-icons">mode_edit</i> Edit</a></li>
                            @endif
                            <li><a href="{{url('/product/view/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$product->product_name}} Details"><i class="material-icons">visibility</i> View</a></li>
                            @if(Auth::user()->hasRole('Admin') && empty($product->rack_number)) 
                            <li><a href="{{url('/product/delete/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$product->product_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$product->product_name}}"><i class="material-icons">delete</i> Delete</a></li>		
                            @endif
					
					    </ul>
					</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

