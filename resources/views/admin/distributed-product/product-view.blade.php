@extends('admin.master')

@section('page_title')
View Product
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i> View Product</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW PRODUCT
                    </h2>   
		    <a href="{{url('/product/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                       <ul class="view-list col-md-6">
		<li>
		    <span>Name:</span>
		    <span>{{$product_info->product_name}}</span>
		</li>
		<li>
		    <span>Distributed Branch:</span>
		    <span>{{$product_info->branch->branch_name}}</span>
		</li>
		@if(!empty($product_info->carton_qty))
		<li>
		    <span>Carton Quantity:</span>
		    <span>{{$product_info->carton_qty}}</span>
		</li>
		@endif
		@if(!empty($product_info->box_qty))
		<li>
		    <span>Box Quantity:</span>
		    <span>{{$product_info->box_qty}}</span>
		</li>
		@endif
		@if(!empty($product_info->strip_qty))
		<li>
		    <span>Strip Quantity:</span>
		    <span>{{$product_info->strip_qty}}</span>
		</li>
		@endif
		<li>
		    <span>Piece Quantity:</span>
		    <span>{{$product_info->piece_qty}}</span>
		</li>
		<li>
		    <span>Purchase Rate:</span>
		    <span>{{number_format($product_info->purchase_rate,2)}}TK</span>
		</li>
		<li>
		    <span>Sale Rate:</span>
		    <span>{{number_format($product_info->sale_rate,2)}}TK</span>
		</li>
		
		<li>
		    <span>Added By:</span>
		    <span>{{$product_info->created_by}}</span>
		</li>
		
		
	    </ul>
	    <ul class="view-list col-md-6">
		@if(!empty($product_info->carton_sales_rate))
		<li>
		    <span>Carton Sale Rate:</span>
		    <span>{{number_format($product_info->carton_sales_rate,2)}}Tk</span>
		</li>
		@endif
		@if(!empty($product_info->carton_sales_rate))
		<li>
		    <span>Box Sale Rate:</span>
		    <span>{{number_format($product_info->box_sales_rate,2)}}Tk</span>
		</li>
		@endif
		@if(!empty($product_info->carton_sales_rate))
		<li>
		    <span>Strip Sale Rate:</span>
		    <span>{{number_format($product_info->strip_sales_rate,2)}}Tk</span>
		</li>
		@endif
		<li>
		    <span>Piece Sale Rate:</span>
		    <span>{{number_format($product_info->piece_sales_rate,2)}}Tk</span>
		</li>
		
		<li>
		    <span>Weight:</span>
		    <span>{{$product_info->weight}}</span>
		</li>
		<li>
		    <span>Mfg. Date:</span>
		    <span>{{$product_info->manufacturing_date}}</span>
		</li>
		<li>
		    <span>Exp. Date:</span>
		    <span>{{$product_info->expiry_date}}</span>
		</li>
		<li>
		    <span>Rack Number:</span>
		    <span>{{$product_info->rack_number}}</span>
		</li>
		
		<li>
		    <span>Reorder Level:</span>
		    <span>{{$product_info->reorder_level}}</span>
		</li>
		
		
	    </ul>
	    <div class="col-md-12">
		<a href="{{url('/product/manage')}}">
		    <button type="button" class="btn bg-light-blue waves-effect pull-left" >
			<i class="material-icons">arrow_back</i> BACK
		    </button>
		</a>
	    </div>
                </div>
                 <div style="clear: both;"></div>       
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection

