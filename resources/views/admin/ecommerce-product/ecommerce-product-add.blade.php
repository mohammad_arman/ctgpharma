@extends('admin.master')

@section('page_title')
Add Product
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">shopping_basket</i> Add Product</li>
    </ol>
</div>  
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD PRODUCT
                    </h2>    
		    <div class="btn-group pull-right header-button">
			<button type="button" class="btn bg-brown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			     <i class="material-icons">view_list</i> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu action-menu">
			    <li><a href="{{url('/branch-product/manage/')}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Branch Product List"> Branch Product List</a></li>

			    <li><a href="{{url('/ecommerce-product/manage/')}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ecommerce Product List"> Ecommerce Product List</a></li>
			</ul>
		    </div>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
		    
		    
                    
                    <form method="POST" id="form"  action="{{ url('/product/save') }}"  enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Basic Section</legend>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <label for="stock">Select Stock <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('stock') ? ' has-error' : '' }}" style="z-index:10;">                                
                                            <select class="form-control show-tick" name="stock" id="stock">
                                                <option value="">Please Select</option>
                                                <option value="Ecommerce">Ecommerce</option>
                                                <option value="Branch">Branch</option>
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('stock'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('stock') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="product_name">Product Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                    <div class="form-line{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                        <input type="text" id="product_name" name="product_name" class="form-control" placeholder="Enter product name">
                                    </div>  
                                    @if ($errors->has('product_name'))
                                        <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('product_name') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                                <div class="col-md-4" id="product_image_section">
                                    <label for="product_image">Product Image <b style="color: red;">*</b></label>
                                    <div class="input-group image-preview">
                                        <div class="form-line{{ $errors->has('product_image') ? ' has-error' : '' }}">
                                            <input type="file" id="product_image" name="product_image" class="form-control" placeholder="Select product image">
        
                                        </div>  
                                        @if ($errors->has('product_image'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('product_image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4" id="category_section">
                                    <label for="category_id">Category Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('category_id') ? ' has-error' : '' }}" style="z-index:9;">                                
                                            <select class="form-control show-tick" name="category_id" id="category_id" data-live-search="true">
                                                <option value="">Please Select</option>
                                                @foreach($category as $category_value)
                                                <option value="{{$category_value->id}}">{{$category_value->category_name}}</option>
                                                @endforeach
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="supplier_id">Supplier Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('supplier_id') ? ' has-error' : '' }}" style="z-index:9;">                                
                                            <select class="form-control show-tick" name="supplier_id" id="supplier_id" data-live-search="true" >
                                                <option value="">Please Select</option>
                                                @foreach($supplier as $supplier_value)
                                                <option value="{{$supplier_value->id}}">{{$supplier_value->supplier_name}} <span>({{$supplier_value->company_name}})</span></option>
                                                @endforeach
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('supplier_id'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('supplier_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Package Section</legend>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <label for="package_name">Package Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('package_name') ? ' has-error' : '' }}" style="z-index:8;">                                
                                            <select class="form-control show-tick" name="package_name" id="package_name" data-live-search="true">
                                                <option value="">Please Select</option>
                                                <option value="Carton">Carton</option>
                                                <option value="Box">Box</option>
                                                <option value="Strip">Strip</option>
                            <option value="Single-Piece">Single Piece</option>
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('package_name'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('package_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-4" id="carton_quantity_section">
                                    <label for="items_in_pack">No of Carton <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('carton_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="carton_qty" name="carton_qty" class="form-control" value="0">
                                        </div> 
                                        @if ($errors->has('carton_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('carton_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-4" id="items_package_name_section">
                                    <label for="items_package_name">Items Package Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('items_package_name') ? ' has-error' : '' }}" style="z-index:8;">                                
                                            <select class="form-control show-tick" name="items_package_name" id="items_package_name">
                                                <option value="">Please Select</option>
                            <option value="Box">Box</option>
                                                <option value="Strip">Strip</option>
                                                <option value="Single-Piece">Single Piece</option>
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('items_package_name'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('items_package_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="col-md-4" id="box_quantity_section">
                                    <label for="box_qty">No of Box <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('box_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="box_qty" name="box_qty" class="form-control" value="0">
                                        </div> 
                                        @if ($errors->has('box_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('box_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4" id="sub_items_package_name_section">
                                    <label for="sub_items_package_name">Sub Items Package Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('sub_items_package_name') ? ' has-error' : '' }}" style="z-index:7;">                                
                                            <select class="form-control show-tick" name="sub_items_package_name" id="sub_items_package_name">
                                                <option value="">Please Select</option>
                                                <option value="Strip">Strip</option>
                                                <option value="Single-Piece">Single Piece</option>
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('sub_items_package_name'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('sub_items_package_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                    
                     <div class="col-md-4" id="strip_quantity_section">
                                    <label for="strip_qty">No of Strip <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('strip_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="strip_qty" name="strip_qty" class="form-control" value="0">
                                        </div> 
                                        @if ($errors->has('strip_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('strip_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4" id="piece_quantity_section">
                                    <label for="piece_qty">No of Piece <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('piece_qty') ? ' has-error' : '' }}">
                                            <input type="number" id="piece_qty" name="piece_qty" class="form-control">
                                        </div> 

                                        @if ($errors->has('piece_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('piece_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                         </fieldset>   
			
                                         
			
			
                       
			{{-- <div style="clear: both;"></div>
                        
                        
			
			
			
			 <div style="clear: both;"></div>
			
			
			 
			  <div style="clear: both;"></div> --}}
			  
              <fieldset class="the-fieldset" id="diffuser_fieldset">
                <legend class="the-legend">Price Section</legend>
                <div class="col-md-12">    
                        <div class="col-md-4">
                            <label for="purchase_rate">Purchase Rate <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('purchase_rate') ? ' has-error' : '' }}">
                                    <input type="text" disabled id="purchase_rate" name="purchase_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('purchase_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('purchase_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="sale_rate">Sales Rate <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('sale_rate') ? ' has-error' : '' }}">
                                    <input type="text" disabled id="sale_rate" name="sale_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('sale_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('sale_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
			  <div class="col-md-4" id="carton_sales_rate_section">
                            <label for="carton_sales_rate">Carton Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('carton_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="carton_sales_rate" name="carton_sales_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('carton_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('carton_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="col-md-4" id="box_sales_rate_section">
                            <label for="box_sales_rate">Box Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('box_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="box_sales_rate" name="box_sales_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('box_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('box_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="strip_sales_rate_section">
                            <label for="strip_sales_rate">Strip Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('strip_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="strip_sales_rate" name="strip_sales_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('strip_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('strip_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="piece_sales_rate_section">
                            <label for="piece_sales_rate">Piece Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('piece_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="piece_sales_rate" name="piece_sales_rate" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('piece_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('piece_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>   
                <fieldset class="the-fieldset" id="diffuser_fieldset">
                    <legend class="the-legend">Other Section</legend>
                    <div class="col-md-12">   
                         <div class="col-md-4" id="discount_section">
                            <label for="discount">Discount <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('weight') ? ' has-error' : '' }}">
                                    <input type="text" id="discount" name="discount" class="form-control" placeholder="Enter product weight">
                                </div>  
                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			  
                        <div class="col-md-4">
                            <label for="weight">Weight <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('weight') ? ' has-error' : '' }}">
                                    <input type="text" id="weight" name="weight" class="form-control" placeholder="Enter product weight">
                                </div>  
                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="manufacturing_date">Manufacturing Date <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="manufacturing_date" id="manufacturing_date" placeholder="Please choose a date...">
                                </div> 
                                @if ($errors->has('manufacturing_date'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('manufacturing_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="expiry_date">Expiry Date <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="expiry_date" id="expiry_date" placeholder="Please choose a date...">
                                </div> 
                                @if ($errors->has('expiry_date'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('expiry_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="col-md-4">
                            <label for="rack_number">Rack Number <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('rack_number') ? ' has-error' : '' }}">
                                    <input type="text" id="rack_number" name="rack_number" class="form-control" placeholder="Enter rack number">
                                </div>  
                                @if ($errors->has('rack_number'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('rack_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="reorder_level">Reorder Level <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('reorder_level') ? ' has-error' : '' }}">
                                    <input type="text" id="reorder_level" name="reorder_level" class="form-control" placeholder="Enter reorder level">
                                </div>  
                                @if ($errors->has('reorder_level'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('reorder_level') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			  
			  <div class="col-md-4" id="publication_status_section">
                            <label for="publication_status">Publication Status  <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('publication_status') ? ' has-error' : '' }}" style="z-index:10;">                                
                                    <select class="form-control show-tick" name="publication_status" id="publication_status">
                                        <option value="">Please Select</option>
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    </select>                               
                                </div>  
                                @if ($errors->has('publication_status'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('publication_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
			  <div class="col-md-4" id="feature_product_section">
                            <label for="feature_product">Want To Make It Featured?  <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('feature_product') ? ' has-error' : '' }}" style="z-index:9;">                                
                                    <select class="form-control show-tick" name="feature_product" id="feature_product">
                                        <option value="">Please Select</option>
                                        <option value="1">Featured</option>
                                        <option value="0">Unfeatured</option>
                                    </select>                               
                                </div>  
                                @if ($errors->has('feature_product'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('feature_product') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			  
			  <div class="col-md-12" id="product_description">
                            <label for="description">Product Description <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <textarea name="description" id="description" class="form-control editable"></textarea>
                                </div>  
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">                           
                    </div>
                </fieldset>
                        <br>
                        <div class="col-md-12">
                            <div class="input-group"> 
                                    <button type="reset" class="btn bg-blue-grey waves-effect">
                                            <i class="material-icons">cached</i>
                                            <span>RESET</span>
                                        </button>
                                <button type="submit" id="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
                                    <i class="material-icons">save</i>
                                    <span>SAVE</span>
                                </button>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>                      
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->
</div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script>
    $('#submit').click(function(e){
    
    var package_name = $('select#package_name').val();
    if(package_name === ""){
        if($('span#package_name').is(':visible')){
            $('span#package_name').remove();
        }
        e.preventDefault();
        var msg = 'Package name is required';
        $('#package_name').parent('div').parent('div').parent('div').append("<span id='package_name' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var stock = $('select#stock').val();
    if(stock === ""){
        if($('span#stock').is(':visible')){
            $('span#stock').remove();
        }
        e.preventDefault();
        var msg = 'Stock name is required';
        $('#stock').parent('div').parent('div').parent('div').append("<span id='stock' style='color:red;font-weight:bold'>"+msg+'</span>');
    }else if(stock === 'Ecommerce'){
        var category_id = $('select#category_id').val();
        if(category_id === ""){
            if($('span#category_id').is(':visible')){
                $('span#category_id').remove();
            }
            e.preventDefault();
            var msg = 'Category name is required';
            $('#category_id').parent('div').parent('div').parent('div').append("<span id='category_id' style='color:red;font-weight:bold'>"+msg+'</span>');
        }
        
        var product_image = $('input#product_image').val();
        if(product_image === ""){
            if($('span#product_image').is(':visible')){
                $('span#product_image').remove();
            }
            e.preventDefault();
            var msg = 'Product image is required';
            $('#product_image').parent('div').parent('div').append("<span id='product_image' style='color:red;font-weight:bold'>"+msg+'</span>');
        }
        
        var feature_product = $('select#feature_product').val();
        if(feature_product === ""){
            if($('span#feature_product').is(':visible')){
                $('span#feature_product').remove();
            }
            e.preventDefault();
            var msg = 'Feature product is required';
            $('#feature_product').parent('div').parent('div').parent('div').append("<span id='feature_product' style='color:red;font-weight:bold'>"+msg+'</span>');
        }
        
        var publication_status = $('select#publication_status').val();
        if(publication_status === ""){
            if($('span#publication_status').is(':visible')){
                $('span#publication_status').remove();
            }
            e.preventDefault();
            var msg = 'Publication status is required';
            $('#publication_status').parent('div').parent('div').parent('div').append("<span id='publication_status' style='color:red;font-weight:bold'>"+msg+'</span>');
        }
        
        var description = tinyMCE.get('description').getContent().length;
        if(description < 1){
            if($('span#description').is(':visible')){
                $('span#description').remove();
            }
            e.preventDefault();
            var msg = 'Description is required';
            $('#description').parent('div').parent('div').parent('div').append("<span id='description' style='color:red;font-weight:bold'>"+msg+'</span>');
        }
    }
    
    var supplier_id = $('select#supplier_id').val();
    if(supplier_id === ""){
        if($('span#supplier_id').is(':visible')){
            $('span#supplier_id').remove();
        }
        e.preventDefault();
        var msg = 'Supplier name is required';
        $('#supplier_id').parent('div').parent('div').parent('div').append("<span id='supplier_id' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var product_name = $('input#product_name').val();
    if(product_name === ""){
        if($('span#product_name').is(':visible')){
            $('span#product_name').remove();
        }
        e.preventDefault();
        var msg = 'Product name is required';
        $('#product_name').parent('div').parent('div').append("<span id='product_name' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var weight = $('input#weight').val();
    if(weight === ""){
        if($('span#weight').is(':visible')){
            $('span#weight').remove();
        }
        e.preventDefault();
        var msg = 'Weight is required';
        $('#weight').parent('div').parent('div').append("<span id='weight' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var manufacturing_date = $('input#manufacturing_date').val();
    if(manufacturing_date === ""){
        if($('span#manufacturing_date').is(':visible')){
            $('span#manufacturing_date').remove();
        }
        e.preventDefault();
        var msg = 'Manufacturing date is required';
        $('#manufacturing_date').parent('div').parent('div').append("<span id='manufacturing_date' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var expiry_date = $('input#expiry_date').val();
    if(expiry_date === ""){
        if($('span#expiry_date').is(':visible')){
            $('span#expiry_date').remove();
        }
        e.preventDefault();
        var msg = 'Expiry date is required';
        $('#expiry_date').parent('div').parent('div').append("<span id='expiry_date' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
        
    var rack_number = $('input#rack_number').val();
    if(rack_number === ""){
        if($('span#rack_number').is(':visible')){
            $('span#rack_number').remove();
        }
        e.preventDefault();
        var msg = 'Rack number is required';
        $('#rack_number').parent('div').parent('div').append("<span id='rack_number' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var reorder_level = $('input#reorder_level').val();
    if(reorder_level === ""){
        if($('span#reorder_level').is(':visible')){
            $('span#reorder_level').remove();
        }
        e.preventDefault();
        var msg = 'Reorder level is required';
        $('#reorder_level').parent('div').parent('div').append("<span id='reorder_level' style='color:red;font-weight:bold'>"+msg+'</span>');
    }else if($.isNumeric(reorder_level) === false){
        if($('span#reorder_level').is(':visible')){
            $('span#reorder_level').remove();
        }
        e.preventDefault();
        var msg = 'Reorder level is numeric';
        $('#reorder_level').parent('div').parent('div').append("<span id='reorder_level' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var purchase_rate = $('input#purchase_rate').val();
    if(purchase_rate < 1){
        if($('span#purchase_rate').is(':visible')){
            $('span#purchase_rate').remove();
        }
        e.preventDefault();
        var msg = 'Purchase rate is required';
        $('#purchase_rate').parent('div').parent('div').parent('div').append("<span id='purchase_rate' style='color:red;font-weight:bold'>"+msg+'</span>');
    }
    
    var sale_rate = $('input#sale_rate').val();
    if(sale_rate < 1){
        if($('span#sale_rate').is(':visible')){
            $('span#sale_rate').remove();
        }
        e.preventDefault();
        var msg = 'Sale rate is required';
        $('#sale_rate').parent('div').parent('div').parent('div').append("<span id='sale_rate' style='color:red;font-weight:bold'>"+msg+'</span>');
    }

});
</script>
<script>
    $(document).ready(function(){
	
	
	$('#product_image_section').hide();
	$('#category_section').hide();
	$('#carton_quantity_section').hide();
	$('#items_package_name_section').hide();
	$('#box_quantity_section').hide();
	$('#sub_items_package_name_section').hide();
	$('#strip_quantity_section').hide();
	$('#piece_quantity_section').hide();
	$('#carton_sales_rate_section').hide();
	$('#box_sales_rate_section').hide();
	$('#strip_sales_rate_section').hide();
	$('#piece_sales_rate_section').hide();
	
	$('#discount_section').hide();
	$('#publication_status_section').hide();
	$('#feature_product_section').hide();
	$('#product_description').hide();
	
	
	
	$('#stock').change(function(){
	    if($('#stock').val() == 'Branch'){
            $('#product_image_section').hide();
            $('#category_section').hide();
            $('#publication_status_section').hide();
            $('#feature_product_section').hide();
            $('#product_description').hide();
            $('#discount_section').hide();
	    }else if($('#stock').val() == 'Ecommerce'){
            $('#product_image_section').show();
            $('#category_section').show();
            $('#publication_status_section').show();
            $('#feature_product_section').show();
            $('#product_description').show();
            $('#discount_section').show();
	    }
	});
	$('#package_name').change(function(){
	    if($('#package_name').val() == 'Carton'){
            $('#carton_quantity_section').show();
            $('#items_package_name_section').show();
            $('#box_quantity_section').hide();
            $('#sub_items_package_name_section').hide();
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').hide();
		
	    }else if($('#package_name').val() == 'Box'){
            $('#box_quantity_section').show();
            $('#sub_items_package_name_section').show();
            $('#carton_quantity_section').hide();
            $('#items_package_name_section').hide();
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').hide();
		
	    }else if($('#package_name').val() == 'Strip'){
            $('#carton_quantity_section').hide();
            $('#items_package_name_section').hide();
            $('#box_quantity_section').hide();
            $('#sub_items_package_name_section').hide();
            $('#strip_quantity_section').show();
            $('#piece_quantity_section').show();
            $('#purchase_rate').attr('disabled',false);
            $('#sale_rate').attr('disabled',false);
		
	    }else if($('#package_name').val() == 'Single-Piece'){
            $('#carton_quantity_section').hide();
            $('#items_package_name_section').hide();
            $('#box_quantity_section').hide();
            $('#sub_items_package_name_section').hide();
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').show();
            $('#purchase_rate').attr('disabled',false);
            $('#sale_rate').attr('disabled',false);
	    }else{
            $('#carton_quantity_section').hide();
            $('#items_package_name_section').hide();
            $('#box_quantity_section').hide();
            $('#sub_items_package_name_section').hide();
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').hide();
	    }
	});
	
	$('#items_package_name').change(function(){
	    if($('#items_package_name').val() == 'Box'){
            $('#box_quantity_section').show();
            $('#sub_items_package_name_section').show();
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').hide();
	    }else if($('#items_package_name').val() == 'Strip'){
            $('#box_quantity_section').hide();
            $('#sub_items_package_name_section').hide();
            $('#strip_quantity_section').show();
            $('#piece_quantity_section').show();
            $('#purchase_rate').attr('disabled',false);
            $('#sale_rate').attr('disabled',false);
	    }else{
            $('#box_quantity_section').hide();
            $('#sub_items_package_name_section').hide();
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').show();
            $('#purchase_rate').attr('disabled',false);
            $('#sale_rate').attr('disabled',false);
	    }
	});
	
	$('#sub_items_package_name').change(function(){
	    if($('#sub_items_package_name').val() == 'Strip'){
            $('#strip_quantity_section').show();
            $('#piece_quantity_section').show();
            $('#purchase_rate').attr('disabled',false);
            $('#sale_rate').attr('disabled',false);
	    }else{
            $('#strip_quantity_section').hide();
            $('#piece_quantity_section').show();
            $('#purchase_rate').attr('disabled',false);
            $('#sale_rate').attr('disabled',false);
	    }
	});
	
	
			    
			   
	
	$('#sale_rate').keyup(function(){
	    
	    var carton_qty = $('#carton_qty').val();
	    var box_qty    = $('#box_qty').val();
	    var strip_qty  = $('#strip_qty').val();
	    var piece_qty  = $('#piece_qty').val();
	    var sale_rate = $('#sale_rate').val();
	    
	    if(carton_qty !== ''){
            if(carton_qty == 0){
                var carton_sales_rate = 0;
                $('#carton_sales_rate_section').hide();
            }else{
                var carton_sales_rate = (piece_qty / carton_qty) * sale_rate;
                $('#carton_sales_rate_section').show();
            }
    //		var carton_sales_rate = sale_rate / carton_qty;
            $('#carton_sales_rate').val(carton_sales_rate);
            
                
	    }
	    if(box_qty !== ''){
            if(box_qty == 0){
                var box_sales_rate = 0;
                $('#box_sales_rate_section').hide();
            }else{
                var box_sales_rate = (piece_qty / box_qty) * sale_rate;
                $('#box_sales_rate_section').show();
            }
            $('#box_sales_rate').val(box_sales_rate);
            
	    }
	    if(strip_qty !== ''){
            if(strip_qty == 0){
                var strip_sales_rate = 0;
                $('#strip_sales_rate_section').hide();
            }else{
                var strip_sales_rate = (piece_qty / strip_qty) * sale_rate;
                $('#strip_sales_rate_section').show();
            }
            $('#strip_sales_rate').val(strip_sales_rate);
            
	    }
	    if(piece_qty !== ''){
            var piece_sales_rate = sale_rate;
            $('#piece_sales_rate').val(piece_sales_rate);
            $('#piece_sales_rate_section').show();
	    }
	});
	
	$('#carton_qty').keyup(function(){
	    var carton_qty = $('#carton_qty').val();
	    var sale_rate = $('#sale_rate').val();
	    if(carton_qty !== ''){
		if(carton_qty == 0){
		    var carton_sales_rate = 0;
		}else{
		    var carton_sales_rate = (piece_qty / carton_qty) * sale_rate;
		}
		$('#carton_sales_rate').val(carton_sales_rate);
			
	    }
	});
	$('#box_qty').keyup(function(){
	    var box_qty = $('#box_qty').val();
	    var sale_rate = $('#sale_rate').val();
	    if(box_qty !== ''){
		if(box_qty == 0){
		    var box_sales_rate = 0;
		}else{
		    var box_sales_rate = (piece_qty / box_qty) * sale_rate;
		}
		
		$('#box_sales_rate').val(box_sales_rate);
	    }
	});
	
	$('#strip_qty').keyup(function(){
	    var strip_qty = $('#strip_qty').val();
	    var sale_rate = $('#sale_rate').val();
	    if(strip_qty !== ''){
            if(strip_qty == 0){
                var strip_sales_rate = 0;
            }else{
                var strip_sales_rate = (piece_qty / strip_qty) * sale_rate;
            }
		
		$('#strip_sales_rate').val(strip_sales_rate);
		
	    }
	});
	$('#piece_qty').keyup(function(){
	    var piece_qty = $('#piece_qty').val();
	    var sale_rate = $('#sale_rate').val();
	    if(piece_qty !== ''){
		var piece_sales_rate = sale_rate;
		$('#piece_sales_rate').val(piece_sales_rate);
	    }
	});
	
	
    });
</script>
@endsection

    





