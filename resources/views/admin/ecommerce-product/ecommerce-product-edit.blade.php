@extends('admin.master')

@section('page_title')
Edit Ecommerce Product
@endsection

@section('admin_main_content')
<link href="{{asset('public/admin-frontend-assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">

<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">shopping_basket</i> Edit Ecommerce Product</li>
    </ol>
</div>  
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EDIT ECOMMERCE PRODUCT
                    </h2>    
		    <div class="btn-group pull-right header-button">
			<button type="button" class="btn bg-brown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			     <i class="material-icons">view_list</i> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu action-menu">
			    <li><a href="{{url('/branch-product/manage/')}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Branch Product List"> Branch Product List</a></li>

			    <li><a href="{{url('/ecommerce-product/manage/')}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ecommerce Product List"> Ecommerce Product List</a></li>
			</ul>
		    </div>
                </div>
                
                <div class="body">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$success_message}}
                    </div>
                    @endif
                    @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$error_message}}
                    </div>
                    @endif
                    
                    <form method="POST" action="{{ url('/ecommerce-product/update') }}" id="edit_ecommerce_product"  enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$product_data->id}}" class="form-control"/>
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Basic Section</legend>
                            <div class="col-md-12">
                               
                                <div class="col-md-4">
                                    <label for="product_name">Product Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                    <div class="form-line{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                    <input type="text" id="product_name" name="product_name" value="{{$product_data->product_name}}" class="form-control" placeholder="Enter product name">
                                    </div>  
                                    @if ($errors->has('product_name'))
                                        <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('product_name') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-4" id="category_section">
                                    <label for="category_id">Category Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('category_id') ? ' has-error' : '' }}" style="z-index:9;">                                
                                            <select class="form-control show-tick" name="category_id" id="category_id" data-live-search="true">
                                                <option value="">Please Select</option>
                                                @foreach($category as $category_value)
                                                <option <?php if($product_data->category_id == $category_value->id){echo 'selected';} ?> value="{{$category_value->id}}">{{$category_value->category_name}}</option>
                                                @endforeach
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="supplier_id">Supplier Name <b style="color: red;">*</b></label>
                                    <div class="input-group">
                                        <div class="form-line{{ $errors->has('supplier_id') ? ' has-error' : '' }}" style="z-index:9;">                                
                                            <select class="form-control show-tick" name="supplier_id" id="supplier_id" data-live-search="true" >
                                                <option value="">Please Select</option>
                                                @foreach($supplier as $supplier_value)
                                                <option <?php if($product_data->supplier_id == $supplier_value->id){echo 'selected';} ?> value="{{$supplier_value->id}}">{{$supplier_value->supplier_name}} <span>({{$supplier_value->company_name}})</span></option>
                                                @endforeach
                                            </select>                               
                                        </div>  
                                        @if ($errors->has('supplier_id'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('supplier_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4" id="product_image_section">
                                    <label for="product_image">Product Image <b style="color: red;">*</b></label>
                                    <div class="input-group image-preview">
                                        <div class="form-line{{ $errors->has('product_image') ? ' has-error' : '' }}">
                                            <input type="file" id="product_image" name="product_image" class="form-control" placeholder="Select product image">
        
                                        </div>  
                                        @if ($errors->has('product_image'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('product_image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                        <img src="{{asset('public/admin-frontend-assets/product-image/'.$product_data->product_image)}}" style="max-width:250px;max-height:200px;"/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="the-fieldset" id="diffuser_fieldset">
                            <legend class="the-legend">Package Section</legend>
                            <div class="col-md-12">
                               
                                <div class="col-md-4" id="carton_quantity_section">
                                    <label for="items_in_pack">No of Carton <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('carton_qty') ? ' has-error' : '' }}">
                                        <input type="text" id="carton_qty" name="carton_qty" value="{{$product_data->carton_qty	}}" class="form-control" value="0">
                                        </div> 
                                        @if ($errors->has('carton_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('carton_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-4" id="box_quantity_section">
                                    <label for="box_qty">No of Box <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('box_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="box_qty" name="box_qty" value="{{$product_data->box_qty	}}" class="form-control" value="0">
                                        </div> 
                                        @if ($errors->has('box_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('box_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                               
                    
                     <div class="col-md-4" id="strip_quantity_section">
                                    <label for="strip_qty">No of Strip <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('strip_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="strip_qty" name="strip_qty" value="{{$product_data->strip_qty	}}" class="form-control" value="0">
                                        </div> 
                                        @if ($errors->has('strip_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('strip_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4" id="piece_quantity_section">
                                    <label for="piece_qty">No of Piece <b style="color: red;">*</b></label>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line{{ $errors->has('piece_qty') ? ' has-error' : '' }}">
                                            <input type="text" id="piece_qty" name="piece_qty" value="{{$product_data->piece_qty	}}" class="form-control" data-rule="quantity">
                                        </div> 
                                        <span class="input-group-addon">
                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                        </span>
                                        @if ($errors->has('piece_qty'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('piece_qty') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                         </fieldset>   
			
                                         
			
			
                       
			{{-- <div style="clear: both;"></div>
                        
                        
			
			
			
			 <div style="clear: both;"></div>
			
			
			 
			  <div style="clear: both;"></div> --}}
			  
              <fieldset class="the-fieldset" id="diffuser_fieldset">
                <legend class="the-legend">Price Section</legend>
                <div class="col-md-12">    
                        <div class="col-md-4">
                            <label for="purchase_rate">Total Purchase Rate <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('purchase_rate') ? ' has-error' : '' }}">
                                    <input type="text"  id="purchase_rate" name="purchase_rate" value="{{$product_data->purchase_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('purchase_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('purchase_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="sale_rate">Total Sales Rate <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('sale_rate') ? ' has-error' : '' }}">
                                    <input type="text"  id="sale_rate" name="sale_rate" value="{{$product_data->sale_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('sale_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('sale_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
			  <div class="col-md-4" id="carton_sales_rate_section">
                            <label for="carton_sales_rate">Carton Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('carton_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="carton_sales_rate" name="carton_sales_rate" value="{{$product_data->carton_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('carton_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('carton_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="box_sales_rate_section">
                            <label for="box_sales_rate">Box Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('box_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="box_sales_rate" name="box_sales_rate" value="{{$product_data->box_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('box_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('box_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="strip_sales_rate_section">
                            <label for="strip_sales_rate">Strip Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('strip_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="strip_sales_rate" name="strip_sales_rate" value="{{$product_data->strip_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('strip_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('strip_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4" id="piece_sales_rate_section">
                            <label for="piece_sales_rate">Piece Sales Price <b style="color: red;">*</b></label>
                            <div class="input-group spinner" data-trigger="spinner">
                                <div class="form-line{{ $errors->has('piece_sales_rate') ? ' has-error' : '' }}">
                                    <input type="text" id="piece_sales_rate" name="piece_sales_rate" value="{{$product_data->piece_sales_rate}}" class="form-control" data-rule="currency">
                                </div>  
                                <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                @if ($errors->has('piece_sales_rate'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('piece_sales_rate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>   
                <fieldset class="the-fieldset" id="diffuser_fieldset">
                    <legend class="the-legend">Other Section</legend>
                    <div class="col-md-12">   
                         <div class="col-md-4" id="discount_section">
                            <label for="discount">Discount <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('weight') ? ' has-error' : '' }}">
                                    <input type="text" id="discount" name="discount" value="{{$product_data->discount}}" class="form-control" placeholder="Enter product weight">
                                </div>  
                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			  
                        <div class="col-md-4">
                            <label for="weight">Weight <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('weight') ? ' has-error' : '' }}">
                                    <input type="text" id="weight" name="weight" value="{{$product_data->weight}}" class="form-control" placeholder="Enter product weight">
                                </div>  
                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="manufacturing_date">Manufacturing Date <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="manufacturing_date" value="{{$product_data->manufacturing_date}}" id="manufacturing_date"  autocomplete="off" placeholder="Please choose a date...">
                                </div> 
                                @if ($errors->has('manufacturing_date'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('manufacturing_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label for="expiry_date">Expiry Date <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="expiry_date" value="{{$product_data->expiry_date}}" id="expiry_date" autocomplete="off" placeholder="Please choose a date...">
                                </div> 
                                @if ($errors->has('expiry_date'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('expiry_date') }}</strong>
                                    </span>
                                @endif
                                <span id="error_date" style="color:red;"></span>
                            </div>
                        </div>
                        
                        
                        <div class="col-md-4">
                            <label for="rack_number">Rack Number <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('rack_number') ? ' has-error' : '' }}">
                                    <input type="text" id="rack_number" name="rack_number" value="{{$product_data->rack_number}}" class="form-control" placeholder="Enter rack number">
                                </div>  
                                @if ($errors->has('rack_number'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('rack_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="reorder_level">Reorder Level <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('reorder_level') ? ' has-error' : '' }}">
                                    <input type="text" id="reorder_level" name="reorder_level" value="{{$product_data->reorder_level}}" class="form-control" placeholder="Enter reorder level">
                                </div>  
                                @if ($errors->has('reorder_level'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('reorder_level') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			  
			  
			  
			  <div class="col-md-12" id="product_description">
                            <label for="description">Product Description <b style="color: red;">*</b></label>
                            <div class="input-group">
                                <div class="form-line{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <textarea name="description"  class="form-control editable">{{$product_data->description}}</textarea>
                                </div>  
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        </div>
                </fieldset>
                        <br>
                        <div class="col-md-12">
                            <div class="input-group"> 
                                    <a href="{{url('/ecommerce-product/manage')}}" type="submit" class="btn bg-blue-grey waves-effect">
                                        <i class="material-icons">clear</i>
                                        <span>CANCEL</span>
                                    </a>
                                <button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;" id="submit-btn">
                                    <i class="material-icons">publish</i>
                                    <span>UPDATE</span>
                                </button>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>                      
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->
    <script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/admin-frontend-assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#manufacturing_date').datetimepicker({
        //language:  'fr',
	 direction: "auto",
	format: "yyyy-mm-dd",
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 4
    });
    $('#expiry_date').datetimepicker({
        //language:  'fr',
	 direction: "auto",
	format: "yyyy-mm-dd",
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 4
    });
    $(document).on('click','#submit-btn',function(){
        var mfg_date = $('#manufacturing_date').val();
        var exp_date = $('#expiry_date').val();
        if(exp_date <= mfg_date){
            $('#error_date').text('Expiry date must be greater than manufacturing date!');
            return false;
        }else{
            $('#error_date').text('');
            return true;
        }
        $('#edit_ecommerce_product').submit();
    });
</script>
</div>
@endsection

    





