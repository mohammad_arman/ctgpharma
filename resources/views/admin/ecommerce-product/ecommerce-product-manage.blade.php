@extends('admin.master')

@section('page_title')
Ecommerce Product Manage
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Ecommerce Product Manage</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                        ECOMMERCE PRODUCT MANAGEMENT
                    </h2>
		    <a href="{{url('/product/add')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">add_box</i> ADD PRODUCT
			</button>
		    </a>
                </div>
                <div class="body" style="min-height: 500px;">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <div class="table-responsive" >
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" style="min-height: 500px;">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Product</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Company</th>

		    <th>Carton</th>
                                    <th>Box</th>
                                    <th>Strip</th>
                                    <th>Piece</th>		    
                                    
                                    <th>Ctn Sale Rate</th>
                                    <th>Box Sale Rate</th>
                                    <th>Strip Sale Rate</th>
                                    <th>Piece Sale Rate</th>
                                    <th>Discount(%)</th>
                                    <th>Weight</th>
                                    <th>Exp Date</th>
                                    <th>RackName</th>
                                    <th>Stock</th>
                                    <th>Publication Status</th>
                                    <th>Featured Product</th>
                                    <th>Reorder Level</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp
                                @foreach($product_info as $product)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>{{$product->product_name}}</td>
                                    <td>
			
			<img src="{{asset('public/admin-frontend-assets/product-image/'.$product->product_image)}}" style="width:100px;height:100px;"/>
			
		    </td>
                                    <td>{{$product->category_name}}</td>
                                    <td>
                                        <?php
                                          $company_id = $product->company_id;
       
                                          $company_name = DB::table('companies')->where('id',$company_id)->first();
                                          echo $company_name->company_name;
                                          ?>
                                    </td>
                                    <td>{{$product->carton_qty}}</td>
                                    <td>{{$product->box_qty}}</td>
                                    <td>{{$product->strip_qty}}</td>
                                    <td>{{$product->piece_qty}}</td>
                                    
                                    <td>{{number_format($product->carton_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->box_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->strip_sales_rate,2)}}Tk</td>
                                    <td>{{number_format($product->piece_sales_rate,2)}}Tk</td>
                                    <td>{{$product->discount}}</td>
                                    <td>{{$product->weight}}</td>
                                    <td>{{$product->expiry_date}}</td>
                                    <td>{{$product->rack_number}}</td>
                                    <td>
                                        @if($product->piece_qty > 0)
                                            <span class="label bg-teal">Stock In</span>
                                        @else
                                            <span class="label bg-red">Stock Out</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($product->publication_status == 1)
                                            <span class="label bg-blue">Published</span>
                                        @else
                                            <span class="label bg-brown">Unpublished</span>
                                        @endif
                                    </td>
                                    
                                    
                                    <td>
                                        @if($product->feature_product == 1)
                                            <span class="label bg-green">Featured</span>
                                        @else
                                            <span class="label bg-pink">Unfeatured</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($product->reorder_level >= $product->piece_qty)
                                            <span class="label bg-red">Reorder Product</span>
                                        @else
                                            <span class="label bg-blue">Not Now</span>
                                        @endif
                                    </td>
                                    <td>
					<div class="btn-group">
					    <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
					    <ul class="dropdown-menu action-menu">
						<li><a href="{{url('/ecommerce-product/edit/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$product->product_name}}"><i class="material-icons">mode_edit</i> Edit</a></li>
						<li><a href="{{url('/ecommerce-product/view/'.$product->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$product->product_name}} Details"><i class="material-icons">visibility</i> View</a></li>
						<li><a href="{{url('/ecommerce-product/delete/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$product->product_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$product->product_name}}"><i class="material-icons">delete</i> Delete</a></li>
						@if($product->feature_product == 0)
						<li><a href="{{url('/ecommerce-product/featured/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to make {{$product->product_name}} feature product?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Make {{$product->product_name}} Feature Product"><i class="material-icons">add</i> Feature</a></li>						
						@else
						<li><a href="{{url('/ecommerce-product/unfeatured/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to make {{$product->product_name}} unfeature product?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Make {{$product->product_name}} Un-feature Product"><i class="material-icons">clear</i> Un-feature</a></li>						
						@endif
						@if($product->publication_status == 0)
						<li><a href="{{url('/ecommerce-product/published/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to publish {{$product->product_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Publish {{$product->product_name}}"><i class="material-icons">thumb_up</i> Publish</a></li>						
						@else
						<li><a href="{{url('/ecommerce-product/unpublished/'.$product->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to unpublish {{$product->product_name}}?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Un-publish {{$product->product_name}}"><i class="material-icons">thumb_down</i> Un-publish</a></li>						
						@endif
					    </ul>
					</div>
<!--                                        <a href="{{url('/ecommerce-product/edit/'.$product->id)}}" style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit This Supplier Details">
                                            <button type="button" class="btn bg-light-blue waves-effect">
                                                <i class="material-icons">edit</i>
                                            </button>
                                        </a>
                                        <a href="{{url('/ecommerce-product/delete/'.$product->id)}}" style="text-decoration:none;" onclick="return confirm('Are you sure to delete this supplier?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete This Supplier Details">
                                            <button type="button" class="btn bg-pink waves-effect">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

