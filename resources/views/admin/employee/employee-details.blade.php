@extends('admin.master') 
@section('page_title') View Employee Details
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i> View Employee Details</li>
    </ol>
</div>
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW EMPLOYEE DETAILS
                    </h2>
                    <a href="{{url('/employee/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>

                <div class="body">
                    <div class="user-info col-md-12" style="padding-left:10px;">
                            @if ($employees_data->image == '')
                                <img src="{{asset('public/admin-frontend-assets/images/user.png')}}" style="max-width:120px;max-height:120px;" alt="{{$employees_data->role_name}}" />
                            @else
                                <img src="{{asset('public/admin-frontend-assets/employee_image/'.$employees_data->image)}}" style="max-width:120px;max-height:120px;" alt="{{$employees_data->role_name}}" />
                            @endif
                    </div>
                    <ul class="view-list col-md-12">
                        <li>
                            <span>Name:</span>
                            <span>{{$employees_data->name}}</span>
                        </li>
                        <li>
                            <span>Email:</span>
                            <span>{{$employees_data->email}}</span>
                        </li>
                        <li>
                            <span>Role:</span>
                            <span>{{$employees_data->role_name}}</span>
                        </li>
                        <li>
                            <span>Branch Name:</span>
                            <span>{{$employees_data->branch_name}}</span>
                        </li>

                    </ul>
                    <div class="col-md-12" style="padding-left:8px;">
                        <a href="{{url('/employee/manage')}}">
                            <button type="button" class="btn bg-light-blue waves-effect pull-left" >
                            <i class="material-icons">arrow_back</i> BACK
                            </button>
                        </a>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection