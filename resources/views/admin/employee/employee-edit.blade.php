@extends('admin.master') 
@section('page_title') Edit Employee
@endsection
 
@section('admin_main_content')

<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">playlist_add</i> Edit Employee</li>
    </ol>
</div>
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EDIT EMPLOYEE
                    </h2>
                    <a href="{{url('/employee/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>

                <div class="body">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        
                        {{$success_message}}
                    </div>
                    @endif @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        
                        {{$error_message}}
                    </div>
                    @endif

                    <form id="update_employee" method="POST" action="{{ url('employee/update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" id="user_id" name="user_id" value="{{$employee_data->user_id}}" class="form-control">
                        <label for="name">Name <b style="color: red;">*</b></label>
                        <div class="form-group">
                            <div class="form-line{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" id="name" name="name" value="{{$employee_data->name}}" class="form-control" placeholder="Enter employee name">
                            </div>
                            @if ($errors->has('name'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('name') }}</strong>
                            </span> @endif
                        </div>

                        <label for="email">Email <b style="color: red;">*</b></label>
                        <div class="form-group">
                            <div class="form-line{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" id="email" name="email" value="{{$employee_data->email}}" class="form-control" placeholder="Enter employee email">
                            </div>
                            @if ($errors->has('email'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('email') }}</strong>
                            </span> @endif
                        </div>

                        <label for="password">Password <b style="color: red;">*</b></label>
                        <div class="form-group">
                            <div class="form-line{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" id="password" name="password" value="{{$employee_data->password}}" class="form-control" placeholder="Enter employee password">
                            </div>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('password') }}</strong>
                            </span> 
                            @endif
                        </div>

                        <label for="password-confirm">Confirm Password <b style="color: red;">*</b></label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="password" id="password-confirm" name="password_confirmation"  value="{{$employee_data->password}}" class="form-control" placeholder="Enter password again">
                            </div>
                        </div>


                            <label for="branch_id">Branch Name <b style="color: red;">*</b></label>
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('branch_id') ? ' has-error' : '' }}" style="z-index:100000;">
                                    <select class="form-control show-tick" name="branch_id" id="branch_id">
                                        @foreach($branch as $branch_list)
                                        <option <?php if($branch_list->id == $employee_data->branch_id){echo 'selected'; }?> value="{{$branch_list->id}}">{{$branch_list->branch_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('branch_id'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('branch_id') }}</strong>
                                    </span> 
                                @endif
                            </div>

                            <label for="role_id">Role Name <b style="color: red;">*</b></label>
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('role_id') ? ' has-error' : '' }}" style="z-index:16;">
                                    <select class="form-control show-tick" name="role_id" id="role_id">
                                    
                                        @foreach($role as $role_list)
                                        <option <?php if($role_list->id == $employee_data->role_id){ echo 'selected'; }?> value="{{$role_list->id}}">{{$role_list->role_name}}</option>
                                        
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('role_id'))
                                <span class="help-block">
                                    <strong style="color: red;">{{ $errors->first('role_id') }}</strong>
                                </span> 
                                @endif
                            </div>

                            <label for="product_image">Image</label>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="file" id="image" name="image" class="form-control" placeholder="Select product image">
                                </div> 
                            </div>
                            <div>
                                @if ($employee_data->image == '')
                                    <img src="{{asset('public/admin-frontend-assets/images/user.png')}}" style="max-width:120px;max-height:120px;" alt="{{$employee_data->role_name}}" />
                                @else
                                    <img src="{{asset('public/admin-frontend-assets/employee_image/'.$employee_data->image)}}" style="max-width:120px;max-height:120px;" alt="{{$employee_data->role_name}}" />
                                @endif
                            </div>
                            <br>
                            <a href="{{url('/employee/manage')}}" type="submit" class="btn bg-blue-grey waves-effect">
                                <i class="material-icons">clear</i>
                                <span>CANCEL</span>
                            </a>
                            <button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
                                <i class="material-icons">publish</i>
                                <span>UPDATE</span>
                            </button>

                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection