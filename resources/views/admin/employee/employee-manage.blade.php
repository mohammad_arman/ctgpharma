@extends('admin.master')

@section('page_title')
Manage Employee
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Employee</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                        EMPLOYEE MANAGEMENT
                    </h2>
		    <a href="{{url('/add-employee')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">add_box</i> ADD EMPLOYEE
			</button>
		    </a>
                </div>
                <div class="body" style="min-height: 500px;">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$success_message}}
                    </div>
                    @endif
                    @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$error_message}}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" >
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @php $i=1 @endphp
                                @foreach($employees_data as $employee)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>
                                        @if ($employee->image == '')
                                            <img src="{{asset('public/admin-frontend-assets/images/user.png')}}" width="48" height="48" alt="{{$employee->role_name}}" />
                                        @else
                                            <img src="{{asset('public/admin-frontend-assets/employee_image/'.$employee->image)}}" width="48" height="48" alt="{{$employee->role_name}}" />
                                        @endif
                                        
                                    </td>
                                    <td>{{$employee->name}}</td>
                                    <td>{{$employee->email}}</td>
                                    <td>{{$employee->role_name}}</td>
                                    <td>{{$employee->branch_name}}</td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="material-icons">view_list</i> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu action-menu">

                                                <li><a href="{{url('/employee/edit/'.$employee->user_id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$employee->name}}"><i class="material-icons">mode_edit</i> Edit</a></li>

                                                <li><a href="{{url('/employee/view/'.$employee->user_id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$employee->name}} Details"><i class="material-icons">visibility</i> View</a></li>

                                                <li><a href="{{url('/employee/delete/'.$employee->user_id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete {{$employee->name}} ?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$employee->name}}"><i class="material-icons">delete</i> Delete</a></li>						
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

