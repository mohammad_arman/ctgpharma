@extends('admin.master') 
@section('page_title') 
My Profile
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i>My Profile</li>
    </ol>
</div>
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        NY PROFILE
                    </h2>
                    <a href="{{url('/employee/edit/'.$user_data->user_id)}}">
                    <button type="button" class="btn bg-brown waves-effect pull-right header-button" >
                        <i class="material-icons">mode_edit</i> EDIT
                    </button>
                    </a>
                </div>

                <div class="body">
                    <div class="user-info col-md-12" style="height:200px;padding-left:10px;background-image:url('{{asset('public/admin-frontend-assets/images/profile-background.jpg')}}');background-repeat: no-repeat;background-size: cover;text-align: center;">
                            @if ($user_data->image == '')
                                <img src="{{asset('public/admin-frontend-assets/images/user.png')}}" style="width:100px;height:100px;border: 2px solid #17545d;
                                margin-top: 125px;border-radius: 10px;" alt="{{$user_data->role_name}}" />
                            @else
                                <img src="{{asset('public/admin-frontend-assets/employee_image/'.$user_data->image)}}" style="max-width:120px;max-height:120px;border: 2px solid #17545d;
                                margin-top: 128px;border-radius: 10px;" alt="{{$user_data->role_name}}" />
                            @endif
                    </div>
                    <ul class="view-list col-md-12">
                        <li>
                            <span>Name:</span>
                            <span>{{$user_data->name}}</span>
                        </li>
                        <li>
                            <span>Email:</span>
                            <span>{{$user_data->email}}</span>
                        </li>
                        <li>
                            <span>Role:</span>
                            <span>{{$user_data->role_name}}</span>
                        </li>
                        <li>
                            <span>Branch Name:</span>
                            <span>{{$user_data->branch_name}}</span>
                        </li>

                    </ul>
                    <div class="col-md-12" style="padding-left:8px;">
                        <a href="{{url('/dashboard')}}">
                            <button type="button" class="btn bg-light-blue waves-effect pull-left" >
                            <i class="material-icons">arrow_back</i> BACK
                            </button>
                        </a>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection