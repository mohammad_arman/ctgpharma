@extends('admin.master') 
@section('page_title') Add Expense
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">playlist_add</i> Add Expense</li>
    </ol>
</div>
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD EXPENSE
                    </h2>
                    <a href="{{url('/expense/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>

                <div class="body">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$success_message}}
                    </div>
                    @endif @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$error_message}}
                    </div>
                    @endif
                    <form method="POST" action="{{ url('/expense/save') }}">
                        {{ csrf_field() }}
                        <div class="col-md-12" style="margin-bottom: 0px;">
                            <div class="col-md-6" style="margin-bottom: 0px;">
                                <label for="expense_name">Expense Name<b style="color: red;"> *</b></label>
                                <div class="form-group">
                                    <div class="form-line{{ $errors->has('expense_name') ? ' has-error' : '' }}">
                                        <input type="text" id="expense_name" name="expense_name" class="form-control" placeholder="Enter expense name">
                                    </div>
                                    @if ($errors->has('expense_name'))
                                    <span class="help-block">
					    <strong style="color: red;">{{ $errors->first('expense_name') }}</strong>
					</span> @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 0px;">
                            <div class="col-md-6" style="margin-bottom: 0px;">
                                <label for="branch_id">Branch Name <b style="color: red;">*</b></label>
                                <div class="input-group">
                                    <div class="form-line{{ $errors->has('branch_id') ? ' has-error' : '' }}" style="z-index:8;">
                                        <select class="form-control show-tick" name="branch_id" id="branch_id" >
                                        <option value="">Please Select</option>
                                       @foreach($branch_list as $branch)
                                        <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    @if ($errors->has('branch_id'))
                                    <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('branch_id') }}</strong>
                                    </span> @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 0px;">
                            <div class="col-md-6" style="margin-bottom: 0px;">
                                <label for="amount">Amount(Tk) <b style="color: red;">*</b></label>
                                <div class="input-group spinner" data-trigger="spinner">
                                    <div class="form-line{{ $errors->has('amount') ? ' has-error' : '' }}">
                                        <input type="text" id="amount" name="amount" class="form-control" data-rule="currency">
                                    </div>
                                    <span class="input-group-addon">
                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                        </span> @if ($errors->has('amount'))
                                    <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('amount') }}</strong>
                                            </span> @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <div class="col-md-6" style="margin-bottom: 0px;">
                                    <button type="reset" class="btn bg-blue-grey waves-effect">
                                            <i class="material-icons">cached</i>
                                            <span>RESET</span>
                                        </button>
                                <button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
                                    <i class="material-icons">save</i>
                                    <span>SAVE</span>
                                </button>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection