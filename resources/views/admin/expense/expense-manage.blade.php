@extends('admin.master') 
@section('page_title') Manage Expense
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Expense</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EXPENSE MANAGEMENT
                    </h2>
                    <a href="{{url('/expense/add')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">add_box</i> ADD Expense
			</button>
		    </a>
                </div>
                <div class="body">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$success_message}}
                    </div>
                    @endif @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$error_message}}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Expense Name</th>
                                    <th>Branch Name</th>
                                    <th class="text-right">Amount(Tk)</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 
@endphp @foreach($expense_data as $expense)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$expense->expense_name}}</td>
                                    <td>{{$expense->branch->branch_name}}</td>
                                    <td class="text-right">{{number_format($expense->amount,2)}}</td>
                                    <td>{!! date('F j, Y', strtotime($expense->date)) !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 <i class="material-icons">view_list</i> <span class="caret"></span>
					    </button>
                                            <ul class="dropdown-menu action-menu">
                                                <li><a href="{{url('/expense/edit/'.$expense->id)}}" class=" waves-effect waves-block"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{$expense->expense_name}}"><i class="material-icons">mode_edit</i> Edit</a></li>
                                                <li><a href="{{url('/expense/view/'.$expense->id)}}" class=" waves-effect waves-block"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$expense->expense_name}} Details"><i class="material-icons">visibility</i> View</a></li>
                                                <li><a href="{{url('/expense/delete/'.$expense->id)}}" class=" waves-effect waves-block"
                                                        onclick="return confirm('Are you sure to delete {{$expense->expense_name}}?');"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$expense->expense_name}}"><i class="material-icons">delete</i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>
@endsection