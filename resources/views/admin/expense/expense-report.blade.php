@extends('admin.master') 
@section('page_title') Expense Report
@endsection
 
@section('admin_main_content')
<link href="{{asset('public/admin-frontend-assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">

<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Expense Report</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EXPENSE REPORT
                    </h2>
                   
                </div>
                <div class="body">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$success_message}}
                    </div>
                    @endif @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$error_message}}
                    </div>
                    @endif
                    <div class="row clearfix">
                            {{-- <div class="col-md-4">
                                <label for="expense_name">Start Date<b style="color: red;"> *</b></label>
                                <div class="form-group">
                                    <div class="form-line">
                                            <input type='text' name="start_date" class="form-control start_date" autocomplete="off" placeholder="Pick Start Date" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <label for="expense_name">End Date<b style="color: red;"> *</b></label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type='text' name="end_date" class="form-control end_date" autocomplete="off" placeholder="Pick End Date" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="expense_name"></label>
                                <div class="form-group">
                                    <button type="submit" name="search" class="btn btn-info btn-flat pull-left search">Search</button>
                                </div>
                            </div> --}}
                            <div class="col-md-12">
                                {{-- <label for="expense_name"></label>
                                <div class="form-group"> --}}
                                    <button id="print" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-print"></span> Print me</button>
                                    
                                {{-- </div> --}}
                            </div>
                            {{-- <div style="clear:both;"></div> --}}
                        </div>
                    <div class="table-responsive">
                            
                        <table id="example" class="table table-bordered table-striped table-hover js-basic-example dataTable" >
                            <caption style="text-align:center;font-size: 18px;font-weight: bold;">Expense Report</caption>
                            <thead>
                                <tr>
                                    <th style="text-align:left;border:1px solid #d2d6de;padding: 5px;">SL NO.</th>
                                    <th style="text-align:left;border-top:1px solid #d2d6de;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">Expense Name</th>
                                    <th style="text-align:left;border-top:1px solid #d2d6de;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">Branch Name</th>
                                    <th style="text-align:right;border-top:1px solid #d2d6de;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">Amount(Tk)</th>
                                    <th style="text-align:center;border-top:1px solid #d2d6de;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">Date</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php 
                                    $i=1; 
                                    // echo '<pre>';print_r($expense_data->toArray());exit;
                                ?>
                                
                                @foreach($expense_data as $expense)
                                <tr>
                                    <td style="text-align:left;border-left:1px solid #d2d6de;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">{{$i++}}</td>
                                    <td style="text-align:left;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">{{$expense->expense_name}}</td>
                                    <td style="text-align:left;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">{{$expense->branch->branch_name}}</td>
                                    <td style="text-align:right;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">{{number_format($expense->amount,2)}}</td>
                                    <td style="text-align:center;border-bottom: 1px solid #d2d6de;border-right: 1px solid #d2d6de;padding: 5px;">{!! date('F j, Y', strtotime($expense->date)) !!}</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script src="{{asset('public/admin-frontend-assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script>
    //  $('.start_date').datetimepicker({
	//  direction: "auto",
	// format: "yyyy-mm-dd",
    // weekStart: 1,
    // todayBtn:  1,
	// autoclose: 1,
	// todayHighlight: 1,
	// startView: 4
    // });
    // $('.end_date').datetimepicker({
	//  direction: "auto",
	// format: "yyyy-mm-dd",
    // weekStart: 1,
    // todayBtn:  1,
	// autoclose: 1,
	// todayHighlight: 1,
	// startView: 4
    // });

$(document).ready(function(){
    // $(document).on('click','.search',function(){
    //     // alert(year+"----"+branch);
    //     var start_date = $('.start_date').val();
    //     var end_date = $('.end_date').val();
    //     if(end_date < start_date){
    //         alert('Start date must be less than end date!');
    //     }else{
    //         expenseData(start_date,end_date);
    //     }
       
    // });

    // function expenseData(start_date ='',end_date =''){
    //     // alert(start_date+"----"+end_date);
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             } 
    //     });
    //     $.ajax({
    //         url:"{{url('/expense/data')}}",
    //         method:"POST",
    //         data:{start_date:start_date,end_date:end_date},
    //         success:function(data){
    //             console.log(data);
    //             $("tbody").html(data.replace(/<br\s*[\/]?>/gi, ""));
    //         }
    //     });
    // }
    function printData()
	{
	   var divToPrint=document.getElementById("example");
	   newWin= window.open("");
	   newWin.document.write(divToPrint.outerHTML);
	   newWin.print();
	   newWin.close();
	}

	$('#print').on('click',function(){
	printData();
	})
});
</script>
</div>
@endsection