@extends('admin.master')

@section('page_title')
View Expense
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i> View Expense</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW EXPENSE DETAILS
                    </h2>           
		    <a href="{{url('/expense/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                       <ul class="view-list col-md-12">
		<li>
		    <span>Expense Name:</span>
		    <span>{{$expense->expense_name}}</span>
		</li>
		<li>
		    <span>Branch Name:</span>
		    <span>{{$expense->branch->branch_name}}</span>
		</li>
		<li>
		    <span>Amount(Tk):</span>
		    <span>{{number_format($expense->amount,2)}}</span>
		</li>
		<li>
		    <span>Date:</span>
		    <span>{{date('F j, Y', strtotime($expense->date))}}</span>
		</li>
		
	    </ul>
	    <div class="col-md-12">
		<a href="{{url('/expense/manage')}}">
		    <button type="button" class="btn bg-light-blue waves-effect pull-left" >
			<i class="material-icons">arrow_back</i> BACK
		    </button>
		</a>
	    </div>
                </div>
                 <div style="clear: both;"></div> 
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection




