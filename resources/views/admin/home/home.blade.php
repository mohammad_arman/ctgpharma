@extends('admin.master') 
@section('page_title') Dashboard
@endsection
 
@section('admin_main_content')
<link href="{{asset('public/admin-frontend-assets/css/morris.css')}}" rel="stylesheet">
  
<div class="container-fluid">
    <div class="block-header">
        <h2>DASHBOARD</h2>
    </div>

    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">shopping_cart</i>
                </div>
                <div class="content">
                    <div class="text">NEW ORDERS</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_orders}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">insert_drive_file</i>
                </div>
                <div class="content">
                    <div class="text">NEW INVOICES</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_invoices}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">NEW CUSTOMERS</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_customers}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text " style="text-transform:uppercase;">
                        <?php echo date('F');?> ECOMMERCE INCOME</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_current_month_ecommerce_income}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-deep-purple hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="content">
                    <div class="text " style="text-transform:uppercase;">
                        <?php echo date('F');?> ECOMMERCE EXPENSE</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_current_month_ecommerce_expense}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-brown hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">monetization_on</i>
                </div>
                <div class="content">
                    <div class="text " style="text-transform:uppercase;">
                        <?php echo date('F');?> ECOMMERCE PROFIT</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_current_month_ecommerce_profit}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-indigo hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text " style="text-transform:uppercase;">
                        <?php echo date('F');?> BRANCH INCOME</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_current_month_branch_income}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-teal hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">money_off</i>
                    </div>
                    <div class="content">
                        <div class="text " style="text-transform:uppercase;">
                            <?php echo date('F');?> BRANCH EXPENSE</div>
                        <div class="number count-to" data-from="0" data-to="{{$total_current_month_branch_expense}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-blue-grey hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="content">
                        <div class="text " style="text-transform:uppercase;">
                            <?php echo date('F');?> BRANCH PROFIT</div>
                        <div class="number count-to" data-from="0" data-to="{{$total_current_month_branch_profit}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
    </div>
    <!-- #END# Widgets -->
    <!-- CPU Usage -->
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                
                <div class="body">
                    {{--
                    <div id="real_time_chart" class="dashboard-flot-chart"></div> --}}
                    <div class="row">
                        <div class="col-md-12  col-sm-12 col-xs-12" id="graph-section">
                            <div class="graph" style="width:100%;min-height: 500px;border: 1px solid #00BCD4;background: #fff;">
                                <div class="graph-header" style="width:100%;min-height: 100px;border-bottom: 2px solid #00BCD4;">
                                    <div class=" col-md-4 col-sm-12 col-xs-12 graph-header-left-side">
                                        <h4 style="color:rgb(156, 39, 176);">STATISTICS</h4>
                                        <div class="col-md-3" style="padding-left:0;margin-bottom: 0;">
                                            <div class="col-md-12" style="padding-left:0;margin-bottom: 0;" id="select_year">
                                                <select id="year-selector" style="background: none;width:80px;">
                                                    <option value="<?=date("Y");?>" selected><?=date("Y");?></option>
                                                    <option value="<?=date("Y")-1;?>"><?=date("Y")-1;?></option>
                                                    <option value="<?=date("Y")-2;?>"><?=date("Y")-2;?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-7" style="margin-bottom: 0;">
                                            <div class="col-md-12" id="select_branch" style="margin-bottom: 0;">
                                                <select id="branch-selector" style="background: none;">
                                                        <option value="0">All</option>
                                                    @foreach($branch_list as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-bottom: 0;">
                                            <button type="button" class="btn btn-primary" id="go_btn">Go</button>
                                        </div>
                                    </div>
                                    <div class=" col-md-8 col-sm-12 col-xs-12 graph-header-right-side">
                                        <div class="col-md-4 col-sm-4 col-xs-4 header-right-side-title">
                                            <h4 style="color:#1f91f3;">INCOME</h4>
                                            <span style="color:#1f91f3;font-size: 18px;" id="total_yearly_income"></span>
                                            <span
                                                style="color: #1f91f3;font-size: 18px;">Tk</span>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 header-right-side-title">
                                            <h4 style="color:#E91E63 ;">EXPENSE</h4>
                                            <span style="color:#E91E63 ;font-size: 18px;" id="total_yearly_expense"></span>
                                            <span
                                                style="color:#E91E63;font-size: 18px;">TK</span>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 header-right-side-title">
                                            <h4 style="color:#8BC34A;">PROFIT</h4>
                                            <span style="color:#8BC34A;font-size: 18px;" id="total_yearly_profit"></span>
                                            <span
                                                style="color: #8BC34A;font-size: 18px;">TK</span>
                                        </div>
                                        <div style="clear:both;"></div>
                                    </div>
                                </div>
                                <div class="graph-body">
                                    <div id="bar-chart" class="col-md-12" style="min-height:400px;width: 100%;"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.row (main row)
                </div>
            </div>
        </div>
    </div>
    <!-- #END# CPU Usage -->
                    

                    
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script src="{{asset('public/admin-frontend-assets/js/graph-js/raphael-min.js')}}"></script>
<script src="{{asset('public/admin-frontend-assets/js/graph-js/morris.min.js')}}"></script>

<script>

$(document).ready(function(){
    //alert("ok");
    if($('#year-selector option:selected')){
        var year = $('#year-selector option:selected').val();
    }
    // $('#year-selector').change(function(){
    //     var year = $('#year-selector').val();
    // });

    if($('#branch-selector option:selected')){
        var branch = $('#branch-selector option:selected').val();
        chartYear(year,branch);
    }
    // $('#branch-selector').change(function(){
    //     var branch = $(this).val();
    //     chartYear(year,branch);
    // });
    $(document).on('click','#go_btn',function(){
        // alert(year+"----"+branch);
        var year = $('#year-selector').val();
        var branch = $('#branch-selector').val();
        chartYear(year,branch);
    });

    function chartYear(year ='',branch =''){
        // alert(year+"----"+branch);
        $("#bar-chart").empty();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                } 
        });
        $.ajax({
            url:"{{url('/branch/statistics')}}",
            method:"POST",
            data:{year:year,branch:branch},
            success:function(data){
                console.log(data);
                var json = $.parseJSON(data);
                console.log(json);
                
                var chartObject = {
                    year:json.year, income:json.income, expense:json.expense,profit:json.totalProfit
                };
                $("#total_yearly_income").text(json.total_yearly_income);
                $("#total_yearly_expense").text(json.total_yearly_expense);
                $("#total_yearly_profit").text(json.totalProfit);
                Morris.Bar({
                    element : 'bar-chart',
                    data:[chartObject],
                    xkey:'year',
                    ykeys:['income', 'expense','profit'],
                    labels:['Income', 'Expense', 'Profit'],
                    barColors: ["#1f91f3", "#E91E63", "#8BC34A"],
                    hideHover:'auto'
                });
                
                
            }
        });
    }
});

</script>


</div>
@endsection