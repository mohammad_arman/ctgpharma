@extends('admin.master')

@section('page_title')
Add Invoice
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">add_circle</i> Add Invoice</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD INVOICE
                    </h2>   
		    <a href="{{url('/buying-invoice/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert bg-red alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		{{session()->get('error_message')}}
	    </div>
		@endif
		<form id="invoice_form" method="POST" action="{{ url('buying-invoice/save') }}">
		{{ csrf_field() }}
		<div class="col-md-12" style="margin-bottom: 0px;">
		    <div class="col-md-6" style="margin-bottom: 0px;">
			<label for="invoice_number">Invoice Number<b style="color: red;"> *</b></label>
			<div class="form-group">
			    <div class="form-line{{ $errors->has('invoice_number') ? ' has-error' : '' }}">
				<input type="text" id="invoice_number" name="invoice_number" value="{{$invoice_number}}" class="form-control" readonly>
			    </div>  
			    @if ($errors->has('invoice_number'))
				<span class="help-block">
				    <strong style="color: red;">{{ $errors->first('invoice_number') }}</strong>
				</span>
			    @endif
			</div>
		    </div>
		    <div class="col-md-6" style="margin-bottom: 0px;">
			<label for="issue_date">Date<b style="color: red;"> *</b></label>
			<div class="form-group">
			    <div class="form-line{{ $errors->has('issue_date') ? ' has-error' : '' }}">
				<input type="text" id="issue_date" name="issue_date" value="{{$issue_date}}" class="form-control" readonly>
			    </div>  
			    @if ($errors->has('issue_date'))
				<span class="help-block">
				    <strong style="color: red;">{{ $errors->first('issue_date') }}</strong>
				</span>
			    @endif
			</div>
		    </div>
		</div>
		<div class="col-md-12" style="margin-bottom: 0px;">
		    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="supplier_id">Supplier Name<b style="color: red;"> *</b></label>
				<div class="form-group" >
					<div class="form-line {{ $errors->has('supplier_id') ? ' has-error' : '' }}" style="z-index: 10;">
						<select name="supplier_id" id="supplier_id" class="form-control show-tick" data-live-search="true">
							<option value="">Please Select</option>
							<?php echo $output;?>
						</select>
					</div>  
					@if ($errors->has('supplier_id'))
					<span class="help-block">
						<strong style="color: red;">{{ $errors->first('supplier_id') }}</strong>
					</span>
					@endif
				</div>
		    </div>
		   
		</div>
		
		<input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">
                            
		<div class="col-md-12" style="margin-bottom: 0px;">
		    <div class="col-md-12" style="margin-bottom: 0px;">
			<table class="table table-bordered table-hover table-striped" id="item_table" style="width:100%;">
			    <thead class="bg-cyan">
				<tr>
				    <th width="5%">Sr.</th>
				    <th width="25%" class="text-center">Name</th>
				    <th width="15%" class="text-center">Type</th>
				    <th width="10%" class="text-center">Qty</th>
				    <th width="20%" class="text-center">Amount(Tk)</th>
				    <th width="20%" class="text-center">Subtotal(Tk)</th>
				    <th width="5%" class="text-center">Action</th>
				</tr>  
			    </thead>
			    <tbody>
				 <!-- <tr>
				    <td><span id="sr_no">1</span></td>
				    <td>
					<select name="item_name[]" id="item_name1" data-srno="1" class="form-control input-sm item_name " required>
					    <option value="">Please Select</option>
					    <?php //echo $output; ?>
					</select>
					
				    </td>
				    <td>
				
					<select name="item_type[]" id="item_type1" data-srno='1' class="form-control input-sm item_type">
						<option value="" >Please Select</option>
						<option value="Carton">Carton</option>
                    <option value="Box">Box</option>
                    <option value="Strip">Strip</option>
                    <option value="Piece">Piece</option> 
					</select>
				    </td>
				    <td><input type="text" name="item_qty[]" id="item_qty1" data-srno="1" class="form-control input-sm item_qty text-center" required /></td>
				    <td><input type="text" name="item_price[]" id="item_price1" data-srno="1" class="form-control input-sm item_price text-right" required /></td>
				    <td><input type="text" name="item_subtotal" id="item_subtotal1" data-srno="1" class="form-control input-sm item_subtotal text-right"  readonly /></td>
				    <td></td>
				</tr>  -->

			    </tbody>
			    <tfoot id="item-table-footer">
				<tr>
				    <td colspan="5" class="text-right">Total</td>
				    <td colspan="2"><input type="text" name="total" class="form-control total text-right" readonly /></td>

				</tr>

				<tr>
				    <td colspan="5" class="text-right" >Net Payable</td>
				    <td colspan="2"><input type="text" name="sum" class="form-control sum text-right" readonly /></td>

				</tr>
				<tr>
				    <td colspan="5" class="text-right" >Paid</td>
				    <td colspan="2"><input type="text" name="paid" value="0.00" class="form-control paid text-right" /></td>

				</tr>
				<tr>
				    <td colspan="5" class="text-right" >Balance Due</td>
				    <td colspan="2"><input type="text" name="outstanding" class="form-control outstanding text-right" readonly /></td>

				</tr>
			    </tfoot>
			</table>
			<input type="hidden" name="total_item" id="total_item" value="1" />
			<button type="button" name="add" id="add" class="btn bg-blue-grey waves-effect" style="margin-bottom:10px;">Add Item</button>

		    </div>			
		</div>			
			     
                             <br>
		<div class="col-md-12">
		    <div class="col-md-12" style="margin-bottom: 0px;">
					<button type="submit" class="btn bg-light-blue waves-effect pull-right" id="save_invoice" style="margin-left:5px;">
							<i class="material-icons">save</i>
							<span>SAVE</span>
						</button>
					<button type="reset" class="btn bg-blue-grey waves-effect pull-right">
							<i class="material-icons">cached</i>
							<span>RESET</span>
						</button>
			
		    </div>
		</div>
		<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
	$("#item-table-footer").hide();
    var total_amount = $(".sum").val();
    var count = 0;
    $(document).on('click','#add',function(){
		$("#item-table-footer").show();
		count++;
		$('#total_item').val(count);
		var html_code = '';
		html_code += '<tr id="row_id_'+count+'" data-rowid="'+count+'">';

		html_code += '<td><span id="sr_no">'+count+'</span></td>';

		html_code += '<td><input type="text" name="item_name[]" id="item_name'+count+'" data-srno="'+count+'" class="form-control input-sm  item_name" ></td>';

		html_code += '<td><select name="item_type[]" id="item_type'+count+'"  data-srno="'+count+'" class="form-control input-sm item_type"><option value="">Please Select</option><option value="Carton">Carton</option><option value="Box">Box</option><option value="Strip">Strip</option><option value="Piece">Piece</option></select></td>';

		html_code += '<td><input type="text" name="item_qty[]" id="item_qty'+count+'" data-srno="'+count+'" class="form-control input-sm item_qty text-center" required /></td>';

		html_code += '<td><input type="text" name="item_price[]" id="item_price'+count+'" data-srno="'+count+'" class="form-control input-sm item_price text-right" required /></td>';

		html_code += '<td><input type="text" name="item_subtotal" id="item_subtotal'+count+'" data-srno="'+count+'" class="form-control input-sm item_subtotal text-right"  readonly /></td>';

		html_code += '<td class="text-center"><span name="remove_row" id="'+count+'" class="glyphicon glyphicon-trash remove_row" style="cursor:pointer;color:darkred;font-weight:bold;"></span></td>';
		html_code += '</tr>';
		
		$("#item_table").append(html_code);
    });

    $(document).on('click', '.remove_row', function(){
		if($("#item_table tr").length <= 6){
			$("#item-table-footer").hide();
		}else{
			$("#item-table-footer").show();
		}
        var row_id = $(this).attr("id");
	    var item_subtotal = $('#item_subtotal'+row_id).val();
	    if(item_subtotal == ''){
		    var subtotal = 0;
	    }else{
		    var subtotal = item_subtotal;
	    }
		var final_amount = $('.total').val();	
		var total_amount = parseFloat(final_amount) - parseFloat(subtotal);		
		$('.total').val(total_amount.toFixed(2));		
		$('.sum').val(total_amount.toFixed(2));
		var paid = $(".paid").val();
		var outstanding = total_amount - paid;
		$('.outstanding').val(outstanding.toFixed(2));		
		$('#row_id_'+row_id).remove();
		count--;
		$('#total_item').val(count);
    });
    
    function cal_final_total(count)
    {
	    var total_amount = 0;

	    for(j=1; j<=count; j++)
        {
			var name = $('#item_name'+j).val();
			var quantity = 0;
			var price = 0;
			var item_subtotal = 0;
			var item_total = 0;
			quantity = $('#item_qty'+j).val();
			if(quantity > 0)
			{
				price = $('#item_price'+j).val();
				if(price > 0)
				{
					item_subtotal = parseFloat(quantity) * parseFloat(price);
					$('#item_subtotal'+j).val(item_subtotal.toFixed(2));
					item_total = parseFloat(item_subtotal);
					total_amount = parseFloat(total_amount) + parseFloat(item_total);
	 			}
      		}else{
				alert("This "+name+" quantity must be greater than zero");
			  }
  		}

	    $('.total').val(total_amount.toFixed(2));
	    $('.sum').val(total_amount.toFixed(2));
	    var paid = $(".paid").val();
	    var outstanding = total_amount - paid;
	    $('.outstanding').val(outstanding.toFixed(2));
	     
    }

    $(document).on('keyup', '.item_price', function(){
		cal_final_total(count);
    });

    $(document).on('keyup', '.item_qty', function(){
	    cal_final_total(count);
    });
     
    $(document).on('keyup', '.paid', function(){
	    cal_final_total(count);
    });

	$(".paid").keyup(function(){
		var paid_amount = $(".paid").val();
		if(paid_amount < 0){
			alert('Paid amount must be greater than zero!');
		}
	 });

    $('#save_invoice').click(function(){
		if($.trim($('#supplier_id').val()).length == 0)
		{
			alert("Please Select Supplier Name");
			$('#supplier_id').focus();
			return false;
		}
		if($("#item_table tr").length < 6){
			alert("Please add at least one item on clicking add item button!");
			return false;
		}

	    for(var no=1; no<=count; no++)
	    {
		 	if($.trim($('#item_name'+no).val()).length == 0)
		 	{
				alert("Please Enter Item Name");
				$('#item_name'+no).focus();
				return false;
		 	}
		 
		 	if($.trim($('#item_type'+no).val()).length == 0)
		 	{
				alert("Please Select Item Type");
				$('#item_type'+no).focus();
				return false;
		 	}

			if($.trim($('#item_qty'+no).val()).length == 0)
			{
				alert("Please Enter Quantity");
				$('#item_qty'+no).focus();
				return false;
			}

			if($.trim($('#item_price'+no).val()).length == 0)
			{
				alert("Please Enter Price");
				$('#item_price'+no).focus();
				return false;
			}

			if($('#item_qty'+no).val() == 0){
				var name = $('#item_name'+no).val();
				alert("This "+name+" quantity must be greater than zero!");
				$('#item_qty'+no).focus();
				return false;
			}
			if($('#item_qty'+no).val() < 0){
				var name = $('#item_name'+no).val();
				alert("This "+name+" quantity must be greater than zero!");
				$('#item_qty'+no).focus();
				return false;
			}

		}
		if($("#item_table tr").length > 5){
			if($(".paid").val() < 0){
				alert('Paid amount must be greater than zero!');
				$('.paid').focus();
				return false;
			}
		}
		var paid_value = $(".paid").val();
		var net_payable_value = $(".sum ").val();
		var value_check = net_payable_value - paid_value;
		if(value_check < 0 ){
			alert("Paid amount can't be greater than net payable amount!");
			$('.paid').focus();
			return false;
		}

		$('#invoice_form').submit();
	});
    

});
</script>

@endsection
