@extends('admin.master')

@section('page_title')
Manage Invoice
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Invoice</li>
    </ol>
</div>    
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">                                
                    <h2>
                        INVOICE MANAGEMENT
                    </h2>
		    <a href="{{url('/buying-invoice/add')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">add_box</i> ADD INVOICE
			</button>
		    </a>
                </div>
                <div class="body" style="min-height: 500px;">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$success_message}}
                    </div>
                    @endif
                    @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$error_message}}
                    </div>
                    @endif
                    <div class="">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" >
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Invoice NO.</th>
                                    <th>Supplier Name</th>
                                    <th class="text-right">Total(Tk)</th>
                                    <th class="text-right">Paid(Tk)</th>
                                    <th class="text-right">Outstanding(Tk)</th>
                                    <th>Issued Date</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i=1 @endphp
                                @foreach($invoice_data as $invoice)
                                <tr>                                   
                                    <td>{{$i++}}</td>
                                    <td>{{$invoice->invoice_number}}</td>
                                    <td>{{$invoice->supplier->supplier_name}}</td>
                                    <td class="text-right">{{number_format($invoice->total_amount,2)}}</td>
                                    <td class="text-right">{!!number_format( $invoice->paid_amount,2) !!}</td>
                                    <td class="text-right">{{number_format($invoice->due_amount,2)}}</td>
                                    <td>{{date('F j,Y, g:i A',strtotime($invoice->issue_date))}}</td>
                                    <td>
                                        @if ($invoice->invoice_status == 1)
                                        <span class="label label-success">Paid</span>
                                        @else
                                        <span class="label label-danger">Due</span>                   @endif
                                    </td>
                                    <td>{{$invoice->created_by}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="material-icons">view_list</i> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu action-menu">
                                            <li><a href="{{url('/buying-invoice/view/'.$invoice->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$invoice->invoice_name}} Details"><i class="material-icons">visibility</i> View</a></li>

                                            <li><a href="{{url('/buying-invoice/print/'.$invoice->id)}}" class=" waves-effect waves-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print {{$invoice->invoice_name}} Details" target="_blank"><i class="material-icons">print</i> Print</a></li>

                                            <li><a href="{{url('/buying-invoice/delete/'.$invoice->id)}}" class=" waves-effect waves-block" onclick="return confirm('Are you sure to delete this {{$invoice->invoice_number}} invoice?');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$invoice->invoice_name}}"><i class="material-icons">delete</i> Delete</a></li>						
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

