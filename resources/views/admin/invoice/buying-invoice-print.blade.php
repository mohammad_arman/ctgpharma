<!DOCTYPE html>
<html>

<head>
	<title>CtgPharma || Invoice</title>
	<meta charset="UTF-8">
	<meta name="title" content="CtgPharma - is a online pharmacy shop." />
	<meta name="description" content=" " />
	<meta name="author" content="Mohammad Arman">
	<meta name="robots" content="index, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Favicon-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

	<div style="width:100%;">
		<div style="width:70%;float:left;">
			<div class="logo" style="width:30%;height:100px;float:left;">
				<img src="{{asset('public/ecommerce-frontend-assets/images/logo-2.png')}}" alt="invoice-logo" style="width:130px;height:80px;"
				/>
			</div>
			<div class="address" style="width:70%;height:100px;float:right;font-size:12px;">
				<p style="margin-top:27px;"><b>Ctg Pharma Online Medicine Shop</b><br/>
				Agrabad, Panwala Para Boro Mosjid, Chittagong.<br/>
				<b>Contact:</b> +8801521225987</p>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div style="width:30%;float:left;">
			<?php
					if ($invoice_data->invoice_status == 1) {
					?>
				<div style="width:100%;height:100px;">
					<div style="padding:10px;width:100px;height:25px;border:2p solid green;text-align:center;font-size:18px;color:green;border-radius:10px;margin-left:70px;margin-top:27px;">PAID</div>
				</div>
				<?php
						}elseif ($invoice_data->invoice_status == 2) {
					?>
					<dov style="width:100%;height:100px;">
						<div style="padding:10px;width:100px;height:25px;border:2p solid red;text-align:center;font-size:18px;color:red;border-radius:10px;margin-left:70px;margin-top:27px;">DUE</div>
					</dov>
					<?php
						}
					?>
		</div>
		<div style="clear:both;"></div>
	</div>

	<div class="col-md-12" style="margin-top:10px;margin-bottom:10px;background: #00BCD4;text-align:center;">
		<h4 style="margin:5px;font-size:16px;color:white;font-weight:bold;">PRODUCT RECEIVED INVOICE</h4>
	</div>
	<div class="col-md-12" style="border-bottom:2px solid #00BCD4;padding-bottom:10px;margin-bottom:20px;">
		<table width="100%">
			<tr>
				<td width="50%"><b>Invoice No.: </b>{{$invoice_data->invoice_number}}</td>
				<td width="50%"  style="text-align:right;"><b>Issued Date: </b>{{date('d-m-Y h:i:s A',strtotime($invoice_data->issue_date))}}</td>
			</tr>
			<tr>
				<td width="50%"><b>Name: </b>{{$invoice_data->supplier->supplier_name}}</td>
				<td width="50%"  style="text-align:right;"><b>Phone Number: </b>{{$invoice_data->supplier->contact_number}}</td>
			</tr>

		</table>
	</div>

	<div class="col-md-12">
		<table class="table table-bordered table-hover" id="item_table" style="width:100%;">
			<thead style="background:#00BCD4;color:white;">
				<tr>
					<th width="5%">SL</th>
					<th width="25%">Name</th>
					<th width="15%">Type</th>
					<th width="10%" style="text-align:center;">Qty</th>
					<th width="20%" style="text-align:right;">Amount(Tk)</th>
					<th width="25%" style="text-align:right;">Subtotal(Tk)</th>
				</tr>
			</thead>
			<tbody>
				<?php
		
			
			$i = 1;
			$sum = 0;
			foreach ($item_data as $row) {
    
			?>

					<tr>
						<td>
							<?php echo $i++;?>
						</td>
						<td class="text-left">
							<?php echo $row->product_name;?>
						</td>
						<td>
							<?php echo $row->product_type;?>
						</td>
						<td style="text-align:center;">
							<?php echo $row->quantity;?>
						</td>
						<td style="text-align:right;">
							<?php echo number_format((float)$row->price,2,'.','');?>
						</td>
						<td style="text-align:right;">
							<?php 
				$sub_total = $row->quantity * $row->price;
				echo number_format((float)$sub_total,2,'.','');
				$sum = $sum + $sub_total;
				?>
						</td>
					</tr>
					<?php
			    }

			?>
			</tbody>
			<tfoot id="item-table-footer">
				<tr>
					<td colspan="5" style="text-align:right;">Total(Tk)</td>
					<td style="text-align:right;">
						<?php echo number_format($sum,2);?>
					</td>

				</tr>
				<tr>
					<td colspan="5" style="text-align:right;">Net Payable(Tk)</td>
					<td style="text-align:right;">
						<?php echo number_format($invoice_data->total_amount,2);?>
					</td>

				</tr>
				<tr>
					<td colspan="5"  style="text-align:right;">Paid(Tk)</td>
					<td style="text-align:right;">
						<?php echo number_format($invoice_data->paid_amount,2);?>
					</td>

				</tr>
				<tr>
					<td colspan="5" style="text-align:right;">Balance Due(Tk)</td>
					<td style="text-align:right;">
						<?php echo number_format($invoice_data->due_amount,2);?>
					</td>

				</tr>
			</tfoot>
		</table>
	</div>
	

	<div class="footer col-md-12" style="margin-top:100px;">
		<div style="text-align:center;border-top:1px dashed black;width:200px;height:30px;float:right;"><p>Receiver Signature</p></div>
	</div>
	<div style="clear:both;"></div>

	
	<script type="text/javascript">
		this.print();
	</script>
</body>

</html>