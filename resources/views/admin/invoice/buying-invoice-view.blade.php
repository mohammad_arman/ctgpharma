@extends('admin.master') 
@section('page_title') View Invoice
@endsection
 
@section('admin_main_content')
<link href="{{asset('public/admin-frontend-assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> View Invoice</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        INVOICE DETAILS
                    </h2>
                    <a href="{{url('/buying-invoice/manage')}}">
                        <button type="button" class="btn bg-brown waves-effect pull-right header-button" >
                            <i class="material-icons">view_list</i> LIST
                        </button>
                    </a>
                    @if ($invoice_data->invoice_status != 1)
                    <button type="button" class="btn bg-indigo waves-effect pull-right header-button" data-toggle="modal" data-target="#addPayment" style="margin-right:5px;">
                            <i class="material-icons">add_box</i> Add Payment
                    </button>
                    @endif
                    <a href="{{url('/buying-invoice/print/'.$invoice_data->id)}}" target="_blank">
                        <button type="button" class="btn bg-pink waves-effect pull-right header-button"  style="margin-right:5px;">
                                <i class="material-icons">print</i> Print
                        </button>
                    </a>

                </div>
                <div class="body">
                    @if ($success = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{$success}}
                        </div>
                    @endif
                    @if ($error = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert" id="msg">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{$error}}
                        </div>
                    @endif
                    <div class="col-md-12">
                        <ul class="view-list col-md-6">
                            <li>
                                <span>Invoice No.:</span>
                                <span>{{$invoice_data->invoice_number}}</span>
                            </li>
                            <li>
                                <span>Supplier Name:</span>
                                <span>{{$invoice_data->supplier->supplier_name}}</span>
                            </li>
                            <li>
                                <span>Mobile No.:</span>
                                <span>{{$invoice_data->supplier->contact_number}}</span>
                            </li>
                        </ul>
                        <ul class="view-list col-md-6">
                            <li>
                                <span>Company Name:</span>
                                <span>{{$company_name->company_name}}</span>
                            </li>
                            <li>
                                <span>Issued Date:</span>
                                <span>{{date('d-m-Y h:i:s A',strtotime($invoice_data->issue_date))}}</span>
                            </li>
                            <li>
                                <span>Invoice Status:</span>
                                <span>{{json_decode(INVOICE_STATUS)[$invoice_data->invoice_status]}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h4>Products</h4>
                        <table class="table table-bordered table-hover" id="item_table" style="width:100%;">
                            <thead style="background:#00BCD4;color:white;">
                                <tr>
                                    <th width="5%">SL</th>
                                    <th width="25%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%" class="text-center">Qty</th>
                                    <th width="20%" class="text-right">Amount(Tk)</th>
                                    <th width="25%" class="text-right">Subtotal(Tk)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                    
                                    $i = 1;
                                    $sum = 0;
                                    foreach ($item_data as $row) {
                            
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $i++;?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo $row->product_name;?>
                                        </td>
                                        <td>
                                            <?php echo $row->product_type;?>
                                        </td>
                                        <td class="text-center">
                                            <?php echo $row->quantity;?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo number_format((float)$row->price,2,'.','');?>
                                        </td>
                                        <td class="text-right">
                                            <?php 
                                        $sub_total = $row->quantity * $row->price;
                                        echo number_format((float)$sub_total,2,'.','');
                                        $sum = $sum + $sub_total;
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                        
                                    ?>
                            </tbody>
                            <tfoot id="item-table-footer">
                                <tr>
                                    <td colspan="5" class="text-right">Total(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($sum,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Net Payable(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($invoice_data->total_amount,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Paid(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($invoice_data->paid_amount,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Balance Due(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($invoice_data->due_amount,2);?>
                                    </td>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <h4>Peyments</h4>
                        <table class="table table-bordered table-hover" style="width:100%;">
                            <thead style="background:#00BCD4;color:white;">
                                <tr>
                                    <th>SL</th>
                                    <th>Paid Date</th>
                                    <th class="text-right">Amount(Tk)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; ?> @foreach ($invoice_payment_data as $payment)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$payment->payment_date}}</td>
                                    <td class="text-right">{{number_format((float)$payment->amount,2,'.','')}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2" class="text-right text-bold">Total Paid(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format((float)$invoice_data->paid_amount,2,'.','');?>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" class="text-right text-bold">Outstanding(Tk)</td>
                                    <td class="text-right">
                                        <?php
                                            echo number_format((float)$invoice_data->due_amount	,2,'.','');	
                                        ?>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div style="clear:both;"></div>
                    <!--======== Payment Add Modal ============-->
                    <div class="modal fade" id="addPayment" tabindex="-1" role="dialog" style="z-index:10000;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background:#00BCD4 !important;color:white;">
                                    <h4 class="modal-title" id="defaultModalLabel">Add Payment</h4>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{url('/update/payment/') }}" id="add_payment_form">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="id" name="id" value="{{$invoice_data->id}}" class="form-control">
                                        <input type="hidden" id="total_amount" name="total_amount" value="{{$invoice_data->total_amount}}" class="form-control">
                                        <input type="hidden" id="paid_amount" name="paid_amount" value="{{$invoice_data->paid_amount}}" class="form-control">
                                        <input type="hidden" id="due_amount" name="due_amount" value="{{$invoice_data->due_amount}}" class="form-control">
                                        <div class="col-md-12">
                                            <label for="amount">Amount <b style="color: red;">*</b></label>
                                            <div class="input-group">
                                                <div class="form-line{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                                <input type="text" id="amount" name="amount" value="{{number_format($invoice_data->due_amount,2)}}" class="form-control">
                                                </div>
                                                <span id="error_amount" style="color:red;"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="date">Date <b style="color: red;">*</b></label>
                                            <div class="input-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="date" id="date" autocomplete="off">
                                                </div> 
                                                <span id="error_date" style="color:red;"></span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" id="save_payment" class="btn btn-link bg-cyan waves-effect">SAVE</button>
                                    <button type="button" class="btn btn-link bg-blue-grey waves-effect" data-dismiss="modal">CLOSE</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--======== Payment Add Modal ============-->


                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    
</div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script src="{{asset('public/admin-frontend-assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#date').datetimepicker({
        //language:  'fr',
	 direction: "auto",
	format: "yyyy-mm-dd",
        weekStart: 1,
        todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        // alert();
        $("#amount").keyup(function(){

            if($.trim($(this).val()).length == 0)
		 	{
                $("#error_amount").text('This field is required');
				$(this).focus();
                $('#save_payment').attr("disabled", true);
				return false;
		 	}else{
                $("#error_amount").text('');
                $('#save_payment').attr("disabled", false);
             }
        });
        $("#date").keyup(function(){
            
            if($.trim($(this).val()).length == 0)
		 	{
                $("#error_date").text('This field is required');
				$(this).focus();
                $('#save_payment').attr("disabled", true);
				return false;
		 	}else{
                $("#error_date").text('');
                $('#save_payment').attr("disabled", false);
             }
        });
        $('#save_payment').click( function(){

                if($.trim($('#amount').val()).length == 0){
                    $("#error_amount").text('This field is required');
                    return false;
                }
                if($.trim($('#date').val()).length == 0){
                    $("#error_date").text('This field is required');
                    return false;
                }
                $("#add_payment_form").submit();

        });
    });
    </script>
@endsection
