@extends('admin.master')

@section('page_title')
Add Invoice
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">add_circle</i> Add Invoice</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD INVOICE
                    </h2>   
		    <a href="{{url('/invoice/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
					@if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$success_message}}
                    </div>
                    @endif
                    @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{$error_message}}
                    </div>
                    @endif
                    <form id="invoice_form" method="POST" action="{{ url('invoice/save') }}">
                        {{ csrf_field() }}
		<div class="col-md-12" style="margin-bottom: 0px;">
		    <div class="col-md-6" style="margin-bottom: 0px;">
			<label for="invoice_number">Invoice Number<b style="color: red;"> *</b></label>
			<div class="form-group">
			    <div class="form-line{{ $errors->has('invoice_number') ? ' has-error' : '' }}">
				<input type="text" id="invoice_number" name="invoice_number" value="{{$invoice_number}}" class="form-control" readonly>
			    </div>  
			    @if ($errors->has('invoice_number'))
				<span class="help-block">
				    <strong style="color: red;">{{ $errors->first('invoice_number') }}</strong>
				</span>
			    @endif
			</div>
		    </div>
		    <div class="col-md-6" style="margin-bottom: 0px;">
			<label for="issue_date">Date<b style="color: red;"> *</b></label>
			<div class="form-group">
			    <div class="form-line{{ $errors->has('issue_date') ? ' has-error' : '' }}">
				<input type="text" id="issue_date" name="issue_date" value="{{$issue_date}}" class="form-control" readonly>
			    </div>  
			    @if ($errors->has('issue_date'))
				<span class="help-block">
				    <strong style="color: red;">{{ $errors->first('issue_date') }}</strong>
				</span>
			    @endif
			</div>
		    </div>
		</div>
		<div class="col-md-12" style="margin-bottom: 0px;">
		    <div class="col-md-6" style="margin-bottom: 0px;">
			<label for="receiver_name">RECEIVER (BILL TO)<b style="color: red;"> *</b></label>
			<div class="form-group">
			    <div class="form-line{{ $errors->has('receiver_name') ? ' has-error' : '' }}">
				<input type="text" id="receiver_name" name="receiver_name" class="form-control" >
			    </div>  
			    @if ($errors->has('receiver_name'))
				<span class="help-block">
				    <strong style="color: red;">{{ $errors->first('receiver_name') }}</strong>
				</span>
			    @endif
			</div>
		    </div>
		    <div class="col-md-6" style="margin-bottom: 0px;">
			<label for="mobile_number">Mobile Number<b style="color: red;"> *</b></label>
			<div class="form-group">
			    <div class="form-line{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
				<input type="text" id="mobile_number" name="mobile_number" class="form-control">
			    </div>  
			    @if ($errors->has('mobile_number'))
				<span class="help-block">
				    <strong style="color: red;">{{ $errors->first('mobile_number') }}</strong>
				</span>
			    @endif
			</div>
		    </div>
		</div>
		
		<input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">
                            
		<div class="col-md-12" style="margin-bottom: 0px;">
		    <div class="col-md-12" style="margin-bottom: 0px;">
			<table class="table table-bordered table-hover table-striped" id="item_table" style="width:100%;">
			    <thead class="bg-cyan">
				<tr>
				    <th width="5%">Sr.</th>
				    <th width="25%" class="text-center">Name</th>
				    <th width="15%" class="text-center">Type</th>
				    <th width="10%" class="text-center">Qty</th>
				    <th width="20%" class="text-center">Amount(Tk)</th>
				    <th width="20%" class="text-center">Subtotal(Tk)</th>
				    <th width="5%" class="text-center">Action</th>
				</tr>  
			    </thead>
			    <tbody>
				 
			    </tbody>
			    <tfoot id="item-table-footer">
				<tr>
				    <td colspan="5" class="text-right">Total</td>
				    <td colspan="2"><input type="text" name="total" class="form-control total text-right" readonly /></td>

				</tr>

				<tr>
				    <td colspan="5" class="text-right" >Net Payable</td>
				    <td colspan="2"><input type="text" name="sum" class="form-control sum text-right" readonly /></td>

				</tr>
				<tr>
				    <td colspan="5" class="text-right" >Received</td>
				    <td colspan="2"><input type="text" name="paid" id="received" value="0.00" class="form-control paid text-right" /></td>

				</tr>
				<tr>
				    <td colspan="5" class="text-right" >Balance Due</td>
				    <td colspan="2"><input type="text" name="outstanding" class="form-control outstanding text-right" readonly /></td>

				</tr>
			    </tfoot>
			</table>
			<input type="hidden" name="total_item" id="total_item" value="1" />
			<button type="button" name="add" id="add" class="btn bg-blue-grey waves-effect" style="margin-bottom:10px;">Add Item</button>

		    </div>			
		</div>			
			     
                             <br>
		<div class="col-md-12">
		    <div class="col-md-12" style="margin-bottom: 0px;">
					<button type="submit" class="btn bg-light-blue waves-effect pull-right" id="save_invoice" style="margin-left:5px;">
							<i class="material-icons">save</i>
							<span>SAVE</span>
						</button>
					<button type="reset" class="btn bg-blue-grey waves-effect pull-right">
							<i class="material-icons">cached</i>
							<span>RESET</span>
						</button>
			
		    </div>
		</div>
		<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
	fetch_product_type();
	fetch_product_price();

	$("#item-table-footer").hide();
    var total_amount = $(".sum").val();
    var count = 0;
    
    $(document).on('click','#add',function(){
		$("#item-table-footer").show();
		count++;
		// alert(count);
		$('#total_item').val(count);
		
		var html_code = '';
		html_code += '<tr id="row_id_'+count+'" data-rowid="'+count+'">';

		html_code += '<td><span id="sr_no">'+count+'</span></td>';

		html_code += '<td><select name="item_name[]" id="item_name'+count+'" data-srno="'+count+'" class="form-control  item_name"><option value="">Please Select</option><?php echo $output;?></select></td>';

		html_code += '<td><select name="item_type[]" id="item_type'+count+'"  data-srno="'+count+'" class="form-control input-sm item_type"></select><input type="hidden" name="product_id" id="product_id'+count+'" value=""></td>';

		html_code += '<td><input type="text" name="item_qty[]" id="item_qty'+count+'" data-srno="'+count+'" class="form-control input-sm item_qty text-center"  /> <input type="hidden" name="old_qty[]" id="old_qty'+count+'" data-srno="'+count+'" class="form-control input-sm old_qty text-center" /></td>';

		html_code += '<td><input type="text" name="item_price[]" id="item_price'+count+'" data-srno="'+count+'" class="form-control input-sm item_price text-right" readonly /></td>';

		html_code += '<td><input type="text" name="item_subtotal" id="item_subtotal'+count+'" data-srno="'+count+'" class="form-control input-sm item_subtotal text-right"  readonly /></td>';

		html_code += '<td class="text-center"><span name="remove_row" id="'+count+'" class="glyphicon glyphicon-trash remove_row" style="cursor:pointer;color:darkred;font-weight:bold;"></span></td>';
		html_code += '</tr>';
		
		$("#item_table").append(html_code);
    });
	
        function fetch_product_type(query = '',row = ''){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
            $.ajax({
                url:"{{action('InvoiceController@productType')}}",
                method:'POST',
                data:{query:query},
				dataType:'json',
                success:function(data){
                    console.log(data);
					// alert("ok"+row);
					$('#item_type'+row).html(data.output);
					$('#product_id'+row).val(data.id);
                }
            });
            
        }
		$(document).on('change','.item_name',function(){
			var key = $(this).parent().parent().find('.item_name').val();
			var query = $(this).val();
			var row_no = $(this).parent().parent().find('.item_name').data('srno');
			var row = $(this).data('srno');
			// var row_index = $(this).closest("tr").index();
			// var row = row_index + 1;

			fetch_product_type(query,row);
		});

		function fetch_product_price(type = '', id ='',row =''){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
            $.ajax({
                url:"{{action('InvoiceController@productPrice')}}",
                method:'POST',
                data:{id:id,type:type},
				dataType:'json',
                success:function(data){
                    console.log(data);
					// alert("ok"+row);
					$('#item_price'+row).val(data.price);
					$('#old_qty'+row).val(data.qty);
                }
            });
		}
		$(document).on('change','.item_type',function(){
			var key = $(this).parent().parent().find('.item_type').val();
			var type = $(this).val();
			var row_no = $(this).parent().parent().find('.item_type').data('srno');
			var row = $(this).data('srno');
			var id = $('#product_id'+row).val();
			// var row_index = $(this).closest("tr").index();
			// var row = row_index + 1;
			// alert("type="+type+" -- id="+id+" -- row="+row);
			fetch_product_price(type,id,row);
		});
		

    
    $(document).on('click', '.remove_row', function(){
		
		if($("#item_table tr").length <= 6){
			$("#item-table-footer").hide();
		}else{
			$("#item-table-footer").show();
		}
	   var row_id = $(this).attr("id");
	   var item_subtotal = $('#item_subtotal'+row_id).val();
	    if(item_subtotal == ''){
		    var subtotal = 0;
	    }else{
		    var subtotal = item_subtotal;
	    }
		var final_amount = $('.total').val();
	
		var total_amount = parseFloat(final_amount) - parseFloat(subtotal);
		
		$('.total').val(total_amount.toFixed(2));
		
		$('.sum').val(total_amount.toFixed(2));

		var paid = $(".paid").val();

		var outstanding = total_amount - paid;

		$('.outstanding').val(outstanding.toFixed(2));
		
		$('#row_id_'+row_id).remove();
		count--;
		$('#total_item').val(count);
	
	
	

	     
    });
    
    function cal_final_total(count)
    {
	var total_amount = 0;

	for(j=1; j<=count; j++)
    {
		var name = $('#item_name'+j).val();
	   var quantity = 0;
	   var price = 0;
	   var item_subtotal = 0;
	   quantity = $('#item_qty'+j).val();
	   var item_total = 0;

		if(quantity > 0){
				price = $('#item_price'+j).val();
				if(price > 0)
				{
					item_subtotal = parseFloat(quantity) * parseFloat(price);
					$('#item_subtotal'+j).val(item_subtotal.toFixed(2));

					item_total = parseFloat(item_subtotal);
					total_amount = parseFloat(total_amount) + parseFloat(item_total);

				}
		}else{
			alert("This "+name+" quantity must be greater than zero");
			
		}
	       
        
    }

	     $('.total').val(total_amount.toFixed(2));

	    $('.sum').val(total_amount.toFixed(2));

	    var paid = $(".paid").val();

	    var outstanding = total_amount - paid;

	    $('.outstanding').val(outstanding.toFixed(2));
	     
    }

     $(document).on('keyup', '.item_price', function(){

	     cal_final_total(count);
     });

     $(document).on('keyup', '.item_qty', function(){
	     cal_final_total(count);
     });
     
     $(document).on('keyup', '.paid', function(){
	     cal_final_total(count);
     });

	 $("#receiver_name").keyup(function(){
		if($.trim($('#receiver_name').val()).length == 0)
	       {
		 alert("Please Enter Reciever Name");
	       }
	 });
	 $("#mobile_number").keyup(function(){
		if($.trim($('#mobile_number').val()).length == 0)
	       {
		 alert("Please Enter Mobile Number");
	       }
	 });
	 $("#received").keyup(function(){
		var received_amount = $("#received").val();
		if(received_amount <= 0){
			alert('Received amount must be greater than zero!');
		}else if($.trim($('#received').val()).length == 0){
			alert("Received amount can't be empty!");
		}
	 });

     $('#save_invoice').click(function(){
	       if($.trim($('#receiver_name').val()).length == 0)
	       {
		 alert("Please Enter Reciever Name");
		 return false;
	       }
//
	       if($.trim($('#mobile_number').val()).length == 0)
	       {
		 alert("Please Enter Mobile Number");
		 return false;
	       }
		   if($("#item_table tr").length < 6){
			alert("Please add at least one item on clicking add item button!");
		 	return false;
		   }
//
//	       if($.trim($('#order_date').val()).length == 0)
//	       {
//		 alert("Please Select Invoice Date");
//		 return false;
//	       }

	       for(var no=1; no<=count; no++)
	       {
		 if($.trim($('#item_name'+no).val()).length == 0)
		 {
		   alert("Please Enter Item Name");
		   $('#item_name'+no).focus();
		   return false;
		 }
		 
		 if($.trim($('#item_type'+no).val()).length == 0)
		 {
		   alert("Please Enter Item Type");
		   $('#item_type'+no).focus();
		   return false;
		 }

		 if($.trim($('#item_qty'+no).val()).length == 0)
		 {
		   alert("Please Enter Quantity");
		   $('#item_qty'+no).focus();
		   return false;
		 }

		 if($.trim($('#item_price'+no).val()).length == 0)
		 {
		   alert("Please Enter Price");
		   $('#item_price'+no).focus();
		   return false;
		 }
		 if($('#item_qty'+no).val() == 0){
			var name = $('#item_name'+no).val();
			alert("This "+name+" quantity must be greater than zero!");
			$('#item_qty'+no).focus();
			return false;
		 }
		 if($('#item_qty'+no).val() < 0){
			var name = $('#item_name'+no).val();
			alert("This "+name+" quantity must be greater than zero!");
			$('#item_qty'+no).focus();
			return false;
		 }
		 if($('#old_qty'+no).val() != '')
		 {
			
			var name = $('#item_name'+no).val();
	        var type = $('#item_type'+no).val();
	        var old_quantity = $('#old_qty'+no).val();
			var new_qty = $('#item_qty'+no).val();
			var qty_check =  old_quantity - new_qty;
			if(qty_check < 0){
				alert("This "+name+" quantity can't be greater than stock quantity! Your stock have "+old_quantity+" "+type);
				$('#item_qty'+no).focus();
				return false;
			}
			
		 }

	       }
		   if($("#item_table tr").length > 5){
				if($("#received").val() <= 0){

					alert('Received amount must be greater than zero!');
					return false;

				}else if($.trim($('#received').val()).length == 0){
					alert("Received amount can't be empty!");
					return false;
				}
			}
			var receiver_value = $("#received").val();
			var net_payable_value = $(".sum ").val();
			var value_check = net_payable_value - receiver_value;
		   if(value_check > 0 ){
				alert("Balance can't be due!");
				return false;
		   }else if(value_check < 0 ){
				alert("Received amount can't be greater than net payable amount!");
				return false;
		   }
		   if(net_payable_value != receiver_value){
				alert("Balance can't be due!");
				return false;
		   }
		   if($.trim($('#received').val()).length == 0){
				alert("Received amount can't be empty!");
				return false;
			}

	       $('#invoice_form').submit();
	    });
    

});
</script>

@endsection
