@extends('admin.master') 
@section('page_title') 
View Invoice
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> View Invoice</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        INVOICE DETAILS
                    </h2>
                    <a href="{{url('/invoice/manage')}}">
                        <button type="button" class="btn bg-brown waves-effect pull-right header-button" >
                            <i class="material-icons">view_list</i> LIST
                        </button>
                    </a>
                    <a href="{{url('/invoice/print/'.$invoice_data->id)}}" target="_blank">
                        <button type="button" class="btn bg-pink waves-effect pull-right header-button"  style="margin-right:5px;">
                                <i class="material-icons">print</i> Print
                        </button>
                    </a>

                </div>
                <div class="body">
                    <div class="col-md-12">
                        <ul class="view-list col-md-6">
                            <li>
                                <span>Invoice No.:</span>
                                <span>{{$invoice_data->invoice_number}}</span>
                            </li>
                            <li>
                                <span>Customer Name:</span>
                                <span>{{$invoice_data->receiver_name}}</span>
                            </li>
                            <li>
                                <span>Mobile No.:</span>
                                <span>{{$invoice_data->mobile_number}}</span>
                            </li>
                            <li>
                                <span>Created By:</span>
                                <span>{{$invoice_data->created_by}}</span>
                            </li>
                        </ul>
                        <ul class="view-list col-md-6">
                            <li>
                                <span>Issued Date:</span>
                                <span>{{date('d-m-Y h:i:s A',strtotime($invoice_data->issue_date))}}</span>
                            </li>
                            <li>
                                <span>Paid Date:</span>
                                <span>{{date('d-m-Y',strtotime($invoice_data->paid_date))}}</span>
                            </li>
                            <li>
                                <span>Invoice Status:</span>
                                <span>{{json_decode(INVOICE_STATUS)[$invoice_data->invoice_status]}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h4>Products</h4>
                        <table class="table table-bordered table-hover" id="item_table" style="width:100%;">
                            <thead style="background:#00BCD4;color:white;">
                                <tr>
                                    <th width="5%">SL</th>
                                    <th width="25%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%" class="text-center">Qty</th>
                                    <th width="20%" class="text-right">Amount(Tk)</th>
                                    <th width="25%" class="text-right">Subtotal(Tk)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                    
                                    $i = 1;
                                    $sum = 0;
                                    foreach ($item_data as $row) {
                            
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $i++;?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo $row->product_name;?>
                                        </td>
                                        <td>
                                            <?php echo $row->product_type;?>
                                        </td>
                                        <td class="text-center">
                                            <?php echo $row->quantity;?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo number_format((float)$row->price,2,'.','');?>
                                        </td>
                                        <td class="text-right">
                                            <?php 
                                        $sub_total = $row->quantity * $row->price;
                                        echo number_format((float)$sub_total,2,'.','');
                                        $sum = $sum + $sub_total;
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                        
                                    ?>
                            </tbody>
                            <tfoot id="item-table-footer">
                                <tr>
                                    <td colspan="5" class="text-right">Total(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($sum,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Net Payable(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($invoice_data->total_amount,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Received(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($invoice_data->paid_amount,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Balance Due(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($invoice_data->due_amount,2);?>
                                    </td>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    
</div>
@endsection
