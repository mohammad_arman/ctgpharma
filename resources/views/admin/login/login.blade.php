<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Admin Login</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset('public/ecommerce-frontend-assets/images/favicon.png')}}" type="image/png" sizes="16x16">

    <!-- Google Fonts -->
   
    <!-- Custom Css -->
    <link href="{{asset('public/admin-frontend-assets/css/style.css')}}" rel="stylesheet" type="text/css"/>
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>Ctg Pharma</b></a>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    @if ($errors->has('email'))
                    <div class="alert bg-pink text-center">  
                        {{ $errors->first('email') }}
                    </div>
                   @endif
                    <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                            
                        </div>                       
                    </div>
                    
                    <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                            
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        {{-- <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="remember" id="rememberme" class="filled-in chk-col-pink" {{ old('remember') ? 'checked' : '' }}>
                            <label for="rememberme">Remember Me</label>
                        </div> --}}
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">Login</button>
                        </div>
                    </div>
                    {{-- <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="{{ route('register') }}">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="{{ route('password.request') }}">Forgot Password?</a>
                        </div>
                    </div> --}}
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}" type="text/javascript"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('public/admin-frontend-assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('public/admin-frontend-assets/js/waves.min.js')}}" type="text/javascript"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('public/admin-frontend-assets/js/jquery.validate.js')}}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{asset('public/admin-frontend-assets/js/admin.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/admin-frontend-assets/js/sign-in.js')}}" type="text/javascript"></script>
</body>

</html>