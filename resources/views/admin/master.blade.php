
<!DOCTYPE html>
<html>
    <head>
        <title>CtgPharma || @yield('page_title')</title>
        <meta charset="UTF-8">
        <meta name="title" content="CtgPharma - is a online pharmacy shop."/>
        <meta name="description" content=" "/> 
        <meta name="author" content="Mohammad Arman">
        <meta name="robots" content="index, follow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon-->
        <link rel="icon" href="{{asset('public/ecommerce-frontend-assets/images/favicon.png')}}" type="image/png" sizes="16x16">

<!--         Bootstrap Select Css -->
        <link href="{{asset('public/admin-frontend-assets/css/bootstrap-spinner.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('public/admin-frontend-assets/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('public/admin-frontend-assets/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css"/>
        
<!--         JQuery DataTable Css -->
        <link href="{{asset('public/admin-frontend-assets/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
        
        
        <!-- Custom Css -->
        <link href="{{asset('public/admin-frontend-assets/css/style.css')}}" rel="stylesheet">

        <!-- Admin Theme Css -->
        <link href="{{asset('public/admin-frontend-assets/css/all-themes.min.css')}}" rel="stylesheet" type="text/css"/>
    </head>

    <body class="theme-cyan">
        <!-- Page Loader -->
<!--        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>-->
        <!-- #END# Page Loader -->

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->

        <!-- Search Bar -->
        {{-- <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            <input type="text" placeholder="START TYPING...">
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div> --}}
        <!-- #END# Search Bar -->

        <!-- Top Bar -->
        
        <nav class="navbar" style="z-index: 9999;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="{{url('/dashboard')}}">
                        <img src="{{asset('public/admin-frontend-assets/images')}}/admin-site-logo.png" alt="CTG-PHARMA" style="margin-top: -15px;height: 50px;"/>
                    </a>
                </div>
                @if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Sub-Admin'))
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Call Search -->
                        {{-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> --}}
                        <!-- #END# Call Search -->
                        <!-- Notifications -->
                        <?php
                            $distribute_branch_product = DB::select("select branch_product_distributions.*,branches.branch_name from
                             branch_product_distributions INNER JOIN branches ON branch_product_distributions.branch_id = branches.id
                              where branch_product_distributions.reorder_level >= branch_product_distributions.piece_qty");
                            //   echo '<pre>';print_r($distribute_branch_product);exit;
                            $total_branch_product = count($distribute_branch_product);
                            $color = [
                                '1'=>'bg-blue',
                                '2'=>'bg-orange',
                                '3'=>'bg-green',
                                '4'=>'bg-purple',
                                '5'=>'bg-pink',
                            ];
                        ?>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">notifications</i>
                            <span class="label-count">{{$total_branch_product}}</span>
                            </a>
                            
                            <ul class="dropdown-menu ">
                                <li class="header bg-blue" style="color:white;margin-top: -5px;">BRANCH NOTIFICATIONS</li>
                                <li class="body" >
                                    <ul class="menu">
                                        @foreach ($distribute_branch_product as $bitem)
                                        <?php 
                                        $bg_color = rand(1,5);
                                        ?>
                                        <li>
                                        <a href="{{url('/product/view/'.$bitem->id)}}">
                                                <div class="icon-circle <?php echo $color[$bg_color];?>">
                                                    <i class="material-icons">assignment</i>
                                                </div>
                                                <div class="menu-info">
                                                <h4>{{$bitem->product_name}} ({{$bitem->branch_name}})</h4>
                                                    <p>
                                                        {{date('d-m-Y h:i:s A',strtotime($bitem->updated_at))}}
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        @endforeach
                                  
                                    </ul>
                                </li>
                                
                            </ul>
                        </li>
                        <!-- #END# Notifications -->
                        <!-- Tasks -->
                        <?php 
                         $branch_product = DB::select("select id,product_name,piece_qty,
                        reorder_level from branch_products where reorder_level>=piece_qty");
                        
                        $ecommerce_product = DB::select("select id,product_name,piece_qty,
                        reorder_level from ecommerce_products where reorder_level>=piece_qty");

                        $total_branch_product = count( $branch_product);
                        
                        $total_ecommerce_product = count( $ecommerce_product);
                    
                        $total_product = $total_branch_product + $total_ecommerce_product;
                        ?>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">flag</i>
                            <span class="label-count">{{$total_product}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header bg-blue" style="color:white;margin-top: -5px;">Reorder Product Level</li>
                                <li class="body">
                                    <ul class="menu tasks">
                                            <a href="{{url('/branch-product/manage')}}" class="list-group-item"><span class="badge bg-purple">{{$total_branch_product}}</span> Branch Product</a>
                                         
                                        @foreach ($branch_product as $item)
                                        @php 
                                        $item_percentage = ($item->piece_qty/$item->reorder_level) * 100; 
                                        
                                        @endphp
                                            @if (($item->piece_qty <= $item->reorder_level) && ($item->piece_qty >=(($item->reorder_level/3)*2) ))
                                            
                                            <li>
                                            <a href="{{url('/branch-product/view/'.$item->id)}}">
                                                        <h4>
                                                            {{$item->product_name}}
                                                        <small>{{$item_percentage}}%</small>
                                                        </h4>
                                                        <div class="progress">
                                                            <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: {{$item_percentage}}%">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                            @if (($item->piece_qty < (($item->reorder_level/3)*2) ) && ($item->piece_qty >=($item->reorder_level/3)))
                                            <li>
                                                    <a href="{{url('/branch-product/view/'.$item->id)}}">
                                                        <h4>
                                                            {{$item->product_name}}
                                                            <small>{{$item_percentage}}%</small>
                                                        </h4>
                                                        <div class="progress">
                                                            <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: {{$item_percentage}}%">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                            @if ($item->piece_qty < ($item->reorder_level/3))
                                                <li>
                                                    <a href="{{url('/branch-product/view/'.$item->id)}}">
                                                        <h4>
                                                            {{$item->product_name}}
                                                            <small>{{$item_percentage}}%</small>
                                                        </h4>
                                                        <div class="progress">
                                                            <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: {{$item_percentage}}%">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                        
                                        <li style="height:3px;background:#2196F3;margin-top:10px;margin-bottom:10px;"></li>
                                              
                                        
                                       
                                            <a href="{{url('/ecommerce-product/manage')}}" class="list-group-item"><span class="badge bg-cyan">{{$total_ecommerce_product}}</span> Ecommerce Product</a>
                                       @foreach ($ecommerce_product as $eitem)
                                        @php 
                                        $item_percentage = ($eitem->piece_qty/$eitem->reorder_level) * 100; 
                                        
                                        @endphp
                                            @if (($eitem->piece_qty <= $eitem->reorder_level) && ($eitem->piece_qty >= (($eitem->reorder_level/3)*2) ))
                                            
                                            <li>
                                                    <a href="{{url('/ecommerce-product/view/'.$eitem->id)}}">
                                                        <h4>
                                                            {{$eitem->product_name}}
                                                        <small>{{$item_percentage}}%</small>
                                                        </h4>
                                                        <div class="progress">
                                                            <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: {{$item_percentage}}%">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                            @if (($eitem->piece_qty < (($eitem->reorder_level/3)*2) ) && ($eitem->piece_qty >=($eitem->reorder_level/3)))
                                            <li>
                                                    <a href="{{url('/ecommerce-product/view/'.$eitem->id)}}">
                                                        <h4>
                                                            {{$eitem->product_name}}
                                                            <small>{{$item_percentage}}%</small>
                                                        </h4>
                                                        <div class="progress">
                                                            <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: {{$item_percentage}}%">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                            @if ($eitem->piece_qty < ($eitem->reorder_level/3))
                                                <li>
                                                    <a href="{{url('/ecommerce-product/view/'.$eitem->id)}}">
                                                        <h4>
                                                            {{$eitem->product_name}}
                                                            <small>{{$item_percentage}}%</small>
                                                        </h4>
                                                        <div class="progress">
                                                            <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: {{$item_percentage}}%">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                                
                            </ul>
                        </li>
                        <!-- #END# Tasks -->
                        
                    </ul>
                </div>
                @endif
            </div>
        </nav>
        
        <!-- #Top Bar -->
        
        <!-- Left Sidebar -->
        <section>           
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                
                <div class="user-info">
                    
                    <div class="image">
                        @if (Auth::user()->image == '')
                            <img src="{{asset('public/admin-frontend-assets/images/user.png')}}" width="48" height="48" alt="User" />
                        @else
                         <img src="{{asset('public/admin-frontend-assets/employee_image/'.Auth::user()->image)}}" style="max-width:70px;max-height:70px;" alt="User" />
                        @endif
                        
                        
                    </div>
                    <div class="info-container" style="top:10px;">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                        <div class="email">{{ Auth::user()->email }}</div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                            <li><a href="{{url('/view/profile/'.Auth::user()->id)}}"><i class="material-icons">person</i>Profile</a></li>
                                <li role="seperator" class="divider"></li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="material-icons">input</i>Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
               
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">MAIN NAVIGATION</li>
                        @if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Sub-Admin'))
                        <li class="{{ Request::path() =='dashboard' ? 'active' : '' }}">
                            <a href="{{url('/dashboard')}}">
                                <i class="material-icons">home</i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                       
                        <li class="<?php 
                        if(Request::path() ==  'add-employee'){ echo 'active'; }
                        elseif(Request::path() ==  'employee/manage' ) { echo 'active'; }
                        else{ echo ' ';}?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">account_box</i>
                                <span>Employee</span>
                            </a>
                            <ul class="ml-menu">

                                <li>
                                    <a href="{{url('/add-employee')}}">
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Employee</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('/employee/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Manage Employee</span>
                                    </a>
                                </li>                                
                            </ul>
                        </li>

                        <li class="{{ Request::path() =='customer/manage' ? 'active' : '' }}">
                            <a href="{{url('/customer/manage')}}">
                                <i class="material-icons">account_circle</i>
                                <span>Customers</span>
                            </a>
                        </li>
                        <li class="{{ Request::path() =='order/manage' ? 'active' : '' }}">
                            <a href="{{url('/order/manage')}}">
                                <i class="material-icons">shopping_cart</i>
                                <span>Orders</span>
                            </a>
                        </li>

                        <li class="<?php 
                            if(Request::path() ==  'category/add'){ echo 'active'; }
                            elseif(Request::path() ==  'category/manage' ) { echo 'active'; }
                            else{ echo ' ';}?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">playlist_add</i>
                                <span>Category</span>
                            </a>
                            <ul class="ml-menu">

                                <li>
                                    <a href="{{url('/category/add')}}">
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Category</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('/category/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Manage Category</span>
                                    </a>
                                </li>                                
                            </ul>
                        </li>
                        
                        <li class="<?php if(Request::path() ==  'company/add'){ echo 'active'; }elseif(Request::path() ==  'company/manage' ) { echo 'active'; }else{ echo ' ';}?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">business</i>
                                <span>Company</span>
                            </a>
                            <ul class="ml-menu">
 
                                <li>
                                    <a href="{{url('/company/add')}}">
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Company</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('/company/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Manage Company</span>
                                    </a>
                                </li>                                
                            </ul>
                        </li>
                        
                        <li class="<?php if(Request::path() ==  'branch/add'){ echo 'active'; }elseif(Request::path() ==  'branch/manage' ) { echo 'active'; }else{ echo ' ';}?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">store</i>
                                <span>Branch</span>
                            </a>
                            <ul class="ml-menu">

                                <li>
                                    <a href="{{url('/branch/add')}}">
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Branch</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('/branch/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Manage Branch</span>
                                    </a>
                                </li>                                
                            </ul>
                        </li>
                        
                        <li class="<?php if(Request::path() ==  'supplier/add'){ echo 'active'; }elseif(Request::path() ==  'supplier/manage' ) { echo 'active'; }else{ echo ' ';}?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">people</i>
                                <span>Supplier</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{url('/supplier/add')}}">  
                                        <i class="material-icons">person_add</i>
                                        <span>Add Supplier</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{url('/supplier/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Manage Supplier</span>
                                    </a>
                                </li>                                
                            </ul>
                        </li>   
                        @endif
	
                        <li class="<?php 
                        if(Request::path() ==  'product/add'){ echo 'active'; }
                        elseif(Request::path() ==  'branch-product/manage' ) { echo 'active'; }
                        elseif(Request::path() ==  'ecommerce-product/manage' ) { echo 'active'; }
                        elseif(Request::path() ==  'product/manage' ) { echo 'active'; }else{ echo ' ';}
                        
                        ?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">shopping_basket</i>
                                <span>Product</span>
                            </a>
                            <ul class="ml-menu">
                                @if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Sub-Admin'))
                                <li>
                                    <a href="{{url('/product/add')}}">
                                        <i class="material-icons">add_circle</i>
                                        <span>Product Add</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/branch-product/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Branch Product Manage</span>
                                    </a>
                                </li>                                
                                <li>
                                    <a href="{{url('/ecommerce-product/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Ecommerce Product Manage</span>
                                    </a>
                                </li>                                
                                <li>
                                    <a href="{{url('/product/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Distributed Branch Product Manage</span>
                                    </a>
                                </li> 
                                @endif  
                                @if(Auth::user()->hasRole('Manager') || Auth::user()->hasRole('Employee'))
                                <li>
                                    <a href="{{url('/product/manage')}}">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Product Manage</span>
                                    </a>
                                </li> 
                                @endif
                            </ul>
                        </li>   
	
	
     <li class="<?php 
     if(Request::path() ==  'invoice/add'){ echo 'active'; }
     elseif(Request::path() ==  'invoice/manage' ) { echo 'active'; }
     elseif(Request::path() ==  'buying-invoice/manage' ) { echo 'active'; }
     elseif(Request::path() ==  'buying-invoice/add' ) { echo 'active'; }
     else{ echo ' ';}?>">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">insert_drive_file</i>
                                <span>Invoice</span>
                            </a>
                            <ul class="ml-menu">
                                @if(Auth::user()->hasRole('Manager') || Auth::user()->hasRole('Employee'))
                                <li>
                                    <a href="{{url('/invoice/add')}}">  
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Invoice</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/invoice/manage')}}">
                                        <i class="material-icons">insert_drive_file</i>
                                        <span>Manage Invoice</span>
                                    </a>
                                </li>  
                                @endif
                                @if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Sub-Admin'))
                                <li>
                                    <a href="{{url('/buying-invoice/add')}}">  
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Buying Invoice</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="{{url('/buying-invoice/manage')}}">
                                        <i class="material-icons">insert_drive_file</i>
                                        <span>Manage Buying Invoice</span>
                                    </a>
                                </li>      
                                
                                <li>
                                    <a href="{{url('/invoice/add')}}">  
                                        <i class="material-icons">add_circle</i>
                                        <span>Add Selling Invoice</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="{{url('/invoice/manage')}}">
                                        <i class="material-icons">insert_drive_file</i>
                                        <span>Manage Selling Invoice</span>
                                    </a>
                                </li>         
                                @endif     
                                                              
                            </ul>
                        </li>   
                        @if(Auth::user()->hasRole('Admin'))
                                <li class="<?php if(Request::path() ==  'expense/add'){ echo 'active'; }elseif(Request::path() ==  'expense/manage' ) { echo 'active'; }else{ echo ' ';}?>">
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="material-icons">money_off</i>
                                        <span>Expense</span>
                                    </a>
                                    <ul class="ml-menu">
         
                                        <li>
                                            <a href="{{url('/expense/add')}}">
                                                <i class="material-icons">add_circle</i>
                                                <span>Add Expense</span>
                                            </a>
                                        </li>
        
                                        <li>
                                            <a href="{{url('/expense/manage')}}">
                                                <i class="material-icons">chrome_reader_mode</i>
                                                <span>Manage Expense</span>
                                            </a>
                                        </li>                                
                                    </ul>
                                </li>


                                <li class="<?php if(Request::path() ==  'expense/report'){ echo 'active'; }elseif(Request::path() ==  'income/report' ) { echo 'active'; }else{ echo ' ';}?>">
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="material-icons">chrome_reader_mode</i>
                                        <span>Report</span>
                                    </a>
                                    <ul class="ml-menu">
         
                                        <li>
                                            <a href="{{url('/income/report')}}">
                                                <i class="material-icons">view_agenda</i>
                                                <span>Income Report</span>
                                            </a>
                                        </li>
        
                                        <li>
                                            <a href="{{url('/expense/report')}}">
                                                <i class="material-icons">view_agenda</i>
                                                <span>Expense Report</span>
                                            </a>
                                        </li>                                
                                    </ul>
                                </li>
                                @endif 
                    </ul>
                </div>
                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        <strong>&copy; - <?php echo date('Y');?></strong> <a href="{{url('/dashboard')}}">CTG PHARMA</a>.
                    </div>
                </div>
                <!-- #Footer -->
            </aside>            
        </section>
        <!-- #END# Left Sidebar -->
        
        <!-- Main Content Section -->
        <section class="content">
            @yield('admin_main_content')
        </section>
        <!-- #END# Main Content Section -->
        
        
        <!-- Jquery Core Js -->
        <script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>

        <!-- Bootstrap Core Js -->
        <script src="{{asset('public/admin-frontend-assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
        
<!--         Select Plugin Js -->
        <script src="{{asset('public/admin-frontend-assets/js/bootstrap-select.min.js')}}"></script>

<!--         Slimscroll Plugin Js -->
        <script src="{{asset('public/admin-frontend-assets/js/jquery.slimscroll.js')}}"></script>

<!--         Waves Effect Plugin Js -->
        <script src="{{asset('public/admin-frontend-assets/js/waves.min.js')}}"></script>
        <script src="{{asset('public/admin-frontend-assets/js/autosize.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/admin-frontend-assets/js/moment.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/admin-frontend-assets/js/bootstrap-material-datetimepicker.js')}}" type="text/javascript"></script>

        <script src="{{asset('public/admin-frontend-assets/js/jquery.spinner.js')}}" type="text/javascript"></script>
        <!-- Jquery DataTable Plugin Js -->
        <script src="{{asset('public/admin-frontend-assets/js/jquery-datatable/jquery.dataTables.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/admin-frontend-assets/js/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
        
        <!-- Jquery CountTo Plugin Js -->
        <script src="{{asset('public/admin-frontend-assets/js/jquery.countTo.js')}}"></script>

        <!-- Flot Charts Plugin Js -->
        <script src="{{asset('public/admin-frontend-assets/js/jquery.flot.js')}}"></script>

        <!-- Custom Js -->
        <script src="{{asset('public/admin-frontend-assets/js/admin.js')}}"></script>
        <script src="{{asset('public/admin-frontend-assets/js/basic-form-elements.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('public/admin-frontend-assets/js/jquery-datatable.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('/public/admin-frontend-assets/js/tooltips-popovers.js')}}" type="text/javascript"></script>
        <script src="{{asset('/public/admin-frontend-assets/js/tinymce/tinymce.min.js')}}" type="text/javascript"></script>
        <script>tinymce.init({ selector:'textarea.editable' });</script>

<!--        <script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=b8jn5oxw2wsq20pm7uc2nnu56ea992nztzxossmob6lhj56j'></script>
        <script>
            tinymce.init({
                selector: 'textarea#tinymice',
                theme: 'modern',
                plugins: [
                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen',
                  'insertdatetime media nonbreaking save table contextmenu directionality',
                  'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
                templates: [
                  { title: 'Test template 1', content: 'Test 1' },
                  { title: 'Test template 2', content: 'Test 2' }
                ],
                content_css: [


                ]
            });
        </script>-->
        <script src="{{asset('public/admin-frontend-assets/js/index.js')}}" type="text/javascript"></script>
        
        <!-- Demo Js -->
        <script src="{{asset('public/admin-frontend-assets/js/demo.js')}}"></script>
        
	<script type="text/javascript">
            $(function () {
                $('#manufacturing_date').datetimepicker();
                $('#expiry_date').datetimepicker({
                        useCurrent: false //Important! See issue #1075
                });
                $("#manufacturing_date").on("dp.change", function (e) {
                        $('#expiry_date').data("DateTimePicker").minDate(e.date);
                });
                $("#expiry_date").on("dp.change", function (e) {
                        $('#manufacturing_date').data("DateTimePicker").maxDate(e.date);
                });
            });
        </script>
<!--	<script>
	$(function(){
	    setNavigation();
	});
	function setNavigation(){
	    var path = window.location.pathname;
	    path = path.replace(/\/$/,"");
	    path = decodeURIComponent(path);
	    
	    $(".list a").each(function(){
		var href = $(this).attr('href');
		if(path.substring(0,href.length) === href){
		    $(this).closest('li').addClass('active');
		}
	    });
	}
	</script>-->
    </body>

</html>
 