@extends('admin.master') 
@section('page_title') 
View Order Details
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> View Order Details</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ORDER DETAILS
                    </h2>
                    <a href="{{url('/order/manage')}}">
                        <button type="button" class="btn bg-brown waves-effect pull-right header-button" >
                            <i class="material-icons">view_list</i> LIST
                        </button>
                    </a>
                </div>
                <div class="body">
                    <div class="col-md-12">
                            <div class="header bg-cyan">
                                    <h1 class="text-center">Order No. : #{{$order_details_data->id}}</h1>
                            </div>
                        
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="header bg-cyan">
                                    <h2>Billing Address </h2>
                                </div>
                                <div class="body" style="min-height: 300px;">
                                <p><b>Name:</b> {{$order_details_data->firstname.' '.$order_details_data->lastname}}</p>
                                <p><b>Email:</b> {{$order_details_data->email}}</p>
                                <p> <?php if($order_details_data->phone_num_two != ''){ echo '<b>First</b>'; } ?> <b>Contact Number:</b> {{$order_details_data->phone_num_one}}</p>
                                @if($order_details_data->phone_num_two != '')
                                    <p><b>Second Contact number:</b> {{$order_details_data->phone_num_two}}</p>
                                @endif
                                <p><?php if($order_details_data->address_two != ''){ echo '<b>First</b>';} ?> <b>Address:</b> {{$order_details_data->address}}</p>
                                @if($order_details_data->address_two != '')
                                <p><b>Second Address:</b> {{$order_details_data->address_two}}</p>
                                @endif
                                @if($order_details_data->additional_information != '')
                                <p><b>Additional Information :</b> {{$order_details_data->additional_information}}</p>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="header bg-cyan">
                                    <h2>
                                        Shipping Address
                                    </h2>
                                </div>
                                <div class="body" style="min-height: 300px;">
                                    <p><b>Name:</b> {{$shipping_info->name}}</p>
                                    <p><b>Email:</b> {{$shipping_info->email}}</p>
                                    <p> <?php if($shipping_info->phone_num_two != ''){ echo '<b>First</b>'; } ?> <b>Contact Number:</b> {{$shipping_info->phone_num_one}}</p>
                                    @if($shipping_info->phone_num_two != '')
                                        <p><b>Second Contact number:</b> {{$shipping_info->phone_num_two}}</p>
                                    @endif
                                    <p><?php if($shipping_info->address_two != ''){ echo '<b>First</b>';} ?> <b>Address:</b> {{$shipping_info->address}}</p>
                                    @if($shipping_info->address_two != '')
                                    <p><b>Second Address:</b> {{$shipping_info->address_two}}</p>
                                    @endif
                                    @if($shipping_info->additional_information != '')
                                    <p><b>Additional Information :</b> {{$shipping_info->additional_information}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if($order_details_data->prescription_image !== NULL)
                        <div class="col-md-12">
                            <a href="{{asset('public/admin-frontend-assets/prescription/'.$order_details_data->prescription_image)}}"   target="_blank" style="text-decoration:none;"><button class="btn bg-light-blue center-block"><i class="material-icons">insert_photo</i> View Prescription Image</button></a>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <h4>Ordered Products</h4>
                        <table class="table table-bordered table-hover" id="item_table" style="width:100%;">
                            <thead style="background:#00BCD4;color:white;">
                                <tr>
                                    <th width="5%">Sr No.</th>
                                    <th width="25%">Name</th>
                                    <th width="10%">Weight</th>
                                    <th width="10%"  class="text-center">Qty</th>
                                    <th width="20%" class="text-right">Amount(Tk)</th>
                                    <th width="15%"  class="text-right">Subtotal(Tk)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                    
                                    $i = 1;
                                    $sum = 0;
                                    foreach ($ordered_products as $row) {
                            
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $i++;?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo $row->product_name;?>
                                        </td>
                                        <td>
                                            <?php echo $row->weight;?>
                                        </td>
                                        <td  class="text-center">
                                            <?php echo $row->product_sales_qty;?>
                                        </td>
                                        <td  class="text-right">
                                            <?php echo number_format((float)$row->product_price,2,'.','');?>
                                        </td>
                                        <td  class="text-right">
                                            <?php 
                                        $sub_total = $row->product_sales_qty * $row->product_price;
                                        echo number_format((float)$sub_total,2,'.','');
                                        $sum = $sum + $sub_total;
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                        
                                    ?>
                            </tbody>
                            <tfoot id="item-table-footer">
                                <tr>
                                    <td colspan="5" class="text-right">Total Subtotal(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($sum,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Shipping Cost(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format(30,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Tax(Free)</td>
                                    <td class="text-right">
                                        <?php echo number_format(0,2);?>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right">Total(Tk)</td>
                                    <td class="text-right">
                                        <?php echo number_format($order_details_data->order_total,2);?>
                                    </td>

                                </tr>
                                
                            </tfoot>
                        </table>
                    </div>
                    
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    
</div>
@endsection
