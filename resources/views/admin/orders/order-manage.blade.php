@extends('admin.master') 
@section('page_title') 
Manage Orders
@endsection
 
@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">chrome_reader_mode</i> Manage Orders</li>
    </ol>
</div>
<div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ORDERS MANAGEMENT
                    </h2>
                </div>
                <div class="body" style="min-height:500px;">
                    @if($success_message = Session::get('success'))
                    <div class="alert bg-teal alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$success_message}}
                    </div>
                    @endif @if($error_message = Session::get('error'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        {{$error_message}}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>SL NO.</th>
                                    <th>Order No.</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Contact No.</th>
                                    <th>Total Amount(Tk)</th>
                                    <th>Ordered Date</th>
                                    <th>Order Status</th>
                                    <th>Payment Status</th>
                                    <th>Payment Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 
                                @endphp @foreach($order_data as $row)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>#{{$row->id}}</td>
                                    <td>{{$row->firstname.' '.$row->lastname}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{ $row->phone_num_one }}</td>
                                    <td>{{ number_format($row->order_total,2) }}</td>
                                    <td>{{ date('d-m-Y h:i:s A', strtotime($row->created_at)) }}</td>
                                    <td>
                                        @if($row->order_status == 'pending')
                                        <span class="label bg-pink">Pending</span> 
                                        @elseif($row->order_status == 'done')
                                        <span class="label label-success">Done</span>
                                        @else
                                        <span class="label label-danger">Cancel</span>
                                         @endif
                                    </td>
                                    <td>
                                        @if($row->payment_status == 'pending')
                                        <span class="label bg-pink">Pending</span> 
                                        @else
                                        <span class="label label-success">Paid</span>
                                         @endif
                                    </td>
                                    <td>
                                        @if($row->payment_type == 'Cash')
                                        <span class="label label-primary">Cash</span> 
                                        @elseif($row->payment_type == 'Bkash')
                                        <span class="label label-pink">Bkash</span> 
                                        @elseif($row->payment_type == 'Stripe')
                                        <span class="label bg-deep-purple">Stripe</span> 
                                        @else
                                        <span class="label bg-brown">SSLCommerz</span> 
                                        @endif
                                        </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="material-icons">view_list</i> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" style="margin-top: 0px !important;left:-100px;">

                                                @if ($row->order_status == 'pending')
                                                <li><a href="{{url('/order/done/'.$row->id)}}" class=" waves-effect waves-block"
                                                        onclick="return confirm('Are you sure to done this {{$row->id}} order?');"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Order {{$row->id}} done"><i class="material-icons">done</i> Done</a></li>
                                                @endif
                                                @if($row->payment_type == 'Cash' || $row->payment_type == 'Bkash')
                                                    @if ($row->payment_status == 'pending')
                                                    <li><a href="{{url('/order/paid/'.$row->id)}}" class=" waves-effect waves-block"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Order {{$row->id}} payment paid"><i class="material-icons">thumb_up</i> Paid</a></li>
                                                    @endif
                                                @endif



                                                <li><a href="{{url('/order/view/'.$row->id)}}" class=" waves-effect waves-block"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="View {{$row->id}} Details"><i class="material-icons">remove_red_eye</i> View</a></li>

                                                <li><a href="{{url('/order/delete/'.$row->id)}}" class=" waves-effect waves-block"
                                                        onclick="return confirm('Are you sure to delete {{$row->id}}?');"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{$row->id}}"><i class="material-icons">delete</i> Delete</a></li>

                                            </ul>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>
@endsection