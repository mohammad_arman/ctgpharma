@extends('admin.master')

@section('page_title')
Add Employee
@endsection

@section('admin_main_content')

<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">playlist_add</i> Add Employee</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD EMPLOYEE
                    </h2>   
		    <a href="{{url('/employee/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <form id="save_employee" method="POST" action="{{ url('employee/save') }}">
                        {{ csrf_field() }}

			    <label for="name">Name <b style="color: red;">*</b></label>
			    <div class="form-group">
				<div class="form-line{{ $errors->has('name') ? ' has-error' : '' }}">
				    <input type="text" id="name" name="name" class="form-control" placeholder="Enter employee name">
				</div>  
				@if ($errors->has('name'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('name') }}</strong>
				    </span>
				@endif
			    </div>

			    <label for="email">Email <b style="color: red;">*</b></label>
			    <div class="form-group">
				<div class="form-line{{ $errors->has('email') ? ' has-error' : '' }}">
				    <input type="email" id="email" name="email" class="form-control" placeholder="Enter employee email">
				</div>  
				@if ($errors->has('email'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('email') }}</strong>
				    </span>
				@endif
			    </div>
			    <label for="password">Password <b style="color: red;">*</b></label>
			    <div class="form-group">
				<div class="form-line{{ $errors->has('password') ? ' has-error' : '' }}">
				    <input type="password" id="password" name="password" class="form-control" placeholder="Enter employee password">
				</div>  
				@if ($errors->has('password'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('password') }}</strong>
				    </span>
				@endif
			    </div>
			    <label for="password-confirm">Confirm Password <b style="color: red;">*</b></label>
			    <div class="form-group">
				<div class="form-line">
				    <input type="password" id="password-confirm" name="password_confirmation" class="form-control" placeholder="Enter password again">
				</div>  

			    </div>


			    <label for="branch_id">Branch Name <b style="color: red;">*</b></label>
			    <div class="form-group">
				<div class="form-line{{ $errors->has('branch_id') ? ' has-error' : '' }}" style="z-index:100000;">                                
				    <select class="form-control show-tick" name="branch_id" id="branch_id">
					<option value="">-- Please Select Branch --</option>
					@foreach($branch as $branch_list)
					<option value="{{$branch_list->id}}">{{$branch_list->branch_name}}</option>
					@endforeach
				    </select>                               
				</div>  
				@if ($errors->has('branch_id'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('branch_id') }}</strong>
				    </span>
				@endif
			    </div>
			    <label for="role_id">Role Name <b style="color: red;">*</b></label>
			    <div class="form-group">
				<div class="form-line{{ $errors->has('role_id') ? ' has-error' : '' }}" style="z-index:16;">                                
				    <select class="form-control show-tick" name="role_id" id="role_id">
					<option value="">-- Please Select Role --</option>
					@foreach($role as $role_list)
					@if($role_list->id !==1)
					<option value="{{$role_list->id}}">{{$role_list->role_name}}</option>
					@endif
					@endforeach
				    </select>                               
				</div>  
				@if ($errors->has('role_id'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('role_id') }}</strong>
				    </span>
				@endif
			    </div>
				<br>
				<button type="reset" class="btn bg-blue-grey waves-effect">
					<i class="material-icons">cached</i>
					<span>RESET</span>
				</button>

			    <button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
				<i class="material-icons">save</i>
				<span>SAVE</span>
			   
			
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection
