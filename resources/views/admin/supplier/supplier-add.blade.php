@extends('admin.master')

@section('page_title')
Add Supplier
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">person_add</i> Add Supplier</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        ADD SUPPLIER
                    </h2> 
		    <a href="{{url('/supplier/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <form id="save_category" method="POST" action="{{ url('supplier/save') }}">
                        {{ csrf_field() }}

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="supplier_name">Supplier Name <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('supplier_name') ? ' has-error' : '' }}">
					<input type="text" id="supplier_name" name="supplier_name" class="form-control" placeholder="Enter supplier name">
				    </div>  
				    @if ($errors->has('supplier_name'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('supplier_name') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="email">Supplier Email</label>
				<div class="form-group">
				    <div class="form-line">
					<input type="email" id="email" name="email" class="form-control" placeholder="Enter supplier email">
				    </div>  
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="contact_number">Contact Number <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('contact_number') ? ' has-error' : '' }}">
					<input type="text" id="contact_number" name="contact_number" class="form-control" placeholder="Enter supplier contact number">
				    </div>  
				    @if ($errors->has('contact_number'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('contact_number') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="company_id">Company Name <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('company_id') ? ' has-error' : '' }}" style="z-index: 10;">                                
					<select class="form-control show-tick" name="company_id" id="company_id" data-live-search="true">
					    <option value="">-- Select Company --</option>
					    @foreach($company as $company_value)
					    <option value="{{$company_value->id}}">{{$company_value->company_name}}</option>
					    @endforeach
					</select>                               
				    </div>  
				    @if ($errors->has('company_id'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('company_id') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

<!--			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="branch_id">Branch Name <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('branch_id') ? ' has-error' : '' }}" style="z-index: 5;">                                
					<select class="form-control show-tick" name="branch_id" id="branch_id" data-live-search="true" required>
					    <option>-- Select Branch --</option>
					    @foreach($branch as $branch_value)
					    <option value="{{$branch_value->id}}">{{$branch_value->branch_name}}</option>
					    @endforeach
					</select>                               
				    </div>  
				    @if ($errors->has('branch_id'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('branch_id') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>-->

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="address">Supplier Address <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('address') ? ' has-error' : '' }}">
					<textarea name="address"  class="editable"></textarea>
				    </div>
				    @if ($errors->has('address'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('address') }}</strong>
				    </span>
				    @endif
				</div> 
			    </div> 
                        </div> 



                        <input type="hidden" name="created_by" class="form-control" value="{{ Auth::user()->name }}">

                        <br>
			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
						<button type="reset" class="btn bg-blue-grey waves-effect">
								<i class="material-icons">cached</i>
								<span>RESET</span>
							</button>
				<button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
				    <i class="material-icons">save</i>
				    <span>SAVE</span>
				</button>
			    </div>
			</div>
			<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection




