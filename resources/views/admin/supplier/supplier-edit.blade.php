@extends('admin.master')

@section('page_title')
Edit Supplier Details
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">border_color</i> Edit Supplier Details</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        EDIT SUPPLIER DETAILS
                    </h2>           
		    <a href="{{url('/supplier/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                    @if(session()->has('message'))
                    <div class="alert bg-teal alert-dismissible" role="alert" id="msg">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{session()->get('message')}}
                    </div>
                    @endif
                    <form id="save_category" method="POST" action="{{ url('supplier/update') }}">
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="id" value="{{$supplier_info_by_id->id}}" />

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="supplier_name">Supplier Name <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('supplier_name') ? ' has-error' : '' }}">
					<input type="text" id="supplier_name" name="supplier_name" value="{{$supplier_info_by_id->supplier_name}}" class="form-control" placeholder="Enter supplier name">
				    </div>  
				    @if ($errors->has('supplier_name'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('supplier_name') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="email">Supplier Email</label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('email') ? ' has-error' : '' }}">
					<input type="email" id="email" name="email" value="{{$supplier_info_by_id->email}}" class="form-control" placeholder="Enter supplier email">
				    </div>  
				    @if ($errors->has('email'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('email') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="contact_number">Contact Number <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('contact_number') ? ' has-error' : '' }}">
					<input type="text" id="contact_number" name="contact_number" value="{{$supplier_info_by_id->contact_number}}" class="form-control" placeholder="Enter supplier contact number">
				    </div>  
				    @if ($errors->has('contact_number'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('contact_number') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>

			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="company_id">Company Name <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('company_id') ? ' has-error' : '' }}" style="z-index: 10;">                                
					<select class="form-control show-tick" name="company_id" id="company_id" data-live-search="true">
					    <option>-- Select Company --</option>
					    @foreach($company as $company_value)
					    <option <?php if ($supplier_info_by_id->company_id == $company_value->id) { ?> selected="selected" <?php } ?> value="{{$company_value->id}}">{{$company_value->company_name}}</option>
					    @endforeach
					</select>                               
				    </div>  
				    @if ($errors->has('company_id'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('company_id') }}</strong>
				    </span>
				    @endif
				</div>
			    </div>
                        </div>


			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
				<label for="address">Supplier Address <b style="color: red;">*</b></label>
				<div class="form-group">
				    <div class="form-line{{ $errors->has('address') ? ' has-error' : '' }}">
					<textarea name="address"  class="editable">{{$supplier_info_by_id->address}}</textarea>
				    </div>
				    @if ($errors->has('address'))
				    <span class="help-block">
					<strong style="color: red;">{{ $errors->first('address') }}</strong>
				    </span>
				    @endif
				</div> 
			    </div> 
                        </div> 

                        <br>
			<div class="col-md-12" style="margin-bottom: 0px;">
			    <div class="col-md-6" style="margin-bottom: 0px;">
						<a href="{{url('/supplier/manage')}}" type="submit" class="btn bg-blue-grey waves-effect">
							<i class="material-icons">clear</i>
							<span>CANCEL</span>
						</a>
				<button type="submit" class="btn bg-light-blue waves-effect" style="margin-left:5px;">
				    <i class="material-icons">publish</i>
				    <span>UPDATE</span>
				</button>
			    </div>
			</div>
			<div style="clear: both;"></div>
                    </form>
                </div>
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection




