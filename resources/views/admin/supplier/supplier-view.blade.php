@extends('admin.master')

@section('page_title')
View Supplier Details
@endsection

@section('admin_main_content')
<div class="container-fluid">
    <ol class="breadcrumb breadcrumb-bg-cyan">
        <li><a href="{{url('/dashboard')}}"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">visibility</i> View Supplier Details</li>
    </ol>
</div>  
<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">
                    <h2>
                        VIEW SUPPLIER DETAILS
                    </h2>           
		    <a href="{{url('/supplier/manage')}}">
			<button type="button" class="btn bg-brown waves-effect pull-right header-button" >
			    <i class="material-icons">view_list</i> LIST
			</button>
		    </a>
                </div>
                
                <div class="body">
                       <ul class="view-list col-md-12">
		<li>
		    <span>Supplier Name:</span>
		    <span>{{$supplier_info_by_id->supplier_name}}</span>
		</li>
		<li>
		    <span>Company Name:</span>
		    <span>{{$supplier_info_by_id->company->company_name}}</span>
		</li>
		<li>
		    <span>Supplier Email:</span>
		    <span>{{$supplier_info_by_id->email}}</span>
		</li>
		<li>
		    <span>Mobile Number:</span>
		    <span>{{$supplier_info_by_id->contact_number}}</span>
		</li>
		
		<li>
		    <span>Supplier Address:</span>
		    <span>@php echo $supplier_info_by_id->address; @endphp</span>
		</li>
		
		
		<li>
		    <span>Added By:</span>
		    <span>{{$supplier_info_by_id->created_by}}</span>
		</li>
		
	    </ul>
	    <div class="col-md-12">
		<a href="{{url('/supplier/manage')}}">
		    <button type="button" class="btn bg-light-blue waves-effect pull-left" >
			<i class="material-icons">arrow_back</i> BACK
		    </button>
		</a>
	    </div>
                </div>
                 <div style="clear: both;"></div> 
                        
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->

</div>
@endsection




