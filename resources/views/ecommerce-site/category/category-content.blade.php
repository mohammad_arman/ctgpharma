@extends('ecommerce-site.master')

@section('title')
Category
@endsection

@section('site_main_content')

        <div class="container main-container headerOffset">

            <div class="row">
                <div class="breadcrumbDiv col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">{{$category_name->category_name}}</li>
                    </ul>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="w100 clearfix category-top">
                        <h2> {{$category_name->category_name}} </h2>
                        <div class="categoryImage"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/category_1.jpg" style="width:100%" height="300px" alt="img"></div>
                    </div>

<!--                    <div class="row subCategoryList clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
                            <div class="thumbnail equalheight"><a class="subCategoryThumb" href="sub-category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/product/3.jpg" class="img-rounded " alt="img"> </a> <a class="subCategoryTitle"><span> T shirt </span></a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center">
                            <div class="thumbnail equalheight"><a class="subCategoryThumb" href="sub-category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/casual.jpg" class="img-rounded " alt="img"> </a> <a class="subCategoryTitle"><span> Shirt </span></a></div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center">
                            <div class="thumbnail equalheight"><a class="subCategoryThumb" href="sub-category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/shoe.jpg" class="img-rounded " alt="img"> </a> <a class="subCategoryTitle"><span> shoes </span></a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center">
                            <div class="thumbnail equalheight"><a class="subCategoryThumb" href="sub-category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/jewelry.jpg" class="img-rounded " alt="img"> </a> <a class="subCategoryTitle"><span> Accessories </span></a></div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center">
                            <div class="thumbnail equalheight"><a class="subCategoryThumb" href="sub-category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/winter.jpg" class="img-rounded  " alt="img"> </a> <a class="subCategoryTitle"><span> Winter Collection </span></a></div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center">
                            <div class="thumbnail equalheight"><a class="subCategoryThumb" href="sub-category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/Male-Fragrances.jpg" class="img-rounded " alt="img"> </a> <a class="subCategoryTitle"><span> Fragrances </span></a></div>
                        </div>
                    </div>-->

                    <div class="w100 productFilter clearfix">
                        <p class="pull-left"> Showing <strong>{{$cat_list->total()}}</strong> products </p>
                        {{-- <div class="pull-right ">
                            <div class="change-order pull-right">
                                <select class="form-control" name="orderby">
                                    <option selected="selected">Default sorting</option>
                                    <option value="popularity">Sort by popularity</option>
                                    <option value="rating">Sort by average rating</option>
                                    <option value="date">Sort by newness</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select>
                            </div>
                            <div class="change-view pull-right"><a href="#" title="Grid" class="grid-view"> <i class="fa fa-th-large"></i> </a> <a href="#" title="List" class="list-view "><i class="fa fa-th-list"></i></a></div>
                        </div> --}}
                    </div>

                    <div class="row  categoryProduct xsResponse clearfix">
			@foreach($cat_list as $product)
                        <div class="item col-sm-4 col-lg-3 col-md-3 col-xs-6">
                            <div class="product">
				{{-- <a href="{{url('/wishlist/'.$product->id)}}"class="add-fav tooltipHere" data-toggle="tooltip" data-original-title="Add to Wishlist" data-placement="left">
				    <i class="glyphicon glyphicon-heart"></i>
				</a> --}}
				<div class="image">
				    <div class="quickview">
					<a href="{{url('/product-details/'.$product->id)}}" class="btn btn-xs btn-quickview">View Details </a>
				    </div>
				    <a href="{{url('/product-details/'.$product->id)}}"><img src="{{asset('public/admin-frontend-assets/product-image/'.$product->product_image)}}" alt="img" style="max-width:100%;height: 250px;"></a>
				    @if($product->discount > 0 )<div class="promotion"><span class="new-product"> OFF</span> <span class="discount">{{$product->discount}}%</span></div>@endif
				</div>
				<div class="title" style="margin-top: 10px;">
				    <h4><a href="{{url('/product-details/'.$product->id)}}">{{strtoupper($product->product_name)}}</a></h4>
                </div>
                <div class="price">
                @if($product->discount > 0 )
                <?php 
                $discount = $product->piece_sales_rate * ($product->discount/100);
                ?>
                <del><span>{{number_format($product->piece_sales_rate,2)}} Tk</span></del>

                <span>{{number_format($product->piece_sales_rate - $discount  ,2)}} Tk</span>
                
                @else
                <span>{{number_format($product->piece_sales_rate,2)}} Tk</span>
                
                @endif
				</div>
				<div class="action-control">
				    {!! Form::open(['url'=>'/add-cart','method'=>'post']) !!}
					<input type="hidden" name="qty" value="<?php echo 1;?>"/>
					<input type="hidden" name="id" value="{{$product->id}}"/>
					<button type="submit" class="btn btn-success"><span class="add2cart"><i class="glyphicon glyphicon-shopping-cart"> </i> Add to cart </span></button>

				    {!! Form::close() !!}
				</div>
			    </div>
                        </div>

			@endforeach
                    </div>

                    <div class="w100 categoryFooter">
                        <div class="pagination pull-left no-margin-top">
                            <ul class="pagination no-margin-top">
				{{ $cat_list->links() }}
                            </ul>
                        </div>
                        <div class="pull-right pull-right col-sm-4 col-xs-12 no-padding text-right text-left-xs">
                            <p>Showing {{$cat_list->firstItem()}}–{{$cat_list->lastItem()}} of {{$cat_list->total()}} results</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="gap"></div>
@endsection

