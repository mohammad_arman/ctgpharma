@extends('ecommerce-site.master') 
@section('title') Cart
@endsection
 
@section('site_main_content')
<div class="container main-container headerOffset">
	<div class="row">
		<div class="breadcrumbDiv col-lg-12">
			<ul class="breadcrumb">
				<li><a href="{{url('/')}}">Home</a></li>
				<li class="active">Cart</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-9 col-md-9 col-sm-7 col-xs-6 col-xxs-12 text-center-xs">
			<h1 class="section-title-inner"><span><i class="glyphicon glyphicon-shopping-cart"></i> Shopping cart </span></h1>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-5 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
			<h4 class="caps"><a href="{{url('/')}}"><i class="fa fa-chevron-left"></i> Back to shopping </a></h4>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-9 col-md-9 col-sm-7">
			<div class="row userInfo">
				<div class="col-xs-12 col-sm-12">
					<div class="cartContent w100">
						<table class="cartTable table-responsive" style="width:100%" border="1">

							<thead class="CartProduct cartTableHeader">
								<th class="text-center" style="width:15%"> Product Image</th>
								<th class="text-center" style="width:20%">Product Name</th>
								<th class="text-center" style="width:10%">Price(BDT)</th>
								<th class="text-center" style="width:7%">Delete</th>
								<th class="text-center" style="width:15%">Quantity</th>
								<th class="text-center" style="width:10%">Update</th>
								<th class="text-center" style="width:15%">Total(BDT)</th>
							</thead>
							<tbody>
								<?php //echo '<pre>';print_r(Cart::content());exit;?>
								@foreach(Cart::content() as $row)
								<tr class="CartProduct">
									<td class="CartProductThumb">
										<div>
											<img src="{{asset('public/admin-frontend-assets/product-image/'.$row->options->image)}}" alt="img" style="width:100px;height: 100px;">
										</div>
									</td>
									<td>
										<h4 class="text-center" id="p_name{{$row->rowId}}">{{$row->name}}</h4>
									</td>
									<td>
										<h4>{{number_format($row->price,2)}}</h4>
									</td>

									{!! Form::open(['url'=>'/delete-to-cart','method'=>'post']) !!}
									<td class="delete">
										<input type="hidden" name="rowId" value="{{$row->rowId}}" />
										<button type="submit"><i class="glyphicon glyphicon-trash fa-2x"></i></button>
									</td>
									{!! Form::close() !!}
									 {!! Form::open(['url'=>'/update-cart','method'=>'post','id'=>'update_form']) !!}
									<td>

										<input type="hidden" data-srno="{{$row->rowId}}" name="rowId" value="{{$row->rowId}}" />
										<input type="hidden" id="old_qty{{$row->rowId}}" data-srno="{{$row->rowId}}" name="old_qty" value="{{$row->options->old_qty}}" />
										<input class="quanitySniper" id="qty{{$row->rowId}}" data-qty="{{$row->qty}}" data-srno="{{$row->rowId}}" type="text" value="{{$row->qty}}" name="qty">
									</td>
									<td>
										<input type="submit"  data-srno="{{$row->rowId}}"  value="UPDATE" class="btn btn-primary btn-xs submit" />

									</td>
									{!! Form::close() !!}
									<td class="price">{{number_format($row->total,2)}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="cartFooter w100">
						<div class="box-footer">
							<div class="pull-left"><a href="{{url('/')}}" class="btn btn-default"> <i class="fa fa-arrow-left"></i> &nbsp; Continue shopping </a></div>

						</div>
					</div>

				</div>
			</div>

		</div>
		<div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
			<div class="contentBox">
				<div class="w100 costDetails">
					<div class="table-block" id="order-detail-content">
						@php
							$customer_id = Session::get('customer_id');
							$shipping_id = Session::get('shipping_id');
							if($customer_id != ''){
								$customer_data = DB::table('customers')->where('id',$customer_id)->first();
								$billing_address = $customer_data->address;
							}
							
						@endphp
						@if ($customer_id != '' && $shipping_id == '' && !empty($billing_address))
							<a class="btn btn-primary btn-lg btn-block checkout_btn" title="checkout" href="{{url('/shipping')}}" style="margin-bottom:20px" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> Proceed to checkout &nbsp; <i class="fa fa-arrow-right"></i> </a>
						@elseif($customer_id != '' && $shipping_id != '' && !empty($billing_address))
							<a class="btn btn-primary btn-lg btn-block checkout_btn" title="checkout" href="{{url('/payment')}}" style="margin-bottom:20px" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> Proceed to checkout &nbsp; <i class="fa fa-arrow-right"></i> </a>						
						@else
							<a class="btn btn-primary btn-lg btn-block checkout_btn" title="checkout" href="{{url('/checkout')}}" style="margin-bottom:20px" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> Proceed to checkout &nbsp; <i class="fa fa-arrow-right"></i> </a>
						@endif
						
						
						
						<div class="w100 cartMiniTable">

							<table id="cart-summary" class="std table">
								<tbody>
									<tr>
										<td style="font-size: 13px;">Total products</td>
										<td style="font-size: 13px;">BDT {{ Cart::subtotal()}}</td>
									</tr>
									<tr style="">
										<td style="font-size: 13px;"><span class="success">Shipping</span></td>
										<td style="font-size: 13px;">
											@if(count(Cart::content()) > 0)
											<span class="success">BDT {{number_format($shipping = 50,2)}}</span>
											@else
											<span class="success">BDT {{number_format($shipping = 0,2)}}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td style="font-size: 13px;">Total tax(Free)</td>
										<td style="font-size: 13px;" id="total-tax">BDT {{ Cart::tax()}}</td>
									</tr>
									<tr>
										<td style="font-size: 15px;" class=" site-color" id="total-price"> Total</td>
										<td style="font-size: 15px;" class=" site-color" id="total-price">
											@if(count(Cart::content()) > 0)
											<?php
												$total = Cart::total();
												$value = str_replace(',','', $total);
												$number = str_replace('.00','', $value);
												
												$final_total = $shipping + $number;
												if($final_total != 0){
													Session::put('final_total',$final_total);
												}
												echo 'BDT '.number_format($final_total,2);
												
											?>
											@else
												<?php echo 'BDT '.number_format(0,2);?>
											@endif
										</td>
									</tr>

								</tbody>

							</table>
						</div>
						
					</div>
				</div>
			</div>

		</div>

	</div>

	<div style="clear:both"></div>
</div>

<div class="gap"></div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click','.submit',function(){
	    var id = $(this).parent().parent().find('.submit').data('srno');
		var product = $('#p_name'+id).text();
		var qty = $('#qty'+id).data('qty');
		var old_qty = $('#old_qty'+id).val();
		var new_qty = $('#qty'+id).val();
		var qty_check = old_qty - new_qty;
		if(qty_check < 0){
			
			alert(product+' quantity croos our stock quantity');
			$('#qty'+id).val(qty);
			// $('.checkout_btn').prop("disabled",true);
			return false;
		}
		if(new_qty <= 0){
			alert(product+' quantity must be greater than zero');
			// $('.checkout_btn').prop("disabled",true);
			return false;
		}
		$("#update_form").submit();
		});
		
	});
	</script>
@endsection