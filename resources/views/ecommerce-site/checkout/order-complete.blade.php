@extends('ecommerce-site.master') 
@section('title') Cart
@endsection
 
@section('site_main_content')
<div class="container main-container headerOffset">
    <div class="row">
        <div class="breadcrumbDiv col-lg-12">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Order</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 ">
            <div class="row userInfo">
                <div class="thanxContent text-center">
                <h1> Thank you for your order <a href="order-status.html">#{{$order_id}}</a></h1>
                    <h4>we'll let you know when your items are on their way</h4>
                </div>
                <div class="col-lg-7 col-center">
                    <h4></h4>
                    <div class="cartContent table-responsive  w100">
                        <table style="width:100%" class="cartTable cartTableBorder">
                            <tbody>
                                <tr class="CartProduct  cartTableHeader">
                                    <td colspan="2"> Items to be Shipped</td>
                                    <td style="width:15%"></td>
                                </tr>
                                @foreach ($products as $product)
                                    
                                
                                <tr class="CartProduct">
                                    <td class="CartProductThumb">
                                        <div><a href=''><img alt="img" src="{{asset('public/admin-frontend-assets/product-image/'.$product->product_image)}}"></a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="CartDescription">
                                            <h4><a href="product-details.html">{{$product->product_name}} </a></h4>
                                        <span class="size">Qty: {{$product->product_sales_qty}}</span>
                                        <div class="price"><span>Price: BDT {{number_format($product->product_price,2)}}</span></div>
                                        </div>
                                    </td>
                                <td class="price">Total : {{number_format(($product->product_sales_qty * $product->product_price),2)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div style="clear:both"></div>
</div>

<div class="gap"></div>
@endsection