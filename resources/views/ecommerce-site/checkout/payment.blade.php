@extends('ecommerce-site.master')

@section('title')
Payment
@endsection

@section('site_main_content')
<div class="container main-container headerOffset">
    <div class="row">
	<div class="breadcrumbDiv col-lg-12">
	    <ul class="breadcrumb">
		<li><a href="{{url('/')}}">Home</a></li>
		<li><a href="{{url('/cart')}}">Cart</a></li>
		<li class="active"> Checkout</li>
	    </ul>
	</div>
    </div>
    <div class="row">
	<div class="col-lg-9 col-md-9 col-sm-7 col-xs-6 col-xxs-12 text-center-xs">
	    <h1 class="section-title-inner"><span><i class="glyphicon glyphicon-shopping-cart"></i> Checkout</span></h1>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-5 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
	    <h4 class="caps"><a href="{{url('/')}}"><i class="fa fa-chevron-left"></i> Back to shopping </a></h4>
	</div>
    </div>
    <div class="row">
	<div class="col-lg-9 col-md-9 col-sm-12">
	    <div class="row userInfo">
		<div class="col-xs-12 col-sm-12">
		    <div class="w100 clearfix">
			<ul class="orderStep orderStepLook2">
					<li><a href="{{url('/billing')}}"> <i class="fa fa fa-envelope  "></i> <span> Billing </span></a></li>
					<li><a href="{{url('/shipping')}}"><i class="fa fa-truck "> </i><span>Shipping</span> </a></li>
					<li class="active"><a href="{{url('/payment')}}"><i class="fa fa-money  "> </i><span>Payment</span> </a></li>
			    {{-- <li><a href="{{url('/order-confirm')}}"><i class="fa fa-check-square "> </i><span>Order</span></a> --}}
			    </li>
			</ul>

		    </div>
		    <div class="w100 clearfix">
			<div class="row userInfo">
			    <div class="col-lg-12">
				<h2 class="block-title-2"> Payment method </h2>
				<p>Please select the preferred payment method to pay for this order.</p>
				<hr>
			    </div>
			    <div class="col-xs-12 col-sm-12">
				<div class="paymentBox">
				    <div class="panel-group paymentMethod" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading panel-heading-custom"> </div>				
							<div class="panel-body">
								<p>All transactions are secure and encrypted</p>
								<br>
								@if($error_message = Session::get('error'))
								<div class="alert alert-danger alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									{{$error_message}}
								</div>
								@endif
								<form action="{{url('place-order')}}" method="POST">
										{{ csrf_field() }}
									<div class="col-md-12">
										<label class="radio-inline" for="radios-4">
										<input name="payment_type" id="radios-4" value="Cash" type="radio">
										Cash On Delivery </label>
									</div>
									<div class="col-md-12">
										<label class="radio-inline" for="radios-4">
										<input name="payment_type" id="radios-4" value="Stripe" type="radio">
										Payment with stripe </label>
									</div>
									{{-- <div class="col-md-12">
										<label class="radio-inline" for="radios-4">
										<input name="payment_type" id="radios-4" value="SSLCommerz" type="radio">
										Payment with SSLCommerz </label>
									</div> --}}
									<div class="pull-right">
										<button type="submit" class="btn btn-primary btn-small "> Order &nbsp; <i class="fa fa-arrow-circle-right"></i> </button>
									</div>

								</form>
							</div>

						</div>
					

				    </div>
				</div>

			    </div>
			</div>
		    </div>

<!--		    <div class="cartFooter w100">
			<div class="box-footer">
			    <div class="pull-left"><a class="btn btn-default" href="checkout-3.html"> <i class="fa fa-arrow-left"></i> &nbsp; Billing address </a></div>
			</div>
		    </div>-->
		</div>

	    </div>
	</div>

	<div class="col-lg-3 col-md-3 col-sm-12 rightSidebar">
	    <div class="w100 cartMiniTable">
		<table id="cart-summary" class="std table">
		    <tbody>
			<tr>
			    <td style="font-size: 13px;">Total products</td>
			    <td style="font-size: 13px;">BDT {{ Cart::subtotal()}}</td>
			</tr>
			<tr style="">
			    <td style="font-size: 13px;"><span class="success">Shipping</span></td>
			    <td style="font-size: 13px;"><span class="success">BDT {{number_format($shipping = 50,2)}}</span></td>
			</tr>
			<tr>
			    <td style="font-size: 13px;">Total tax(Free)</td>
			    <td style="font-size: 13px;" id="total-tax">BDT {{ Cart::tax()}}</td>
			</tr>
			<tr>
			    <td style="font-size: 15px;" class=" site-color" id="total-price"> Total</td>
			    <td style="font-size: 15px;" class=" site-color" id="total-price">
				<?php
				$total = Cart::total();
				$value = str_replace(',', '', $total);
				$number = str_replace('.00', '', $value);

				$final_total = $shipping + $number;
				echo 'BDT ' . number_format($final_total, 2);
				?></td>
			</tr>

		    </tbody>
		    <tbody>
		    </tbody>
		</table>
	    </div>
	</div>

    </div>

    <div style="clear:both"></div>
</div>

<div class="gap"></div>
@endsection
