@extends('ecommerce-site.master') 
@section('title') 
Payment With Stripe
@endsection
 
@section('site_main_content')
<div class="container main-container headerOffset">
    <div class="row">
        <div class="breadcrumbDiv col-lg-12">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Payment</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="col-md-4">
                <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{{url('/addmoney/stripe')}}">
                    {{ csrf_field() }}

                    <div class='form-row'>
                        <div class='col-xs-12 form-group card required'>
                            <label class='control-label'>Card Number</label>
                            <input autocomplete='off' class='form-control card-number' size='20' type='text' name="card_no">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class='col-md-4 form-group cvc required'>
                            <label class='control-label'>CVV</label>
                            <input autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text' name="cvvNumber">
                        </div>
                        <div class='col-md-4 form-group expiration required'>
                            <label class='control-label'>Expiration</label>
                            <input class='form-control card-expiry-month' placeholder='MM' size='2'  type='text' name="ccExpiryMonth">
                        </div>
                        <div class='col-md-4 form-group expiration required'>
                            <label class='control-label'> </label>
                            <input class='form-control card-expiry-year' style="margin-top: 7px;" placeholder='YYYY'  size='4' type='text' name="ccExpiryYear">
                        <input class='form-control' type='hidden' name="amount" value="{{$total}}">
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    {{-- <div class='form-row'>
                        <div class='col-md-12' style="padding-left:0px;">
                            <div class='form-control total btn btn-info' style="width: 320px;margin-bottom:10px;">
                                Total:
                                <span class='amount'>$300</span>
                            </div>
                        </div>
                    </div> --}}

                    <div class='form-row'>
                        <div class='col-md-12 form-group'>
                        <div class='col-md-6' style="padding-left:0;">
                        <button class='form-control btn btn-primary submit-button' type='submit'>Pay BDT {{number_format($total,2)}}</button>
                        </div>
                        <div class='col-md-6' style="padding-right:0;">
                            <a href="{{url('/')}}" class='form-control btn btn-default' >Cancel</a>
                        </div>

                        
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class='col-md-12 error form-group hide'>
                            <div class='alert-danger alert'>
                                Please correct the errors and try again.
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </form>

            </div>
        </div>
    </div>

    <div style="clear:both;"></div>
</div>

<div class="gap"></div>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.submit-button',function(){
            if($.trim($('.card-number').val()).length == 0)
		 	{
				alert("Please enter card number");
				$('.card-number').focus();
				return false;
		 	}
            if(!$.isNumeric($('.card-number').val()))
		 	{
				alert("Please enter card number numeric value");
				$('.card-number').focus();
				return false;
		 	}
            if($.trim($('.card-number').val()).length != 16)
		 	{
				alert("Card number must be 16 digit");
				$('.card-number').focus();
				return false;
		 	}
            if($('.card-number').val() < 0)
		 	{
				alert("Card number can'\t be less than zero");
				$('.card-number').focus();
				return false;
		 	}

            if($.trim($('.card-cvc').val()).length == 0)
		 	{
				alert("Please enter card cvc number");
				$('.card-cvc').focus();
				return false;
		 	}
            if(!$.isNumeric($('.card-cvc').val()))
		 	{
				alert("Please enter card cvc number numeric value");
				$('.card-cvc').focus();
				return false;
		 	}
            if($.trim($('.card-cvc').val()).length != 3)
		 	{
				alert("Card cvc number must be 3 digit");
				$('.card-cvc').focus();
				return false;
		 	}
            if($('.card-cvc').val() < 0)
		 	{
				alert("Card cvc number can'\t be less than zero");
				$('.card-cvc').focus();
				return false;
		 	}

            if($.trim($('.card-expiry-month').val()).length == 0)
		 	{
				alert("Please enter card expiry month");
				$('.card-expiry-month').focus();
				return false;
		 	}
            if(!$.isNumeric($('.card-expiry-month').val()))
		 	{
				alert("Please enter card expiry month numeric value");
				$('.card-expiry-month').focus();
				return false;
		 	}
           
            if($.trim($('.card-expiry-month').val()).length != 2)
		 	{
				alert("Card expiry month value must be 2 digit");
				$('.card-expiry-month').focus();
				return false;
		 	}
            // var month = $('.card-expiry-month').val(); 
            // if(month < 1 && month > 12))
		 	// {
			// 	alert("Card expiry month value must be between 1-12");
			// 	$('.card-expiry-month').focus();
			// 	return false;
		 	// }

            if($.trim($('.card-expiry-year').val()).length == 0)
		 	{
				alert("Please enter card expiry year");
				$('.card-expiry-year').focus();
				return false;
		 	}
            if(!$.isNumeric($('.card-expiry-year').val()))
		 	{
				alert("Please enter card expiry year numeric value");
				$('.card-expiry-year').focus();
				return false;
		 	}
            if($.trim($('.card-expiry-year').val()).length != 4)
		 	{
				alert("Card expiry year value must be 4 digit");
				$('.card-expiry-year').focus();
				return false;
		 	}
            if($('.card-expiry-year').val() < 0)
		 	{
				alert("Card expiry year can\'t be less than zero");
				$('.card-expiry-year').focus();
				return false;
		 	}
            var current_year = "<?php echo date('Y');?>";
            if($('.card-expiry-year').val() < current_year)
		 	{
				alert("Card expiry year can\'t be less than current year");
				$('.card-expiry-year').focus();
				return false;
		 	}
             $('#payment-form').submit();
        });
    });
</script>
@endsection