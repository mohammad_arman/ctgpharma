@extends('ecommerce-site.master')

@section('title')
Shipping Address
@endsection

@section('site_main_content')
<div class="container main-container headerOffset">
    <div class="row">
	<div class="breadcrumbDiv col-lg-12">
	    <ul class="breadcrumb">
		<li><a href="{{url('/')}}">Home</a></li>
		<li><a href="{{url('/cart')}}">Cart</a></li>
		<li class="active"> Checkout</li>
	    </ul>
	</div>
    </div>
    <div class="row">
	<div class="col-lg-9 col-md-9 col-sm-7 col-xs-6 col-xxs-12 text-center-xs">
	    <h1 class="section-title-inner"><span><i class="glyphicon glyphicon-shopping-cart"></i> Checkout</span></h1>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-5 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
	    <h4 class="caps"><a href="{{url('/')}}"><i class="fa fa-chevron-left"></i> Back to shopping </a></h4>
	</div>
    </div>
    <div class="row">
	<div class="col-lg-9 col-md-9 col-sm-12">
	    <div class="row userInfo">
		<div class="col-xs-12 col-sm-12">
		    <div class="w100 clearfix">
			<ul class="orderStep orderStepLook2">
	
					<li ><a href="{{url('/billing')}}"> <i class="fa fa fa-envelope  "></i> <span> Billing </span></a></li>
					<li class="active"><a href="{{url('/shipping')}}"><i class="fa fa-truck "> </i><span>Shipping</span> </a></li>
					<li><a href="{{url('/payment')}}"><i class="fa fa-money  "> </i><span>Payment</span> </a></li>
			    {{-- <li><a href="{{url('/order-confirm')}}"><i class="fa fa-check-square "> </i><span>Order</span></a> --}}
			    </li>
			   
			</ul>

		    </div>
		    <div class="w100 clearfix">
			<div class="row userInfo">
			    <div class="col-lg-12">
				<h2 class="block-title-2"> To add a shipping address, please fill out the form
				    below. </h2>
			    </div>
<!--			    <div class="col-xs-12 col-sm-12">
				<label class="checkbox-inline" for="checkboxes-0">
				    <input name="checkboxes" id="checkboxes-0" value="1" type="checkbox">
				    My shipping address are same as my billing address. </label>
				<hr>
			    </div>-->
			    {!! Form::open(['url'=>'/save-shipping','method'=>'post']) !!}
				<div class="col-xs-12 col-sm-6">
				    <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name">Name <sup style="color: red;">*</sup> </label>
						<input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}"  class="form-control" id="customer_id" >
						<input type="text" name="name"  class="form-control" id="name" >
						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
				    </div>
				    <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">
					    <label for="email">Email <sup style="color: red;">*</sup></label>
						<input type="text" name="email"class="form-control" id="email">
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
				    </div>
				    <div class="form-group">
						<label for="company_name">Company Name </label>
						<input type="text" name="company_name" class="form-control" id="company_name" >
				    </div>

				    <div class="form-group  {{ $errors->has('phone_num_one') ? ' has-error' : '' }}">
						<label for="phone_num_one">Mobile phone One<sup style="color: red;">*</sup></label>
						<input type="text" name="phone_num_one" class="form-control" id="phone_num_one">
						@if ($errors->has('phone_num_one'))
							<span class="help-block">
								<strong>{{ $errors->first('phone_num_one') }}</strong>
							</span>
						@endif
				    </div>
				    <div class="form-group ">
						<label for="phone_num_two">Mobile phone Two</label>
						<input type="text" name="phone_num_two" class="form-control" id="phone_num_two">
				    </div>
				</div>

				<div class="col-xs-12 col-sm-6">
				    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
						<label for="address">Address <sup style="color: red;">*</sup> </label>
						<textarea rows="3" cols="26" name="address" class="form-control" id="address"></textarea>
						@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
						@endif
				    </div>
				    <div class="form-group">
						<label for="address_two">Address (Line 2) </label>
						<textarea rows="3" cols="26" name="address_two" class="form-control" id="address_two"></textarea>
				    </div>
				    <div class="form-group">
						<label for="additional_information">Additional information</label>
						<textarea rows="3" cols="26" name="additional_information" class="form-control" id="additional_information" style="height: 112px;"></textarea>
				    </div>


				</div>
				<div class="col-xs-12 col-sm-12 col-md-12">
				    <div class="pull-right">
					<button type="submit" class="btn btn-primary btn-small "> Save & Next &nbsp; <i class="fa fa-arrow-circle-right"></i> </button>
				    </div>
				</div>
			    {!! Form::close() !!}
			</div>

		    </div>
		    <div class="cartFooter w100">
			<div class="box-footer">
<!--			    <div class="pull-left">
				<a class="btn btn-default" href="{{url('/')}}"> <i class="fa fa-arrow-left"></i>
				    &nbsp; Continue Shopping </a></div>-->
<!--			    <div class="pull-right">
				<a href="{{url('/shipping')}}" class="btn btn-primary btn-small "> Shipping address &nbsp; <i class="fa fa-arrow-circle-right"></i> </a></div>-->
			</div>
		    </div>

		</div>
	    </div>

	</div>
	<div class="col-lg-3 col-md-3 col-sm-12 rightSidebar">
	    <div class="w100 cartMiniTable">
		<table id="cart-summary" class="std table">
		    <tbody>
			<tr>
			    <td style="font-size: 13px;">Total products</td>
			    <td style="font-size: 13px;">BDT {{ Cart::subtotal()}}</td>
			</tr>
			<tr style="">
			    <td style="font-size: 13px;"><span class="success">Shipping</span></td>
			    <td style="font-size: 13px;"><span class="success">BDT {{number_format($shipping = 50,2)}}</span></td>
			</tr>
			<tr>
			    <td style="font-size: 13px;">Total tax(Free)</td>
			    <td style="font-size: 13px;" id="total-tax">BDT {{ Cart::tax()}}</td>
			</tr>
			<tr>
			    <td style="font-size: 15px;" class=" site-color" id="total-price"> Total</td>
			    <td style="font-size: 15px;" class=" site-color" id="total-price">
				<?php
				$total = Cart::total();
				$value = str_replace(',', '', $total);
				$number = str_replace('.00', '', $value);

				$final_total = $shipping + $number;
				echo 'BDT ' . number_format($final_total, 2);
				?></td>
			</tr>

		    </tbody>
		    <tbody>
		    </tbody>
		</table>
	    </div>

	</div>

    </div>

    <div style="clear:both"></div>
</div>

<div class="gap"></div>
@endsection
