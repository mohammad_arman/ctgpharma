@extends('ecommerce-site.master')

@section('title')
Home
@endsection

@section('site_main_content')
<div class="banner">
    <div class="full-container">
        <div class="slider-content">
            <ul id="pager2" class="container">
            </ul>

            <span class="prevControl sliderControl"> <i class="fa fa-angle-left fa-3x "></i></span> <span class="nextControl sliderControl"> <i class="fa fa-angle-right fa-3x "></i></span>
            <div class="slider slider-v1" data-cycle-swipe=true data-cycle-prev=".prevControl" data-cycle-next=".nextControl" data-cycle-loader="wait">
                <div class="slider-item slider-item-img1">
                    <img src="{{asset('public/ecommerce-frontend-assets/images')}}/slider/slide1.jpg" class="img-responsive parallaximg sliderImg" alt="img">
                </div>
                <div class="slider-item slider-item-img1">
                    <div class="sliderInfo">
                        <div class="container">
                            <div class="col-lg-12 col-md-12 col-sm-12 sliderTextFull ">
                                <div class="inner text-center">
                                    <div class="topAnima animated">
                                        <h1 class="uppercase xlarge">BEST MEDICINE</h1>
                                        <h3 class="hidden-xs" style="color: black;"> Free Standard Shipping on Orders Over 300.00 tk </h3>
                                    </div>
                                    <a class="btn btn-danger btn-lg bottomAnima animated opacity0">SHOP NOW ON CTG-PHARMA
                                        <span class="arrowUnicode">►</span></a></div>
                            </div>
                        </div>
                    </div>
                    <img src="{{asset('public/ecommerce-frontend-assets/images')}}/slider/slide2.jpg" class="img-responsive parallaximg sliderImg" alt="img">
                </div>

                <div class="slider-item slider-item-img2 ">
                    <div class="sliderInfo">
                        <div class="container">
                            <div class="col-lg-12 col-md-12 col-sm-12 sliderTextFull  ">
                                <div class="inner dark maxwidth500 text-center animated topAnima">
                                    <div class=" ">
                                        <h1 class="uppercase xlarge"> CTG PHARMA</h1>
                                        <h3 class="hidden-xs"> Online Drugs Store </h3>
                                    </div>
                                    <a class="btn btn-danger btn-lg">SHOP NOW ON CTG-PHARMA <span class="arrowUnicode">►</span></a></div>
                            </div>
                        </div>
                    </div>
                    <img src="{{asset('public/ecommerce-frontend-assets/images')}}/slider/slide3.jpg" class="img-responsive parallaximg sliderImg" alt="img">
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-hero section-gray" id="section-shop">
    <div class="container">
        <div class="hero-section-header ">
            <h3 class="hero-section-title">OUR FEATURED PRODUCTS</h3>
        </div>
        <div class="section-content">
            <div class="row ">
                <div id="productslider" class="owl-carousel owl-theme">
		    @foreach($featured_product as $featured_product_value)
                    <div class="product-item item-flat ">
                        <div class="product">
                            {{-- <a href="{{url('/wishlist/'.$featured_product_value->id)}}"class="add-fav tooltipHere" data-toggle="tooltip" data-original-title="Add to Wishlist" data-placement="left">
                                <i class="glyphicon glyphicon-heart"></i>
                            </a> --}}
                            <div class="image">
                                <div class="quickview">
                                    <a href="{{url('/product-details/'.$featured_product_value->id)}}" class="btn btn-xs btn-quickview">View Details</a>
                                </div>
                                <a href="{{url('/product-details/'.$featured_product_value->id)}}"><img src="{{asset('public/admin-frontend-assets/product-image/'.$featured_product_value->product_image)}}" alt="img" style="max-width:100%;height: 250px;"></a>
                                <!--<div class="promotion"><span class="new-product"> NEW</span> <span class="discount">15% OFF</span></div>-->
                            </div>
                            <div class="title" style="margin-top: 10px;">
                                <h4><a href="{{url('/product-details/'.$featured_product_value->id)}}">{{strtoupper($featured_product_value->product_name)}}</a></h4>
			    </div>
                            <div class="price"><span>{{number_format($featured_product_value->piece_sales_rate,2)}} Tk</span></div>
                            <div class="action-control">
				{!! Form::open(['url'=>'/add-cart','method'=>'post']) !!}
				    <input type="hidden" name="qty" value="<?php echo 1;?>"/>
				    <input type="hidden" name="id" value="{{$featured_product_value->id}}"/>
				    <button type="submit" class="btn btn-success"><span class="add2cart"><i class="glyphicon glyphicon-shopping-cart"> </i> Add to cart </span></button>

				{!! Form::close() !!}
			    </div>
                        </div>
                    </div>  
		    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-hero white-bg" id="section-category">
    <div class="container">
        <div class="hero-section-header ">
            <h3 class="hero-section-title">OUR CATEGORY</h3>
        </div>
        <div class="row featuredPostContainer ">
            <div class="featuredImageLook3 flat">
		@foreach($category as $cat_value)
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="inner">
                        <div class="box-content-overly box-content-overly-white flat">
                            <div class="box-text-table">
                                <div class="box-text-cell ">
                                    <div class="box-text-cell-inner dark">
                                        <h1 class="uppercase">{{strtoupper($cat_value->category_name)}}</h1>
                                        <hr class="submini">
                                        <a href="{{url('/category/'.$cat_value->id)}}" class="btn btn-flat btn-dark btn-sm"> SHOP NOW</a></div>
                                </div>
                            </div>
                        </div>

                        <div class="img-title"> {{strtoupper($cat_value->category_name)}}</div>
                        <a href="{{url('/category/'.$cat_value->id)}}" class="img-block"> <img alt="img" src="{{asset('public/ecommerce-frontend-assets/images/category/'.$cat_value->category_image)}}" class="img-responsive" style="width: 100%;height: 220px;"></a>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>

<section class="section-hero section-gray" id="section-shop">
    <div class="container">
        <div class="hero-section-header ">
            <h3 class="hero-section-title">OUR DISCOUNT PRODUCTS</h3>
        </div>
        <div class="section-content">
            <div class="row has-equal-height-child">
		@foreach($discount_product as $discount_product_value)
                <div class="item col-sm-4 col-lg-3 col-md-3 col-xs-12">
                    <div class="product">
			{{-- <a href="{{url('/wishlist/'.$discount_product_value->id)}}"class="add-fav tooltipHere" data-toggle="tooltip" data-original-title="Add to Wishlist" data-placement="left">
			    <i class="glyphicon glyphicon-heart"></i>
			</a> --}}
			<div class="image">
			    <div class="quickview">
				<a href="{{url('/product-details/'.$discount_product_value->id)}}" class="btn btn-xs btn-quickview">View Details</a>
			    </div>
			    <a href="{{url('/product-details/'.$discount_product_value->id)}}"><img src="{{asset('public/admin-frontend-assets/product-image/'.$discount_product_value->product_image)}}" alt="img" style="max-width:100%;height: 250px;"></a>
			    @if($discount_product_value->discount > 0 )<div class="promotion"><span class="new-product"> OFF</span> <span class="discount">{{$discount_product_value->discount}}%</span></div>@endif
			</div>
			<div class="title" style="margin-top: 10px;">
			    <h4><a href="{{url('/product-details/'.$discount_product_value->id)}}">{{strtoupper($discount_product_value->product_name)}}</a></h4>
            </div>
            <div class="price">
            @if($discount_product_value->discount > 0 )
            <?php 
            $discount = $discount_product_value->piece_sales_rate * ($discount_product_value->discount/100);
            ?>
            <del><span>{{number_format($discount_product_value->piece_sales_rate,2)}} Tk</span>
			</del>
            <span>{{number_format($discount_product_value->piece_sales_rate - $discount  ,2)}} Tk</span>
			
            @else
            <span>{{number_format($discount_product_value->piece_sales_rate,2)}} Tk</span>
			
            @endif
            </div>
            
			
			<div class="action-control">
			    {!! Form::open(['url'=>'/add-cart','method'=>'post']) !!}
				<input type="hidden" name="qty" value="<?php echo 1;?>"/>
				<input type="hidden" name="id" value="{{$discount_product_value->id}}"/>
				<button type="submit" class="btn btn-success"><span class="add2cart"><i class="glyphicon glyphicon-shopping-cart"> </i> Add to cart </span></button>
				
			    {!! Form::close() !!}
			</div>
		    </div>
                </div>
		@endforeach
            </div>
        </div>
    </div>
</section>

<section class="section-hero section-gray" id="section-shop">
        <div class="container">
            <div class="hero-section-header ">
                <h3 class="hero-section-title">OUR REGULAR PRODUCTS</h3>
            </div>
            <div class="section-content">
                    <div class="row has-equal-height-child">
                            @foreach($regular_product as $regular_product_value)
                                    <div class="item col-sm-4 col-lg-3 col-md-3 col-xs-12">
                            <div class="product">
                                {{-- <a href="{{url('/wishlist/'.$featured_product_value->id)}}"class="add-fav tooltipHere" data-toggle="tooltip" data-original-title="Add to Wishlist" data-placement="left">
                                    <i class="glyphicon glyphicon-heart"></i>
                                </a> --}}
                                <div class="image">
                                    <div class="quickview">
                                        <a href="{{url('/product-details/'.$regular_product_value->id)}}" class="btn btn-xs btn-quickview">View Details</a>
                                    </div>
                                    <a href="{{url('/product-details/'.$regular_product_value->id)}}"><img src="{{asset('public/admin-frontend-assets/product-image/'.$regular_product_value->product_image)}}" alt="img" style="max-width:100%;height: 250px;"></a>
                                    <!--<div class="promotion"><span class="new-product"> NEW</span> <span class="discount">15% OFF</span></div>-->
                                </div>
                                <div class="title" style="margin-top: 10px;">
                                    <h4><a href="{{url('/product-details/'.$regular_product_value->id)}}">{{strtoupper($regular_product_value->product_name)}}</a></h4>
                    </div>
                                <div class="price"><span>{{number_format($regular_product_value->piece_sales_rate,2)}} Tk</span></div>
                                <div class="action-control">
                    {!! Form::open(['url'=>'/add-cart','method'=>'post']) !!}
                        <input type="hidden" name="qty" value="<?php echo 1;?>"/>
                        <input type="hidden" name="id" value="{{$regular_product_value->id}}"/>
                        <button type="submit" class="btn btn-success"><span class="add2cart"><i class="glyphicon glyphicon-shopping-cart"> </i> Add to cart </span></button>
    
                    {!! Form::close() !!}
                    </div>
                            </div>
                        </div>  
                @endforeach
                   
                </div>
            </div>
        </div>
    </section>

<div class="container">
    <hr> 
</div>


<div class="container section-block ">
    <div class="row featureImg">
        <div class="col-md-3 col-sm-3 col-xs-6"><a href="category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/1.jpg"  alt="img" width="100%"height="120px"></a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6"><a href="category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/2.jpg" alt="img" width="100%"height="120px"></a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6"><a href="category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/3.jpg" alt="img" width="100%"height="120px"></a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6"><a href="category.html"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/4.jpg" alt="img" width="100%"height="120px"></a>
        </div>
    </div>
</div>


{{-- <div class="width100 section-block">
    <div class="container">
        <h3 class="section-title"><span> BRAND</span> <a id="nextBrand" class="link pull-right carousel-nav"> <i class="fa fa-angle-right"></i></a> <a id="prevBrand" class="link pull-right carousel-nav"> <i class="fa fa-angle-left"></i> </a></h3>
        <div class="row">
            <div class="col-lg-12">
                <ul class="no-margin brand-carousel owl-carousel owl-theme">
                    <li><a><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/1.gif" alt="img"></a></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/2.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/3.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/4.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/5.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/6.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/7.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/8.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/1.gif" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/2.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/3.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/4.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/5.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/6.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/7.png" alt="img"></li>
                    <li><img src="{{asset('public/ecommerce-frontend-assets/images')}}/brand/8.png" alt="img"></li>
                </ul>
            </div>
        </div>
    </div>
</div> --}}

<section class="section-hero-parallax parallax-section " id="hero-parallax" style="background-image: url({{asset('public/ecommerce-frontend-assets/images')}}/parallax/7.png);background-size: cover">
    <div class="overly-shade">
        <div class="container">
            <div class="hero-parallax-content ">
                <h3 class="hero-section-title"> CTG PHARMA ONLINE MEDICINE SHOP </h3>
                <a class="btn btn-stroke thin lite" href="{{url('/')}}">Start Shopping Now</a>
            </div>
        </div>
    </div>
</section>
@endsection