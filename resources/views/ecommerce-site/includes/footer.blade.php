<footer class="main-footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                
                <div style="clear: both"></div>
                <div class="col-lg-12">
                    <div class=" text-center paymanet-method-logo">
                        <img src="{{asset('public/ecommerce-frontend-assets/images')}}/site/payment/master_card.png" alt="img">
                        <img alt="img" src="{{asset('public/ecommerce-frontend-assets/images')}}/site/payment/visa_card.png">
                        {{-- <img alt="img" src="{{asset('public/ecommerce-frontend-assets/images')}}/site/payment/paypal.png">
                        <img alt="img" src="{{asset('public/ecommerce-frontend-assets/images')}}/site/payment/american_express_card.png"> <img alt="img" src="{{asset('public/ecommerce-frontend-assets/images')}}/site/payment/discover_network_card.png">
                        <img alt="img" src="{{asset('public/ecommerce-frontend-assets/images')}}/site/payment/google_wallet.png"> --}}
                    </div>
                    <div class="copy-info text-center">
                        &copy; <?php echo date('Y'); ?>. CtgPharma All Rights Reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>