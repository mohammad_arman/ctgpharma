<div id="search-overly" class="search-overly-mask">
    <a class=" search-close search-overly-close-trigger "> <i class=" fa fa-times-circle"> </i> </a>
    <div class="container">
        <form class="form-horizontal" action="{{url('/search')}}" method="GET">
            {{-- {{ csrf_field() }} --}}
            <fieldset>
                <div class="control-group">
                    <label class="control-label">Search into the shop..</label>
                    <div class="controls">
                        <div class="search " role="search" id="mySearch">
                            <input class="form-control" name="search" placeholder="Enter product name.." type="text" style="font-size:20px;">      

                        </div>
                        <p class="help-block hide">help</p>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<div class="overly-mask"></div>

<aside class="cart-sidebar">
    <div class="cart-sidebar-content">
        <div class="cartMenu   col-xs-12 no-margin-no-padding  ">
            <div class="cart-sidebar-header">
                <h3>Your Bag </h3>
                <div class="cart-close-trigger">
                    <button class="close" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div class="w100  scroll-pane">
                <table>
                    <tbody>
			@foreach(Cart::content() as $row)
                        <tr class="miniCartProduct">
                            <td class="miniCartProductThumb" style="20%">
                               <div>
				    <img src="{{asset('public/admin-frontend-assets/product-image/'.$row->options->image)}}" alt="img" style="width:100px;height: 100px;">
				</div>
                            </td>
                            <td style="40%">
                                <div class="miniCartDescription">
                                    <h4><a href="">{{$row->name}} </a></h4>
				    <h4><a href="">Price: BDT {{$row->price}} </a></h4>
                                    <h4><a href="">Qty: {{$row->qty}} </a></h4>                                   
                                    <div class="price"><span>Total: BDT {{$row->total}}</span></div>
                                </div>
                            </td>
                            <td class="delete" style="20%">
				{!! Form::open(['url'=>'/delete-to-cart','method'=>'post']) !!}
				    <input type="hidden" name="rowId" value="{{$row->rowId}}"/>
				    <button type="submit" class="close"><span aria-hidden="true">×</span></button>
				{!! Form::close() !!}
                            </td>
                        </tr>
			@endforeach
                    </tbody>
                </table>
            </div>

            <div class="miniCartFooter cart-panel-footer clearfix">
                @if(count(Cart::content()) > 0)
                <h4 class="text-right">Subtotal : BDT {{ Cart::subtotal()}}</h4>
                <h4 class="text-right">
                    Shipping : BDT 
                    <?php
                    $subtotal = Cart::subtotal();
                    if ($subtotal  > 0){
                    $shipping = 50;
                    } else{
                    $shipping = 0;
                    }
                    echo number_format($shipping,2);
                    ?>
                </h4>
                <h4 class="text-right">Total tax(Free) : BDT {{ Cart::tax()}}</h4>
                
                <h3 class="text-right subtotal"> Total : 
                <?php
                    $total = Cart::total();
                    $value = str_replace(',','', $total);
                    $number = str_replace('.00','', $value);
                    
                    $final_total = $shipping + $number;
                    if($final_total != 0){
                        Session::put('final_total',$final_total);
                    }
                    echo 'BDT '.number_format($final_total,2);
                    
                    ?>
                </h3>
                @endif
                <div class="col-sm-6 ">
                    <a href="{{url('/cart')}}" class="btn btn-danger  btn-block"><i class="fa fa-shopping-cart"> </i> VIEW CART </a>
                </div>
                <div class="col-sm-6 ">
                    @php
                    $customer_id = Session::get('customer_id');
                    $shipping_id = Session::get('shipping_id');
                    @endphp
                    @if ($customer_id != '' && $shipping_id == '')
                    <a class="btn  btn-primary btn-block " href="{{url('/shipping')}}" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> CHECKOUT </a>
                    @elseif($customer_id != '' && $shipping_id != '')
                    <a class="btn  btn-primary btn-block " href="{{url('/payment')}}" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> CHECKOUT </a>
                    @else
                    <a class="btn  btn-primary btn-block " href="{{url('/checkout')}}" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> CHECKOUT </a>
                    @endif
                </div>
            </div>

        </div>

    </div>

</aside>


<div class="navbar navbar-default navbar-hero  navbar-fixed-top megamenu" role="navigation">
    <div class="navbar-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6">
                    <div class="pull-left ">
                        <ul class="userMenu ">
                            <li>
                                <a href="#"> 
                                    <span class="hidden-xs">HELP</span><i class="glyphicon glyphicon-info-sign hide hidden-xs "></i>
                                </a>
                            </li>
                            <li class="phone-number">
                                <a href="callto:+8801521225987"> <span> <i class="glyphicon glyphicon-phone-alt "></i></span> 
                                    <span class="" style="margin-left:5px"> +8801521225987 </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 no-margin no-padding">
		    <?php
		    $customer_id = Session::get('customer_id');
		    
		    if(!empty($customer_id)){
		    ?>
                    <div class="pull-right">
                        <ul class="userMenu">

                            <!--                                    <li>
                                                                    <a href="#">
                                                                        <span class="hidden-xs">Sign In</span>
                                                                        <i class="glyphicon glyphicon-log-in hide visible-xs "></i> 
                                                                    </a>
                                                                </li>-->
                            <li class="dropdown hasUserMenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="glyphicon glyphicon-log-in hidden-xs "></i> Hi, {{Session::get('customer_name')}} <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url('/my-account')}}"> <i class="fa fa-user"></i> Account</a></li>
                                    <li><a href="{{url('/my-profile')}}"><i class="fa fa fa-cog"></i> Profile</a></li>
                                    {{-- <li><a href="{{url('/my-address')}}"><i class="fa fa-map-marker"></i> Addresses</a></li> --}}
                                    <li><a href="{{url('/orderlist')}}"><i class="fa  fa-calendar"></i> Orders</a></li>
                                    {{-- <li><a href="{{url('/wishlist')}}" title="My wishlists"><i class="fa fa-heart"></i> My wishlists</a></li> --}}
                                    <li class="divider"></li>
                                    <li><a href="{{url('/logout')}}"><i class="fa  fa-sign-out"></i> Log Out</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
		    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">                
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only"> Toggle navigation </span> <span class="icon-bar"> </span> <span class="icon-bar"> </span> <span class="icon-bar"> </span></button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-cart"><i class="fa fa-shopping-cart colorWhite"> </i> <span class="cartRespons colorWhite"> Cart (BDT {{ Cart::total()}} ) </span></button>
            <a class="navbar-brand " href="{{url('/')}}"> <img src="{{asset('public/ecommerce-frontend-assets/images')}}/logo-2.png" alt="CTG-PHARMA" style="margin-top: -10px;height: 48px;"> </a>

            <div class="search-box pull-right hidden-lg hidden-md hidden-sm">
                <div class="input-group">
                    <button class="btn btn-nobg search-trigger" type="button"><i class="fa fa-search"> </i></button>
                </div>

            </div>
        </div>

        <div class="navbar-cart  collapse">
            <div class="cartMenu  col-lg-4 col-xs-12 col-md-4 ">
                <div class="w100 miniCartTable scroll-pane">
                    <table>
                        <tbody>
                           @foreach(Cart::content() as $row)
			    <tr class="miniCartProduct">
                    <td class="miniCartProductThumb" style="20%">
                    <div>
                        <img src="{{asset('public/admin-frontend-assets/product-image/'.$row->options->image)}}" alt="img" style="width:100px;height: 100px;">
                        </div>
                    </td>
                    <td style="40%">
                        <div class="miniCartDescription">
                        <h4><a href="">{{$row->name}} </a></h4>
                        <h4><a href="">Price: BDT {{$row->price}} </a></h4>
                        <h4><a href="">Qty: {{$row->qty}} </a></h4>                                   
                        <div class="price"><span>Total: BDT {{$row->total}}</span></div>
                        </div>
                    </td>
                    <td class="delete" style="20%">
                        {!! Form::open(['url'=>'/delete-to-cart','method'=>'post']) !!}
                            <input type="hidden" name="rowId" value="{{$row->rowId}}"/>
                            <button type="submit" class="close"><span aria-hidden="true">×</span></button>
                        {!! Form::close() !!}
                    </td>
			    </tr>
			    @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="miniCartFooter  miniCartFooterInMobile text-right">
                        @if(!empty(Cart::content()))
                        <h4 class="text-right">Subtotal : BDT {{ Cart::subtotal()}}</h4>
                        <h4 class="text-right">
                            Shipping : BDT 
                            <?php
                            $subtotal = Cart::subtotal();
                            if ($subtotal  > 0){
                            $shipping = 50;
                            } else{
                            $shipping = 0;
                            }
                            echo number_format($shipping,2);
                            ?>
                        </h4>
                        <h4 class="text-right">Total tax(Free) : BDT {{ Cart::tax()}}</h4>
                        
                        <h3 class="text-right subtotal"> Total : 
                        <?php
                            $total = Cart::total();
                            $value = str_replace(',','', $total);
                            $number = str_replace('.00','', $value);
                            
                            $final_total = $shipping + $number;
                            if($final_total != 0){
                                Session::put('final_total',$final_total);
                            }
                            echo 'BDT '.number_format($final_total,2);
                            
                            ?> 
                        </h3>
                        @endif
                        <a href="{{url('/cart')}}" class="btn btn-sm btn-danger"> <i class="fa fa-shopping-cart"></i> VIEW CART</a> 
                        @php
                        $customer_id = Session::get('customer_id');
                        $shipping_id = Session::get('shipping_id');
                        if($customer_id != ''){
								$customer_data = DB::table('customers')->where('id',$customer_id)->first();
								$billing_address = $customer_data->address;
							}
                        @endphp
                        @if ($customer_id != '' && $shipping_id == '' && !empty($billing_address))
                        <a class="btn  btn-primary btn-sm " href="{{url('/shipping')}}" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> CHECKOUT </a>
                        @elseif($customer_id != '' && $shipping_id != '' && !empty($billing_address))
                        <a class="btn  btn-primary btn-sm " href="{{url('/payment')}}" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> CHECKOUT </a>
                        @else
                        <a class="btn  btn-primary btn-sm " href="{{url('/checkout')}}" <?php if(count(Cart::content()) == 0){echo 'disabled';}?>> CHECKOUT </a>
                        @endif
		</div>

            </div>

        </div>



        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav">
		<li class="<?php if(Request::path() ==  '/'){ echo 'active'; } ?>"><a href="{{url('/')}}"> Home </a></li>
		<?php
		$category = DB::table('ecommerce_categories')->where('publication_status',1)->get();
		?>
		@foreach($category as $cat_name)
        <li class="<?php 
            if(Request::path() ==  '/category/'.$cat_name->category_name){ echo 'active'; } ?>">
            <a href="{{url('/category/'.$cat_name->id)}}">{{$cat_name->category_name}} </a>
        </li>
		@endforeach 
<!--                <li class="active"><a href="#"> Home </a></li>
                <li class=" "><a href="#"> New </a></li>

                <li class="dropdown megamenu-fullwidth "><a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        Browse
                        <b class="caret"> </b> </a>
                    <ul class="dropdown-menu hero-submenu">
                        <li class="megamenu-content">

                            <ul class="col-lg-2  col-sm-2 col-md-2  unstyled">
                                <li>
                                    <p class="menu-title"><strong> Men Collection </strong></p>
                                </li>
                                <li><a href=""> Male Fragrances </a></li>
                                <li><a href="#"> Scarf </a></li>
                                <li><a href="#"> Underwear </a></li>
                                <li><a href="#"> Coats & Jackets </a></li>
                                <li><a href="#"> Basics </a></li>
                            </ul>
                            <ul class="col-lg-2  col-sm-2 col-md-2  unstyled">
                                <li>
                                    <p class="menu-title"><strong> Women Collection </strong></p>
                                </li>
                                <li><a href="#"> Tops </a></li>
                                <li><a href="#"> Bottoms </a></li>
                                <li><a href="#"> Dresses </a></li>
                                <li><a href="#"> Coats & Jackets </a></li>
                                <li><a href="#"> Trends </a></li>
                            </ul>
                            <ul class="col-lg-2  col-sm-2 col-md-2  unstyled">
                                <li>
                                    <p class="menu-title"><strong> Accessories </strong></p>
                                </li>
                                <li><a href="#"> Sunglasses </a></li>
                                <li><a href="#"> Scarves </a></li>
                                <li><a href="#"> Watches </a></li>
                                <li><a href="#"> Belts </a></li>
                                <li><a href="#"> Socks </a></li>
                            </ul>
                            <ul class="col-lg-2  col-sm-2 col-md-2  unstyled">
                                <li>
                                    <p class="menu-title"><strong> Top Brands </strong></p>
                                </li>
                                <li><a href="#"> Diesel </a></li>
                                <li><a href="#"> Farah </a></li>
                                <li><a href="#"> G-Star RAW </a></li>
                                <li><a href="#"> Lyle & Scott </a></li>
                                <li><a href="#"> Pretty Green </a></li>
                            </ul>
                            <ul class="col-lg-2  col-sm-2 col-md-2  unstyled">
                                <li>
                                    <p class="menu-title"><strong> Shoes </strong></p>
                                </li>
                                <li><a href="#"> Flats </a></li>
                                <li><a href="#"> Sandals </a></li>
                                <li><a href="#"> Wedges </a></li>
                                <li><a href="#"> Sneakers </a></li>
                                <li><a href="#"> Heels & Pumps </a></li>
                            </ul>
                            <ul class="col-lg-2  col-sm-2 col-md-2  unstyled">
                                <li>
                                    <p class="menu-title"><strong> Popular </strong></p>
                                </li>
                                <li><a href="#">Denim</a></li>
                                <li><a href="#">Essentials</a></li>
                                <li><a href="#">Shirts</a></li>
                                <li><a href="#">Pants</a></li>
                                <li><a href="#">Outerwear</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown "><a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        SHOP <b class="caret"> </b> </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">New</a></li>
                        <li><a href="#">Denim</a></li>
                        <li><a href="#">Essentials</a></li>
                        <li><a href="#">Shirts</a></li>
                        <li><a href="#">Pants</a></li>
                        <li><a href="#">Outerwear</a></li>
                        <li><a href="#">Sweaters</a></li>
                        <li><a href="#">Shorts</a></li>
                        <li><a href="#">Sale</a></li>
                    </ul>
                </li>-->
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="hide-xs"><a class="btn btn-nobg  search-trigger"><i class="fa fa-search"> </i></a></li>
               <?php
		$customer_id = Session::get('customer_id');
		if(empty($customer_id)){
		?>
		<li><a href="{{url('/authentication')}}"> Login <i class="fa fa-user"></i> </a></li>
		<?php
		}
		?>
                <li class="hide-xs cart-sidebar-toggle"><a> Cart @if(count(Cart::content()) > 0)<span class="badge" style="background:#4ec67f;">{{count(Cart::content())}}</span> @endif <i class="glyphicon-shopping-cart glyphicon"></i>( {{ Cart::total()}} Tk)</a></li>
            </ul>
        </div>

    </div>

    <div class="search-full text-right"><a class="pull-right search-close"> <i class=" fa fa-times-circle"> </i> </a>
        <div class="searchInputBox pull-right">
            <input type="search" data-searchurl="search?=" name="q" placeholder="start typing and hit enter to search" class="search-input">
            <button class="btn-nobg search-btn" type="submit"><i class="fa fa-search"> </i></button>
        </div>
    </div>
</div>