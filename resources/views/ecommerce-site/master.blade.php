<!DOCTYPE html>
<html lang="en">
    <head>
	<title>CtgPharma || @yield('title')</title>
        <meta charset="UTF-8">
        <meta name="title" content="OPharma - is a online pharmacy shop."/>
        <meta name="description" content=" "/> 
        <meta name="author" content="Mohammad Arman">
        <meta name="robots" content="index, follow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" href="{{asset('public/ecommerce-frontend-assets/images/favicon.png')}}" type="image/png" sizes="16x16">
        <link href="{{asset('public/ecommerce-frontend-assets/assets/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('public/ecommerce-frontend-assets/assets/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        
        <link id="pagestyle" rel="stylesheet" type="text/css" href="{{asset('public/ecommerce-frontend-assets/assets/css/skin-1.css')}}">
	<link href="{{asset('public/ecommerce-frontend-assets/assets/plugins/swiper-master/css/swiper.min.css')}}" rel="stylesheet">

        <link href="{{asset('public/ecommerce-frontend-assets/assets/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('public/ecommerce-frontend-assets/assets/css/home-v7.css')}}" rel="stylesheet">
        <link href="{{asset('public/ecommerce-frontend-assets/assets/css/cart-nav.css')}}" rel="stylesheet">
	<link href="{{asset('public/ecommerce-frontend-assets/assets/css/footable-0.1.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('public/ecommerce-frontend-assets/assets/css/footable.sortable-0.1.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('public/ecommerce-frontend-assets/assets/css/product-details-5.css')}}" rel="stylesheet" type="text/css"/>
	<script>
            paceOptions = {
                elements: true
            };
        </script>
        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/pace.min.js')}}"></script>
        <script type="text/javascript">
            function swapStyleSheet(sheet) {
                document.getElementById('pagestyle').setAttribute('href', sheet);
            }
        </script> 
    </head>
    <body>

        @include('ecommerce-site.includes.header')
	
        @yield('site_main_content')
        
        @include('ecommerce-site.includes.footer')




        <script type="text/javascript" src="{{asset('public/ecommerce-frontend-assets/assets/js/jquery/jquery-1.10.1.min.js')}}"></script>
        <script src="{{asset('public/ecommerce-frontend-assets/assets/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/footable.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/ecommerce-frontend-assets/assets/js/footable.sortable.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
	    $(function () {
		$('.footable').footable();
	    });
	</script>
        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/jquery.cycle2.min.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/jquery.easing.1.3.js')}}"></script>

        <script type="text/javascript" src="{{asset('public/ecommerce-frontend-assets/assets/js/jquery.parallax-1.1.js')}}"></script>

        <script type="text/javascript" src="{{asset('public/ecommerce-frontend-assets/assets/js/helper-plugins/jquery.mousewheel.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('public/ecommerce-frontend-assets/assets/js/jquery.mCustomScrollbar.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/plugins/icheck-1.x/icheck.min.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/grids.j')}}s"></script>
	<script src="{{asset('public/ecommerce-frontend-assets/assets/js/select2.min.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/owl.carousel.min.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/select2.min.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/bootstrap.touchspin.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/home.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/script.js')}}"></script>
        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/sidebar-nav.js')}}"></script>

        <script src="{{asset('public/ecommerce-frontend-assets/assets/js/jquery.scrollme.min.js')}}"></script>
	<script type="text/javascript">


            $(function () {
                var target = $("div.has-overly-shade"),
                        targetHeight = target.outerHeight();
                $(document).scroll(function () {
                    var scrollPercent = (targetHeight - window.scrollY) / targetHeight;
                    scrollPercent >= 0 && (target.css("background-color", "rgba(0,0,0," + (1.1 - scrollPercent) + ")"))
                })
            });


            $(function () {
                if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
                    $('#ios-notice').removeClass('hidden');
                    $('.parallax-container').height($(window).height() * 0.5 | 0);
                } else {
                    $(window).resize(function () {
                        var parallaxHeight = Math.max($(window).height() * 0.7, 200) | 0;
                        $('.parallax-container').height(parallaxHeight);
                    }).trigger('resize');
                }
            });


            $(document).ready(function () {
                var isMobile = function () {
                    //console.log("Navigator: " + navigator.userAgent);
                    return /(iphone|ipod|ipad|android|blackberry|windows ce|palm|symbian)/i.test(navigator.userAgent);
                };

                if (isMobile()) {
                    // For  mobile , ipad, tab
                    $('.animateme').removeClass('animateme');
                    $('.if-is-mobile').addClass('ismobile');

                } else {
                }


            }); // end




        </script>
	<script>

    // For Demo purposes only
    [].slice.call(document.querySelectorAll('nav.nav-narrow-svg > a')).forEach(function (el) {
        el.addEventListener('click', function (ev) {
            ev.preventDefault();
        });
    });

    // productShowCase  carousel
    var pic = $(".product-images-carousel");

    pic.owlCarousel({
        autoPlay: false,
        lazyLoad: true,
        navigation: false,
        paginationSpeed: 1000,
        goToFirstSpeed: 2000,
        singleItem: true,
        autoHeight: true


    });

    // Custom Navigation Events
    $(".product-images-carousel-wrapper nav.slider-nav .next").click(function () {
        pic.trigger('owl.next');
    })
    $(".product-images-carousel-wrapper nav.slider-nav  .prev").click(function () {
        pic.trigger('owl.prev');
    })

</script>
<script>
    $(function () {
        $('.rating-tooltip-manual').rating({
            extendSymbol: function () {
                var title;
                $(this).tooltip({
                    container: 'body',
                    placement: 'bottom',
                    trigger: 'manual',
                    title: function () {
                        return title;
                    }
                });
                $(this).on('rating.rateenter', function (e, rate) {
                    title = rate;
                    $(this).tooltip('show');
                })
                        .on('rating.rateleave', function () {
                            $(this).tooltip('hide');
                        });
            }
        });

    });
</script>
<script src="{{asset('public/ecommerce-frontend-assets/assets/plugins/intense-images-master/intense.js')}}"></script>
<script>
    var elements = document.querySelectorAll('.zoom-image-overly');
    Intense(elements);
</script>
<script type="text/javascript" src="{{asset('public/ecommerce-frontend-assets/assets/js/skrollr.min.js')}}"></script>
<script type="text/javascript">
    var isMobile = function () {
        //console.log("Navigator: " + navigator.userAgent);
        return /(iphone|ipod|ipad|android|blackberry|windows ce|palm|symbian)/i.test(navigator.userAgent);
    };

    if (isMobile()) {
        // For  mobile , ipad, tab

    } else {

        if ($(window).width() < 768) {
        } else {
            var s = skrollr.init({forceHeight: false});
        }

    }


</script>
 
<script src="{{asset('public/ecommerce-frontend-assets/assets/js/wow.min.js')}}"></script>
<script>
    new WOW().init();
</script>
    </body>
</html>


