@extends('ecommerce-site.master')

@section('title')
Product Details
@endsection

@section('site_main_content')
<section class="section-product-info">
    <div class="container-1400 container   main-container product-details-container">
	<div class="row">
	    <div class="breadcrumbDiv col-lg-12">
		<ul class="breadcrumb">
		    <li><a href="{{url('/')}}">Home</a></li>
		    <li><a href="{{url('/category/'.$product->category_id)}}">{{$product->category_name}}</a></li>
		    <li class="active">{{$product->product_name}}</li>
		</ul>
	    </div>
	</div>
	<div class="row">

	    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
		<div class="product-images-carousel-wrapper text-center">
		    <img src='{{asset('public/admin-frontend-assets/product-image/'.$product->product_image)}}' alt='{{$product->product_name}}' style="width: 60%;height: 400px;box-shadow: 0px 0px 15px 0px #888;"/>
		
		</div>
	    </div>


	    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
		<div class="product-details-info-wrapper">
		    <h2 class="product-details-product-title" id="p_name"> {{$product->product_name}}</h2>
		    <div class="product-price">
					@if($product->discount > 0 )<div ><span class="new-product"> OFF</span> <span class="discount">{{$product->discount}}%</span></div><br/>@endif
				
					@if($product->discount > 0 )
					<?php 
					$discount = $product->piece_sales_rate * ($product->discount/100);
					?>
					<del><div class="price-sales"><span>{{number_format($product->piece_sales_rate,2)}} Tk</span></div></del>
	
					<div class="price-sales"><span>{{number_format($product->piece_sales_rate - $discount  ,2)}} Tk</span></div>
					
					@else
					<div class="price-sales"><span>{{number_format($product->piece_sales_rate,2)}} Tk</span></div>
					
					@endif
		    </div>
		    {!! Form::open(['url'=>'/add-cart','method'=>'post','id'=>'p_form']) !!}
		    <div class="row row-filter clearfix ">
			<div class="col-xs-12">
			    <input type="text" name="qty" id="qty" class="form-control" placeholder="Enter quantity of product"/>
			    <input type="hidden" name="id" value="{{$product->id}}"/>
			    <input type="hidden" name="old_qty" id="old_qty" value="{{$product->piece_qty}}"/>
			</div>
		    </div>

		    <div class="row row-cart-actions clearfix ">
			<div class="col-sm-12 ">
			    <button type="submit" class="btn btn-block btn-dark submit-btn">
				Add to cart
			    </button>
	            {!! Form::close() !!}
			</div>
		    </div>
		   
		    <div class="clear"></div>
		    <div class="product-share clearfix">
			<p> SHARE </p>
			<div class="socialIcon">
			    <a href="#"> <i class="fa fa-facebook"></i></a>
			    <a href="#"> <i class="fa fa-twitter"></i></a>
			    <a href="#"> <i class="fa fa-google-plus"></i></a>
			    <a href="#"> <i class="fa fa-pinterest"></i></a></div>
			<br>
		    </div>
		    <div style="clear:both"></div>
		</div>
	    </div>

	</div>

	<div style="clear:both"></div>
    </div>

</section>
<section class="section-product-info-bottom">
    <div class="product-details-bottom-bar">
	<div class="container-1400 container">
	    <div class="row">
		<div class="col-lg-8">

		    <ul class="nav nav-tabs flat list-unstyled list-inline social-inline" role="tablist">
			<li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">
				Product details</a></li>
<!--			<li role="presentation"><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab">
				Additional info</a></li>
			<li role="presentation"><a href="#tab3" aria-controls="messages" role="tab" data-toggle="tab">
				Fit & Care</a></li>
			<li role="presentation"><a href="#tab4" aria-controls="settings" role="tab" data-toggle="tab">
				Product Review</a>
			</li>-->
		    </ul>
		</div>
		<div class="col-lg-4">
		    <div class="review-title-bar">
		    </div>
		</div>
	    </div>
	</div>
    </div>
</section>
<section class="section-tab-content">

    <div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="tab1">
	    <div class="product-story-inner ">
		<div class="container">
		    <div class="row ">
			<div class="col-lg-12 ">
			    <h3 class="title">{{$product->product_name}}</h3>
			    <p class="desc"><?php echo $product->description; ?></p>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</section>
<script src="{{asset('public/admin-frontend-assets/js/jquery.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click','.submit-btn',function(){
		var product = $('#p_name').text();
		var old_qty = $('#old_qty').val();
		var new_qty = $('#qty').val();
		var qty_check = old_qty - new_qty;
		if(qty_check < 0){
			alert(product+' quantity croos our stock quantity');
			$('#qty').val('');
			// $('.checkout_btn').prop("disabled",true);
			return false;
		}
		if(new_qty <= 0){
			alert(product+' quantity must be greater than zero');
			// $('.checkout_btn').prop("disabled",true);
			return false;
		}
		$("#p_form").submit();
		});
		
	});
	</script>
@endsection

