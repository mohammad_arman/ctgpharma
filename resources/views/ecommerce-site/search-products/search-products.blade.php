@extends('ecommerce-site.master') 
@section('title') Search Product
@endsection
 
@section('site_main_content')

<div class="container main-container headerOffset">

    <div class="row">
        <div class="breadcrumbDiv col-lg-12">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Search</li>
            </ul>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="w100 clearfix category-top">
                <h2> Search</h2>
                <div class="categoryImage"><img src="{{asset('public/ecommerce-frontend-assets/images')}}/search.jpg" style="width:100%" height="300px" alt="img"></div>
            </div>


            <div class="w100 productFilter clearfix">
                <p class="pull-left"> Showing search products </p>
            </div>

            <div class="row  categoryProduct xsResponse clearfix">
                <?php
                if(count($result) > 0){
			        foreach($result  as $product){
                ?>
                    <div class="item col-sm-4 col-lg-3 col-md-3 col-xs-6">
                        <div class="product">
                            <a href="{{url('/wishlist/'.$product->id)}}" class="add-fav tooltipHere" data-toggle="tooltip" data-original-title="Add to Wishlist"
                            data-placement="left">
                                <i class="glyphicon glyphicon-heart"></i>
                            </a>
                            <div class="image">
                                <div class="quickview">
                                    <a href="{{url('/product-details/'.$product->id)}}" class="btn btn-xs btn-quickview">View Details </a>
                                </div>
                                <a href="{{url('/product-details/'.$product->id)}}"><img src="{{asset('public/admin-frontend-assets/product-image/'.$product->product_image)}}" alt="img" style="width:100%;height: 250px;"></a>                                @if($product->discount > 0 )
                                    <div class="promotion">
                                        <span class="new-product"> OFF</span> <span class="discount">{{$product->discount}}%</span>
                                    </div>
                                @endif
                            </div>
                            <div class="title" style="margin-top: 10px;">
                                <h4><a href="{{url('/product-details/'.$product->id)}}">{{strtoupper($product->product_name)}}</a></h4>
                            </div>

                            <div class="price">
                                    @if($product->discount > 0 )
                                    <?php 
                                    $discount = $product->piece_sales_rate * ($product->discount/100);
                                    ?>
                                   <del> <span>{{number_format($product->piece_sales_rate,2)}} Tk</span></del>
                    
                                    <span>{{number_format($product->piece_sales_rate - $discount  ,2)}} Tk</span>
                                    
                                    @else
                                    <span>{{number_format($product->piece_sales_rate,2)}} Tk</span>
                                    
                                    @endif
                            </div>

                            <div class="action-control">
                                {!! Form::open(['url'=>'/add-cart','method'=>'post']) !!}
                                <input type="hidden" name="qty" value="<?php echo 1;?>" />
                                <input type="hidden" name="id" value="{{$product->id}}" />
                                <button type="submit" class="btn btn-success"><span class="add2cart"><i class="glyphicon glyphicon-shopping-cart"> </i> Add to cart </span></button>                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <?php
            }
        }else{
        ?>
            <div class="col-md-12">
                <h3 style="color:red;font-weight:bold;">No result found. Try again</h3>
            </div>
        <?php
        }
        ?>
            </div>


        </div>

    </div>

</div>

<div class="gap"></div>
@endsection