@extends('ecommerce-site.master')

@section('title')
Add Personal Information
@endsection

@section('site_main_content')


<div class="container main-container headerOffset">
    <div class="row">
	<div class="breadcrumbDiv col-lg-12">
	    <ul class="breadcrumb">
		<li><a href="{{url('/')}}">Home</a></li>
		<li><a href="{{url('/my-account')}}">My Account</a></li>
		<li class="active"> My information</li>
	    </ul>
	</div>
    </div>
    <div class="row">
	<div class="col-lg-9 col-md-9 col-sm-7">
	    <h1 class="section-title-inner"><span><i class="glyphicon glyphicon-user"></i> My personal information </span></h1>
	    <div class="row userInfo">
		<div class="col-lg-12">
		    <h2 class="block-title-2"> Please be sure to update your personal information if it has
			changed. </h2>
			<p class="required"><sup>*</sup> Required field</p>
			@if($success_message = Session::get('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        
					{{$success_message}}
				</div>
				@endif @if($error_message = Session::get('error'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>                        
					{{$error_message}}
				</div>
			@endif
		</div>
	<form action="{{url('/update-personal-information')}}" method="POST">
			{{csrf_field()}}
		    	<div class="col-xs-12 col-sm-6">
						<div class="form-group  {{ $errors->has('firstname') ? ' has-error' : '' }}">
							<label for="firstname">First Name <sup>*</sup> </label>
							<input required type="hidden" name="id" value="{{$customer_info->id}}" class="form-control">
							<input required type="text" name="firstname" value="{{$customer_info->firstname}}" class="form-control" id="firstname" >
							@if ($errors->has('firstname'))
								<span class="help-block">
									<strong>{{ $errors->first('firstname') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
							<label for="lastname">Last Name <sup>*</sup> </label>
							<input required type="text" name="lastname" value="{{$customer_info->lastname}}" class="form-control" id="lastname" >
							@if ($errors->has('lastname'))
							<span class="help-block">
								<strong>{{ $errors->first('lastname') }}</strong>
							</span>
						@endif
						</div>
						<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email">Email <sup>*</sup> </label>
							<input type="text" name="email" value="{{$customer_info->email}}" class="form-control" id="email" readonly>
							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group {{ $errors->has('phone_num_one') ? ' has-error' : '' }}">
							<label for="phone_num_one">Mobile phone One<sup>*</sup></label>
							<input required type="text" name="phone_num_one" value="{{$customer_info->phone_num_one}}" class="form-control" id="phone_num_one">
							@if ($errors->has('phone_num_one'))
								<span class="help-block">
									<strong>{{ $errors->first('phone_num_one') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group required">
							<label for="phone_num_two">Mobile phone Two</label>
							<input required type="text" name="phone_num_two" value="{{$customer_info->phone_num_two}}" class="form-control" id="phone_num_two">
						</div>
	
						<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
							<label>Password <b style="color: red;">*</b></label>
							<input minlength="5" type="password" name="password" value={{$customer_info->password}} class="form-control" placeholder="Password" autocomplete="off" />
							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group">
								<label for="password-confirm">Confirm Password <b style="color: red;">*</b></label>
								<input type="password" id="password-confirm" name="password_confirmation"   value={{$customer_info->password}} class="form-control" placeholder="Enter password again">
						</div>  	
					</div>
	
					<div class="col-xs-12 col-sm-6 {{ $errors->has('address') ? ' has-error' : '' }}">
						<div class="form-group required">
							<label for="address">Address <sup>*</sup> </label>
							<textarea rows="3" cols="26" name="address" class="form-control" id="address">{{$customer_info->address}}</textarea>
							@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
						@endif
						</div>
						<div class="form-group">
							<label for="address_two">Address (Line 2) </label>
							<textarea rows="3" cols="26" name="address_two" class="form-control" id="address_two">{{$customer_info->address_two}}</textarea>
						</div>
						<div class="form-group">
							<label for="additional_information">Additional information</label>
							<textarea rows="3" cols="26" name="additional_information" class="form-control" id="additional_information" style="height: 110px;">{{$customer_info->additional_information}}</textarea>
						</div>
	
	
					</div>
		    <div class="col-lg-12">
			<button type="submit" class="btn   btn-primary"><i class="fa fa-refresh"></i> &nbsp; Update</button>
		    </div>
		</form>
		<div class="col-lg-12 clearfix">
		    <ul class="pager">
			<li class="previous pull-right"><a href="{{url('/')}}"> <i class="fa fa-home"></i> Go to Shop </a></li>
			<li class="next pull-left"><a href="{{url('/my-account')}}"> &larr; Back to My Account</a></li>
		    </ul>
		</div>
	    </div>

	</div>
	<div class="col-lg-3 col-md-3 col-sm-5"></div>
    </div>

    <div style="clear:both"></div>
</div>

<div class="gap"></div>

@endsection


