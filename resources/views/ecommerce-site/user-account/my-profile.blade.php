@extends('ecommerce-site.master') 
@section('title') 
My Profile
@endsection
 
@section('site_main_content')
<div class="container main-container headerOffset">
    <div class="row">
        <div class="breadcrumbDiv col-lg-12">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">My Profile</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="col-md-12" style="background:white;min-height:500px;padding-top:50px;padding-bottom:50px;">
                    <ul class="view-list col-md-12">
                            <li><span>Username: </span><span>{{$customer_info->username}}</span></li>
                            <li><span>First Name: </span><span>{{$customer_info->firstname}}</span></li>
                            <li><span>Last Name: </span><span>{{$customer_info->lastname}}</span></li>
                            <li><span>Email Address: </span><span>{{$customer_info->email}}</span></li>
                            <li><span>FIrst Contact Number: </span><span>{{$customer_info->phone_num_one}}</span></li>
                            @if($customer_info->phone_num_two != '')
                            <li><span>Second Contact Number: </span><span>{{$customer_info->phone_num_two}}</span></li>
                            @endif
                            <li><span>First Address: </span><span>{{$customer_info->address}}</span></li>
                            @if($customer_info->address_two != '')
                            <li><span>Second Address: </span><span>{{$customer_info->address_two}}</span></li>
                            @endif
                            @if($customer_info->additional_information != '')
                            <li><span>Additional information: </span><span>{{$customer_info->additional_information}}</span></li>
                            @endif
                    </ul> 
                
            </div>  
        </div>
    </div>

    <div style="clear:both;"></div>
</div>

<div class="gap"></div>
@endsection