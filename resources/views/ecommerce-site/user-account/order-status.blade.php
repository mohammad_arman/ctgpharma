@extends('ecommerce-site.master')

@section('title')
Order Status
@endsection

@section('site_main_content')
<div class="container main-container headerOffset">
    <div class="row">
	<div class="breadcrumbDiv col-lg-12">
	    <ul class="breadcrumb">
		<li><a href="{{url('/')}}">Home</a></li>
		<li><a href="{{url('/my-account')}}">My Account</a></li>
		<li class="active"> Order List</li>
	    </ul>
	</div>
    </div>
    <div class="row">
	<div class="col-lg-9 col-md-9 col-sm-7">
	    <h1 class="section-title-inner"><span><i class="fa fa-list-alt"></i> Order Status </span></h1>
	    <div class="row userInfo">
		<div class="col-lg-12">
		    <h2 class="block-title-2"> Your Order Status </h2>
		</div>
		<div class="statusContent">
		    <div class="col-sm-12">
			<div class=" statusTop">
			<p><strong>Status:</strong> {{$order_data->order_status}}</p>
			<p><strong>Order Date:</strong> {{date('F j, Y, g:i A',strtotime($order_data->created_at))}}</p>
			    <p><strong>Order Number:</strong> #{{$order_data->id}} </p>
			</div>
		    </div>
		    <div class="col-sm-6">
			<div class="order-box">
			    <div class="order-box-header">
				Billing Address
			    </div>
			    <div class="order-box-content">
				<div class="address" style="min-height:250px;">
				<p><strong>Name: </strong>{{$order_data->firstname.' '.$order_data->lastname}}</p>
				<p><strong>Email Address: </strong>{{$order_data->email}}</p>
				<p><strong>Contact No.: </strong>{{$order_data->phone_num_one}}</p>
				@if($order_data->phone_num_two != '')
				<p><strong>Additional Contact No.: </strong>{{$order_data->phone_num_two}}</p>
				@endif
				<div class="adr">
					<strong>Address: </strong>{{$order_data->address}}
				</div>
				@if($order_data->address_two != '')
				<div class="adr">
					<strong>Additional Address: </strong>{{$order_data->address_two}}
				</div>
				@endif
				@if($order_data->additional_information != '')
				<div class="adr">
					<strong>Additional Information: </strong>{{$order_data->additional_information}}
				</div>
				@endif
				</div>
			    </div>
			</div>
		    </div>
		    <div class="col-sm-6">
			<div class="order-box">
			    <div class="order-box-header">
				Shipping Address
			    </div>
			    <div class="order-box-content">
				<div class="address" style="min-height:250px;">
						<p><strong>Name: </strong>{{$shipping_info->name}}</p>
						<p><strong>Email Address: </strong>{{$shipping_info->email}}</p>
						<p><strong>Contact No.: </strong>{{$shipping_info->phone_num_one}}</p>
						@if($shipping_info->phone_num_two != '')
						<p><strong>Additional Contact No.: </strong>{{$shipping_info->phone_num_two}}</p>
						@endif
						<div class="adr">
							<strong>Address: </strong>{{$shipping_info->address}}
						</div>
						@if($shipping_info->address_two != '')
						<div class="adr">
							<strong>Additional Address: </strong>{{$shipping_info->address_two}}
						</div>
						@endif
						@if($shipping_info->additional_information != '')
						<div class="adr">
							<strong>Additional Information: </strong>{{$shipping_info->additional_information}}
						</div>
						@endif
				</div>
			    </div>
			</div>
		    </div>
		    <div style="clear: both"></div>
		    <div class="col-sm-6">
			<div class="order-box">
			    <div class="order-box-header">
				Payment Method
			    </div>
			    <div class="order-box-content">
				<div class="address">
					<p>Payment via {{$order_data->payment_type}} 
						@if($order_data->payment_status == 'done')
						<span style="color: green" class="green"> <strong>(Paid)</strong> </span>
						@elseif($order_data->payment_status == 'pending')
						<span style="color: #337ab7;" class="green"> <strong>(Pending)</strong> </span>
						@else
						<span style="color: red;" class="green"> <strong>(Cancel)</strong> </span>
						@endif
					</p>
					@if($order_data->payment_date != '')
					<p>Payment Date: {{date('F j, Y',strtotime($order_data->payment_date))}}</p>
					@endif
				</div>
			    </div>
			</div>
		    </div>
		    <div class="col-sm-6">
			<div class="order-box">
			    <div class="order-box-header">
				Shipping Method
			    </div>
			    <div class="order-box-content">
				<div class="address">
				    <p> Via transport</p>
				</div>
			    </div>
			</div>
		    </div>
		    <div class="col-sm-12 clearfix">
			<div class="order-box">
			    <div class="order-box-header">
				Order Items
			    </div>
			    <div class="order-box-content">
				<div class="table-responsive">
				    <table class="order-details-cart">
					<tbody>
						<?php 
						$sum = 0;
						?>
						@foreach ($ordered_products as $item)
							<tr class="cartProduct">
								<td class="cartProductThumb" style="width:20%">
									<div><img alt="img" src="{{asset('public/admin-frontend-assets/product-image/'.$item->product_image)}}"></div>
								</td>
								<td style="width:40%">
									<div class="miniCartDescription">
									<h4><a href="product-details.html"> {{$item->product_name}}  </a></h4>
									<span class="size"> {{$item->weight}} </span>
									<div class="price"><span> BDT {{number_format($item->product_price,2)}} </span></div>
									</div>
								</td>
								<td class="" style="width:10%"><a> X {{$item->product_sales_qty}} </a></td>
								<td class="" style="width:15%">
									<span> BDT
										<?php
											$sub_total = $item->product_sales_qty * $item->product_price;
											echo number_format((float)$sub_total,2,'.','');
											$sum = $sum + $sub_total;
										?>
									</span>
								</td>
							</tr>
						@endforeach
					    
					    
					   
					    
					    <tr class="cartTotalTr blank">
						<td class="" style="width:20%">
						    <div></div>
						</td>
						<td style="width:40%"></td>
						<td class="" style="width:20%"></td>
						<td class="" style="width:15%"><span> </span></td>
					    </tr>
					    <tr class="cartTotalTr">
						<td class="" style="width:20%">
						    <div></div>
						</td>
						<td colspan="2" style="width:40%">Total products</td>
						<td class="" style="width:15%"><span> BDT {{$sum}} </span></td>
					    </tr>
					    <tr class="cartTotalTr">
						<td class="" style="width:20%">
						    <div></div>
						</td>
						<td colspan="2" style="width:40%">Shipping</td>
						<td class="" style="width:15%"><span> BDT {{number_format(50,2)}} </span></td>
					    </tr>
					    <tr class="cartTotalTr">
						<td class="" style="width:20%">
						    <div></div>
						</td>
						<td colspan="2" style="width:40%">Tax (freel)</td>
						<td class="" style="width:15%"><span> BDT {{number_format(0,2)}} </span></td>
					    </tr>
					    <tr class="cartTotalTr">
						<td class="" style="width:20%">
						    <div></div>
						</td>
						<td style="width:40%"></td>
						<td class="" style="width:20%">Total</td>
						<td class="" style="width:15%"><span class="price"> BDT {{number_format(($sum + 50),2)}} </span></td>
					    </tr>
					</tbody>
				    </table>
				</div>
			    </div>
			</div>
		    </div>
		</div>
		<div class="col-lg-12 clearfix">
		    <ul class="pager">
			<li class="previous pull-right"><a href="{{url('/')}}"> <i class="fa fa-home"></i> Go to Shop </a></li>
			<li class="next pull-left"><a href="{{url('/orderlist')}}"> &larr; Back</a></li>
		    </ul>
		</div>
	    </div>

	</div>
	<div class="col-lg-3 col-md-3 col-sm-5"></div>
    </div>

    <div style="clear:both"></div>
</div>

<div class="gap"></div>
@endsection