@extends('ecommerce-site.master') 
@section('title') Order List
@endsection
 
@section('site_main_content')
<div class="container main-container headerOffset">
	<div class="row">
		<div class="breadcrumbDiv col-lg-12">
			<ul class="breadcrumb">
				<li><a href="{{url('/')}}">Home</a></li>
				<li><a href="{{url('/my-account')}}">My Account</a></li>
				<li class="active"> Order List</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-9 col-md-9 col-sm-7">
			<h1 class="section-title-inner"><span><i class="fa fa-list-alt"></i> Order List </span></h1>
			<div class="row userInfo">
				<div class="col-lg-12">
					<h2 class="block-title-2"> Your Order List </h2>
				</div>
				<div style="clear:both"></div>
				<div class="col-xs-12 col-sm-12">
					<table class="footable">
						<thead>
							<tr>
								<th data-class="expand" data-sort-initial="true"><span title="table sorted by this column on load">Order ID</span></th>
								<th data-hide="phone,tablet" data-sort-ignore="true">No. of items</th>
								<th data-hide="phone,tablet"><strong>Payment Method</strong></th>
								<th data-hide="phone,tablet"><strong></strong></th>
								<th data-hide="default"> Price</th>
								<th data-hide="default" data-type="numeric"> Date</th>
								<th data-hide="phone" data-type="numeric"> Status</th>
							</tr>
						</thead>
						<tbody>
							@if(count($order_list) > 0)
							@foreach ($order_list as $order)
								
								<tr>
									<td>#{{$order['id']}}</td>
									<td>{{$order['total_product']}}
										<small>item(s)</small>
									</td>
									<td>{{$order['payment_type']}}</td>
									<td><a href="{{url('/order-status/'.$order['id'])}}" class="btn btn-primary btn-sm">view status</a></td>
									<td>{{number_format($order['order_total'],2)}} TK</td>
									<td>{{date('F j, Y, g:i A',strtotime($order['created_at']))}}</td>
									<td>
										@if ($order['order_status'] == 'pending')
											<span class="label label-primary">Pending</span>
										@elseif($order['order_status'] == 'done')
											<span class="label label-success">Done</span>
										@else
											<span class="label label-danger">Cancel</span>
										@endif
										
									</td>
								</tr>
								
							 @endforeach
							 @else
							 <tr>
								 <td colspan="4">No data found</td>
							 </tr>
							 @endif
						</tbody>
					</table>
				</div>
				<div style="clear:both"></div>
				<div class="col-lg-12 clearfix">
					<ul class="pager">
						<li class="previous pull-right"><a href="{{url('/')}}"> <i class="fa fa-home"></i> Go to Shop </a></li>
						<li class="next pull-left"><a href="{{url('/my-account')}}"> &larr; Back to My Account</a></li>
					</ul>
				</div>
			</div>

		</div>
		<div class="col-lg-3 col-md-3 col-sm-5"></div>
	</div>

	<div style="clear:both"></div>
</div>

<div class="gap"></div>
@endsection