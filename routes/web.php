<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
Route::get('/dashboard','HomeController@index');
Route::post('/branch/statistics','HomeController@branchStatistics');

Route::get('/add-employee','RegistrationController@index');
Route::post('/employee/save','RegistrationController@saveEmployee');
Route::get('/employee/manage','RegistrationController@manageEmployee');
Route::get('/employee/edit/{id}','RegistrationController@editEmployee');
Route::post('/employee/update','RegistrationController@updateEmployee');
Route::get('/employee/view/{id}','RegistrationController@viewEmployee');
Route::get('/employee/delete/{id}','RegistrationController@deleteEmployee');
Route::get('/view/profile/{id}','RegistrationController@viewProfile');


Route::get('/customer/manage','CustomerController@index');
Route::get('/customer/active/{id}','CustomerController@active');
Route::get('/customer/inactive/{id}','CustomerController@inactive');
Route::get('/customer/view/{id}','CustomerController@view');
Route::get('/customer/delete/{id}','CustomerController@delete');

/*******************************************************
======= #Start# Ecommerce Product Category Info ========
********************************************************/
Route::get('/category/add','EcommerceCategoryController@index');
Route::post('/category/save','EcommerceCategoryController@storeCategory');
Route::get('/category/manage','EcommerceCategoryController@manageCategory');
Route::get('/category/unpublished/{id}','EcommerceCategoryController@unpublishedCategory');
Route::get('/category/published/{id}','EcommerceCategoryController@publishedCategory');
Route::get('/category/edit/{id}','EcommerceCategoryController@editCategory');
Route::post('/category/update','EcommerceCategoryController@updateCategory');
Route::get('/category/view/{id}','EcommerceCategoryController@viewCategory');
Route::get('/category/delete/{id}','EcommerceCategoryController@deleteCategory');

/*****************************************************
======= #End# Ecommerce Product Category Info ========
******************************************************/
/*-----------------------------------------------------------*/
/***********************************************************
================= #Start# Company Info =====================
************************************************************/
Route::get('/company/add','CompanyController@index');
Route::post('/company/save','CompanyController@storeCompany');
Route::get('/company/manage','CompanyController@manageCompany');
Route::get('/company/edit/{id}','CompanyController@editCompany');
Route::post('/company/update','CompanyController@updateCompany');
Route::get('/company/view/{id}','CompanyController@viewCompany');
Route::get('/company/delete/{id}','CompanyController@deleteCompany');
/***********************************************************
================= #End# Company Info =======================
************************************************************/
/*-----------------------------------------------------------*/
/***********************************************************
=================== #Start# Branch Info ====================
************************************************************/
Route::get('/branch/add','BranchController@index');
Route::post('/branch/save','BranchController@storeBranch');
Route::get('/branch/manage','BranchController@manageBranch');
Route::get('/branch/edit/{id}','BranchController@editBranch');
Route::post('/branch/update','BranchController@updateBranch');
Route::get('/branch/view/{id}','BranchController@viewBranch');
Route::get('/branch/delete/{id}','BranchController@deleteBranch');
/***********************************************************
==================== #End# Branch Info =====================
************************************************************/
/*-----------------------------------------------------------*/
/***********************************************************
=================== #Start# Branch Info ====================
************************************************************/
Route::get('/supplier/add','SupplierController@index');
Route::post('/supplier/save','SupplierController@storeSupplier');
Route::get('/supplier/manage','SupplierController@manageSupplier');
Route::get('/supplier/edit/{id}','SupplierController@editSupplier');
Route::post('/supplier/update','SupplierController@updateSupplier');
Route::get('/supplier/view/{id}','SupplierController@viewSupplier');
Route::get('/supplier/delete/{id}','SupplierController@deleteSupplier');
/***********************************************************
==================== #End# Branch Info =====================
************************************************************/
/*-----------------------------------------------------*/
/*******************************************************
======= #Start# Ecommerce & Branch Product Info ========
********************************************************/
Route::get('/product/add','ProductController@index');
Route::post('/product/save','ProductController@storeProduct');

Route::get('/branch-product/manage','ProductController@manageBranchProduct');
Route::get('/ecommerce-product/manage','ProductController@manageEcommerceProduct');



Route::get('/branch-product/edit/{id}','ProductController@editBranchProduct');
Route::post('/branch-product/update','ProductController@updateBranchProduct');

Route::get('/branch-product/delete/{id}','ProductController@deleteBranchProduct');
Route::get('/branch-product/view/{id}','ProductController@viewBranchProduct');

Route::get('/ecommerce-product/edit/{id}','ProductController@editEcommerceProduct');
Route::post('/ecommerce-product/update','ProductController@updateEcommerceProduct');

Route::get('/ecommerce-product/delete/{id}','ProductController@deleteEcommerceProduct');
Route::get('/ecommerce-product/view/{id}','ProductController@viewEcommerceProduct');
Route::get('/ecommerce-product/unpublished/{id}','ProductController@unpublishedProduct');
Route::get('/ecommerce-product/published/{id}','ProductController@publishedProduct');
Route::get('/ecommerce-product/featured/{id}','ProductController@featuredProduct');
Route::get('/ecommerce-product/unfeatured/{id}','ProductController@unfeaturedProduct');

/*****************************************************
======= #End# Ecommerce & Branch Product Info ========
******************************************************/
/*-----------------------------------------------------*/


/*-----------------------------------------------------*/
/*******************************************************
======= #Start# Branch Product Distribution Info ========
********************************************************/
Route::get('/branch-product/add/{id}','BranchProductController@index');
Route::post('/branch-product/save','BranchProductController@storeProduct');
Route::get('/product/manage','BranchProductController@manageBranchDistributedProduct');

Route::get('/product/edit/{id}','BranchProductController@editProduct');
Route::post('/product/update','BranchProductController@updateProduct');

Route::get('/product/view/{id}','BranchProductController@viewProduct');
Route::get('/product/delete/{id}','BranchProductController@deleteProduct');
/*****************************************************
======= #End# Branch Product Distribution Info ========
******************************************************/
/*--------------------------------------------------------------------------*/
/*-----------------------------------------------------*/
/*******************************************************
======= #Start# Invoice Management ========
********************************************************/
Route::get('/invoice/add/','InvoiceController@index');
Route::get('/buying-invoice/add/','InvoiceController@createInvoice');

Route::post('/invoice/save','InvoiceController@storeInvoice');
Route::post('/buying-invoice/save','InvoiceController@storeBuyingInvoice');

Route::get('/invoice/manage','InvoiceController@manageInvoice');
Route::get('/buying-invoice/manage','InvoiceController@manageBuyingInvoice');

Route::get('/invoice/view/{id}','InvoiceController@viewInvoice');
Route::get('/buying-invoice/view/{id}','InvoiceController@viewBuyingInvoice');
Route::post('/update/payment/','InvoiceController@updatePayment');

Route::get('/invoice/print/{id}','InvoiceController@printInvoice');
Route::get('/buying-invoice/print/{id}','InvoiceController@printBuyingInvoice');

Route::get('/invoice/delete/{id}','InvoiceController@deleteInvoice');
Route::get('/buying-invoice/delete/{id}','InvoiceController@deleteBuyingInvoice');

Route::post('/product/type','InvoiceController@productType');
Route::post('/product/price','InvoiceController@productPrice');
/*****************************************************
======= #End# Invoice Management ========
******************************************************/
/*-----------------------------------------------------*/
Route::get('/expense/add/','ExpenseController@index');
Route::post('/expense/save','ExpenseController@saveExpense');
Route::get('/expense/manage','ExpenseController@manageExpense');

Route::get('/expense/edit/{id}','ExpenseController@editExpense');
Route::post('/expense/update','ExpenseController@updateExpense');

Route::get('/expense/view/{id}','ExpenseController@viewExpense');
Route::get('/expense/delete/{id}','ExpenseController@deleteExpense');

Route::get('/expense/report','ExpenseController@expenseReport');
Route::post('/expense/data','ExpenseController@expenseData');
Route::get('/income/report','ExpenseController@incomeReport');
Route::post('/income/data','ExpenseController@incomeData');
/************************************************************/
/*******************************************************
======= #Start# Order Management ========
********************************************************/
Route::get('/order/manage','OrderController@index');
Route::get('/order/view/{id}','OrderController@orderDetails');
Route::get('/order/done/{id}','OrderController@orderStatusDone');
Route::get('/order/paid/{id}','OrderController@orderPaymentDone');
Route::get('/order/delete/{id}','OrderController@orderDelete');

/*******************************************************
======= #End# Order Management ========
********************************************************/
/*---------------------------------------------------------------------------*/
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
/*===================================================*/
/***********Ecommerce Site Controllers ***************/
/*===================================================*/
Route::get('/','EcommerceSiteController@index');
Route::get('/category/{id}','EcommerceSiteController@category');
Route::get('/product-details/{id}','EcommerceSiteController@productDetails');

// Route::post('/search','EcommerceSiteController@search');
Route::get('/search','EcommerceSiteController@search');


Route::get('/authentication','EcommerceSiteController@authentication');

Route::get('/my-account','EcommerceSiteController@myAccount');
Route::get('/my-profile','EcommerceSiteController@myProfile');
// Route::get('/my-address','EcommerceSiteController@myAddress');
// Route::get('/add-address','EcommerceSiteController@addAddress');
Route::get('/personal-information','EcommerceSiteController@personalInformation');
Route::post('/update-personal-information','EcommerceSiteController@updateCPersonalInfo');
// Route::get('/wishlist/{id}','EcommerceSiteController@wishlist');
Route::get('/orderlist','EcommerceSiteController@orderList');
Route::get('/order-status/{id}','EcommerceSiteController@orderStatus');

Route::get('/checkout','CheckoutController@checkout');
Route::post('/customer-registration','CheckoutController@customerRegistration');
Route::post('/customer-login','CheckoutController@customerLogin');
Route::get('/billing','CheckoutController@billing');
Route::post('/update-customer','CheckoutController@updateCustomer');
Route::get('/shipping','CheckoutController@shipping');
Route::post('/save-shipping','CheckoutController@saveShipping');
Route::get('/payment','CheckoutController@payment');
Route::post('/place-order','CheckoutController@placeOrder');
Route::post('/addmoney/stripe', 'CheckoutController@paymentWithStripe');
Route::get('/logout','CheckoutController@logout');

Route::get('/prescription','CheckoutController@prescription');
Route::post('/prescription/save','CheckoutController@prescriptionSave');



	
Route::post('/add-cart','CartController@addCart');
Route::get('/cart','CartController@cart');
Route::post('/delete-to-cart','CartController@deleteCart');
Route::post('/update-cart','CartController@updateCart');

